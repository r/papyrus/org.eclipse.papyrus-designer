package org.eclipse.papyrus.designer.patterns.transformations;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.patterns.transformations.messages"; //$NON-NLS-1$
	public static String SelectDesignPatternDialog_APPLY_PATTERN;
	public static String SelectDesignPatternDialog_AVAIL_PATTERNS;
	public static String SelectDesignPatternDialog_SELECT_PATTERN;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}

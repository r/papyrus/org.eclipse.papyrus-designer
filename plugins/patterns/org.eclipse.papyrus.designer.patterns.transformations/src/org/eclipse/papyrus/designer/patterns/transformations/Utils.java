/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.profile.utils.PatternUtils;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.DirectedRelationship;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuralFeature;
import org.eclipse.uml2.uml.UMLPackage;

public class Utils {

	/**
	 * return the top-level owner of an element. This function returns the same value
	 * as getModel, if the top-level element is a model. While this is the case for
	 * models, model libraries have a top-level package (not a model). In this case,
	 * getTop returns the top-level package whereas getModel would return null.
	 * 
	 * @param element
	 * @return the top-level owning package
	 */
	public static Package getTop(Element element) {
		while(element != null) {
			Element owner = element.getOwner();
			if(owner == null) {
				if(element instanceof Package) {
					return (Package)element;
				}
			}
			element = owner;
		}
		return null;
	}

	/**
	 * Check whether a component is a hardware component, i.e.
	 * either applies the design pattern HwCompCapability stereotype or the MARTE
	 * HwComponent stereotype
	 * 
	 * @param element
	 *        the element that is checked
	 * @return true, if a hardware component
	 */
	public static boolean isHwComponent(Element element) {
		return false;
		// TODO
		/*
		return StUtils.isApplied(element, HWCompCapability.class) ||
			StUtils.isApplied(element, HwComponent.class);
		*/
	}

	/**
	 * Get a role from the pattern that corresponds to a given name
	 * 
	 * @param designPattern a design pattern
	 * @param name the name of a role
	 * @return the associated role (connectable element) or null
	 */
	public static ConnectableElement getNamedRole(Pattern designPattern, String name) {
		EList<ConnectableElement> roles = PatternUtils.getRoles(designPattern);
		for(ConnectableElement role : roles) {
			String roleName = role.getName();
			if(roleName != null) {
				if(roleName.equals(name)) {
					return role;
				}
			}
		}
		return null;
	}

	/**
	 * Get the collaboration use (if any) for a given element. This function will be used to validate
	 * constraints that are associate with a design pattern, since it enables the navigation from
	 * an element in the model to the CollaborationUse and thus to the pattern.
	 * 
	 * @param element
	 * @return a list of structural features (properties) that represent roles
	 */
	public static EList<StructuralFeature> getRoleInCollaboration(Element element) {
		EList<StructuralFeature> list = new UniqueEList<StructuralFeature>();
		for(DirectedRelationship relationShip : element.getTargetDirectedRelationships(UMLPackage.eINSTANCE.getDependency())) {
			if(relationShip instanceof Dependency) {
				Dependency dependency = (Dependency)relationShip;
				for(NamedElement ne : dependency.getClients()) {
					if(ne instanceof StructuralFeature) {
						StructuralFeature feature = (StructuralFeature)ne;
						if(feature.getOwner() instanceof Collaboration) {
							list.add(feature);
						}
					}
				}
			}
		}
		return list;
	}
	
	/**
	 * Check whether a connector connects the two parts that are given as parameters.
	 * Will support either direct connections to a part or a connection to a port of a part.
	 * 
	 * @param conn a connector
	 * @param part1 a part (within a composite class)
	 * @param part2 a part (within a composite class)
	 * @return true, if the connector connects the passed parts
	 */
	public static boolean connectsParts(Connector conn, Property part1, Property part2) {
		ConnectorEnd connEnd1 = conn.getEnds().get(0);
		ConnectorEnd connEnd2 = conn.getEnds().get(1);
		if (
			((connEnd1.getPartWithPort() == part1) || (connEnd1.getRole() == part1)) &&
			((connEnd2.getPartWithPort() == part2) || (connEnd2.getRole() == part2))) {
				return true;
		}
		if (
			((connEnd1.getPartWithPort() == part2) || (connEnd1.getRole() == part2)) &&
			((connEnd2.getPartWithPort() == part1) || (connEnd2.getRole() == part1))) {
				return true;
		}	
		return false;	
	}
	
	/**
	 * Filter un-set values (null), return default text instead
	 * 
	 * @param comment a comment, may be null
	 * @return the body of the comment or "not available if the passed comment is null
	 */
    public static String filterNP(Comment comment) {
    	if(comment != null) {
    		return filterNP(comment.getBody());
    	}
    	else {
    		return "not available"; //$NON-NLS-1$
    	}
    }

	/**
	 * Filter un-set values (null), return default text instead
	 * 
	 * @param info a string we want to present to the user, may be null
	 * @return the passed string, or "not available", if null
	 */
    public static String filterNP(String info) {
    	if (info != null) {
    		return info;
    	}
    	else {
    		return "not available"; //$NON-NLS-1$
    	}
    }
}

/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;


public class ApplyPatternFunctions {

	private static final String APPLICATION_HELPERS = "org.eclipse.papyrus.designer.profiles.designpattern.applicationHelpers"; //$NON-NLS-1$

	public static IApplyPattern getCustomFunction(String functionName)
	{
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] configElements = reg.getConfigurationElementsFor(APPLICATION_HELPERS);
		for(IConfigurationElement configElement : configElements) {
			try {
				final String extFunctionName = configElement.getAttribute("functionName"); //$NON-NLS-1$
				if(extFunctionName.equals(functionName)) {
					final Object obj = configElement.createExecutableExtension("class"); //$NON-NLS-1$
					if(obj instanceof IApplyPattern) {
						return (IApplyPattern)obj;
					}
				}
			} catch (CoreException exception) {
				exception.printStackTrace();
			}
		}
		return null;
	}
}

/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations.dialogs;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;

public class PatternLabelProvider extends LabelProvider {

	public String getText(Object element) {
		if(element instanceof Pattern) {
			return ((Pattern)element).getBase_Package().getName();
		}
		else {
			return element.toString();
		}
	}
};

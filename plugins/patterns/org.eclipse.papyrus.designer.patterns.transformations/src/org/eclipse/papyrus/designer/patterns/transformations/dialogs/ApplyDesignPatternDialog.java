/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/
package org.eclipse.papyrus.designer.patterns.transformations.dialogs;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.AutomaticBinding;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.profile.utils.PatternUtils;
import org.eclipse.papyrus.designer.patterns.transformations.ApplyPatternFunctions;
import org.eclipse.papyrus.designer.patterns.transformations.IApplyPattern;
import org.eclipse.papyrus.designer.patterns.transformations.Utils;
import org.eclipse.papyrus.designer.transformation.ui.provider.QNameLabelProvider;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;

/**
 * Apply a design pattern
 * 
 * Show roles within design pattern and assign existing or create new roles in
 * model TODO: let user select a specific solution?
 * 
 * for each role: decide whether to (1) bind: (select element from source
 * model?) (2) create: (show => info, i.e. add additional column to dialog)
 * 
 */
public class ApplyDesignPatternDialog extends SelectionStatusDialog {

	// protected Combo fProperties;

	protected TableViewer fRoles;

	protected TableViewer fAppRoles;

	protected NamedElement m_role;

	protected NamedElement m_appRole;

	protected EList<NamedElement> m_appRoles;

	protected Pattern m_pattern;

	protected Package m_model;

	protected PatternInfoGroup pig;

	/**
	 * Element on which "apply pattern" has been called
	 */
	protected Package m_package;

	protected Map<NamedElement, Object> userRoleMap;

	protected Map<NamedElement, Object> autoRoleMap;

	protected Button fMapRoleButton;

	protected Button fUnMapRoleButton;

	protected IApplyPattern applicationHelper;

	protected RoleLabelProvider roleLabelProvider;

	/**
	 * @param parent
	 *            Parent shell
	 * @param pkg
	 *            (nearest) Package in which the dialog has been applied
	 * @param pattern
	 *            the selected design pattern
	 */
	public ApplyDesignPatternDialog(Shell parent, Package pkg, Pattern pattern) {
		super(parent);
		// visitedPackages = new BasicEList<Package> ();

		m_pattern = pattern;
		m_model = Utils.getTop(pkg);
		m_package = pkg;
		pig = new PatternInfoGroup();
		applicationHelper = ApplyPatternFunctions.getCustomFunction(m_pattern.getBase_Package().getName());
	}

	/**
	 * @see SelectionStatusDialog#computeResult()
	 */
	@Override
	protected void computeResult() {
		// make roleMap available to caller
		List<Map<NamedElement, Object>> result = new BasicEList<Map<NamedElement, Object>>();
		result.add(userRoleMap);
		result.add(autoRoleMap);
		setResult(result);
	}

	@Override
	public Control createDialogArea(Composite parent) {
		Composite contents = (Composite) super.createDialogArea(parent);
		// (parent, "Container rules", "Avail. extensions/interceptors");

		createRoleAssignmentGroup(contents);
		pig.createPatternInfoGroup(contents);
		pig.updateInfo(m_pattern);
		contents.setLayout(new GridLayout(2, false));

		// createPatternInfoGroup(contents);

		return contents;
	}

	protected void createRoleAssignmentGroup(Composite parent) {
		GridData groupGridData = new GridData();
		// data3.widthHint = 400;
		// data3.heightHint = 300;
		groupGridData.grabExcessVerticalSpace = true;
		groupGridData.grabExcessHorizontalSpace = true;
		groupGridData.horizontalAlignment = GridData.FILL;
		groupGridData.verticalAlignment = GridData.FILL;
		//
		// --------------- pattern selection -------------------
		//
		Group roleAssignmentGroup = new Group(parent, SWT.BORDER);
		roleAssignmentGroup.setText(" available roles and binding "); //$NON-NLS-1$
		roleAssignmentGroup.setLayout(new GridLayout(1, false));
		roleAssignmentGroup.setLayoutData(groupGridData);
		roleAssignmentGroup.setSize(400, 300);

		fRoles = new TableViewer(roleAssignmentGroup, SWT.BORDER);
		// Add a column header named "Column 2" that's left justified
		TableViewerColumn c1Viewer = new TableViewerColumn(fRoles, SWT.LEFT);
		TableColumn column1 = c1Viewer.getColumn();
		column1.setText("Role"); //$NON-NLS-1$
		column1.setWidth(100);
		TableViewerColumn c2Viewer = new TableViewerColumn(fRoles, SWT.LEFT);
		TableColumn column2 = c2Viewer.getColumn();
		column2.setText("Kind"); //$NON-NLS-1$
		column2.setWidth(100);
		TableViewerColumn c3Viewer = new TableViewerColumn(fRoles, SWT.LEFT);
		TableColumn column3 = c3Viewer.getColumn();
		column3.setText("Bound to"); //$NON-NLS-1$
		column3.setWidth(100);

		// Show the column headers
		fRoles.getTable().setHeaderVisible(true);

		GridData data = new GridData(GridData.FILL_BOTH);
		data.heightHint = 150;
		data.widthHint = 400;
		fRoles.setContentProvider(new ArrayContentProvider());
		userRoleMap = new HashMap<NamedElement, Object>();
		autoRoleMap = new HashMap<NamedElement, Object>();
		c1Viewer.setLabelProvider(new RoleLabelProvider(1, userRoleMap, autoRoleMap));
		c2Viewer.setLabelProvider(new RoleLabelProvider(2, userRoleMap, autoRoleMap));
		roleLabelProvider = new RoleLabelProvider(3, userRoleMap, autoRoleMap);
		c3Viewer.setLabelProvider(roleLabelProvider);
		fRoles.getTable().setLayoutData(data);
		if (m_pattern.getSolutions().size() == 0) {
			MessageDialog.openError(parent.getShell(), "No solutions", //$NON-NLS-1$
					"The pattern definition does not contain any solution"); //$NON-NLS-1$
			return;
		}
		Solution solution = m_pattern.getSolutions().iterator().next();
		fRoles.setInput(PatternUtils.getSolutionElements(solution).toArray());

		fRoles.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				ISelection selection = fRoles.getSelection();
				if (selection instanceof StructuredSelection) {
					Object[] selected = ((StructuredSelection) selection)
							.toArray();
					if ((selected.length == 1)
							&& (selected[0] instanceof NamedElement)) {
						selectRole((NamedElement) selected[0]);
					}
				}
			}
		});

		fMapRoleButton = new Button(roleAssignmentGroup, SWT.NONE);
		fMapRoleButton.setText("Bind role"); //$NON-NLS-1$
		fMapRoleButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				userRoleMap.put(m_role, m_appRole);
				if (applicationHelper != null) {
					autoRoleMap = applicationHelper.automaticBinding(m_pattern, userRoleMap);
					if (autoRoleMap == null) {
						autoRoleMap = new HashMap<NamedElement, Object>();
					}
					roleLabelProvider.setAutoRoleMap(autoRoleMap);
					fAppRoles.refresh();
				}
				fRoles.refresh();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		fUnMapRoleButton = new Button(roleAssignmentGroup, SWT.NONE);
		fUnMapRoleButton.setText("Reset role binding"); //$NON-NLS-1$
		fUnMapRoleButton.addSelectionListener(new SelectionListener() {

			public void widgetSelected(SelectionEvent e) {
				userRoleMap.put(m_role, null);
				if (applicationHelper != null) {
					autoRoleMap = applicationHelper.automaticBinding(m_pattern, userRoleMap);
					roleLabelProvider.setAutoRoleMap(autoRoleMap);
					fAppRoles.refresh();
				}
				fRoles.refresh();
			}

			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});

		// Group appRolesGroup = new Group(roleAssignmentGroup, SWT.BORDER);
		// appRolesGroup.setText(" available candidates for binding within application ");
		new Label(roleAssignmentGroup, SWT.NONE).setText("Available candidates for binding within application:"); //$NON-NLS-1$
		// test.setText("This table is filtered");
		fAppRoles = new TableViewer(roleAssignmentGroup, SWT.BORDER);
		fAppRoles.setContentProvider(new ArrayContentProvider());

		m_appRoles = new UniqueEList<NamedElement>();

		fAppRoles.setInput(m_appRoles.toArray());
		fAppRoles.getTable().setLayoutData(data);
		fAppRoles.setLabelProvider(new QNameLabelProvider());

		fAppRoles.addSelectionChangedListener(new ISelectionChangedListener() {

			public void selectionChanged(SelectionChangedEvent event) {
				ISelection selection = fAppRoles.getSelection();
				if (selection instanceof StructuredSelection) {
					Object[] selected = ((StructuredSelection) selection)
							.toArray();
					if ((selected.length == 1)
							&& (selected[0] instanceof NamedElement)) {
						selectAppRole((NamedElement) selected[0]);
					}
				}
			}
		});

		roleAssignmentGroup.pack();
	}

	/**
	 * Find candidates for role binding in application model
	 */
	protected void getAppRoles(Package pkg, EList<Package> visitedPackages) {
		for (Element el : pkg.getOwnedMembers()) {
			if (m_role != null && m_role.eClass().isSuperTypeOf(el.eClass())) {
				m_appRoles.add((NamedElement) el);
			} else if ((el instanceof Package) && (Utils.getTop(el) == Utils.getTop(pkg))) {
				// 2nd condition: only select roles within current model, not within imported
				if (!visitedPackages.contains(el)) {
					visitedPackages.add((Package) el);
					getAppRoles((Package) el, visitedPackages);
				}
			}
		}
	}

	/**
	 * Return a list of classifier that are candidates for a role in a pattern.
	 */
	public EList<Classifier> getCandidatesForRole(Property role) {
		EList<Classifier> list = new BasicEList<Classifier>();

		return list;
	}

	protected void selectRole(NamedElement role) {
		m_role = role;
		// remove all filters
		m_appRoles = new UniqueEList<NamedElement>();
		getAppRoles(m_package, new BasicEList<Package>());

		fAppRoles.setFilters(new ViewerFilter[0]);
		// now add standard filters
		if (role instanceof Port) {
			fAppRoles.addFilter(PortRoleFilter.getInstance());
		}

		// now add custom filter (if any)
		if (applicationHelper != null) {
			ViewerFilter specificFilter = applicationHelper.getFilter(m_pattern, role);
			if (specificFilter != null) {
				fAppRoles.addFilter(specificFilter);
			}
		}
		boolean isAutomatic = StereotypeUtil.isApplied(role, AutomaticBinding.class);
		fMapRoleButton.setEnabled(!isAutomatic);
		fUnMapRoleButton.setEnabled(!isAutomatic);
		if (isAutomatic) {
			fAppRoles.setInput(new String[] { "No selection (automatic binding)" }); //$NON-NLS-1$
		} else {
			fAppRoles.setInput(m_appRoles.toArray());
		}
	}

	protected void selectAppRole(NamedElement role) {
		m_appRole = role;
	}
}

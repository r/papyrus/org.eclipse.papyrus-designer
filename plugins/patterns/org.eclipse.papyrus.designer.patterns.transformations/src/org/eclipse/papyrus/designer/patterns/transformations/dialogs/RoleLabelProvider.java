/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations.dialogs;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.AutomaticBinding;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;

public class RoleLabelProvider extends ColumnLabelProvider {

	private int column;

	private Map<NamedElement, Object> userRoleMap;

	private Map<NamedElement, Object> autoRoleMap;

	public RoleLabelProvider(int column, Map<NamedElement, Object> userRoleMap, Map<NamedElement, Object> autoRoleMap) {
		this.column = column;
		this.userRoleMap = userRoleMap;
		this.autoRoleMap = autoRoleMap;
	}

	public void setAutoRoleMap(Map<NamedElement, Object> autoRoleMap) {
		this.autoRoleMap = autoRoleMap;
	}

	@SuppressWarnings("unchecked")
	public String getText(Object element) {
		NamedElement role = (NamedElement) element;
		if(column == 1) {
			return role.getName();
		}
		else if(column == 2) {
			return role.eClass().getName();
		}
		else if(column == 3) {
			Object mappedRole = userRoleMap.get(role);
			if((mappedRole == null) && (autoRoleMap != null)) {
				mappedRole = autoRoleMap.get(role);
			}
			if(mappedRole instanceof EList) {
				String roleStr = "["; //$NON-NLS-1$
				for(Object roleInList : (EList<Property>)mappedRole) {
					if(roleStr.length() > 1) {
						roleStr += ", "; //$NON-NLS-1$
					}
					if(roleInList instanceof Property) {
						roleStr += ((Property)roleInList).getName();
					}
				}
				roleStr += "]"; //$NON-NLS-1$
				return roleStr;
			}
			if (mappedRole instanceof NamedElement) {
				NamedElement roleNE = (NamedElement) mappedRole;
				String name = roleNE.getName();
				if (roleNE.getOwner() instanceof NamedElement) {
					name += " (from " + ((NamedElement) roleNE.getOwner()).getQualifiedName() + ")";  //$NON-NLS-1$//$NON-NLS-2$
				}
				return name;
			}
			else if (StereotypeUtil.isApplied(role, AutomaticBinding.class)) {
				return "[] (automatic)"; //$NON-NLS-1$
			}
			else {
				return "not assigned (create)"; //$NON-NLS-1$
			}	
		}
		return ""; //$NON-NLS-1$
	}
};

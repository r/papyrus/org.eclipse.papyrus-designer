/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations.dialogs;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.transformations.Utils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;


public class PatternInfoGroup {

	protected StyledText fProblem;

	protected StyledText fApplicability;

	protected StyledText fIntent;

	protected StyledText fSolution;

	protected Label fClassification;

	final static String classificationOpts[] = {
			"Creational", //$NON-NLS-1$
			"Structural", //$NON-NLS-1$
			"Behavioral" //$NON-NLS-1$
	};

	// protected Combo fProperties;

	protected void createPatternInfoGroup(Composite parent) {
		GridData groupGridData = new GridData();
		groupGridData.widthHint = 400;
		// data3.heightHint = 300;
		groupGridData.grabExcessVerticalSpace = true;
		groupGridData.grabExcessHorizontalSpace = true;
		groupGridData.horizontalAlignment = GridData.FILL;
		groupGridData.verticalAlignment = GridData.FILL;

		//
		// --------------- design pattern info -------------------
		//
		Group patternInfoGroup = new Group(parent, SWT.BORDER);
		patternInfoGroup.setText(" pattern information "); //$NON-NLS-1$
		// ruleGroup.setLayout(new RowLayout (SWT.VERTICAL));
		patternInfoGroup.setLayout(new GridLayout(1, false));
		patternInfoGroup.setLayoutData(groupGridData);
		patternInfoGroup.setSize(400, 300);
		/*
		 * fRules = DialogUtils.createFilteredList (ruleSelGroup, new
		 * RuleLabelProvider(), 200, 200, SWT.BORDER | SWT.V_SCROLL |
		 * SWT.H_SCROLL);
		 */

		GridData spanWidth = new GridData();
		spanWidth.horizontalAlignment = GridData.FILL;
		spanWidth.grabExcessHorizontalSpace = true;
		spanWidth.grabExcessVerticalSpace = true;
		spanWidth.heightHint = 60;

		new Label(patternInfoGroup, SWT.NONE).setText("Intent:"); //$NON-NLS-1$
		fIntent = new StyledText(patternInfoGroup, SWT.WRAP | /* SWT.V_SCROLL | */SWT.READ_ONLY);
		fIntent.setAlwaysShowScrollBars(false);
		fIntent.setLayoutData(spanWidth);

		new Label(patternInfoGroup, SWT.NONE).setText("Applicability:"); //$NON-NLS-1$
		fApplicability = new StyledText(patternInfoGroup, SWT.WRAP | SWT.V_SCROLL | SWT.READ_ONLY);
		fApplicability.setAlwaysShowScrollBars(false);

		fApplicability.setLayoutData(spanWidth);

		new Label(patternInfoGroup, SWT.NONE).setText("Problem:"); //$NON-NLS-1$
		fProblem = new StyledText(patternInfoGroup, SWT.WRAP | SWT.V_SCROLL | SWT.READ_ONLY);
		fProblem.setAlwaysShowScrollBars(false);
		fProblem.setLayoutData(spanWidth);

		new Label(patternInfoGroup, SWT.NONE).setText("Solution:"); //$NON-NLS-1$
		fSolution = new StyledText(patternInfoGroup, SWT.WRAP | SWT.V_SCROLL | SWT.READ_ONLY);
		fSolution.setAlwaysShowScrollBars(false);
		fSolution.setLayoutData(spanWidth);

		fClassification = new Label(patternInfoGroup, SWT.NONE);
		fClassification.setText("Classification:                            "); //$NON-NLS-1$
	}

	public void updateInfo(Pattern pattern) {
		if(pattern != null) {
			fIntent.setText(Utils.filterNP(pattern.getIntent() != null ? pattern.getIntent().getBase_Comment() : null));
			fApplicability.setText(Utils.filterNP(pattern.getApplicability() != null ? pattern.getApplicability().getBase_Comment() : null));
			fProblem.setText(Utils.filterNP(pattern.getProblem() != null ? pattern.getProblem().getBase_Comment() : null));
			EList<Solution> solutions = pattern.getSolutions(); 
			Solution solution = solutions.size() > 0 ? solutions.iterator().next() : null;
			fSolution.setText(Utils.filterNP(solution != null && solution.getDescription() != null ? solution.getDescription().getBase_Comment() : null));
			fClassification.setText("Classification: " + //$NON-NLS-1$
					classificationOpts[pattern.getClassification().getValue()]);
		}
	}
}

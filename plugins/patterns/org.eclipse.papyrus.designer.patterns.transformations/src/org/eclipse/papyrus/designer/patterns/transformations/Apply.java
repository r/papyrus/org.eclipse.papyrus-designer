/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations;

import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.BasicCompartment;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.NotationFactory;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.profile.utils.PatternUtils;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier.CopyExtResources;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier.CopyStatus;
import org.eclipse.papyrus.designer.transformation.core.transformations.filters.FilterContainment;
import org.eclipse.papyrus.infra.core.sasheditor.editor.ISashWindowsContainer;
import org.eclipse.papyrus.infra.core.sashwindows.di.PageRef;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.ui.util.ServiceUtilsForActionHandlers;
import org.eclipse.papyrus.uml.diagram.composite.providers.UMLViewProvider;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.StUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StructuredClassifier;

@SuppressWarnings("deprecation")
public class Apply {

	private static final String TO = " to "; //$NON-NLS-1$

	private static final String FROM = "from "; //$NON-NLS-1$

	private static final String PATTERN_APPLICATION = "PatternApplication"; //$NON-NLS-1$

	protected Pattern m_pattern;

	/**
	 * role-binding map of user defined bindings. Target is of type object to allow-for bindings
	 * of a single role to a list of elements (which is perhaps not a good idea).
	 */
	protected Map<NamedElement, Object> userRoleMap;

	/**
	 * role-binding map of automatically calculated bindings. Target is of type object to allow-for bindings
	 * of a single role to a list of elements (which is perhaps not a good idea).
	 */
	protected Map<NamedElement, Object> autoRoleMap;

	public Apply(Pattern pattern, Map<NamedElement, Object> userRoleMap, Map<NamedElement, Object> autoRoleMap) {
		m_pattern = pattern;
		this.userRoleMap = userRoleMap;
		this.autoRoleMap = autoRoleMap;
	}

	/**
	 * Apply a design pattern
	 * 
	 * @param m_context
	 */
	public void apply(NamedElement m_context) {
		// apply pattern - copier or bind elements from solution
		// TODO: select solution instead of getting first one

		if (m_pattern.getSolutions().size() > 0) {
			Solution solution = m_pattern.getSolutions().get(0); // TODO: let user choose a particular solution instead of taking the first.
			Package solution_pkg = solution.getBase_Package();
			
			// TODO: create proper sub-function for graphics...
			try {
				ISashWindowsContainer windowsContainer = ServiceUtilsForActionHandlers.getInstance()
						.getISashWindowsContainer();
				Object model = windowsContainer.getActiveSashWindowsPage().getRawModel();
				if (model instanceof PageRef) {
					EObject diagramEObj = ((PageRef) model).getEmfPageIdentifier();
					if (diagramEObj instanceof Diagram) {
						// ..
					}
				}
			} catch (ServiceException svcE) {
				System.err.println(svcE.toString());
			}

			Package destination = PackageUtil.getRootPackage(m_context);
			LazyCopier copier = new LazyCopier(
					solution_pkg, destination, CopyExtResources.ALL, true);

			FilterContainment preCopy = FilterContainment.getInstance();
			preCopy.setRoot(destination);
			copier.preCopyListeners.add(preCopy);
		
			for (NamedElement key : userRoleMap.keySet()) {
				if (userRoleMap.get(key) instanceof EObject) {
					copier.putPair(key, (EObject) userRoleMap.get(key));
				}
			}

			// prepare copy, put bound element (role-bindings) into copier map
			for (NamedElement role : PatternUtils.getSolutionElements(solution)) {
				Object boundElement = userRoleMap.get(role);
				if (boundElement == null) {
					boundElement = autoRoleMap.get(role);
				}
				if (boundElement instanceof EList) {
					@SuppressWarnings("unchecked")
					EList<EObject> boundElemList = (EList<EObject>) boundElement;
					for (EObject singleBE : boundElemList) {
						copier.put(role, singleBE);
						copier.setStatus(singleBE, CopyStatus.SHALLOW);
					}
				}
				else if (boundElement instanceof EObject) {
					copier.put(role, (EObject) boundElement);
					copier.setStatus((EObject) boundElement, CopyStatus.SHALLOW);
				}
			}
			// now copy
			for (NamedElement role : PatternUtils.getSolutionElements(solution)) {
				copier.getCopy(role);
			}
			Classifier cuCl = PatternUtils.getCUClassifier(solution);
			Classifier copiedCuCl = copier.getCopy(cuCl);
			copiedCuCl.setName(PATTERN_APPLICATION + m_pattern.getBase_Package().getName());
			PatternApplication pa = StereotypeUtil.applyApp(copiedCuCl, PatternApplication.class);
			if (pa == null) {
				// apply profile, the re-apply stereotype
				Package dpProfile = PackageUtil.loadPackage(PatternUtils.DP_PATH, destination.eResource().getResourceSet());
				if (dpProfile instanceof Profile) {
					PackageUtil.applyProfile(PackageUtil.getRootPackage(destination), (Profile) dpProfile, false);
					pa = StereotypeUtil.applyApp(copiedCuCl, PatternApplication.class);
				}
			}
			if (pa != null) {
				Date date = new Date();
				pa.setDate(DateFormat.getDateInstance().format(date) + " " //$NON-NLS-1$
						+ DateFormat.getTimeInstance().format(date));
				pa.setUser(System.getenv("USER")); //$NON-NLS-1$
			}
	
			// AddToDiagram addToDiagram = new AddToDiagram((Diagram) diagramEObj, collaboration);
			// addToDiagram.addToDiagram(context, solutionClass, cu);

			IApplyPattern fct = ApplyPatternFunctions.getCustomFunction(m_pattern.getBase_Package().getName());
			if (fct != null) {
				fct.postPatternApplication(m_pattern, userRoleMap);
				fct.postPatternApplication(m_pattern, autoRoleMap);
			}
		}
	}

	public View addCuToCompositeDiagram(Class context, StructuredClassifier solutionClass, CollaborationUse cu,
			View view, Diagram diagram) {
		// add CU to compartment
		Object compartment = view.getChildren().get(1);
		View roleBindingView = compViewProvider.createCollaborationUse_Shape(cu, (BasicCompartment) compartment, -1,
				true, org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
		Location location = NotationFactory.eINSTANCE.createLocation();
		location.setX(400);
		location.setY(40);
		if (roleBindingView instanceof Node) {
			((Node) roleBindingView).setLayoutConstraint(location);
		}
		return roleBindingView;
	}

	@SuppressWarnings("unused")
	public void addToCompositeDiagram(Class context, StructuredClassifier solutionClass, CollaborationUse cu, View view,
			View roleBindingView, Diagram diagram) {
		Object compartment = view.getChildren().get(1);
		if (compartment instanceof BasicCompartment) {
			int x = 10;
			int y = 10;
			for (ConnectableElement role : solutionClass.getAllAttributes()) {
				Object boundElement = userRoleMap.get(role);
				if (boundElement == null) {
					boundElement = autoRoleMap.get(role);
				}
				if (boundElement instanceof EList) {
					@SuppressWarnings("rawtypes")
					EList boundElemList = (EList) boundElement;
					if (boundElemList.size() > 0) {
						boundElement = boundElemList.get(0);
					}
				}
				Dependency rb;
				View roleView = null;
				if (boundElement instanceof NamedElement) {
					// element already exists, do not create, but try to obtain
					// reference
					// (since we try to create a link ...)
					NamedElement boundNE = (NamedElement) boundElement;
					StUtils.copyStereotypes(role, boundNE);
					rb = cu.createRoleBinding(FROM + role.getName() + TO + boundNE.getName());
					rb.getClients().add(role);
					rb.getSuppliers().add(boundNE);
					for (Object child : ((BasicCompartment) compartment).getChildren()) {
						if (child instanceof View) {
							if (((View) child).getElement() == boundNE) {
								roleView = (View) child;

								break;
							}
						}
					}
				} else {
					// two different cases:
					// (1) attribute belongs to context composite
					// (2) attribute does not belong to it, e.g. if a pattern
					// identifies HW as well as SW roles
					// => identified roles might not belong to same composite.
					// Additional Java code can help or ...
					// For demo: simple modify example? Modify functionality?
					// (don't want to see xyz).
					rb = cu.createRoleBinding(FROM + role.getName() + TO + role.getName());

					// Other problem: detect whether connectors are existing,
					// create, if not
					// => Connector Utils
					Property copy = context.createOwnedAttribute(role.getName(), role.getType());
					rb.getSuppliers().add(copy);
					// roleView =
					// ViewService.getInstance().createView(Node.class, new
					// EObjectAdapter(copier),
					// (BasicCompartment)compartment,
					// semanticHint(UMLElementTypes.Property_3070),
					// ViewUtil.APPEND, true,
					// org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
					if (false) {
						roleView = new UMLViewProvider().createProperty_Shape(copy, (BasicCompartment) compartment, -1,
								true,
								org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
						Location location = NotationFactory.eINSTANCE.createLocation();
						location.setX(x);
						location.setY(y);
						if (roleView instanceof Node) {
							((Node) roleView).setLayoutConstraint(location);
						}
						x += 30;
						y += 20;
					}
				}
				rb.getClients().add(role);
				if (roleView != null) {
					View cuEdge = compViewProvider.createDependency_RoleBindingEdge(rb,
							((BasicCompartment) compartment).getDiagram(), -1, true,
							org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
					if (cuEdge instanceof Edge) {
						((Edge) cuEdge).setSource(roleBindingView);
						((Edge) cuEdge).setTarget(roleView);
					}
				}
			}
			;

		}

	}

	// ViewProvider getCompViewProvider() {
	protected UMLViewProvider compViewProvider = new UMLViewProvider();

	public void addToInternalBlockDiagram(View view, Diagram diagram) {

		Object compartment = view.getChildren().get(1);
		if (compartment instanceof BasicCompartment) {
			/*
			 * ViewService.getInstance().createView(Node.class, new
			 * EObjectAdapter(tst1), (BasicCompartment)compartment, null,
			 * ViewUtil.APPEND, true,
			 * org.eclipse.papyrus.sysml.diagram.internalblock.part.
			 * InternalBlockDiagramEditor. DIAGRAM_PREFERENCES_HINT);
			 * ViewService.getInstance().createView(Node.class, new
			 * EObjectAdapter(tst1), (BasicCompartment)compartment, null,
			 * ViewUtil.APPEND, true,
			 * org.eclipse.papyrus.sysml.diagram.internalblock.part.
			 * UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT );
			 */
		}

	}

	public void addToClassDiagram(Class context, Collaboration collaboration, View view, Diagram diagram) {

		int x = 10;
		for (ConnectableElement role : collaboration.getRoles()) {
			if (!userRoleMap.containsKey(role)) {
				Property copy = context.createOwnedAttribute(role.getName(), role.getType());
				View roleView = ViewService.getInstance()
						.createView(Node.class, new EObjectAdapter(
								copy), diagram, null, ViewUtil.APPEND, true,
								org.eclipse.papyrus.uml.diagram.clazz.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
				if (roleView instanceof Node) {
					Location location = NotationFactory.eINSTANCE.createLocation();
					location.setX(x);
					location.setY(0);
					((Node) roleView).setLayoutConstraint(location);
					x += 20;
				}
			}
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations.dialogs;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.uml2.uml.Port;


public class PortRoleFilter extends ViewerFilter {

	public static PortRoleFilter getInstance() {
		if(portRoleFilter == null) {
			portRoleFilter = new PortRoleFilter();
		}
		return portRoleFilter;
	}

	@Override
	public boolean select(Viewer viewer, Object parentElement, Object element) {
		return (element instanceof Port);
	}

	protected static PortRoleFilter portRoleFilter = null;
}

/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations.diagram;

import org.eclipse.gmf.runtime.notation.BasicCompartment;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Location;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.NotationFactory;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.uml.diagram.composite.providers.UMLViewProvider;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.StructuredClassifier;

/**
 * A class handling the addition of elements to diagrams.
 * CAVEAT: class is incomplete and contains commented code (needs major revision)
 */
public class AddToDiagram {

	private static final String PAPYRUS_UML_CLASS_DIAGRAM = "PapyrusUMLClassDiagram"; //$NON-NLS-1$

	private static final String INTERNAL_BLOCK = "InternalBlock"; //$NON-NLS-1$

	private static final String COMPOSITE_STRUCTURE = "CompositeStructure"; //$NON-NLS-1$

	Diagram diagram;

	Collaboration collaboration;

	/**
	 * 
	 * @param diagram
	 *            a diagram
	 * @param collaboration
	 *            the collaboration to add to the diagram
	 */
	public AddToDiagram(Diagram diagram, Collaboration collaboration) {
		this.diagram = diagram;
		this.collaboration = collaboration;
	}

	/**
	 * Get the view within the diagram that corresponds to the passed element
	 * 
	 * @param parentView
	 *            the view of a parent element
	 * @param element
	 *            the element for which we want to obtain the view
	 * @return the view, if it is found (null, otherwise)
	 */
	public View getView(View parentView, Element element) {
		// get first view
		for (Object child : parentView.getChildren()) {
			if (child instanceof View) {
				if (((View) child).getElement() == element) {
					return (View) child;
				}
			}
		}
		return null;
	}

	public void addToDiagram(Class context, StructuredClassifier solutionClass, CollaborationUse cu) {
		View contextView = getView(diagram, context);
		View cuView = null;
		if (diagram.getType().equals(COMPOSITE_STRUCTURE)) {
			cuView = addCuToCompositeDiagram(context, solutionClass, cu, contextView, diagram);
		}

		// TODO: separate addition of collaboration use to
		// the view and the creation of a collaboration
		// binding (currently in the diagram specific loop)
		if (diagram.getType().equals(COMPOSITE_STRUCTURE)) {
			addToCompositeDiagram(context, solutionClass, cu, contextView, cuView, diagram);
		} else if (diagram.getType().equals(INTERNAL_BLOCK)) {
			addToInternalBlockDiagram(contextView, diagram);
		} else if (diagram.getType().equals(PAPYRUS_UML_CLASS_DIAGRAM)) {
			addToClassDiagram(context, collaboration, contextView, diagram);
		}
	}

	public View addCuToCompositeDiagram(Class context, StructuredClassifier solutionClass, CollaborationUse cu,
			View view, Diagram diagram) {
		// add CU to compartment
		Object compartment = view.getChildren().get(2); // TODO - avoid fixed index (can change)
		View roleBindingView = compViewProvider.createCollaborationUse_Shape(cu, (BasicCompartment) compartment, -1,
				true, org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
		Location location = NotationFactory.eINSTANCE.createLocation();
		// TODO: proper calculation
		location.setX(400);
		location.setY(40);
		if (roleBindingView instanceof Node) {
			((Node) roleBindingView).setLayoutConstraint(location);
		}
		return roleBindingView;
	}

	public void addToCompositeDiagram(Class context, StructuredClassifier solutionClass, CollaborationUse cu, View view,
			View roleBindingView, Diagram diagram) {
		Object compartmentObj = view.getChildren().get(2);
		if (compartmentObj instanceof BasicCompartment) {
			BasicCompartment compartment = (BasicCompartment) compartmentObj;
			for (Dependency rb : cu.getClientDependencies()) {

				if ((rb.getClients().size() > 0) && (rb.getSuppliers().size() > 0)) {
					NamedElement source = rb.getClients().get(0);
					NamedElement target = rb.getSuppliers().get(0);

					View sourceView = getView(compartment, source);
					View targetView = getView(compartment, target);

					View cuEdge = compViewProvider.createDependency_RoleBindingEdge(rb,
							((BasicCompartment) compartment).getDiagram(), -1, true,
							org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
					if (cuEdge instanceof Edge) {
						((Edge) cuEdge).setSource(sourceView);
						((Edge) cuEdge).setTarget(targetView);
					}
				}
			}

			// two different cases:
			// (1) attribute belongs to context composite
			// (2) attribute does not belong to it, e.g. if a pattern
			// identifies HW as well as SW roles
			// => identified roles might not belong to same composite.
			// Additional Java code can help or ...
			// For demo: simple modify example? Modify functionality?
			// (don't want to see xyz).

			// roleView =
			// ViewService.getInstance().createView(Node.class, new
			// EObjectAdapter(copier),
			// (BasicCompartment)compartment,
			// semanticHint(UMLElementTypes.Property_3070),
			// ViewUtil.APPEND, true,
			// org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);

			/*
			 * roleView = new UMLViewProvider().createProperty_3070(copier, (BasicCompartment) compartment, -1,
			 * true,
			 * org.eclipse.papyrus.uml.diagram.composite.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
			 * Location location = NotationFactory.eINSTANCE.createLocation();
			 * location.setX(x);
			 * location.setY(y);
			 * if (roleView instanceof Node) {
			 * ((Node) roleView).setLayoutConstraint(location);
			 * }
			 * x += 30;
			 * y += 20;
			 * }
			 * }
			 */
		}

	}

	protected UMLViewProvider compViewProvider = new UMLViewProvider();

	public void addToInternalBlockDiagram(View view, Diagram diagram) {

		Object compartment = view.getChildren().get(1);
		System.err.println("got element:" + view.getElement()); //$NON-NLS-1$
		if (compartment instanceof BasicCompartment) {
			/*
			 * ViewService.getInstance().createView(Node.class, new
			 * EObjectAdapter(tst1), (BasicCompartment)compartment, null,
			 * ViewUtil.APPEND, true,
			 * org.eclipse.papyrus.sysml.diagram.internalblock.part.
			 * InternalBlockDiagramEditor. DIAGRAM_PREFERENCES_HINT);
			 * ViewService.getInstance().createView(Node.class, new
			 * EObjectAdapter(tst1), (BasicCompartment)compartment, null,
			 * ViewUtil.APPEND, true,
			 * org.eclipse.papyrus.sysml.diagram.internalblock.part.
			 * UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT );
			 */
		}

	}

	public void addToClassDiagram(Class context, Collaboration collaboration, View view, Diagram diagram) {

		/*
		 * int x = 10;
		 * for (ConnectableElement role : collaboration.getRoles()) {
		 * if (!userRoleMap.containsKey(role)) {
		 * Property copier = context.createOwnedAttribute(role.getName(), role.getType());
		 * View roleView = ViewService.getInstance()
		 * .createView(Node.class, new EObjectAdapter(
		 * copier), diagram, null, ViewUtil.APPEND, true,
		 * org.eclipse.papyrus.uml.diagram.clazz.part.UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT);
		 * if (roleView instanceof Node) {
		 * Location location = NotationFactory.eINSTANCE.createLocation();
		 * location.setX(x);
		 * location.setY(0);
		 * ((Node) roleView).setLayoutConstraint(location);
		 * x += 20;
		 * }
		 * }
		 * }
		 */
	}
}

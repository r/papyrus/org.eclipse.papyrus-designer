/*******************************************************************************
 * Copyright (c) 2018 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.patterns.transformations;

import java.util.Map;

import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.uml2.uml.NamedElement;

/**
 * Helper functions in the context of pattern application
 */
public interface IApplyPattern {

	/**
	 * Return filter for pattern: it checks whether the passed role is an eligible roles within a given pattern
	 * 
	 * @param designPattern
	 *            The design pattern
	 * @param role
	 *            the role defined within the pattern
	 * @return a viewerFilter that tests role candidates. If null, no filtering is done.
	 */
	public ViewerFilter getFilter(Pattern designPattern, NamedElement role);

	/**
	 * return the automatic bindings for elements within the role.
	 * 
	 * @param designPattern
	 *            The design pattern
	 * @param explicitBinding
	 * @return the map for automatic binding
	 */
	public Map<NamedElement, Object> automaticBinding(Pattern designPattern, Map<NamedElement, Object> explicitBinding);

	/**
	 * Called before a pattern is applied
	 * 
	 * @param designPattern
	 * @param roleBinding
	 */
	public void prePatternApplication(Pattern designPattern, Map<NamedElement, Object> roleBinding);

	/**
	 * Called after a pattern is applied
	 * 
	 * @param designPattern
	 * @param roleBinding
	 */
	public void postPatternApplication(Pattern designPattern, Map<NamedElement, Object> roleBinding);
}

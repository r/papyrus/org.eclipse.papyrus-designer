/**
 */
package org.eclipse.papyrus.designer.patterns.profile;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsFactory;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsFactoryImpl;

/**
 */
public class DesignPatternsCustomFactoryImpl extends DesignPatternsFactoryImpl implements DesignPatternsFactory {
	/**
	 */
	public Solution createSolution() {
		return new SolutionCustomImpl();
	}

	/**
	 */
	public Pattern createPattern() {
		return new PatternCustomImpl();
	}
}
package org.eclipse.papyrus.designer.patterns.profile;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl;
import org.eclipse.papyrus.designer.patterns.profile.utils.PatternUtils;

public class SolutionCustomImpl extends SolutionImpl {

	/**
	 */
	@Override
	public EList<Consequence> getConsequence() {
		return PatternUtils.getOwnedCommentsWithStereotype(getBase_Package(), Consequence.class);
	}

	/**
	 */
	@Override
	public EList<SampleCode> getSampleCode() {
		return PatternUtils.getPackagedElementsWithStereotype(getBase_Package(), SampleCode.class);
	}

	/**
	 */
	@Override
	public SolutionDesc basicGetDescription() {
		return PatternUtils.getPackagedElementWithStereotype(getBase_Package(), SolutionDesc.class);
	}
}

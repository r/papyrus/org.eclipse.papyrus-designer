package org.eclipse.papyrus.designer.patterns.profile;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Context;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.KnownUse;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl;
import org.eclipse.papyrus.designer.patterns.profile.utils.PatternUtils;

/**
 * Implementation of derived attributes
 */
public class PatternCustomImpl extends PatternImpl {
	@Override
	public EList<KnownUse> getKnownUse() {
		return PatternUtils.getOwnedCommentsWithStereotype(getBase_Package(), KnownUse.class);
	}

	/**
	 */
	@Override
	public Applicability basicGetApplicability() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), Applicability.class);
	}

	/**
	 */
	@Override
	public Intent basicGetIntent() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), Intent.class);
	}

	/**
	 */
	@Override
	public Problem basicGetProblem() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), Problem.class);
	}

	/**
	 */
	@Override
	public Context basicGetContext() {
		return PatternUtils.getPackagedElementWithStereotype(getBase_Package(), Context.class);
	}

	/**
	 */
	@Override
	public EList<Solution> getSolutions() {
		return PatternUtils.getPackagedElementsWithStereotype(getBase_Package(), Solution.class);
	}

	/**
	 */
	@Override
	public SolutionDesc basicGetSolutiondesc() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), SolutionDesc.class);
	}
}

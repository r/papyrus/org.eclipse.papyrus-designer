/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Consequence Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getConsequenceKind()
 * @model
 * @generated
 */
public enum ConsequenceKind implements Enumerator {
	/**
	 * The '<em><b>Result</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESULT_VALUE
	 * @generated
	 * @ordered
	 */
	RESULT(0, "Result", "Result"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Tradeoff</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRADEOFF_VALUE
	 * @generated
	 * @ordered
	 */
	TRADEOFF(1, "Tradeoff", "Tradeoff"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Impact On Flexibility</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPACT_ON_FLEXIBILITY_VALUE
	 * @generated
	 * @ordered
	 */
	IMPACT_ON_FLEXIBILITY(2, "ImpactOnFlexibility", "ImpactOnFlexibility"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Impact On Extensibility</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPACT_ON_EXTENSIBILITY_VALUE
	 * @generated
	 * @ordered
	 */
	IMPACT_ON_EXTENSIBILITY(3, "ImpactOnExtensibility", "ImpactOnExtensibility"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Impact On Portability</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPACT_ON_PORTABILITY_VALUE
	 * @generated
	 * @ordered
	 */
	IMPACT_ON_PORTABILITY(4, "ImpactOnPortability", "ImpactOnPortability"); //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Result</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Result</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RESULT
	 * @model name="Result"
	 * @generated
	 * @ordered
	 */
	public static final int RESULT_VALUE = 0;

	/**
	 * The '<em><b>Tradeoff</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Tradeoff</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRADEOFF
	 * @model name="Tradeoff"
	 * @generated
	 * @ordered
	 */
	public static final int TRADEOFF_VALUE = 1;

	/**
	 * The '<em><b>Impact On Flexibility</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Impact On Flexibility</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPACT_ON_FLEXIBILITY
	 * @model name="ImpactOnFlexibility"
	 * @generated
	 * @ordered
	 */
	public static final int IMPACT_ON_FLEXIBILITY_VALUE = 2;

	/**
	 * The '<em><b>Impact On Extensibility</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Impact On Extensibility</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPACT_ON_EXTENSIBILITY
	 * @model name="ImpactOnExtensibility"
	 * @generated
	 * @ordered
	 */
	public static final int IMPACT_ON_EXTENSIBILITY_VALUE = 3;

	/**
	 * The '<em><b>Impact On Portability</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Impact On Portability</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPACT_ON_PORTABILITY
	 * @model name="ImpactOnPortability"
	 * @generated
	 * @ordered
	 */
	public static final int IMPACT_ON_PORTABILITY_VALUE = 4;

	/**
	 * An array of all the '<em><b>Consequence Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ConsequenceKind[] VALUES_ARRAY =
		new ConsequenceKind[] {
			RESULT,
			TRADEOFF,
			IMPACT_ON_FLEXIBILITY,
			IMPACT_ON_EXTENSIBILITY,
			IMPACT_ON_PORTABILITY,
		};

	/**
	 * A public read-only list of all the '<em><b>Consequence Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ConsequenceKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Consequence Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ConsequenceKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConsequenceKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Consequence Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ConsequenceKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ConsequenceKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Consequence Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ConsequenceKind get(int value) {
		switch (value) {
			case RESULT_VALUE: return RESULT;
			case TRADEOFF_VALUE: return TRADEOFF;
			case IMPACT_ON_FLEXIBILITY_VALUE: return IMPACT_ON_FLEXIBILITY;
			case IMPACT_ON_EXTENSIBILITY_VALUE: return IMPACT_ON_EXTENSIBILITY;
			case IMPACT_ON_PORTABILITY_VALUE: return IMPACT_ON_PORTABILITY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ConsequenceKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ConsequenceKind

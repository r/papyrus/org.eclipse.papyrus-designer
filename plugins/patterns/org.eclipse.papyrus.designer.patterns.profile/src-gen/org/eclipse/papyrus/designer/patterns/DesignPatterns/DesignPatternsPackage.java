/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsFactory
 * @model kind="package"
 * @generated
 */
public interface DesignPatternsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "DesignPatterns"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/DP/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "DesignPatterns"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DesignPatternsPackage eINSTANCE = org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl <em>Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getContext()
	 * @generated
	 */
	int CONTEXT = 0;

	/**
	 * The feature id for the '<em><b>Applicability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT__APPLICABILITY = 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT__BASE_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Precondition</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT__PRECONDITION = 2;

	/**
	 * The feature id for the '<em><b>Motivation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT__MOTIVATION = 3;

	/**
	 * The number of structural features of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTEXT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ApplicabilityImpl <em>Applicability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ApplicabilityImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getApplicability()
	 * @generated
	 */
	int APPLICABILITY = 1;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICABILITY__BASE_COMMENT = 0;

	/**
	 * The number of structural features of the '<em>Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICABILITY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Applicability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.IntentImpl <em>Intent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.IntentImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getIntent()
	 * @generated
	 */
	int INTENT = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTENT__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTENT__BASE_COMMENT = 1;

	/**
	 * The number of structural features of the '<em>Intent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Intent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionDescImpl <em>Solution Desc</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionDescImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSolutionDesc()
	 * @generated
	 */
	int SOLUTION_DESC = 3;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_DESC__BASE_COMMENT = 0;

	/**
	 * The number of structural features of the '<em>Solution Desc</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_DESC_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Solution Desc</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_DESC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemImpl <em>Problem</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getProblem()
	 * @generated
	 */
	int PROBLEM = 4;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROBLEM__KIND = 0;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROBLEM__BASE_COMMENT = 1;

	/**
	 * The number of structural features of the '<em>Problem</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROBLEM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Problem</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROBLEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemKindImpl <em>Problem Kind</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemKindImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getProblemKind()
	 * @generated
	 */
	int PROBLEM_KIND = 5;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROBLEM_KIND__BASE_CLASS = 0;

	/**
	 * The number of structural features of the '<em>Problem Kind</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROBLEM_KIND_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Problem Kind</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROBLEM_KIND_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl <em>Solution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSolution()
	 * @generated
	 */
	int SOLUTION = 6;

	/**
	 * The feature id for the '<em><b>Consequence</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__CONSEQUENCE = 0;

	/**
	 * The feature id for the '<em><b>Sample Code</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__SAMPLE_CODE = 1;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__BASE_PACKAGE = 2;

	/**
	 * The feature id for the '<em><b>Safety Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__SAFETY_LEVEL = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION__DESCRIPTION = 4;

	/**
	 * The number of structural features of the '<em>Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Solution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOLUTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ConsequenceImpl <em>Consequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ConsequenceImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getConsequence()
	 * @generated
	 */
	int CONSEQUENCE = 7;

	/**
	 * The feature id for the '<em><b>Conseq Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSEQUENCE__CONSEQ_KIND = 0;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSEQUENCE__BASE_COMMENT = 1;

	/**
	 * The number of structural features of the '<em>Consequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSEQUENCE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Consequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSEQUENCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SampleCodeImpl <em>Sample Code</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SampleCodeImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSampleCode()
	 * @generated
	 */
	int SAMPLE_CODE = 8;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLE_CODE__BASE_CLASS = 0;

	/**
	 * The number of structural features of the '<em>Sample Code</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLE_CODE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Sample Code</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAMPLE_CODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl <em>Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getPattern()
	 * @generated
	 */
	int PATTERN = 9;

	/**
	 * The feature id for the '<em><b>Known Use</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__KNOWN_USE = 0;

	/**
	 * The feature id for the '<em><b>Applicability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__APPLICABILITY = 1;

	/**
	 * The feature id for the '<em><b>Related Pattern</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__RELATED_PATTERN = 2;

	/**
	 * The feature id for the '<em><b>Intent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__INTENT = 3;

	/**
	 * The feature id for the '<em><b>Problem</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__PROBLEM = 4;

	/**
	 * The feature id for the '<em><b>Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__CONTEXT = 5;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__BASE_PACKAGE = 6;

	/**
	 * The feature id for the '<em><b>Solutions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__SOLUTIONS = 7;

	/**
	 * The feature id for the '<em><b>Classification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__CLASSIFICATION = 8;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN__LEVEL = 9;

	/**
	 * The number of structural features of the '<em>Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.KnownUseImpl <em>Known Use</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.KnownUseImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getKnownUse()
	 * @generated
	 */
	int KNOWN_USE = 10;

	/**
	 * The feature id for the '<em><b>Base Comment</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KNOWN_USE__BASE_COMMENT = 0;

	/**
	 * The number of structural features of the '<em>Known Use</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KNOWN_USE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Known Use</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int KNOWN_USE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl <em>Replication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getReplication()
	 * @generated
	 */
	int REPLICATION = 11;

	/**
	 * The feature id for the '<em><b>Base Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPLICATION__BASE_CONSTRAINT = 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPLICATION__KIND = 1;

	/**
	 * The feature id for the '<em><b>Initial Number Of Replicas</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPLICATION__INITIAL_NUMBER_OF_REPLICAS = 2;

	/**
	 * The feature id for the '<em><b>Min Number Of Replicas</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPLICATION__MIN_NUMBER_OF_REPLICAS = 3;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPLICATION__BASE_PROPERTY = 4;

	/**
	 * The number of structural features of the '<em>Replication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPLICATION_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Replication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REPLICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.AutomaticBindingImpl <em>Automatic Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.AutomaticBindingImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getAutomaticBinding()
	 * @generated
	 */
	int AUTOMATIC_BINDING = 12;

	/**
	 * The feature id for the '<em><b>Base Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATIC_BINDING__BASE_PROPERTY = 0;

	/**
	 * The number of structural features of the '<em>Automatic Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATIC_BINDING_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Automatic Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTOMATIC_BINDING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternApplicationImpl <em>Pattern Application</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternApplicationImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getPatternApplication()
	 * @generated
	 */
	int PATTERN_APPLICATION = 13;

	/**
	 * The feature id for the '<em><b>Base Collaboration Use</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATION__BASE_COLLABORATION_USE = 0;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATION__DATE = 1;

	/**
	 * The feature id for the '<em><b>User</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATION__USER = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATION__BASE_CLASS = 3;

	/**
	 * The number of structural features of the '<em>Pattern Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATION_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Pattern Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_APPLICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternSystemImpl <em>Pattern System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternSystemImpl
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getPatternSystem()
	 * @generated
	 */
	int PATTERN_SYSTEM = 14;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SYSTEM__BASE_PACKAGE = 0;

	/**
	 * The number of structural features of the '<em>Pattern System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SYSTEM_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Pattern System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATTERN_SYSTEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ConsequenceKind <em>Consequence Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ConsequenceKind
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getConsequenceKind()
	 * @generated
	 */
	int CONSEQUENCE_KIND = 15;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel <em>Safety Level</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSafetyLevel()
	 * @generated
	 */
	int SAFETY_LEVEL = 16;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification <em>Classification</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getClassification()
	 * @generated
	 */
	int CLASSIFICATION = 17;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel <em>Reference Level</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getReferenceLevel()
	 * @generated
	 */
	int REFERENCE_LEVEL = 18;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind <em>Replication Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getReplicationKind()
	 * @generated
	 */
	int REPLICATION_KIND = 19;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Context</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Context
	 * @generated
	 */
	EClass getContext();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getApplicability <em>Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applicability</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getApplicability()
	 * @see #getContext()
	 * @generated
	 */
	EReference getContext_Applicability();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getBase_Class()
	 * @see #getContext()
	 * @generated
	 */
	EReference getContext_Base_Class();

	/**
	 * Returns the meta object for the attribute list '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getPrecondition <em>Precondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Precondition</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getPrecondition()
	 * @see #getContext()
	 * @generated
	 */
	EAttribute getContext_Precondition();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getMotivation <em>Motivation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Motivation</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getMotivation()
	 * @see #getContext()
	 * @generated
	 */
	EAttribute getContext_Motivation();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability <em>Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Applicability</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability
	 * @generated
	 */
	EClass getApplicability();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability#getBase_Comment()
	 * @see #getApplicability()
	 * @generated
	 */
	EReference getApplicability_Base_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent <em>Intent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Intent</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent
	 * @generated
	 */
	EClass getIntent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Description</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent#getDescription()
	 * @see #getIntent()
	 * @generated
	 */
	EReference getIntent_Description();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent#getBase_Comment()
	 * @see #getIntent()
	 * @generated
	 */
	EReference getIntent_Base_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc <em>Solution Desc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solution Desc</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc
	 * @generated
	 */
	EClass getSolutionDesc();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc#getBase_Comment()
	 * @see #getSolutionDesc()
	 * @generated
	 */
	EReference getSolutionDesc_Base_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem <em>Problem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Problem</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem
	 * @generated
	 */
	EClass getProblem();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Kind</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem#getKind()
	 * @see #getProblem()
	 * @generated
	 */
	EReference getProblem_Kind();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem#getBase_Comment()
	 * @see #getProblem()
	 * @generated
	 */
	EReference getProblem_Base_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ProblemKind <em>Problem Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Problem Kind</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ProblemKind
	 * @generated
	 */
	EClass getProblemKind();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ProblemKind#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ProblemKind#getBase_Class()
	 * @see #getProblemKind()
	 * @generated
	 */
	EReference getProblemKind_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution <em>Solution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Solution</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution
	 * @generated
	 */
	EClass getSolution();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getConsequence <em>Consequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Consequence</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getConsequence()
	 * @see #getSolution()
	 * @generated
	 */
	EReference getSolution_Consequence();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getSampleCode <em>Sample Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sample Code</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getSampleCode()
	 * @see #getSolution()
	 * @generated
	 */
	EReference getSolution_SampleCode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Description</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getDescription()
	 * @see #getSolution()
	 * @generated
	 */
	EReference getSolution_Description();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getBase_Package()
	 * @see #getSolution()
	 * @generated
	 */
	EReference getSolution_Base_Package();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getSafetyLevel <em>Safety Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Safety Level</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getSafetyLevel()
	 * @see #getSolution()
	 * @generated
	 */
	EAttribute getSolution_SafetyLevel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence <em>Consequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Consequence</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence
	 * @generated
	 */
	EClass getConsequence();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence#getConseqKind <em>Conseq Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Conseq Kind</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence#getConseqKind()
	 * @see #getConsequence()
	 * @generated
	 */
	EAttribute getConsequence_ConseqKind();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence#getBase_Comment()
	 * @see #getConsequence()
	 * @generated
	 */
	EReference getConsequence_Base_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode <em>Sample Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sample Code</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode
	 * @generated
	 */
	EClass getSampleCode();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode#getBase_Class()
	 * @see #getSampleCode()
	 * @generated
	 */
	EReference getSampleCode_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern <em>Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern
	 * @generated
	 */
	EClass getPattern();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getKnownUse <em>Known Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Known Use</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getKnownUse()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_KnownUse();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getApplicability <em>Applicability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Applicability</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getApplicability()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_Applicability();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getRelatedPattern <em>Related Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Related Pattern</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getRelatedPattern()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_RelatedPattern();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getIntent <em>Intent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Intent</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getIntent()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_Intent();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getProblem <em>Problem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Problem</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getProblem()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_Problem();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Context</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getContext()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_Context();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getBase_Package()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_Base_Package();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getSolutions <em>Solutions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Solutions</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getSolutions()
	 * @see #getPattern()
	 * @generated
	 */
	EReference getPattern_Solutions();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getClassification <em>Classification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Classification</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getClassification()
	 * @see #getPattern()
	 * @generated
	 */
	EAttribute getPattern_Classification();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern#getLevel()
	 * @see #getPattern()
	 * @generated
	 */
	EAttribute getPattern_Level();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.KnownUse <em>Known Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Known Use</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.KnownUse
	 * @generated
	 */
	EClass getKnownUse();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.KnownUse#getBase_Comment <em>Base Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Comment</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.KnownUse#getBase_Comment()
	 * @see #getKnownUse()
	 * @generated
	 */
	EReference getKnownUse_Base_Comment();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication <em>Replication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Replication</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication
	 * @generated
	 */
	EClass getReplication();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Constraint <em>Base Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Constraint</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Constraint()
	 * @see #getReplication()
	 * @generated
	 */
	EReference getReplication_Base_Constraint();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getKind()
	 * @see #getReplication()
	 * @generated
	 */
	EAttribute getReplication_Kind();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getInitialNumberOfReplicas <em>Initial Number Of Replicas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Number Of Replicas</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getInitialNumberOfReplicas()
	 * @see #getReplication()
	 * @generated
	 */
	EAttribute getReplication_InitialNumberOfReplicas();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getMinNumberOfReplicas <em>Min Number Of Replicas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Number Of Replicas</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getMinNumberOfReplicas()
	 * @see #getReplication()
	 * @generated
	 */
	EAttribute getReplication_MinNumberOfReplicas();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication#getBase_Property()
	 * @see #getReplication()
	 * @generated
	 */
	EReference getReplication_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.AutomaticBinding <em>Automatic Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Automatic Binding</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.AutomaticBinding
	 * @generated
	 */
	EClass getAutomaticBinding();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.AutomaticBinding#getBase_Property <em>Base Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Property</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.AutomaticBinding#getBase_Property()
	 * @see #getAutomaticBinding()
	 * @generated
	 */
	EReference getAutomaticBinding_Base_Property();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication <em>Pattern Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern Application</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication
	 * @generated
	 */
	EClass getPatternApplication();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_CollaborationUse <em>Base Collaboration Use</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Collaboration Use</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_CollaborationUse()
	 * @see #getPatternApplication()
	 * @generated
	 */
	EReference getPatternApplication_Base_CollaborationUse();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getDate()
	 * @see #getPatternApplication()
	 * @generated
	 */
	EAttribute getPatternApplication_Date();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getUser()
	 * @see #getPatternApplication()
	 * @generated
	 */
	EAttribute getPatternApplication_User();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication#getBase_Class()
	 * @see #getPatternApplication()
	 * @generated
	 */
	EReference getPatternApplication_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternSystem <em>Pattern System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pattern System</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternSystem
	 * @generated
	 */
	EClass getPatternSystem();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternSystem#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternSystem#getBase_Package()
	 * @see #getPatternSystem()
	 * @generated
	 */
	EReference getPatternSystem_Base_Package();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ConsequenceKind <em>Consequence Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Consequence Kind</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ConsequenceKind
	 * @generated
	 */
	EEnum getConsequenceKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel <em>Safety Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Safety Level</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel
	 * @generated
	 */
	EEnum getSafetyLevel();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification <em>Classification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Classification</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification
	 * @generated
	 */
	EEnum getClassification();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel <em>Reference Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Reference Level</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel
	 * @generated
	 */
	EEnum getReferenceLevel();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind <em>Replication Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Replication Kind</em>'.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind
	 * @generated
	 */
	EEnum getReplicationKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DesignPatternsFactory getDesignPatternsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl <em>Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getContext()
		 * @generated
		 */
		EClass CONTEXT = eINSTANCE.getContext();

		/**
		 * The meta object literal for the '<em><b>Applicability</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTEXT__APPLICABILITY = eINSTANCE.getContext_Applicability();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTEXT__BASE_CLASS = eINSTANCE.getContext_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Precondition</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTEXT__PRECONDITION = eINSTANCE.getContext_Precondition();

		/**
		 * The meta object literal for the '<em><b>Motivation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTEXT__MOTIVATION = eINSTANCE.getContext_Motivation();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ApplicabilityImpl <em>Applicability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ApplicabilityImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getApplicability()
		 * @generated
		 */
		EClass APPLICABILITY = eINSTANCE.getApplicability();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLICABILITY__BASE_COMMENT = eINSTANCE.getApplicability_Base_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.IntentImpl <em>Intent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.IntentImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getIntent()
		 * @generated
		 */
		EClass INTENT = eINSTANCE.getIntent();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTENT__DESCRIPTION = eINSTANCE.getIntent_Description();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTENT__BASE_COMMENT = eINSTANCE.getIntent_Base_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionDescImpl <em>Solution Desc</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionDescImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSolutionDesc()
		 * @generated
		 */
		EClass SOLUTION_DESC = eINSTANCE.getSolutionDesc();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION_DESC__BASE_COMMENT = eINSTANCE.getSolutionDesc_Base_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemImpl <em>Problem</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getProblem()
		 * @generated
		 */
		EClass PROBLEM = eINSTANCE.getProblem();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROBLEM__KIND = eINSTANCE.getProblem_Kind();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROBLEM__BASE_COMMENT = eINSTANCE.getProblem_Base_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemKindImpl <em>Problem Kind</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ProblemKindImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getProblemKind()
		 * @generated
		 */
		EClass PROBLEM_KIND = eINSTANCE.getProblemKind();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROBLEM_KIND__BASE_CLASS = eINSTANCE.getProblemKind_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl <em>Solution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSolution()
		 * @generated
		 */
		EClass SOLUTION = eINSTANCE.getSolution();

		/**
		 * The meta object literal for the '<em><b>Consequence</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION__CONSEQUENCE = eINSTANCE.getSolution_Consequence();

		/**
		 * The meta object literal for the '<em><b>Sample Code</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION__SAMPLE_CODE = eINSTANCE.getSolution_SampleCode();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION__DESCRIPTION = eINSTANCE.getSolution_Description();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOLUTION__BASE_PACKAGE = eINSTANCE.getSolution_Base_Package();

		/**
		 * The meta object literal for the '<em><b>Safety Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOLUTION__SAFETY_LEVEL = eINSTANCE.getSolution_SafetyLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ConsequenceImpl <em>Consequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ConsequenceImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getConsequence()
		 * @generated
		 */
		EClass CONSEQUENCE = eINSTANCE.getConsequence();

		/**
		 * The meta object literal for the '<em><b>Conseq Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSEQUENCE__CONSEQ_KIND = eINSTANCE.getConsequence_ConseqKind();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSEQUENCE__BASE_COMMENT = eINSTANCE.getConsequence_Base_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SampleCodeImpl <em>Sample Code</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SampleCodeImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSampleCode()
		 * @generated
		 */
		EClass SAMPLE_CODE = eINSTANCE.getSampleCode();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SAMPLE_CODE__BASE_CLASS = eINSTANCE.getSampleCode_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl <em>Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getPattern()
		 * @generated
		 */
		EClass PATTERN = eINSTANCE.getPattern();

		/**
		 * The meta object literal for the '<em><b>Known Use</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__KNOWN_USE = eINSTANCE.getPattern_KnownUse();

		/**
		 * The meta object literal for the '<em><b>Applicability</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__APPLICABILITY = eINSTANCE.getPattern_Applicability();

		/**
		 * The meta object literal for the '<em><b>Related Pattern</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__RELATED_PATTERN = eINSTANCE.getPattern_RelatedPattern();

		/**
		 * The meta object literal for the '<em><b>Intent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__INTENT = eINSTANCE.getPattern_Intent();

		/**
		 * The meta object literal for the '<em><b>Problem</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__PROBLEM = eINSTANCE.getPattern_Problem();

		/**
		 * The meta object literal for the '<em><b>Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__CONTEXT = eINSTANCE.getPattern_Context();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__BASE_PACKAGE = eINSTANCE.getPattern_Base_Package();

		/**
		 * The meta object literal for the '<em><b>Solutions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN__SOLUTIONS = eINSTANCE.getPattern_Solutions();

		/**
		 * The meta object literal for the '<em><b>Classification</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PATTERN__CLASSIFICATION = eINSTANCE.getPattern_Classification();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PATTERN__LEVEL = eINSTANCE.getPattern_Level();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.KnownUseImpl <em>Known Use</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.KnownUseImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getKnownUse()
		 * @generated
		 */
		EClass KNOWN_USE = eINSTANCE.getKnownUse();

		/**
		 * The meta object literal for the '<em><b>Base Comment</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference KNOWN_USE__BASE_COMMENT = eINSTANCE.getKnownUse_Base_Comment();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl <em>Replication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getReplication()
		 * @generated
		 */
		EClass REPLICATION = eINSTANCE.getReplication();

		/**
		 * The meta object literal for the '<em><b>Base Constraint</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPLICATION__BASE_CONSTRAINT = eINSTANCE.getReplication_Base_Constraint();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPLICATION__KIND = eINSTANCE.getReplication_Kind();

		/**
		 * The meta object literal for the '<em><b>Initial Number Of Replicas</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPLICATION__INITIAL_NUMBER_OF_REPLICAS = eINSTANCE.getReplication_InitialNumberOfReplicas();

		/**
		 * The meta object literal for the '<em><b>Min Number Of Replicas</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REPLICATION__MIN_NUMBER_OF_REPLICAS = eINSTANCE.getReplication_MinNumberOfReplicas();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REPLICATION__BASE_PROPERTY = eINSTANCE.getReplication_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.AutomaticBindingImpl <em>Automatic Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.AutomaticBindingImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getAutomaticBinding()
		 * @generated
		 */
		EClass AUTOMATIC_BINDING = eINSTANCE.getAutomaticBinding();

		/**
		 * The meta object literal for the '<em><b>Base Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUTOMATIC_BINDING__BASE_PROPERTY = eINSTANCE.getAutomaticBinding_Base_Property();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternApplicationImpl <em>Pattern Application</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternApplicationImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getPatternApplication()
		 * @generated
		 */
		EClass PATTERN_APPLICATION = eINSTANCE.getPatternApplication();

		/**
		 * The meta object literal for the '<em><b>Base Collaboration Use</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_APPLICATION__BASE_COLLABORATION_USE = eINSTANCE.getPatternApplication_Base_CollaborationUse();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PATTERN_APPLICATION__DATE = eINSTANCE.getPatternApplication_Date();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PATTERN_APPLICATION__USER = eINSTANCE.getPatternApplication_User();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_APPLICATION__BASE_CLASS = eINSTANCE.getPatternApplication_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternSystemImpl <em>Pattern System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternSystemImpl
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getPatternSystem()
		 * @generated
		 */
		EClass PATTERN_SYSTEM = eINSTANCE.getPatternSystem();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PATTERN_SYSTEM__BASE_PACKAGE = eINSTANCE.getPatternSystem_Base_Package();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ConsequenceKind <em>Consequence Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ConsequenceKind
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getConsequenceKind()
		 * @generated
		 */
		EEnum CONSEQUENCE_KIND = eINSTANCE.getConsequenceKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel <em>Safety Level</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getSafetyLevel()
		 * @generated
		 */
		EEnum SAFETY_LEVEL = eINSTANCE.getSafetyLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification <em>Classification</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getClassification()
		 * @generated
		 */
		EEnum CLASSIFICATION = eINSTANCE.getClassification();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel <em>Reference Level</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getReferenceLevel()
		 * @generated
		 */
		EEnum REFERENCE_LEVEL = eINSTANCE.getReferenceLevel();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind <em>Replication Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind
		 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.DesignPatternsPackageImpl#getReplicationKind()
		 * @generated
		 */
		EEnum REPLICATION_KIND = eINSTANCE.getReplicationKind();

	}

} //DesignPatternsPackage

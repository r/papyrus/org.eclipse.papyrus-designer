/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Pre-condition Rules that have to be satisfied 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getApplicability <em>Applicability</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getMotivation <em>Motivation</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getContext()
 * @model
 * @generated
 */
public interface Context extends EObject {
	/**
	 * Returns the value of the '<em><b>Applicability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Applicability</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Applicability</em>' reference.
	 * @see #setApplicability(Applicability)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getContext_Applicability()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Applicability getApplicability();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getApplicability <em>Applicability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Applicability</em>' reference.
	 * @see #getApplicability()
	 * @generated
	 */
	void setApplicability(Applicability value);

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getContext_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

	/**
	 * Returns the value of the '<em><b>Precondition</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Precondition</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Precondition</em>' attribute list.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getContext_Precondition()
	 * @model dataType="org.eclipse.uml2.types.String" ordered="false"
	 * @generated
	 */
	EList<String> getPrecondition();

	/**
	 * Returns the value of the '<em><b>Motivation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Motivation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Motivation</em>' attribute.
	 * @see #setMotivation(String)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getContext_Motivation()
	 * @model dataType="org.eclipse.uml2.types.String" required="true" ordered="false"
	 * @generated
	 */
	String getMotivation();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Context#getMotivation <em>Motivation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Motivation</em>' attribute.
	 * @see #getMotivation()
	 * @generated
	 */
	void setMotivation(String value);

} // Context

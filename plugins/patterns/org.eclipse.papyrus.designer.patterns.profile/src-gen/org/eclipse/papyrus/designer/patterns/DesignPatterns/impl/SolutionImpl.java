/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc;
import org.eclipse.papyrus.designer.patterns.profile.utils.PatternUtils;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Solution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl#getConsequence <em>Consequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl#getSampleCode <em>Sample Code</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl#getBase_Package <em>Base Package</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl#getSafetyLevel <em>Safety Level</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.SolutionImpl#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SolutionImpl extends MinimalEObjectImpl.Container implements Solution {
	/**
	 * The cached value of the '{@link #getBase_Package() <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Package()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package base_Package;

	/**
	 * The default value of the '{@link #getSafetyLevel() <em>Safety Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSafetyLevel()
	 * @generated
	 * @ordered
	 */
	protected static final SafetyLevel SAFETY_LEVEL_EDEFAULT = SafetyLevel.SIL0;

	/**
	 * The cached value of the '{@link #getSafetyLevel() <em>Safety Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSafetyLevel()
	 * @generated
	 * @ordered
	 */
	protected SafetyLevel safetyLevel = SAFETY_LEVEL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SolutionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsPackage.Literals.SOLUTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated 
	 */
	public EList<Consequence> getConsequence() {
		return PatternUtils.getOwnedCommentsWithStereotype(getBase_Package(), Consequence.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated 
	 */
	public EList<SampleCode> getSampleCode() {
		return PatternUtils.getPackagedElementsWithStereotype(getBase_Package(), SampleCode.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionDesc getDescription() {
		SolutionDesc description = basicGetDescription();
		return description != null && description.eIsProxy() ? (SolutionDesc)eResolveProxy((InternalEObject)description) : description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionDesc basicGetDescription() {
		return PatternUtils.getPackagedElementWithStereotype(getBase_Package(), SolutionDesc.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getBase_Package() {
		if (base_Package != null && base_Package.eIsProxy()) {
			InternalEObject oldBase_Package = (InternalEObject)base_Package;
			base_Package = (org.eclipse.uml2.uml.Package)eResolveProxy(oldBase_Package);
			if (base_Package != oldBase_Package) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsPackage.SOLUTION__BASE_PACKAGE, oldBase_Package, base_Package));
			}
		}
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetBase_Package() {
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Package(org.eclipse.uml2.uml.Package newBase_Package) {
		org.eclipse.uml2.uml.Package oldBase_Package = base_Package;
		base_Package = newBase_Package;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.SOLUTION__BASE_PACKAGE, oldBase_Package, base_Package));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SafetyLevel getSafetyLevel() {
		return safetyLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSafetyLevel(SafetyLevel newSafetyLevel) {
		SafetyLevel oldSafetyLevel = safetyLevel;
		safetyLevel = newSafetyLevel == null ? SAFETY_LEVEL_EDEFAULT : newSafetyLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.SOLUTION__SAFETY_LEVEL, oldSafetyLevel, safetyLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsPackage.SOLUTION__CONSEQUENCE:
				return getConsequence();
			case DesignPatternsPackage.SOLUTION__SAMPLE_CODE:
				return getSampleCode();
			case DesignPatternsPackage.SOLUTION__BASE_PACKAGE:
				if (resolve) return getBase_Package();
				return basicGetBase_Package();
			case DesignPatternsPackage.SOLUTION__SAFETY_LEVEL:
				return getSafetyLevel();
			case DesignPatternsPackage.SOLUTION__DESCRIPTION:
				if (resolve) return getDescription();
				return basicGetDescription();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsPackage.SOLUTION__CONSEQUENCE:
				getConsequence().clear();
				getConsequence().addAll((Collection<? extends Consequence>)newValue);
				return;
			case DesignPatternsPackage.SOLUTION__SAMPLE_CODE:
				getSampleCode().clear();
				getSampleCode().addAll((Collection<? extends SampleCode>)newValue);
				return;
			case DesignPatternsPackage.SOLUTION__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)newValue);
				return;
			case DesignPatternsPackage.SOLUTION__SAFETY_LEVEL:
				setSafetyLevel((SafetyLevel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.SOLUTION__CONSEQUENCE:
				getConsequence().clear();
				return;
			case DesignPatternsPackage.SOLUTION__SAMPLE_CODE:
				getSampleCode().clear();
				return;
			case DesignPatternsPackage.SOLUTION__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)null);
				return;
			case DesignPatternsPackage.SOLUTION__SAFETY_LEVEL:
				setSafetyLevel(SAFETY_LEVEL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.SOLUTION__CONSEQUENCE:
				return !getConsequence().isEmpty();
			case DesignPatternsPackage.SOLUTION__SAMPLE_CODE:
				return !getSampleCode().isEmpty();
			case DesignPatternsPackage.SOLUTION__BASE_PACKAGE:
				return base_Package != null;
			case DesignPatternsPackage.SOLUTION__SAFETY_LEVEL:
				return safetyLevel != SAFETY_LEVEL_EDEFAULT;
			case DesignPatternsPackage.SOLUTION__DESCRIPTION:
				return basicGetDescription() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (safetyLevel: "); //$NON-NLS-1$
		result.append(safetyLevel);
		result.append(')');
		return result.toString();
	}

} //SolutionImpl

/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Replication Kind</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getReplicationKind()
 * @model
 * @generated
 */
public enum ReplicationKind implements Enumerator {
	/**
	 * The '<em><b>Identical Replication</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IDENTICAL_REPLICATION_VALUE
	 * @generated
	 * @ordered
	 */
	IDENTICAL_REPLICATION(0, "IdenticalReplication", "IdenticalReplication"), //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Diversified Replication</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIVERSIFIED_REPLICATION_VALUE
	 * @generated
	 * @ordered
	 */
	DIVERSIFIED_REPLICATION(1, "DiversifiedReplication", "DiversifiedReplication"); //$NON-NLS-1$ //$NON-NLS-2$

	/**
	 * The '<em><b>Identical Replication</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Identical Replication</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IDENTICAL_REPLICATION
	 * @model name="IdenticalReplication"
	 * @generated
	 * @ordered
	 */
	public static final int IDENTICAL_REPLICATION_VALUE = 0;

	/**
	 * The '<em><b>Diversified Replication</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diversified Replication</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIVERSIFIED_REPLICATION
	 * @model name="DiversifiedReplication"
	 * @generated
	 * @ordered
	 */
	public static final int DIVERSIFIED_REPLICATION_VALUE = 1;

	/**
	 * An array of all the '<em><b>Replication Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ReplicationKind[] VALUES_ARRAY =
		new ReplicationKind[] {
			IDENTICAL_REPLICATION,
			DIVERSIFIED_REPLICATION,
		};

	/**
	 * A public read-only list of all the '<em><b>Replication Kind</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ReplicationKind> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Replication Kind</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ReplicationKind get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ReplicationKind result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Replication Kind</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ReplicationKind getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ReplicationKind result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Replication Kind</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static ReplicationKind get(int value) {
		switch (value) {
			case IDENTICAL_REPLICATION_VALUE: return IDENTICAL_REPLICATION;
			case DIVERSIFIED_REPLICATION_VALUE: return DIVERSIFIED_REPLICATION;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ReplicationKind(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ReplicationKind

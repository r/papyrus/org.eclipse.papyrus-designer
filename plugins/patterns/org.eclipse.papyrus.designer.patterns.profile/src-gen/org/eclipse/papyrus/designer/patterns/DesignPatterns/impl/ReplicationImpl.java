/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind;

import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Property;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Replication</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl#getBase_Constraint <em>Base Constraint</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl#getInitialNumberOfReplicas <em>Initial Number Of Replicas</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl#getMinNumberOfReplicas <em>Min Number Of Replicas</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ReplicationImpl#getBase_Property <em>Base Property</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReplicationImpl extends MinimalEObjectImpl.Container implements Replication {
	/**
	 * The cached value of the '{@link #getBase_Constraint() <em>Base Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Constraint()
	 * @generated
	 * @ordered
	 */
	protected Constraint base_Constraint;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final ReplicationKind KIND_EDEFAULT = ReplicationKind.IDENTICAL_REPLICATION;

	/**
	 * The cached value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected ReplicationKind kind = KIND_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitialNumberOfReplicas() <em>Initial Number Of Replicas</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialNumberOfReplicas()
	 * @generated
	 * @ordered
	 */
	protected static final int INITIAL_NUMBER_OF_REPLICAS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getInitialNumberOfReplicas() <em>Initial Number Of Replicas</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialNumberOfReplicas()
	 * @generated
	 * @ordered
	 */
	protected int initialNumberOfReplicas = INITIAL_NUMBER_OF_REPLICAS_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinNumberOfReplicas() <em>Min Number Of Replicas</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinNumberOfReplicas()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_NUMBER_OF_REPLICAS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinNumberOfReplicas() <em>Min Number Of Replicas</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinNumberOfReplicas()
	 * @generated
	 * @ordered
	 */
	protected int minNumberOfReplicas = MIN_NUMBER_OF_REPLICAS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Property() <em>Base Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Property()
	 * @generated
	 * @ordered
	 */
	protected Property base_Property;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReplicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsPackage.Literals.REPLICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getBase_Constraint() {
		if (base_Constraint != null && base_Constraint.eIsProxy()) {
			InternalEObject oldBase_Constraint = (InternalEObject)base_Constraint;
			base_Constraint = (Constraint)eResolveProxy(oldBase_Constraint);
			if (base_Constraint != oldBase_Constraint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsPackage.REPLICATION__BASE_CONSTRAINT, oldBase_Constraint, base_Constraint));
			}
		}
		return base_Constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint basicGetBase_Constraint() {
		return base_Constraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Constraint(Constraint newBase_Constraint) {
		Constraint oldBase_Constraint = base_Constraint;
		base_Constraint = newBase_Constraint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.REPLICATION__BASE_CONSTRAINT, oldBase_Constraint, base_Constraint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReplicationKind getKind() {
		return kind;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKind(ReplicationKind newKind) {
		ReplicationKind oldKind = kind;
		kind = newKind == null ? KIND_EDEFAULT : newKind;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.REPLICATION__KIND, oldKind, kind));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInitialNumberOfReplicas() {
		return initialNumberOfReplicas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialNumberOfReplicas(int newInitialNumberOfReplicas) {
		int oldInitialNumberOfReplicas = initialNumberOfReplicas;
		initialNumberOfReplicas = newInitialNumberOfReplicas;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.REPLICATION__INITIAL_NUMBER_OF_REPLICAS, oldInitialNumberOfReplicas, initialNumberOfReplicas));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinNumberOfReplicas() {
		return minNumberOfReplicas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinNumberOfReplicas(int newMinNumberOfReplicas) {
		int oldMinNumberOfReplicas = minNumberOfReplicas;
		minNumberOfReplicas = newMinNumberOfReplicas;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.REPLICATION__MIN_NUMBER_OF_REPLICAS, oldMinNumberOfReplicas, minNumberOfReplicas));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property getBase_Property() {
		if (base_Property != null && base_Property.eIsProxy()) {
			InternalEObject oldBase_Property = (InternalEObject)base_Property;
			base_Property = (Property)eResolveProxy(oldBase_Property);
			if (base_Property != oldBase_Property) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsPackage.REPLICATION__BASE_PROPERTY, oldBase_Property, base_Property));
			}
		}
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property basicGetBase_Property() {
		return base_Property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Property(Property newBase_Property) {
		Property oldBase_Property = base_Property;
		base_Property = newBase_Property;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.REPLICATION__BASE_PROPERTY, oldBase_Property, base_Property));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsPackage.REPLICATION__BASE_CONSTRAINT:
				if (resolve) return getBase_Constraint();
				return basicGetBase_Constraint();
			case DesignPatternsPackage.REPLICATION__KIND:
				return getKind();
			case DesignPatternsPackage.REPLICATION__INITIAL_NUMBER_OF_REPLICAS:
				return getInitialNumberOfReplicas();
			case DesignPatternsPackage.REPLICATION__MIN_NUMBER_OF_REPLICAS:
				return getMinNumberOfReplicas();
			case DesignPatternsPackage.REPLICATION__BASE_PROPERTY:
				if (resolve) return getBase_Property();
				return basicGetBase_Property();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsPackage.REPLICATION__BASE_CONSTRAINT:
				setBase_Constraint((Constraint)newValue);
				return;
			case DesignPatternsPackage.REPLICATION__KIND:
				setKind((ReplicationKind)newValue);
				return;
			case DesignPatternsPackage.REPLICATION__INITIAL_NUMBER_OF_REPLICAS:
				setInitialNumberOfReplicas((Integer)newValue);
				return;
			case DesignPatternsPackage.REPLICATION__MIN_NUMBER_OF_REPLICAS:
				setMinNumberOfReplicas((Integer)newValue);
				return;
			case DesignPatternsPackage.REPLICATION__BASE_PROPERTY:
				setBase_Property((Property)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.REPLICATION__BASE_CONSTRAINT:
				setBase_Constraint((Constraint)null);
				return;
			case DesignPatternsPackage.REPLICATION__KIND:
				setKind(KIND_EDEFAULT);
				return;
			case DesignPatternsPackage.REPLICATION__INITIAL_NUMBER_OF_REPLICAS:
				setInitialNumberOfReplicas(INITIAL_NUMBER_OF_REPLICAS_EDEFAULT);
				return;
			case DesignPatternsPackage.REPLICATION__MIN_NUMBER_OF_REPLICAS:
				setMinNumberOfReplicas(MIN_NUMBER_OF_REPLICAS_EDEFAULT);
				return;
			case DesignPatternsPackage.REPLICATION__BASE_PROPERTY:
				setBase_Property((Property)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.REPLICATION__BASE_CONSTRAINT:
				return base_Constraint != null;
			case DesignPatternsPackage.REPLICATION__KIND:
				return kind != KIND_EDEFAULT;
			case DesignPatternsPackage.REPLICATION__INITIAL_NUMBER_OF_REPLICAS:
				return initialNumberOfReplicas != INITIAL_NUMBER_OF_REPLICAS_EDEFAULT;
			case DesignPatternsPackage.REPLICATION__MIN_NUMBER_OF_REPLICAS:
				return minNumberOfReplicas != MIN_NUMBER_OF_REPLICAS_EDEFAULT;
			case DesignPatternsPackage.REPLICATION__BASE_PROPERTY:
				return base_Property != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (kind: "); //$NON-NLS-1$
		result.append(kind);
		result.append(", initialNumberOfReplicas: "); //$NON-NLS-1$
		result.append(initialNumberOfReplicas);
		result.append(", minNumberOfReplicas: "); //$NON-NLS-1$
		result.append(minNumberOfReplicas);
		result.append(')');
		return result.toString();
	}

} //ReplicationImpl

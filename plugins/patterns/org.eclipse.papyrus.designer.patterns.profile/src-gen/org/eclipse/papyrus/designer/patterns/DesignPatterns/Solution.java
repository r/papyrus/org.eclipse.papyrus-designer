/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Solution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getConsequence <em>Consequence</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getSampleCode <em>Sample Code</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getBase_Package <em>Base Package</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getSafetyLevel <em>Safety Level</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getDescription <em>Description</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getSolution()
 * @model
 * @generated
 */
public interface Solution extends EObject {
	/**
	 * Returns the value of the '<em><b>Consequence</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consequence</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consequence</em>' reference list.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getSolution_Consequence()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<Consequence> getConsequence();

	/**
	 * Returns the value of the '<em><b>Sample Code</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sample Code</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sample Code</em>' reference list.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getSolution_SampleCode()
	 * @model transient="true" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	EList<SampleCode> getSampleCode();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' reference.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getSolution_Description()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	SolutionDesc getDescription();

	/**
	 * Returns the value of the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Package</em>' reference.
	 * @see #setBase_Package(org.eclipse.uml2.uml.Package)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getSolution_Base_Package()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getBase_Package();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getBase_Package <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Package</em>' reference.
	 * @see #getBase_Package()
	 * @generated
	 */
	void setBase_Package(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>Safety Level</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Level</em>' attribute.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel
	 * @see #setSafetyLevel(SafetyLevel)
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#getSolution_SafetyLevel()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	SafetyLevel getSafetyLevel();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution#getSafetyLevel <em>Safety Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Safety Level</em>' attribute.
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel
	 * @see #getSafetyLevel()
	 * @generated
	 */
	void setSafetyLevel(SafetyLevel value);

} // Solution

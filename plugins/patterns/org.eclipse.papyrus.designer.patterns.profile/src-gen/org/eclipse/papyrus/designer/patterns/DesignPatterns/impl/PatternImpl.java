/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Context;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.KnownUse;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc;
import org.eclipse.papyrus.designer.patterns.profile.utils.PatternUtils;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getKnownUse <em>Known Use</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getApplicability <em>Applicability</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getRelatedPattern <em>Related Pattern</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getIntent <em>Intent</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getProblem <em>Problem</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getContext <em>Context</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getBase_Package <em>Base Package</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getSolutions <em>Solutions</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getClassification <em>Classification</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl#getLevel <em>Level</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PatternImpl extends MinimalEObjectImpl.Container implements Pattern {
	/**
	 * The cached value of the '{@link #getRelatedPattern() <em>Related Pattern</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedPattern()
	 * @generated
	 * @ordered
	 */
	protected EList<Pattern> relatedPattern;

	/**
	 * The cached value of the '{@link #getBase_Package() <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Package()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package base_Package;

	/**
	 * The default value of the '{@link #getClassification() <em>Classification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassification()
	 * @generated
	 * @ordered
	 */
	protected static final Classification CLASSIFICATION_EDEFAULT = Classification.CREATIONAL;

	/**
	 * The cached value of the '{@link #getClassification() <em>Classification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassification()
	 * @generated
	 * @ordered
	 */
	protected Classification classification = CLASSIFICATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected static final ReferenceLevel LEVEL_EDEFAULT = ReferenceLevel.SYSTEM;

	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected ReferenceLevel level = LEVEL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsPackage.Literals.PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated 
	 */
	public EList<KnownUse> getKnownUse() {
		return PatternUtils.getOwnedCommentsWithStereotype(getBase_Package(), KnownUse.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Applicability getApplicability() {
		Applicability applicability = basicGetApplicability();
		return applicability != null && applicability.eIsProxy() ? (Applicability)eResolveProxy((InternalEObject)applicability) : applicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Applicability basicGetApplicability() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), Applicability.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Pattern> getRelatedPattern() {
		if (relatedPattern == null) {
			relatedPattern = new EObjectResolvingEList<Pattern>(Pattern.class, this, DesignPatternsPackage.PATTERN__RELATED_PATTERN);
		}
		return relatedPattern;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Intent getIntent() {
		Intent intent = basicGetIntent();
		return intent != null && intent.eIsProxy() ? (Intent)eResolveProxy((InternalEObject)intent) : intent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Intent basicGetIntent() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), Intent.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Problem getProblem() {
		Problem problem = basicGetProblem();
		return problem != null && problem.eIsProxy() ? (Problem)eResolveProxy((InternalEObject)problem) : problem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Problem basicGetProblem() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), Problem.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Context getContext() {
		Context context = basicGetContext();
		return context != null && context.eIsProxy() ? (Context)eResolveProxy((InternalEObject)context) : context;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Context basicGetContext() {
		return PatternUtils.getPackagedElementWithStereotype(getBase_Package(), Context.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package getBase_Package() {
		if (base_Package != null && base_Package.eIsProxy()) {
			InternalEObject oldBase_Package = (InternalEObject)base_Package;
			base_Package = (org.eclipse.uml2.uml.Package)eResolveProxy(oldBase_Package);
			if (base_Package != oldBase_Package) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsPackage.PATTERN__BASE_PACKAGE, oldBase_Package, base_Package));
			}
		}
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetBase_Package() {
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Package(org.eclipse.uml2.uml.Package newBase_Package) {
		org.eclipse.uml2.uml.Package oldBase_Package = base_Package;
		base_Package = newBase_Package;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.PATTERN__BASE_PACKAGE, oldBase_Package, base_Package));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Solution> getSolutions() {
		return PatternUtils.getPackagedElementsWithStereotype(getBase_Package(), Solution.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Classification getClassification() {
		return classification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClassification(Classification newClassification) {
		Classification oldClassification = classification;
		classification = newClassification == null ? CLASSIFICATION_EDEFAULT : newClassification;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.PATTERN__CLASSIFICATION, oldClassification, classification));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SolutionDesc basicGetSolutiondesc() {
		return PatternUtils.getOwnedCommentWithStereotype(getBase_Package(), SolutionDesc.class);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceLevel getLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevel(ReferenceLevel newLevel) {
		ReferenceLevel oldLevel = level;
		level = newLevel == null ? LEVEL_EDEFAULT : newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.PATTERN__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsPackage.PATTERN__KNOWN_USE:
				return getKnownUse();
			case DesignPatternsPackage.PATTERN__APPLICABILITY:
				if (resolve) return getApplicability();
				return basicGetApplicability();
			case DesignPatternsPackage.PATTERN__RELATED_PATTERN:
				return getRelatedPattern();
			case DesignPatternsPackage.PATTERN__INTENT:
				if (resolve) return getIntent();
				return basicGetIntent();
			case DesignPatternsPackage.PATTERN__PROBLEM:
				if (resolve) return getProblem();
				return basicGetProblem();
			case DesignPatternsPackage.PATTERN__CONTEXT:
				if (resolve) return getContext();
				return basicGetContext();
			case DesignPatternsPackage.PATTERN__BASE_PACKAGE:
				if (resolve) return getBase_Package();
				return basicGetBase_Package();
			case DesignPatternsPackage.PATTERN__SOLUTIONS:
				return getSolutions();
			case DesignPatternsPackage.PATTERN__CLASSIFICATION:
				return getClassification();
			case DesignPatternsPackage.PATTERN__LEVEL:
				return getLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsPackage.PATTERN__KNOWN_USE:
				getKnownUse().clear();
				getKnownUse().addAll((Collection<? extends KnownUse>)newValue);
				return;
			case DesignPatternsPackage.PATTERN__RELATED_PATTERN:
				getRelatedPattern().clear();
				getRelatedPattern().addAll((Collection<? extends Pattern>)newValue);
				return;
			case DesignPatternsPackage.PATTERN__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)newValue);
				return;
			case DesignPatternsPackage.PATTERN__CLASSIFICATION:
				setClassification((Classification)newValue);
				return;
			case DesignPatternsPackage.PATTERN__LEVEL:
				setLevel((ReferenceLevel)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.PATTERN__KNOWN_USE:
				getKnownUse().clear();
				return;
			case DesignPatternsPackage.PATTERN__RELATED_PATTERN:
				getRelatedPattern().clear();
				return;
			case DesignPatternsPackage.PATTERN__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)null);
				return;
			case DesignPatternsPackage.PATTERN__CLASSIFICATION:
				setClassification(CLASSIFICATION_EDEFAULT);
				return;
			case DesignPatternsPackage.PATTERN__LEVEL:
				setLevel(LEVEL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.PATTERN__KNOWN_USE:
				return !getKnownUse().isEmpty();
			case DesignPatternsPackage.PATTERN__APPLICABILITY:
				return basicGetApplicability() != null;
			case DesignPatternsPackage.PATTERN__RELATED_PATTERN:
				return relatedPattern != null && !relatedPattern.isEmpty();
			case DesignPatternsPackage.PATTERN__INTENT:
				return basicGetIntent() != null;
			case DesignPatternsPackage.PATTERN__PROBLEM:
				return basicGetProblem() != null;
			case DesignPatternsPackage.PATTERN__CONTEXT:
				return basicGetContext() != null;
			case DesignPatternsPackage.PATTERN__BASE_PACKAGE:
				return base_Package != null;
			case DesignPatternsPackage.PATTERN__SOLUTIONS:
				return !getSolutions().isEmpty();
			case DesignPatternsPackage.PATTERN__CLASSIFICATION:
				return classification != CLASSIFICATION_EDEFAULT;
			case DesignPatternsPackage.PATTERN__LEVEL:
				return level != LEVEL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (classification: "); //$NON-NLS-1$
		result.append(classification);
		result.append(", level: "); //$NON-NLS-1$
		result.append(level);
		result.append(')');
		return result.toString();
	}

} //PatternImpl

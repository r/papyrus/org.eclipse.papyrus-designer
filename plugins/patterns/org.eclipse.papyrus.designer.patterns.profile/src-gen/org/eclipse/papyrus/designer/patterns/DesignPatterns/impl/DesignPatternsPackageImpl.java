/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.AutomaticBinding;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Classification;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Consequence;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.ConsequenceKind;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Context;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsFactory;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Intent;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.KnownUse;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternApplication;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.PatternSystem;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Problem;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.ProblemKind;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.ReferenceLevel;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Replication;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.ReplicationKind;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SafetyLevel;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SampleCode;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Solution;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.SolutionDesc;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DesignPatternsPackageImpl extends EPackageImpl implements DesignPatternsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applicabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass solutionDescEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass problemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass problemKindEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass solutionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass consequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sampleCodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass patternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass knownUseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass replicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass automaticBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass patternApplicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass patternSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum consequenceKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum safetyLevelEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum classificationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum referenceLevelEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum replicationKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DesignPatternsPackageImpl() {
		super(eNS_URI, DesignPatternsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link DesignPatternsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DesignPatternsPackage init() {
		if (isInited) return (DesignPatternsPackage)EPackage.Registry.INSTANCE.getEPackage(DesignPatternsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredDesignPatternsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		DesignPatternsPackageImpl theDesignPatternsPackage = registeredDesignPatternsPackage instanceof DesignPatternsPackageImpl ? (DesignPatternsPackageImpl)registeredDesignPatternsPackage : new DesignPatternsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theDesignPatternsPackage.createPackageContents();

		// Initialize created meta-data
		theDesignPatternsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDesignPatternsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DesignPatternsPackage.eNS_URI, theDesignPatternsPackage);
		return theDesignPatternsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContext() {
		return contextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContext_Applicability() {
		return (EReference)contextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContext_Base_Class() {
		return (EReference)contextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getContext_Precondition() {
		return (EAttribute)contextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getContext_Motivation() {
		return (EAttribute)contextEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplicability() {
		return applicabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getApplicability_Base_Comment() {
		return (EReference)applicabilityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntent() {
		return intentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntent_Description() {
		return (EReference)intentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIntent_Base_Comment() {
		return (EReference)intentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSolutionDesc() {
		return solutionDescEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolutionDesc_Base_Comment() {
		return (EReference)solutionDescEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProblem() {
		return problemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProblem_Kind() {
		return (EReference)problemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProblem_Base_Comment() {
		return (EReference)problemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProblemKind() {
		return problemKindEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProblemKind_Base_Class() {
		return (EReference)problemKindEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSolution() {
		return solutionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolution_Consequence() {
		return (EReference)solutionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolution_SampleCode() {
		return (EReference)solutionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolution_Description() {
		return (EReference)solutionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSolution_Base_Package() {
		return (EReference)solutionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSolution_SafetyLevel() {
		return (EAttribute)solutionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConsequence() {
		return consequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConsequence_ConseqKind() {
		return (EAttribute)consequenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConsequence_Base_Comment() {
		return (EReference)consequenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSampleCode() {
		return sampleCodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSampleCode_Base_Class() {
		return (EReference)sampleCodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPattern() {
		return patternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_KnownUse() {
		return (EReference)patternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_Applicability() {
		return (EReference)patternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_RelatedPattern() {
		return (EReference)patternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_Intent() {
		return (EReference)patternEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_Problem() {
		return (EReference)patternEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_Context() {
		return (EReference)patternEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_Base_Package() {
		return (EReference)patternEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPattern_Solutions() {
		return (EReference)patternEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPattern_Classification() {
		return (EAttribute)patternEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPattern_Level() {
		return (EAttribute)patternEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getKnownUse() {
		return knownUseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getKnownUse_Base_Comment() {
		return (EReference)knownUseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReplication() {
		return replicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReplication_Base_Constraint() {
		return (EReference)replicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReplication_Kind() {
		return (EAttribute)replicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReplication_InitialNumberOfReplicas() {
		return (EAttribute)replicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReplication_MinNumberOfReplicas() {
		return (EAttribute)replicationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getReplication_Base_Property() {
		return (EReference)replicationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAutomaticBinding() {
		return automaticBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAutomaticBinding_Base_Property() {
		return (EReference)automaticBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPatternApplication() {
		return patternApplicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternApplication_Base_CollaborationUse() {
		return (EReference)patternApplicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPatternApplication_Date() {
		return (EAttribute)patternApplicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPatternApplication_User() {
		return (EAttribute)patternApplicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternApplication_Base_Class() {
		return (EReference)patternApplicationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPatternSystem() {
		return patternSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPatternSystem_Base_Package() {
		return (EReference)patternSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConsequenceKind() {
		return consequenceKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSafetyLevel() {
		return safetyLevelEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getClassification() {
		return classificationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getReferenceLevel() {
		return referenceLevelEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getReplicationKind() {
		return replicationKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DesignPatternsFactory getDesignPatternsFactory() {
		return (DesignPatternsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		contextEClass = createEClass(CONTEXT);
		createEReference(contextEClass, CONTEXT__APPLICABILITY);
		createEReference(contextEClass, CONTEXT__BASE_CLASS);
		createEAttribute(contextEClass, CONTEXT__PRECONDITION);
		createEAttribute(contextEClass, CONTEXT__MOTIVATION);

		applicabilityEClass = createEClass(APPLICABILITY);
		createEReference(applicabilityEClass, APPLICABILITY__BASE_COMMENT);

		intentEClass = createEClass(INTENT);
		createEReference(intentEClass, INTENT__DESCRIPTION);
		createEReference(intentEClass, INTENT__BASE_COMMENT);

		solutionDescEClass = createEClass(SOLUTION_DESC);
		createEReference(solutionDescEClass, SOLUTION_DESC__BASE_COMMENT);

		problemEClass = createEClass(PROBLEM);
		createEReference(problemEClass, PROBLEM__KIND);
		createEReference(problemEClass, PROBLEM__BASE_COMMENT);

		problemKindEClass = createEClass(PROBLEM_KIND);
		createEReference(problemKindEClass, PROBLEM_KIND__BASE_CLASS);

		solutionEClass = createEClass(SOLUTION);
		createEReference(solutionEClass, SOLUTION__CONSEQUENCE);
		createEReference(solutionEClass, SOLUTION__SAMPLE_CODE);
		createEReference(solutionEClass, SOLUTION__BASE_PACKAGE);
		createEAttribute(solutionEClass, SOLUTION__SAFETY_LEVEL);
		createEReference(solutionEClass, SOLUTION__DESCRIPTION);

		consequenceEClass = createEClass(CONSEQUENCE);
		createEAttribute(consequenceEClass, CONSEQUENCE__CONSEQ_KIND);
		createEReference(consequenceEClass, CONSEQUENCE__BASE_COMMENT);

		sampleCodeEClass = createEClass(SAMPLE_CODE);
		createEReference(sampleCodeEClass, SAMPLE_CODE__BASE_CLASS);

		patternEClass = createEClass(PATTERN);
		createEReference(patternEClass, PATTERN__KNOWN_USE);
		createEReference(patternEClass, PATTERN__APPLICABILITY);
		createEReference(patternEClass, PATTERN__RELATED_PATTERN);
		createEReference(patternEClass, PATTERN__INTENT);
		createEReference(patternEClass, PATTERN__PROBLEM);
		createEReference(patternEClass, PATTERN__CONTEXT);
		createEReference(patternEClass, PATTERN__BASE_PACKAGE);
		createEReference(patternEClass, PATTERN__SOLUTIONS);
		createEAttribute(patternEClass, PATTERN__CLASSIFICATION);
		createEAttribute(patternEClass, PATTERN__LEVEL);

		knownUseEClass = createEClass(KNOWN_USE);
		createEReference(knownUseEClass, KNOWN_USE__BASE_COMMENT);

		replicationEClass = createEClass(REPLICATION);
		createEReference(replicationEClass, REPLICATION__BASE_CONSTRAINT);
		createEAttribute(replicationEClass, REPLICATION__KIND);
		createEAttribute(replicationEClass, REPLICATION__INITIAL_NUMBER_OF_REPLICAS);
		createEAttribute(replicationEClass, REPLICATION__MIN_NUMBER_OF_REPLICAS);
		createEReference(replicationEClass, REPLICATION__BASE_PROPERTY);

		automaticBindingEClass = createEClass(AUTOMATIC_BINDING);
		createEReference(automaticBindingEClass, AUTOMATIC_BINDING__BASE_PROPERTY);

		patternApplicationEClass = createEClass(PATTERN_APPLICATION);
		createEReference(patternApplicationEClass, PATTERN_APPLICATION__BASE_COLLABORATION_USE);
		createEAttribute(patternApplicationEClass, PATTERN_APPLICATION__DATE);
		createEAttribute(patternApplicationEClass, PATTERN_APPLICATION__USER);
		createEReference(patternApplicationEClass, PATTERN_APPLICATION__BASE_CLASS);

		patternSystemEClass = createEClass(PATTERN_SYSTEM);
		createEReference(patternSystemEClass, PATTERN_SYSTEM__BASE_PACKAGE);

		// Create enums
		consequenceKindEEnum = createEEnum(CONSEQUENCE_KIND);
		safetyLevelEEnum = createEEnum(SAFETY_LEVEL);
		classificationEEnum = createEEnum(CLASSIFICATION);
		referenceLevelEEnum = createEEnum(REFERENCE_LEVEL);
		replicationKindEEnum = createEEnum(REPLICATION_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);
		TypesPackage theTypesPackage = (TypesPackage)EPackage.Registry.INSTANCE.getEPackage(TypesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(contextEClass, Context.class, "Context", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getContext_Applicability(), this.getApplicability(), null, "applicability", null, 1, 1, Context.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getContext_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, Context.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getContext_Precondition(), theTypesPackage.getString(), "precondition", null, 0, -1, Context.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getContext_Motivation(), theTypesPackage.getString(), "motivation", null, 1, 1, Context.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(applicabilityEClass, Applicability.class, "Applicability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getApplicability_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, Applicability.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(intentEClass, Intent.class, "Intent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getIntent_Description(), this.getSolutionDesc(), null, "description", null, 1, 1, Intent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getIntent_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, Intent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(solutionDescEClass, SolutionDesc.class, "SolutionDesc", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSolutionDesc_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, SolutionDesc.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(problemEClass, Problem.class, "Problem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getProblem_Kind(), this.getProblemKind(), null, "kind", null, 1, 1, Problem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getProblem_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, Problem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(problemKindEClass, ProblemKind.class, "ProblemKind", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getProblemKind_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, ProblemKind.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(solutionEClass, Solution.class, "Solution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSolution_Consequence(), this.getConsequence(), null, "consequence", null, 0, -1, Solution.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSolution_SampleCode(), this.getSampleCode(), null, "sampleCode", null, 0, -1, Solution.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSolution_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, Solution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getSolution_SafetyLevel(), this.getSafetyLevel(), "safetyLevel", null, 1, 1, Solution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getSolution_Description(), this.getSolutionDesc(), null, "description", null, 1, 1, Solution.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(consequenceEClass, Consequence.class, "Consequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(getConsequence_ConseqKind(), this.getConsequenceKind(), "conseqKind", null, 1, 1, Consequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getConsequence_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, Consequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(sampleCodeEClass, SampleCode.class, "SampleCode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getSampleCode_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, SampleCode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(patternEClass, Pattern.class, "Pattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getPattern_KnownUse(), this.getKnownUse(), null, "knownUse", null, 0, -1, Pattern.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPattern_Applicability(), this.getApplicability(), null, "applicability", null, 1, 1, Pattern.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPattern_RelatedPattern(), this.getPattern(), null, "relatedPattern", null, 0, -1, Pattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPattern_Intent(), this.getIntent(), null, "intent", null, 1, 1, Pattern.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPattern_Problem(), this.getProblem(), null, "problem", null, 1, 1, Pattern.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPattern_Context(), this.getContext(), null, "context", null, 1, 1, Pattern.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPattern_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, Pattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPattern_Solutions(), this.getSolution(), null, "solutions", null, 0, -1, Pattern.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getPattern_Classification(), this.getClassification(), "classification", null, 1, 1, Pattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getPattern_Level(), this.getReferenceLevel(), "level", null, 1, 1, Pattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(knownUseEClass, KnownUse.class, "KnownUse", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getKnownUse_Base_Comment(), theUMLPackage.getComment(), null, "base_Comment", null, 1, 1, KnownUse.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(replicationEClass, Replication.class, "Replication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getReplication_Base_Constraint(), theUMLPackage.getConstraint(), null, "base_Constraint", null, 1, 1, Replication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getReplication_Kind(), this.getReplicationKind(), "kind", null, 1, 1, Replication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getReplication_InitialNumberOfReplicas(), theTypesPackage.getInteger(), "initialNumberOfReplicas", null, 1, 1, Replication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getReplication_MinNumberOfReplicas(), theTypesPackage.getInteger(), "minNumberOfReplicas", null, 1, 1, Replication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getReplication_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, Replication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(automaticBindingEClass, AutomaticBinding.class, "AutomaticBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getAutomaticBinding_Base_Property(), theUMLPackage.getProperty(), null, "base_Property", null, 1, 1, AutomaticBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(patternApplicationEClass, PatternApplication.class, "PatternApplication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getPatternApplication_Base_CollaborationUse(), theUMLPackage.getCollaborationUse(), null, "base_CollaborationUse", null, 1, 1, PatternApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getPatternApplication_Date(), theTypesPackage.getString(), "date", null, 1, 1, PatternApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEAttribute(getPatternApplication_User(), theTypesPackage.getString(), "user", null, 1, 1, PatternApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$
		initEReference(getPatternApplication_Base_Class(), theUMLPackage.getClass_(), null, "base_Class", null, 1, 1, PatternApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		initEClass(patternSystemEClass, PatternSystem.class, "PatternSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(getPatternSystem_Base_Package(), theUMLPackage.getPackage(), null, "base_Package", null, 1, 1, PatternSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$

		// Initialize enums and add enum literals
		initEEnum(consequenceKindEEnum, ConsequenceKind.class, "ConsequenceKind"); //$NON-NLS-1$
		addEEnumLiteral(consequenceKindEEnum, ConsequenceKind.RESULT);
		addEEnumLiteral(consequenceKindEEnum, ConsequenceKind.TRADEOFF);
		addEEnumLiteral(consequenceKindEEnum, ConsequenceKind.IMPACT_ON_FLEXIBILITY);
		addEEnumLiteral(consequenceKindEEnum, ConsequenceKind.IMPACT_ON_EXTENSIBILITY);
		addEEnumLiteral(consequenceKindEEnum, ConsequenceKind.IMPACT_ON_PORTABILITY);

		initEEnum(safetyLevelEEnum, SafetyLevel.class, "SafetyLevel"); //$NON-NLS-1$
		addEEnumLiteral(safetyLevelEEnum, SafetyLevel.SIL0);
		addEEnumLiteral(safetyLevelEEnum, SafetyLevel.SIL1);
		addEEnumLiteral(safetyLevelEEnum, SafetyLevel.SIL2);
		addEEnumLiteral(safetyLevelEEnum, SafetyLevel.SIL3);
		addEEnumLiteral(safetyLevelEEnum, SafetyLevel.SIL4);

		initEEnum(classificationEEnum, Classification.class, "Classification"); //$NON-NLS-1$
		addEEnumLiteral(classificationEEnum, Classification.CREATIONAL);
		addEEnumLiteral(classificationEEnum, Classification.STRUCTURAL);
		addEEnumLiteral(classificationEEnum, Classification.BEHAVIORAL);

		initEEnum(referenceLevelEEnum, ReferenceLevel.class, "ReferenceLevel"); //$NON-NLS-1$
		addEEnumLiteral(referenceLevelEEnum, ReferenceLevel.SYSTEM);
		addEEnumLiteral(referenceLevelEEnum, ReferenceLevel.ARCHITECTURE);
		addEEnumLiteral(referenceLevelEEnum, ReferenceLevel.DESIGN);
		addEEnumLiteral(referenceLevelEEnum, ReferenceLevel.IMPLEMENTATION);

		initEEnum(replicationKindEEnum, ReplicationKind.class, "ReplicationKind"); //$NON-NLS-1$
		addEEnumLiteral(replicationKindEEnum, ReplicationKind.IDENTICAL_REPLICATION);
		addEEnumLiteral(replicationKindEEnum, ReplicationKind.DIVERSIFIED_REPLICATION);

		// Create resource
		createResource(eNS_URI);
	}

} //DesignPatternsPackageImpl

/**
 */
package org.eclipse.papyrus.designer.patterns.DesignPatterns.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.Applicability;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.Context;
import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl#getApplicability <em>Applicability</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl#getBase_Class <em>Base Class</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl#getPrecondition <em>Precondition</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.ContextImpl#getMotivation <em>Motivation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContextImpl extends MinimalEObjectImpl.Container implements Context {
	/**
	 * The cached value of the '{@link #getApplicability() <em>Applicability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getApplicability()
	 * @generated
	 * @ordered
	 */
	protected Applicability applicability;

	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * The cached value of the '{@link #getPrecondition() <em>Precondition</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrecondition()
	 * @generated
	 * @ordered
	 */
	protected EList<String> precondition;

	/**
	 * The default value of the '{@link #getMotivation() <em>Motivation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotivation()
	 * @generated
	 * @ordered
	 */
	protected static final String MOTIVATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMotivation() <em>Motivation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMotivation()
	 * @generated
	 * @ordered
	 */
	protected String motivation = MOTIVATION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DesignPatternsPackage.Literals.CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Applicability getApplicability() {
		if (applicability != null && applicability.eIsProxy()) {
			InternalEObject oldApplicability = (InternalEObject)applicability;
			applicability = (Applicability)eResolveProxy(oldApplicability);
			if (applicability != oldApplicability) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsPackage.CONTEXT__APPLICABILITY, oldApplicability, applicability));
			}
		}
		return applicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Applicability basicGetApplicability() {
		return applicability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplicability(Applicability newApplicability) {
		Applicability oldApplicability = applicability;
		applicability = newApplicability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.CONTEXT__APPLICABILITY, oldApplicability, applicability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject)base_Class;
			base_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DesignPatternsPackage.CONTEXT__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.CONTEXT__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getPrecondition() {
		if (precondition == null) {
			precondition = new EDataTypeUniqueEList<String>(String.class, this, DesignPatternsPackage.CONTEXT__PRECONDITION);
		}
		return precondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMotivation() {
		return motivation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMotivation(String newMotivation) {
		String oldMotivation = motivation;
		motivation = newMotivation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DesignPatternsPackage.CONTEXT__MOTIVATION, oldMotivation, motivation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DesignPatternsPackage.CONTEXT__APPLICABILITY:
				if (resolve) return getApplicability();
				return basicGetApplicability();
			case DesignPatternsPackage.CONTEXT__BASE_CLASS:
				if (resolve) return getBase_Class();
				return basicGetBase_Class();
			case DesignPatternsPackage.CONTEXT__PRECONDITION:
				return getPrecondition();
			case DesignPatternsPackage.CONTEXT__MOTIVATION:
				return getMotivation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DesignPatternsPackage.CONTEXT__APPLICABILITY:
				setApplicability((Applicability)newValue);
				return;
			case DesignPatternsPackage.CONTEXT__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
			case DesignPatternsPackage.CONTEXT__PRECONDITION:
				getPrecondition().clear();
				getPrecondition().addAll((Collection<? extends String>)newValue);
				return;
			case DesignPatternsPackage.CONTEXT__MOTIVATION:
				setMotivation((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.CONTEXT__APPLICABILITY:
				setApplicability((Applicability)null);
				return;
			case DesignPatternsPackage.CONTEXT__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)null);
				return;
			case DesignPatternsPackage.CONTEXT__PRECONDITION:
				getPrecondition().clear();
				return;
			case DesignPatternsPackage.CONTEXT__MOTIVATION:
				setMotivation(MOTIVATION_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DesignPatternsPackage.CONTEXT__APPLICABILITY:
				return applicability != null;
			case DesignPatternsPackage.CONTEXT__BASE_CLASS:
				return base_Class != null;
			case DesignPatternsPackage.CONTEXT__PRECONDITION:
				return precondition != null && !precondition.isEmpty();
			case DesignPatternsPackage.CONTEXT__MOTIVATION:
				return MOTIVATION_EDEFAULT == null ? motivation != null : !MOTIVATION_EDEFAULT.equals(motivation);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (precondition: "); //$NON-NLS-1$
		result.append(precondition);
		result.append(", motivation: "); //$NON-NLS-1$
		result.append(motivation);
		result.append(')');
		return result.toString();
	}

} //ContextImpl

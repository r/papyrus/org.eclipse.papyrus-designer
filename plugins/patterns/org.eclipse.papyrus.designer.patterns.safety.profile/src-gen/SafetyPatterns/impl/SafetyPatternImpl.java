/**
 */
package SafetyPatterns.impl;

import SafetyPatterns.SafetyPattern;
import SafetyPatterns.SafetyPatternsPackage;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.impl.PatternImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Safety Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link SafetyPatterns.impl.SafetyPatternImpl#getSafetyProperties <em>Safety Properties</em>}</li>
 *   <li>{@link SafetyPatterns.impl.SafetyPatternImpl#getResourceProperties <em>Resource Properties</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SafetyPatternImpl extends PatternImpl implements SafetyPattern {
	/**
	 * The cached value of the '{@link #getSafetyProperties() <em>Safety Properties</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSafetyProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<String> safetyProperties;

	/**
	 * The cached value of the '{@link #getResourceProperties() <em>Resource Properties</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceProperties()
	 * @generated
	 * @ordered
	 */
	protected EList<String> resourceProperties;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SafetyPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SafetyPatternsPackage.Literals.SAFETY_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSafetyProperties() {
		if (safetyProperties == null) {
			safetyProperties = new EDataTypeUniqueEList<String>(String.class, this, SafetyPatternsPackage.SAFETY_PATTERN__SAFETY_PROPERTIES);
		}
		return safetyProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getResourceProperties() {
		if (resourceProperties == null) {
			resourceProperties = new EDataTypeUniqueEList<String>(String.class, this, SafetyPatternsPackage.SAFETY_PATTERN__RESOURCE_PROPERTIES);
		}
		return resourceProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SafetyPatternsPackage.SAFETY_PATTERN__SAFETY_PROPERTIES:
				return getSafetyProperties();
			case SafetyPatternsPackage.SAFETY_PATTERN__RESOURCE_PROPERTIES:
				return getResourceProperties();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SafetyPatternsPackage.SAFETY_PATTERN__SAFETY_PROPERTIES:
				getSafetyProperties().clear();
				getSafetyProperties().addAll((Collection<? extends String>)newValue);
				return;
			case SafetyPatternsPackage.SAFETY_PATTERN__RESOURCE_PROPERTIES:
				getResourceProperties().clear();
				getResourceProperties().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SafetyPatternsPackage.SAFETY_PATTERN__SAFETY_PROPERTIES:
				getSafetyProperties().clear();
				return;
			case SafetyPatternsPackage.SAFETY_PATTERN__RESOURCE_PROPERTIES:
				getResourceProperties().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SafetyPatternsPackage.SAFETY_PATTERN__SAFETY_PROPERTIES:
				return safetyProperties != null && !safetyProperties.isEmpty();
			case SafetyPatternsPackage.SAFETY_PATTERN__RESOURCE_PROPERTIES:
				return resourceProperties != null && !resourceProperties.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (safetyProperties: "); //$NON-NLS-1$
		result.append(safetyProperties);
		result.append(", resourceProperties: "); //$NON-NLS-1$
		result.append(resourceProperties);
		result.append(')');
		return result.toString();
	}

} //SafetyPatternImpl

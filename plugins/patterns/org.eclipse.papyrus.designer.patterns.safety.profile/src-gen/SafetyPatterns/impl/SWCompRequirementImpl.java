/**
 */
package SafetyPatterns.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import SafetyPatterns.SWCompRequirement;
import SafetyPatterns.SafetyPatternsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SW Comp Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link SafetyPatterns.impl.SWCompRequirementImpl#getPeriod <em>Period</em>}</li>
 *   <li>{@link SafetyPatterns.impl.SWCompRequirementImpl#getAvailability <em>Availability</em>}</li>
 *   <li>{@link SafetyPatterns.impl.SWCompRequirementImpl#getErrorRate <em>Error Rate</em>}</li>
 *   <li>{@link SafetyPatterns.impl.SWCompRequirementImpl#getBase_Class <em>Base Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SWCompRequirementImpl extends MinimalEObjectImpl.Container implements SWCompRequirement {
	/**
	 * The default value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final String PERIOD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected String period = PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getAvailability() <em>Availability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailability()
	 * @generated
	 * @ordered
	 */
	protected static final String AVAILABILITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAvailability() <em>Availability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvailability()
	 * @generated
	 * @ordered
	 */
	protected String availability = AVAILABILITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getErrorRate() <em>Error Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorRate()
	 * @generated
	 * @ordered
	 */
	protected static final String ERROR_RATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getErrorRate() <em>Error Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getErrorRate()
	 * @generated
	 * @ordered
	 */
	protected String errorRate = ERROR_RATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBase_Class() <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Class()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Class base_Class;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SWCompRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SafetyPatternsPackage.Literals.SW_COMP_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPeriod() {
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPeriod(String newPeriod) {
		String oldPeriod = period;
		period = newPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SafetyPatternsPackage.SW_COMP_REQUIREMENT__PERIOD, oldPeriod, period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAvailability() {
		return availability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvailability(String newAvailability) {
		String oldAvailability = availability;
		availability = newAvailability;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SafetyPatternsPackage.SW_COMP_REQUIREMENT__AVAILABILITY, oldAvailability, availability));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getErrorRate() {
		return errorRate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setErrorRate(String newErrorRate) {
		String oldErrorRate = errorRate;
		errorRate = newErrorRate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SafetyPatternsPackage.SW_COMP_REQUIREMENT__ERROR_RATE, oldErrorRate, errorRate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class getBase_Class() {
		if (base_Class != null && base_Class.eIsProxy()) {
			InternalEObject oldBase_Class = (InternalEObject)base_Class;
			base_Class = (org.eclipse.uml2.uml.Class)eResolveProxy(oldBase_Class);
			if (base_Class != oldBase_Class) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SafetyPatternsPackage.SW_COMP_REQUIREMENT__BASE_CLASS, oldBase_Class, base_Class));
			}
		}
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Class basicGetBase_Class() {
		return base_Class;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Class(org.eclipse.uml2.uml.Class newBase_Class) {
		org.eclipse.uml2.uml.Class oldBase_Class = base_Class;
		base_Class = newBase_Class;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SafetyPatternsPackage.SW_COMP_REQUIREMENT__BASE_CLASS, oldBase_Class, base_Class));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__PERIOD:
				return getPeriod();
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__AVAILABILITY:
				return getAvailability();
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__ERROR_RATE:
				return getErrorRate();
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__BASE_CLASS:
				if (resolve) return getBase_Class();
				return basicGetBase_Class();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__PERIOD:
				setPeriod((String)newValue);
				return;
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__AVAILABILITY:
				setAvailability((String)newValue);
				return;
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__ERROR_RATE:
				setErrorRate((String)newValue);
				return;
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__PERIOD:
				setPeriod(PERIOD_EDEFAULT);
				return;
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__AVAILABILITY:
				setAvailability(AVAILABILITY_EDEFAULT);
				return;
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__ERROR_RATE:
				setErrorRate(ERROR_RATE_EDEFAULT);
				return;
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__BASE_CLASS:
				setBase_Class((org.eclipse.uml2.uml.Class)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__PERIOD:
				return PERIOD_EDEFAULT == null ? period != null : !PERIOD_EDEFAULT.equals(period);
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__AVAILABILITY:
				return AVAILABILITY_EDEFAULT == null ? availability != null : !AVAILABILITY_EDEFAULT.equals(availability);
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__ERROR_RATE:
				return ERROR_RATE_EDEFAULT == null ? errorRate != null : !ERROR_RATE_EDEFAULT.equals(errorRate);
			case SafetyPatternsPackage.SW_COMP_REQUIREMENT__BASE_CLASS:
				return base_Class != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (period: "); //$NON-NLS-1$
		result.append(period);
		result.append(", availability: "); //$NON-NLS-1$
		result.append(availability);
		result.append(", errorRate: "); //$NON-NLS-1$
		result.append(errorRate);
		result.append(')');
		return result.toString();
	}

} //SWCompRequirementImpl

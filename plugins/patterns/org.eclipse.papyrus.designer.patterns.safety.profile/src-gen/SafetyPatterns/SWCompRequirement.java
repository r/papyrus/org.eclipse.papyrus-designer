/**
 */
package SafetyPatterns;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SW Comp Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SafetyPatterns.SWCompRequirement#getPeriod <em>Period</em>}</li>
 *   <li>{@link SafetyPatterns.SWCompRequirement#getAvailability <em>Availability</em>}</li>
 *   <li>{@link SafetyPatterns.SWCompRequirement#getErrorRate <em>Error Rate</em>}</li>
 *   <li>{@link SafetyPatterns.SWCompRequirement#getBase_Class <em>Base Class</em>}</li>
 * </ul>
 *
 * @see SafetyPatterns.SafetyPatternsPackage#getSWCompRequirement()
 * @model
 * @generated
 */
public interface SWCompRequirement extends EObject {
	/**
	 * Returns the value of the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' attribute.
	 * @see #setPeriod(String)
	 * @see SafetyPatterns.SafetyPatternsPackage#getSWCompRequirement_Period()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getPeriod();

	/**
	 * Sets the value of the '{@link SafetyPatterns.SWCompRequirement#getPeriod <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' attribute.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(String value);

	/**
	 * Returns the value of the '<em><b>Availability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Availability</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Availability</em>' attribute.
	 * @see #setAvailability(String)
	 * @see SafetyPatterns.SafetyPatternsPackage#getSWCompRequirement_Availability()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getAvailability();

	/**
	 * Sets the value of the '{@link SafetyPatterns.SWCompRequirement#getAvailability <em>Availability</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Availability</em>' attribute.
	 * @see #getAvailability()
	 * @generated
	 */
	void setAvailability(String value);

	/**
	 * Returns the value of the '<em><b>Error Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Error Rate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Error Rate</em>' attribute.
	 * @see #setErrorRate(String)
	 * @see SafetyPatterns.SafetyPatternsPackage#getSWCompRequirement_ErrorRate()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getErrorRate();

	/**
	 * Sets the value of the '{@link SafetyPatterns.SWCompRequirement#getErrorRate <em>Error Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Error Rate</em>' attribute.
	 * @see #getErrorRate()
	 * @generated
	 */
	void setErrorRate(String value);

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBase_Class(org.eclipse.uml2.uml.Class)
	 * @see SafetyPatterns.SafetyPatternsPackage#getSWCompRequirement_Base_Class()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Class getBase_Class();

	/**
	 * Sets the value of the '{@link SafetyPatterns.SWCompRequirement#getBase_Class <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBase_Class()
	 * @generated
	 */
	void setBase_Class(org.eclipse.uml2.uml.Class value);

} // SWCompRequirement

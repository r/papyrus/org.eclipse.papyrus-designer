/**
 */
package SafetyPatterns;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see SafetyPatterns.SafetyPatternsPackage
 * @generated
 */
public interface SafetyPatternsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SafetyPatternsFactory eINSTANCE = SafetyPatterns.impl.SafetyPatternsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Safety Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Safety Pattern</em>'.
	 * @generated
	 */
	SafetyPattern createSafetyPattern();

	/**
	 * Returns a new object of class '<em>SW Comp Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SW Comp Requirement</em>'.
	 * @generated
	 */
	SWCompRequirement createSWCompRequirement();

	/**
	 * Returns a new object of class '<em>HW Comp Capability</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HW Comp Capability</em>'.
	 * @generated
	 */
	HWCompCapability createHWCompCapability();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SafetyPatternsPackage getSafetyPatternsPackage();

} //SafetyPatternsFactory

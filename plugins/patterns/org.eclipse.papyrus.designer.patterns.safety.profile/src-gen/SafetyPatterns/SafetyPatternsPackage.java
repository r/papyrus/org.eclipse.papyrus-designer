/**
 */
package SafetyPatterns;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.DesignPatternsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see SafetyPatterns.SafetyPatternsFactory
 * @model kind="package"
 * @generated
 */
public interface SafetyPatternsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "SafetyPatterns"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/SDP/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "SafetyPatterns"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SafetyPatternsPackage eINSTANCE = SafetyPatterns.impl.SafetyPatternsPackageImpl.init();

	/**
	 * The meta object id for the '{@link SafetyPatterns.impl.SafetyPatternImpl <em>Safety Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SafetyPatterns.impl.SafetyPatternImpl
	 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getSafetyPattern()
	 * @generated
	 */
	int SAFETY_PATTERN = 0;

	/**
	 * The feature id for the '<em><b>Known Use</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__KNOWN_USE = DesignPatternsPackage.PATTERN__KNOWN_USE;

	/**
	 * The feature id for the '<em><b>Applicability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__APPLICABILITY = DesignPatternsPackage.PATTERN__APPLICABILITY;

	/**
	 * The feature id for the '<em><b>Related Pattern</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__RELATED_PATTERN = DesignPatternsPackage.PATTERN__RELATED_PATTERN;

	/**
	 * The feature id for the '<em><b>Intent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__INTENT = DesignPatternsPackage.PATTERN__INTENT;

	/**
	 * The feature id for the '<em><b>Problem</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__PROBLEM = DesignPatternsPackage.PATTERN__PROBLEM;

	/**
	 * The feature id for the '<em><b>Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__CONTEXT = DesignPatternsPackage.PATTERN__CONTEXT;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__BASE_PACKAGE = DesignPatternsPackage.PATTERN__BASE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Solutions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__SOLUTIONS = DesignPatternsPackage.PATTERN__SOLUTIONS;

	/**
	 * The feature id for the '<em><b>Classification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__CLASSIFICATION = DesignPatternsPackage.PATTERN__CLASSIFICATION;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__LEVEL = DesignPatternsPackage.PATTERN__LEVEL;

	/**
	 * The feature id for the '<em><b>Safety Properties</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__SAFETY_PROPERTIES = DesignPatternsPackage.PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource Properties</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN__RESOURCE_PROPERTIES = DesignPatternsPackage.PATTERN_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Safety Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN_FEATURE_COUNT = DesignPatternsPackage.PATTERN_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Safety Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SAFETY_PATTERN_OPERATION_COUNT = DesignPatternsPackage.PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link SafetyPatterns.impl.SWCompRequirementImpl <em>SW Comp Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SafetyPatterns.impl.SWCompRequirementImpl
	 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getSWCompRequirement()
	 * @generated
	 */
	int SW_COMP_REQUIREMENT = 1;

	/**
	 * The feature id for the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMP_REQUIREMENT__PERIOD = 0;

	/**
	 * The feature id for the '<em><b>Availability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMP_REQUIREMENT__AVAILABILITY = 1;

	/**
	 * The feature id for the '<em><b>Error Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMP_REQUIREMENT__ERROR_RATE = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMP_REQUIREMENT__BASE_CLASS = 3;

	/**
	 * The number of structural features of the '<em>SW Comp Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMP_REQUIREMENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>SW Comp Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SW_COMP_REQUIREMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link SafetyPatterns.impl.HWCompCapabilityImpl <em>HW Comp Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see SafetyPatterns.impl.HWCompCapabilityImpl
	 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getHWCompCapability()
	 * @generated
	 */
	int HW_COMP_CAPABILITY = 2;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMP_CAPABILITY__BASE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Availability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMP_CAPABILITY__AVAILABILITY = 1;

	/**
	 * The feature id for the '<em><b>Error Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMP_CAPABILITY__ERROR_RATE = 2;

	/**
	 * The feature id for the '<em><b>Reliability</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMP_CAPABILITY__RELIABILITY = 3;

	/**
	 * The number of structural features of the '<em>HW Comp Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMP_CAPABILITY_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>HW Comp Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HW_COMP_CAPABILITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '<em>Safety Properties</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getSafetyProperties()
	 * @generated
	 */
	int SAFETY_PROPERTIES = 3;

	/**
	 * The meta object id for the '<em>Resource Properties</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getResourceProperties()
	 * @generated
	 */
	int RESOURCE_PROPERTIES = 4;

	/**
	 * The meta object id for the '<em>Reliability</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getReliability()
	 * @generated
	 */
	int RELIABILITY = 5;

	/**
	 * The meta object id for the '<em>Availability</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getAvailability()
	 * @generated
	 */
	int AVAILABILITY = 6;


	/**
	 * Returns the meta object for class '{@link SafetyPatterns.SafetyPattern <em>Safety Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Safety Pattern</em>'.
	 * @see SafetyPatterns.SafetyPattern
	 * @generated
	 */
	EClass getSafetyPattern();

	/**
	 * Returns the meta object for the attribute list '{@link SafetyPatterns.SafetyPattern#getSafetyProperties <em>Safety Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Safety Properties</em>'.
	 * @see SafetyPatterns.SafetyPattern#getSafetyProperties()
	 * @see #getSafetyPattern()
	 * @generated
	 */
	EAttribute getSafetyPattern_SafetyProperties();

	/**
	 * Returns the meta object for the attribute list '{@link SafetyPatterns.SafetyPattern#getResourceProperties <em>Resource Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Resource Properties</em>'.
	 * @see SafetyPatterns.SafetyPattern#getResourceProperties()
	 * @see #getSafetyPattern()
	 * @generated
	 */
	EAttribute getSafetyPattern_ResourceProperties();

	/**
	 * Returns the meta object for class '{@link SafetyPatterns.SWCompRequirement <em>SW Comp Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SW Comp Requirement</em>'.
	 * @see SafetyPatterns.SWCompRequirement
	 * @generated
	 */
	EClass getSWCompRequirement();

	/**
	 * Returns the meta object for the attribute '{@link SafetyPatterns.SWCompRequirement#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period</em>'.
	 * @see SafetyPatterns.SWCompRequirement#getPeriod()
	 * @see #getSWCompRequirement()
	 * @generated
	 */
	EAttribute getSWCompRequirement_Period();

	/**
	 * Returns the meta object for the attribute '{@link SafetyPatterns.SWCompRequirement#getAvailability <em>Availability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Availability</em>'.
	 * @see SafetyPatterns.SWCompRequirement#getAvailability()
	 * @see #getSWCompRequirement()
	 * @generated
	 */
	EAttribute getSWCompRequirement_Availability();

	/**
	 * Returns the meta object for the attribute '{@link SafetyPatterns.SWCompRequirement#getErrorRate <em>Error Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Error Rate</em>'.
	 * @see SafetyPatterns.SWCompRequirement#getErrorRate()
	 * @see #getSWCompRequirement()
	 * @generated
	 */
	EAttribute getSWCompRequirement_ErrorRate();

	/**
	 * Returns the meta object for the reference '{@link SafetyPatterns.SWCompRequirement#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see SafetyPatterns.SWCompRequirement#getBase_Class()
	 * @see #getSWCompRequirement()
	 * @generated
	 */
	EReference getSWCompRequirement_Base_Class();

	/**
	 * Returns the meta object for class '{@link SafetyPatterns.HWCompCapability <em>HW Comp Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HW Comp Capability</em>'.
	 * @see SafetyPatterns.HWCompCapability
	 * @generated
	 */
	EClass getHWCompCapability();

	/**
	 * Returns the meta object for the reference '{@link SafetyPatterns.HWCompCapability#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see SafetyPatterns.HWCompCapability#getBase_Class()
	 * @see #getHWCompCapability()
	 * @generated
	 */
	EReference getHWCompCapability_Base_Class();

	/**
	 * Returns the meta object for the attribute '{@link SafetyPatterns.HWCompCapability#getAvailability <em>Availability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Availability</em>'.
	 * @see SafetyPatterns.HWCompCapability#getAvailability()
	 * @see #getHWCompCapability()
	 * @generated
	 */
	EAttribute getHWCompCapability_Availability();

	/**
	 * Returns the meta object for the attribute '{@link SafetyPatterns.HWCompCapability#getErrorRate <em>Error Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Error Rate</em>'.
	 * @see SafetyPatterns.HWCompCapability#getErrorRate()
	 * @see #getHWCompCapability()
	 * @generated
	 */
	EAttribute getHWCompCapability_ErrorRate();

	/**
	 * Returns the meta object for the attribute '{@link SafetyPatterns.HWCompCapability#getReliability <em>Reliability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reliability</em>'.
	 * @see SafetyPatterns.HWCompCapability#getReliability()
	 * @see #getHWCompCapability()
	 * @generated
	 */
	EAttribute getHWCompCapability_Reliability();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Safety Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Safety Properties</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getSafetyProperties();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Resource Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Resource Properties</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getResourceProperties();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Reliability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Reliability</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getReliability();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Availability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Availability</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 * @generated
	 */
	EDataType getAvailability();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SafetyPatternsFactory getSafetyPatternsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link SafetyPatterns.impl.SafetyPatternImpl <em>Safety Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SafetyPatterns.impl.SafetyPatternImpl
		 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getSafetyPattern()
		 * @generated
		 */
		EClass SAFETY_PATTERN = eINSTANCE.getSafetyPattern();

		/**
		 * The meta object literal for the '<em><b>Safety Properties</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAFETY_PATTERN__SAFETY_PROPERTIES = eINSTANCE.getSafetyPattern_SafetyProperties();

		/**
		 * The meta object literal for the '<em><b>Resource Properties</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SAFETY_PATTERN__RESOURCE_PROPERTIES = eINSTANCE.getSafetyPattern_ResourceProperties();

		/**
		 * The meta object literal for the '{@link SafetyPatterns.impl.SWCompRequirementImpl <em>SW Comp Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SafetyPatterns.impl.SWCompRequirementImpl
		 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getSWCompRequirement()
		 * @generated
		 */
		EClass SW_COMP_REQUIREMENT = eINSTANCE.getSWCompRequirement();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SW_COMP_REQUIREMENT__PERIOD = eINSTANCE.getSWCompRequirement_Period();

		/**
		 * The meta object literal for the '<em><b>Availability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SW_COMP_REQUIREMENT__AVAILABILITY = eINSTANCE.getSWCompRequirement_Availability();

		/**
		 * The meta object literal for the '<em><b>Error Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SW_COMP_REQUIREMENT__ERROR_RATE = eINSTANCE.getSWCompRequirement_ErrorRate();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SW_COMP_REQUIREMENT__BASE_CLASS = eINSTANCE.getSWCompRequirement_Base_Class();

		/**
		 * The meta object literal for the '{@link SafetyPatterns.impl.HWCompCapabilityImpl <em>HW Comp Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see SafetyPatterns.impl.HWCompCapabilityImpl
		 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getHWCompCapability()
		 * @generated
		 */
		EClass HW_COMP_CAPABILITY = eINSTANCE.getHWCompCapability();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HW_COMP_CAPABILITY__BASE_CLASS = eINSTANCE.getHWCompCapability_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Availability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW_COMP_CAPABILITY__AVAILABILITY = eINSTANCE.getHWCompCapability_Availability();

		/**
		 * The meta object literal for the '<em><b>Error Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW_COMP_CAPABILITY__ERROR_RATE = eINSTANCE.getHWCompCapability_ErrorRate();

		/**
		 * The meta object literal for the '<em><b>Reliability</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HW_COMP_CAPABILITY__RELIABILITY = eINSTANCE.getHWCompCapability_Reliability();

		/**
		 * The meta object literal for the '<em>Safety Properties</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getSafetyProperties()
		 * @generated
		 */
		EDataType SAFETY_PROPERTIES = eINSTANCE.getSafetyProperties();

		/**
		 * The meta object literal for the '<em>Resource Properties</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getResourceProperties()
		 * @generated
		 */
		EDataType RESOURCE_PROPERTIES = eINSTANCE.getResourceProperties();

		/**
		 * The meta object literal for the '<em>Reliability</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getReliability()
		 * @generated
		 */
		EDataType RELIABILITY = eINSTANCE.getReliability();

		/**
		 * The meta object literal for the '<em>Availability</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see SafetyPatterns.impl.SafetyPatternsPackageImpl#getAvailability()
		 * @generated
		 */
		EDataType AVAILABILITY = eINSTANCE.getAvailability();

	}

} //SafetyPatternsPackage

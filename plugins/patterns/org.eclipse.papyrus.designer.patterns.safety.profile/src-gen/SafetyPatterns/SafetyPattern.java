/**
 */
package SafetyPatterns;

import org.eclipse.emf.common.util.EList;

import org.eclipse.papyrus.designer.patterns.DesignPatterns.Pattern;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Safety Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link SafetyPatterns.SafetyPattern#getSafetyProperties <em>Safety Properties</em>}</li>
 *   <li>{@link SafetyPatterns.SafetyPattern#getResourceProperties <em>Resource Properties</em>}</li>
 * </ul>
 *
 * @see SafetyPatterns.SafetyPatternsPackage#getSafetyPattern()
 * @model
 * @generated
 */
public interface SafetyPattern extends Pattern {
	/**
	 * Returns the value of the '<em><b>Safety Properties</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Safety Properties</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Safety Properties</em>' attribute list.
	 * @see SafetyPatterns.SafetyPatternsPackage#getSafetyPattern_SafetyProperties()
	 * @model dataType="SafetyPatterns.SafetyProperties" ordered="false"
	 * @generated
	 */
	EList<String> getSafetyProperties();

	/**
	 * Returns the value of the '<em><b>Resource Properties</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Properties</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Properties</em>' attribute list.
	 * @see SafetyPatterns.SafetyPatternsPackage#getSafetyPattern_ResourceProperties()
	 * @model dataType="SafetyPatterns.ResourceProperties" ordered="false"
	 * @generated
	 */
	EList<String> getResourceProperties();

} // SafetyPattern

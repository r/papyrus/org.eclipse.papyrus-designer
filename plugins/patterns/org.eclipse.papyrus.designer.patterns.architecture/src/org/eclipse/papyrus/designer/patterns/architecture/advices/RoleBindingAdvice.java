/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.patterns.architecture.advices;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper advice for a role binding: add a dependency between an element and a role within a collaboration and add this
 * automatically to a RoleBinding class in the same package as the source element
 */
public class RoleBindingAdvice extends AbstractEditHelperAdvice {

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateRelationshipRequest) {
				return approveCreateRelationshipRequest((CreateRelationshipRequest) context.getEditCommandRequest());
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * Check that the source of the assignment is a technical policy
	 * 
	 * @param request
	 * @return
	 */
	protected boolean approveCreateRelationshipRequest(CreateRelationshipRequest request) {

		IElementType type = request.getElementType();
		EObject target = request.getTarget();
		if (target != null) {
			EObject container = target.eContainer();
			if (type != null && target instanceof Property && container instanceof Collaboration) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * Move dependency (roleBinding) into collaborationUse of the roleBinding class 
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		if (!(newElement instanceof Dependency)) {
			return super.getAfterConfigureCommand(request);
		}
		CompositeCommand compositeCommand = new CompositeCommand("Role binding configuration command"); //$NON-NLS-1$
		Dependency roleBinding = (Dependency) newElement;
		Package container = roleBinding.getNearestPackage();
		if (container != null) {
			PackageableElement roleBindingClass = container.getPackagedElement(RoleBindingClassAdvice.ROLE_BINDING);
			if (roleBindingClass instanceof Class) {
				List<CollaborationUse> cuList = ((Class) roleBindingClass).getCollaborationUses();
				if (cuList.size() > 0) {
					CollaborationUse cu = cuList.get(0);
					IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(newElement);

					EList<Dependency> roleBindings = new BasicEList<Dependency>();
					roleBindings.addAll(cu.getRoleBindings());
					roleBindings.add(roleBinding);
					SetRequest setBindings = new SetRequest(cu, UMLPackage.eINSTANCE.getCollaborationUse_RoleBinding(), roleBindings);
					compositeCommand.add(commandProvider.getEditCommand(setBindings));

				}
			}

		}

		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}

	protected EObject source;
	protected EObject target;
}

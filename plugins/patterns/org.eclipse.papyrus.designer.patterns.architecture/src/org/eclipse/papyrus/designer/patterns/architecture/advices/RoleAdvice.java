/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.patterns.architecture.advices;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.GetEditContextRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.IEditCommandRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.papyrus.infra.services.edit.service.ElementEditServiceUtils;
import org.eclipse.papyrus.infra.services.edit.service.IElementEditService;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * Edit Helper advice for a property (role) that is added to a collaboration. It will automatically be added to the
 * collaboration role list.
 */
public class RoleAdvice extends AbstractEditHelperAdvice {

	@Override
	public boolean approveRequest(IEditCommandRequest request) {
		if (request instanceof GetEditContextRequest) {
			GetEditContextRequest context = (GetEditContextRequest) request;
			if (context.getEditCommandRequest() instanceof CreateElementRequest) {
				return approveCreateElementRequest((CreateElementRequest) context.getEditCommandRequest());
			}
		}
		return super.approveRequest(request);
	}

	/**
	 * Check that the source of the assignment is a technical policy
	 * @param request
	 * @return
	 */
	protected boolean approveCreateElementRequest(CreateElementRequest request) {
		
		IElementType type = request.getElementType();
		EObject container = request.getContainer();
		if (type != null && container instanceof Collaboration) {
			return true;
		}
		return false;
	}

	/**
	 * @see org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice#getAfterConfigureCommand(org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest)
	 * Add newly created role (property) to collaborationRole list
	 */
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		EObject newElement = request.getElementToConfigure();
		EObject container = newElement.eContainer();
		if (!(newElement instanceof Property) || !(container instanceof Collaboration)) {
			return super.getAfterConfigureCommand(request);
		}
		Collaboration collaboration = (Collaboration) container;
		CompositeCommand compositeCommand = new CompositeCommand("Role advice creation configuration command"); //$NON-NLS-1$

		IElementEditService commandProvider = ElementEditServiceUtils.getCommandProvider(newElement);

		EList<ConnectableElement> roles = new BasicEList<ConnectableElement>();
		roles.addAll(collaboration.getCollaborationRoles());
		roles.add((Property) newElement);
		SetRequest setRoles = new SetRequest(collaboration, UMLPackage.eINSTANCE.getCollaboration_CollaborationRole(), roles);
		compositeCommand.add(commandProvider.getEditCommand(setRoles));
		
		return compositeCommand.isEmpty() ? super.getAfterConfigureCommand(request) : compositeCommand;
	}
	
	protected EObject source;
	protected EObject target;
}

/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.components.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Port;

/**
 * Check whether the port is not correctly typed, e.g. the port kind ProvideInterface is used in
 * conjunction with a data type.
 * Technically, the criterion will be that not both, derived provided and derived required interfaces
 * are empty.
 *
 */
public class PortIsCorrectlyTyped extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx)
	{
		Port port = (Port) ctx.getTarget();

		if ((port.getProvideds().size() == 0) && (port.getRequireds().size() == 0)) {
			Class class_ = port.getClass_();
			return ctx.createFailureStatus("The port '" + port.getName() + "' owned by class '" + class_.getQualifiedName() + //$NON-NLS-1$ //$NON-NLS-2$
					"' is not correctly typed, since derived provided and required interface is null"); //$NON-NLS-1$
		}
		return ctx.createSuccessStatus();

	}
}

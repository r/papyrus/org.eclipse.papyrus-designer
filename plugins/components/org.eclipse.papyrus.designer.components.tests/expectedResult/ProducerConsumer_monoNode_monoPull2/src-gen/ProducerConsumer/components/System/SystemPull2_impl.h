// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#ifndef PRODUCERCONSUMER_COMPONENTS_SYSTEM_SYSTEMPULL2_IMPL_H
#define PRODUCERCONSUMER_COMPONENTS_SYSTEM_SYSTEMPULL2_IMPL_H

/************************************************************
 SystemPull2_impl class header
 ************************************************************/

#include "ProducerConsumer/components/System/Pkg_System.h"

#include "ProducerConsumer/DataExchange_PubData/FIFO/FIFO_impl.h"
#include "ProducerConsumer/components/Producer/Producer_impl.h"
#include "ProducerConsumer/components/PullConsumer/PullConsumer_impl.h"
#include "componentlib/ContainerServices/Thread.h"

namespace ProducerConsumer {
namespace components {
namespace System {

/************************************************************/
/**
 * In some cases, we like to model the connector-property-connector relation explicity. This enables (1) the use on n-ary connections and (2) is cleaner with respect to the configuration of the interaction component
 */
class SystemPull2_impl {
public:

	/**
	 * 
	 */
	::ProducerConsumer::DataExchange_PubData::FIFO::FIFO_impl fifoconnector;

	/**
	 * 
	 */
	::ProducerConsumer::components::PullConsumer::PullConsumer_impl con;

	/**
	 * 
	 */
	::componentlib::ContainerServices::Thread pullConThread;

	/**
	 * 
	 */
	::ProducerConsumer::components::Producer::Producer_impl prod;

	/**
	 * 
	 */
	void createConnections();

};
/************************************************************/
/* External declarations (package visibility)               */
/************************************************************/

/* Inline functions                                         */

} // of namespace System
} // of namespace components
} // of namespace ProducerConsumer

/************************************************************
 End of SystemPull2_impl class header
 ************************************************************/

#endif

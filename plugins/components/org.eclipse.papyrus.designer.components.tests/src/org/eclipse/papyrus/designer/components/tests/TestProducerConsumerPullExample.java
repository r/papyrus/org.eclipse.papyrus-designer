/*******************************************************************************
 * Copyright (c) 2017 CEA LIST
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.tests;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.common.testutils.TestConstants;
import org.eclipse.papyrus.designer.languages.common.testutils.TransformationTestSupport;
import org.eclipse.papyrus.junit.utils.rules.HouseKeeper;
import org.eclipse.papyrus.junit.utils.rules.PapyrusEditorFixture;
import org.eclipse.papyrus.junit.utils.rules.PluginResource;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

@PluginResource("org.eclipse.papyrus.designer.components.modellibs.examples:/models/examples/ProducerConsumer/ProducerConsumer.di")
public class TestProducerConsumerPullExample {

	@SuppressWarnings("nls")
	public static final String PRODUCER_CONSUMER_MONO_NODE_MONO_PULL1 = "ProducerConsumer_monoNode_monoPull1";

	@SuppressWarnings("nls")
	public static final String PRODUCER_CONSUMER_MONO_NODE_MONO_PULL2 = "ProducerConsumer_monoNode_monoPull2";


	@SuppressWarnings("nls")
	public static final String DEPLOYMENT_MONO_PULL1 = "ProducerConsumer::deployment::monoPull1";

	@SuppressWarnings("nls")
	public static final String DEPLOYMENT_MONO_PULL2 = "ProducerConsumer::deployment::monoPull2";

	
	@Rule
	/** The model set fixture. */
	public final PapyrusEditorFixture modelSetFixture = new PapyrusEditorFixture();

	@ClassRule
	public static HouseKeeper.Static houseKeeper = new HouseKeeper.Static();

	@Test
	public void testProducerConsumerMonoPull1() throws InterruptedException {

		TransformationTestSupport tts = new TransformationTestSupport(this.getClass(), houseKeeper, modelSetFixture);

		// create generated project. For generation purposes, it is not important that this is not a CDT project
		IProject genProject = houseKeeper.createProject(PRODUCER_CONSUMER_MONO_NODE_MONO_PULL1);

		tts.runTransformation(DEPLOYMENT_MONO_PULL1);
		
		tts.validateResults(genProject, TestConstants.EXPECTED_RESULT + TestConstants.FILE_SEP +
				PRODUCER_CONSUMER_MONO_NODE_MONO_PULL1 + TestConstants.FILE_SEP + TestConstants.SRC_GEN, TestConstants.SRC_GEN);
	}

	@Test
	public void testProducerConsumerMonoPull2() throws InterruptedException {

		TransformationTestSupport tts = new TransformationTestSupport(this.getClass(), houseKeeper, modelSetFixture);

		// create generated project. For generation purposes, it is not important that this is not a CDT project
		IProject genProject = houseKeeper.createProject(PRODUCER_CONSUMER_MONO_NODE_MONO_PULL2);

		tts.runTransformation(DEPLOYMENT_MONO_PULL2);
		
		tts.validateResults(genProject, TestConstants.EXPECTED_RESULT + TestConstants.FILE_SEP +
				PRODUCER_CONSUMER_MONO_NODE_MONO_PULL2 + TestConstants.FILE_SEP + TestConstants.SRC_GEN, TestConstants.SRC_GEN);
	}
}


/*******************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.marte.mappingrules;

import java.util.Iterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.ClientServerFeature;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.ClientServerKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.ClientServerPort;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.ClientServerSpecification;
import org.eclipse.papyrus.designer.components.FCM.Port;
import org.eclipse.papyrus.designer.components.fcm.profile.IMappingRule;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.PortMapUtil;
import org.eclipse.papyrus.designer.transformation.base.utils.OperationSync;
import org.eclipse.papyrus.designer.uml.tools.utils.OperationUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.RealizationUtils;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Map ports with MARTE stereotype for feature based client-server specification to an FCM type
 */
public class FeatureBasedCS implements IMappingRule {

	public static String FEATURE_BASED_CS = "FBCS_"; //$NON-NLS-1$

	protected Interface calcInterface(Port p, String infix, ClientServerKind filter) {
		org.eclipse.uml2.uml.Port port = p.getBase_Port();
		Interface derivedInterface = null;

		EObject csPortEObj = UMLUtil.getStereotypeApplication(port, ClientServerPort.class);
		if (csPortEObj instanceof ClientServerPort) {
			ClientServerPort csPort = (ClientServerPort) csPortEObj;
			ClientServerSpecification csSpec = csPort.getFeaturesSpec();

			Interface typingInterface = csSpec.getBase_Interface();
			// update FCM port type according to CS specification
			p.setType(typingInterface);
			derivedInterface = PortMapUtil.getDerivedInterface(p, FEATURE_BASED_CS + infix);

			for (Operation operation : typingInterface.getOwnedOperations()) {
				String name = operation.getName();

				EObject csFeatureEObj = UMLUtil.getStereotypeApplication(operation, ClientServerFeature.class);
				if (csFeatureEObj instanceof ClientServerFeature) {
					ClientServerFeature csFeature = (ClientServerFeature) csFeatureEObj;
					ClientServerKind csKind = csFeature.getKind();
					// feature is either for both (provided or required) or for one of the passed filter conditions
					if ((csKind == ClientServerKind.PROREQ) || (csKind == filter)) {
						if (derivedInterface == null) {
							// create derived interface
							derivedInterface = PortMapUtil.getOrCreateDerivedInterface(p, FEATURE_BASED_CS + infix);
						}
						Operation derivedOperation = derivedInterface.getOperation(name, null, null);
						// check whether operation already exists. Create, if not
						if (derivedOperation == null) {
							derivedOperation = derivedInterface.createOwnedOperation(name, null, null);
						}

						if (!OperationUtils.isSameOperation(operation, derivedOperation)) {
							OperationSync.sync(operation, derivedOperation);
						}
					} else if (derivedInterface != null) {
						// remove operation from derived interface
						Operation derivedOperation = derivedInterface.getOwnedOperation(name, null, null);
						if (derivedOperation != null) {
							derivedInterface.getOwnedOperations().remove(derivedOperation);
						}
					}
				}
			}
			if (derivedInterface != null) {
				removeFromDerived(typingInterface, derivedInterface);
			}
		} else {
			// remove a derived interface, if it exists
			// org.eclipse.papyrus.designer.components.FCMUtil.removeDerivedInterface (p.getKind (), "_p_", type);
		}

		return derivedInterface;
	}

	/**
	 * check whether operations in derived interface exist in original interface.
	 * If not, remove these operation from the derived interface
	 * 
	 * @param typingInterface
	 *            typing (=original) interface
	 * @param derivedInterface
	 *            derived interface
	 */
	private void removeFromDerived(Interface typingInterface, Interface derivedInterface) {
		Iterator<Operation> derivedOperations = derivedInterface.getOwnedOperations().iterator();
		while (derivedOperations.hasNext()) {
			Operation derivedOperation = derivedOperations.next();
			String name = derivedOperation.getName();
			if (typingInterface.getOperation(name, null, null) == null) {
				// not in typing interface, remove
				derivedOperations.remove();
			}
		}
	}

	@Override
	public boolean needsUpdate(Port p) {
		return false;
	}

	@Override
	public Type calcDerivedType(Port p, boolean update) {
		Interface provided = calcInterface(p, "p_", ClientServerKind.PROVIDED); //$NON-NLS-1$
		Interface required = calcInterface(p, "r_", ClientServerKind.REQUIRED); //$NON-NLS-1$
		Class derivedClass = PortMapUtil.getOrCreateDerivedClass(p, FEATURE_BASED_CS);
		if (required != null) {
			RealizationUtils.addUsage(derivedClass, required);
		}
		if (provided != null) {
			RealizationUtils.addRealization(derivedClass, provided);
		}
		return derivedClass;
	}
}

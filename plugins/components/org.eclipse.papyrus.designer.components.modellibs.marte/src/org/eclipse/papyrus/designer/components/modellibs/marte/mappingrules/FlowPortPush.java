/*******************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.marte.mappingrules;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.FlowDirectionKind;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.FlowProperty;
import org.eclipse.papyrus.designer.components.FCM.Port;
import org.eclipse.papyrus.designer.components.fcm.profile.IMappingRule;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.PortMapUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.RealizationUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * A MARTE FlowPort typed with a flow specification or a datatype. Flow properties for consumption ("in") are obtained via pushing,
 * flow properties for production ("out") are distributed via push as well. In case of a datatype, we assume that
 * the data is produced as well as consumed.
 * Thus, there are two interfaces, one for data consumption that is provided by the port and one for data production
 * that is required by the port.
 */
public class FlowPortPush implements IMappingRule {

	private static final String DATA = "data"; //$NON-NLS-1$

	public static final String I_FLOW_PORT_PUSH_IN = "IFlowPortPushIn"; //$NON-NLS-1$

	public static final String I_FLOW_PORT_PUSH_OUT = "IFlowPortPushOut"; //$NON-NLS-1$

	public static final String FLOW_PORT_PUSH = "FlowPortPush"; //$NON-NLS-1$

	@Override
	public Type calcDerivedType(Port p, boolean update) {
		Type type = p.getBase_Port().getType();
		
		Class derivedType = PortMapUtil.getDerivedClass(p, FLOW_PORT_PUSH);

		if((type instanceof Interface)) {
			Interface intf = (Interface)type;
			int in = 0, out = 0;
			for(Property attribute : intf.getOwnedAttributes()) {
				if(StereotypeUtil.isApplied(attribute, FlowProperty.class)) {
					FlowProperty fp = UMLUtil.getStereotypeApplication(attribute, FlowProperty.class);
					FlowDirectionKind fdk = fp.getDirection();
					if((fdk == FlowDirectionKind.IN) || (fdk == FlowDirectionKind.INOUT)) {
						in++;
					}
					if((fdk == FlowDirectionKind.OUT) || (fdk == FlowDirectionKind.INOUT)) {
						out++;
					}
				}
			}

			if(in > 0) {
				// provided intf.
				Interface derivedInInterface = PortMapUtil.getDerivedInterface(p, I_FLOW_PORT_PUSH_IN);
				RealizationUtils.addRealization(derivedType, derivedInInterface);

				if(derivedInInterface == null) {
					// may happen, if within template (do not want creation of derived interfaces in template)
					return null;
				}

				// check whether operations already exists. Create, if not
				for(Property attribute : intf.getOwnedAttributes()) {
					if(StereotypeUtil.isApplied(attribute, FlowProperty.class)) {
						FlowProperty fp = (FlowProperty)UMLUtil.getStereotypeApplication(attribute, FlowProperty.class);
						FlowDirectionKind fdk = fp.getDirection();
						if((fdk == FlowDirectionKind.IN) || (fdk == FlowDirectionKind.INOUT)) {
							createOrCheckOp("push_", derivedInInterface, fp.getBase_Property(), ParameterDirectionKind.IN_LITERAL);
						}
					}
				}
			}
			if(out > 0) {
				Interface derivedOutInterface = PortMapUtil.getDerivedInterface(p, I_FLOW_PORT_PUSH_OUT);
				RealizationUtils.addUsage(derivedType, derivedOutInterface);

				if(derivedOutInterface == null) {
					// may happen, if within template (do not want creation of derived interfaces in template)
					return null;
				}

				// check whether operations already exists. Create, if not
				for(Property attribute : intf.getOwnedAttributes()) {
					if(StereotypeUtil.isApplied(attribute, FlowProperty.class)) {
						FlowProperty fp = (FlowProperty)UMLUtil.getStereotypeApplication(attribute, FlowProperty.class);
						FlowDirectionKind fdk = fp.getDirection();

						if((fdk == FlowDirectionKind.OUT) || (fdk == FlowDirectionKind.INOUT)) {
							createOrCheckOp("push_", derivedOutInterface, fp.getBase_Property(), ParameterDirectionKind.IN_LITERAL);
						}
					}
				}
			}
			return derivedType;
		}

		else if(type instanceof DataType) {
			// if typed with data type - handle atomic MARTE flow port with inout (can we always assume a data type?)
			Interface derivedInInterface = PortMapUtil.getDerivedInterface(p, I_FLOW_PORT_PUSH_IN);
			createOrCheckOp("push_data", derivedInInterface, type, ParameterDirectionKind.IN_LITERAL);
			return derivedType;
		}

		else {
			return null;
		}
	}

	public static void createOrCheckOp(String prefix, Interface intf, Property fp,
		ParameterDirectionKind direction) {
		String opName = prefix + fp.getName();
		createOrCheckOp(opName, intf, fp.getType(), direction);
	}

	public static void createOrCheckOp(String opName, Interface intf, Type type,
		ParameterDirectionKind direction) {
		Operation derivedOperation = intf.getOperation(opName, null, null);
		if(derivedOperation == null) {
			derivedOperation = intf.createOwnedOperation(opName, null, null);
		}
		EList<Parameter> parameters = derivedOperation.getOwnedParameters();
		if(parameters.size() == 0) {
			derivedOperation.createOwnedParameter(DATA, type);
		} else {
			Parameter parameter = parameters.get(0);
			parameter.setDirection(direction);
			if(!parameter.getName().equals(DATA)) {
				parameter.setName(DATA);
			}
			if(parameter.getType() != type) {
				parameter.setType(type);
			}
		}
	}

	@Override
	public boolean needsUpdate(Port p) {
		// TODO Auto-generated method stub
		return false;
	}
}

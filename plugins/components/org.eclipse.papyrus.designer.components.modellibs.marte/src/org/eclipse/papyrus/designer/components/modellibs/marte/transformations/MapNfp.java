package org.eclipse.papyrus.designer.components.modellibs.marte.transformations;

import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoElem;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;
import org.eclipse.uml2.uml.resource.UMLResource;

/**
 * Map MARTE primitive types and NFP types to others by replacing the types of typed elements.
 * The rationale is that these types come with their own definition of operations that conflict
 * with those in the programming language. There is also a performance penalty to inheritage from NFP_CommonType.
 */
public class MapNfp implements IM2MTrafoElem {

	@SuppressWarnings("nls")
	static private Map<String, String> map = Map.ofEntries(
			Map.entry("MARTE_Library::BasicNFP_Types::NFP_Real", "PrimitiveTypes::Real"),
			Map.entry("MARTE_Library::BasicNFP_Types::NFP_Integer", "PrimitiveTypes::Integer"),
			Map.entry("MARTE_Library::BasicNFP_Types::NFP_Boolean", "PrimitiveTypes::Boolean"),
			Map.entry("MARTE_Library::BasicNFP_Types::NFP_UnlimitedNatural", "PrimitiveTypes::UnlimitedNatural"),
			Map.entry("MARTE_Library::BasicNFP_Types::NFP_String", "PrimitiveTypes::String"),
			Map.entry("MARTE_Library::BasicNFP_Types::NFP_Duration", "PrimitiveTypes::Real"), // TODO: need clock reference?

			Map.entry("MARTE_Library::MARTE_PrimitivesTypes::Real", "PrimitiveTypes::Real"),
			Map.entry("MARTE_Library::MARTE_PrimitivesTypes::Integer", "PrimitiveTypes::Integer"),
			Map.entry("MARTE_Library::MARTE_PrimitivesTypes::Boolean", "PrimitiveTypes::Boolean"),
			Map.entry("MARTE_Library::MARTE_PrimitivesTypes::UnlimitedNatural", "PrimitiveTypes::UnlimitedNatural"),
			Map.entry("MARTE_Library::MARTE_PrimitivesTypes::String", "PrimitiveTypes::String"),

			Map.entry("MARTE_Library::TimeLibrary::TimedValueType", "PrimitiveTypes::Real") // simple mapping without clock
	);

	public void transformTypedElem(TypedElement typedElem) {

		Type type = typedElem.getType();
		if (type != null) {
			String qn = type.getQualifiedName();
			if (qn != null) {
				for (String key : map.keySet()) {
					if (qn.equals(key)) {
						NamedElement mappedObj = ElementUtils.getQualifiedElementFromRS(typedElem,
								URI.createURI(UMLResource.UML_PRIMITIVE_TYPES_LIBRARY_URI), map.get(key));
						if (mappedObj instanceof Type) {
							typedElem.setType((Type) mappedObj);
						}
						break;
					}
				}
			}
		}

	}

	@Override
	public void transformElement(M2MTrafo trafo, Element element) throws TransformationException {
		if (element instanceof Classifier) {
			for (Iterator<EObject> i = element.eAllContents(); i.hasNext();) {
				EObject eObj = i.next();
				if (eObj instanceof TypedElement) {
					transformTypedElem((TypedElement) eObj);
				}
			}
		}
	}
}

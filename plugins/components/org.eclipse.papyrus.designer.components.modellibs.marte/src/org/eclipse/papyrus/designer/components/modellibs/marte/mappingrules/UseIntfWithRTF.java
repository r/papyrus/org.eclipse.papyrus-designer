/*******************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.marte.mappingrules;

import java.util.Iterator;

import org.eclipse.papyrus.designer.components.FCM.Port;
import org.eclipse.papyrus.designer.components.fcm.profile.IMappingRule;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.PortMapUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.RealizationUtils;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Type;

/**
 * Use an interface with real-time feature
 */
@Deprecated
public class UseIntfWithRTF implements IMappingRule {

	static final String RTF_PATH = "MarteCalls::ACCORD_Lib::ActiveObject::RTF"; //$NON-NLS-1$

	static final String WITH_RTF = "WithRTF"; //$NON-NLS-1$
	
	@Override
	public Type calcDerivedType(Port p, boolean update) {
		Type type = p.getBase_Port().getType();
		if(!(type instanceof Interface))
			return null;

		Class derivedType = PortMapUtil.getDerivedClass(p, WITH_RTF);
		Interface typingInterface = (Interface) type;
		Interface derivedInterface = PortMapUtil.getDerivedInterface(p, WITH_RTF);
		RealizationUtils.addUsage(derivedType, derivedInterface);
		
		Element rtfTypeElem = ElementUtils.getQualifiedElement(typingInterface.getModel(), RTF_PATH);
		Type rtfType = null;
		if(rtfTypeElem instanceof Type) {
			rtfType = (Type)rtfTypeElem;
		}

		for(Operation operation : typingInterface.getOwnedOperations()) {
			String name = operation.getName();

			// check whether operation already exists. Create, if not
			Operation derivedOperation = derivedInterface.getOperation(name, null, null);
			if(derivedOperation == null) {
				derivedOperation = derivedInterface.createOwnedOperation(name, null, null);
			}

			for(Parameter parameter : operation.getOwnedParameters()) {
				String paramName = parameter.getName();
				Type paramType = parameter.getType();
				if(derivedOperation.getOwnedParameter(paramName, paramType) == null) {
					Parameter newParameter =
						derivedOperation.createOwnedParameter(parameter.getName(), parameter.getType());
					newParameter.setDirection(parameter.getDirection());
					newParameter.setLower(parameter.getLower());
					newParameter.setUpper(parameter.getUpper());
				}
			}
			// remove those parameters that exist in derived, but not original interface.
			Iterator<Parameter> derivedParameters = derivedOperation.getOwnedParameters().iterator();
			while(derivedParameters.hasNext()) {
				Parameter parameter = derivedParameters.next();
				String paramName = parameter.getName();
				Type paramType = parameter.getType();
				if((!(paramName.equals("rtf"))) &&
					(operation.getOwnedParameter(paramName, paramType) == null)) {
					// not on in original interface, remove from derived as well
					derivedParameters.remove();
				}
			}

			Parameter rtfParam = derivedOperation.getOwnedParameter("rtf", null);
			if(rtfParam == null) {
				derivedOperation.createOwnedParameter("rtf", rtfType);
			} else if(rtfParam.getType() != rtfType) {
				rtfParam.setType(rtfType);
			}
		}

		// check whether operations in derived interface exist in original interface
		// (remove, if not)
		Iterator<Operation> derivedOperations = derivedInterface.getOwnedOperations().iterator();
		while(derivedOperations.hasNext()) {
			Operation derivedOperation = derivedOperations.next();
			String name = derivedOperation.getName();
			if(name == null) {
				continue;
			}
			if(typingInterface.getOperation(name, null, null) == null) {
				// not in typing interface, remove
				derivedOperations.remove();
			}
		}

		return derivedInterface;
	}

	@Override
	public boolean needsUpdate(Port p) {
		return false;
	}

}

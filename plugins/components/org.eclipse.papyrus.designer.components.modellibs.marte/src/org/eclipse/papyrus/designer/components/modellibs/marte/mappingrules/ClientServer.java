/*******************************************************************************
 * Copyright (c) 2023 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.marte.mappingrules;

import org.eclipse.papyrus.MARTE.MARTE_DesignModel.GCM.ClientServerPort;
import org.eclipse.papyrus.designer.components.fcm.profile.IMappingRule;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.PortMapUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.RealizationUtils;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * A MARTE client server port, i.e. a port which provides and requires a set of service;
 * 
 */
public class ClientServer implements IMappingRule {

	public static final String MARTE_CS = "MarteCS"; //$NON-NLS-1$

	@Override
	public boolean needsUpdate(org.eclipse.papyrus.designer.components.FCM.Port p) {
		return false;
	}

	@Override
	public Type calcDerivedType(org.eclipse.papyrus.designer.components.FCM.Port p, boolean update) {
		Port port = p.getBase_Port();
		ClientServerPort csPort = UMLUtil.getStereotypeApplication(port, ClientServerPort.class);
		Class derivedType = PortMapUtil.getDerivedClass(p, MARTE_CS, update);

		if (update) {
			// remove all relationships, then add provided and required
			derivedType.getSourceDirectedRelationships().clear();

			// provided
			if (csPort != null) {
				for (Interface provided : csPort.getProvInterface()) {
					RealizationUtils.addRealization(derivedType, provided);
				}
			}

			// required
			if (csPort != null) {
				for (Interface required : csPort.getReqInterface()) {
					RealizationUtils.addUsage(derivedType, required);
				}
			}
		}

		return derivedType;
	}
}

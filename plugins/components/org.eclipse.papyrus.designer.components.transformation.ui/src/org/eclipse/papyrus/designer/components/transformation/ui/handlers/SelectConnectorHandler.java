/********
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.components.transformation.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.papyrus.designer.components.FCM.InteractionComponent;
import org.eclipse.papyrus.designer.components.transformation.ui.dialogs.ConnectorSelectionDialog;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Feature;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Implementation class for ClassAction action
 */
public class SelectConnectorHandler extends CmdHandler {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();
		// feature is a common superclass of Connector and Property
		if (!(selectedEObject instanceof Feature)) {
			return null;
		}
		// get selected connector
		final Feature selectedFeature = (Feature) selectedEObject;
		Shell shell = Display.getCurrent().getActiveShell();

		// 1. select possible connectors according to port types
		// (only show compatible connectors check-box?)
		// 2. select implementation group according to connector type

		Model model = selectedFeature.getModel();

		ConnectorSelectionDialog elementSelector = new ConnectorSelectionDialog(shell, model, selectedFeature);
		elementSelector.setTitle("Select connector"); //$NON-NLS-1$
		elementSelector.setMessage("Select an implementation for connector " + selectedFeature.getName()); //$NON-NLS-1$
		elementSelector.open();
		if (elementSelector.getReturnCode() == IDialogConstants.OK_ID) {
			final Object[] result = elementSelector.getResult();
			if ((result.length == 1) && (result[0] instanceof Class)) {
				CommandSupport.exec((Class) result[0], "Select connector", new Runnable() { //$NON-NLS-1$

					@Override
					public void run() {
						org.eclipse.papyrus.designer.components.FCM.Connector fcmSelectedConnector = StereotypeUtil
								.applyApp(selectedFeature, org.eclipse.papyrus.designer.components.FCM.Connector.class);
						InteractionComponent newConnType = UMLUtil.getStereotypeApplication((Class) result[0],
								InteractionComponent.class);
						fcmSelectedConnector.setIc(newConnType);
					}
				});
			}
		}
		return null;
	}
}

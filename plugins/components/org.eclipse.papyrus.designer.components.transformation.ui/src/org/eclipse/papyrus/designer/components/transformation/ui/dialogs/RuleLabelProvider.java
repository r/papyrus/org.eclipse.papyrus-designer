/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.components.transformation.ui.dialogs;

import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.papyrus.designer.components.FCM.ContainerRule;

/**
 * Return a label for ContainerRules. Since the name of the rule itself, is not
 * really interesting for the user, the returned name is based on the interceptor
 * (or extension)
 *
 */
public class RuleLabelProvider extends LabelProvider {

	@Override
	public String getText(Object element) {
		if (element instanceof ContainerRule) {
			ContainerRule rule = (ContainerRule) element;
			// TODO? Add information about contained parts?
			return rule.getBase_Class().getName();
		}
		return null;
	}
};

/**
 * Copyright (c) 2015 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Ansgar Radermacher - Initial API and implementation
 * 
 */
package org.eclipse.papyrus.designer.components.FCM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Template ports are ports that enable the binding of an extended port with an actual parameter: the resulting port kind is the port kind bound to the current port type.
 * For instance, we cound type a port with "MyType" and use an extended port Writer with a template with formal parameter T. Resulting port would be Writer with T bound to MyType.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.components.FCM.TemplatePort#getBoundType <em>Bound Type</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.components.FCM.FCMPackage#getTemplatePort()
 * @model
 * @generated
 */
public interface TemplatePort extends Port {
	/**
	 * Returns the value of the '<em><b>Bound Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bound Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bound Type</em>' reference.
	 * @see org.eclipse.papyrus.designer.components.FCM.FCMPackage#getTemplatePort_BoundType()
	 * @model transient="true" changeable="false" volatile="true" derived="true" ordered="false"
	 * @generated
	 */
	PortKind getBoundType();

} // TemplatePort

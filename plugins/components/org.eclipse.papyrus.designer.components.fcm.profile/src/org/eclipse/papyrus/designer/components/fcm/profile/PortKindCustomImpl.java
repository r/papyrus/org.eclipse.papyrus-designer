/**
 * Copyright (c) 2021 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - Initial API and implementation
 *
 */
package org.eclipse.papyrus.designer.components.fcm.profile;

import org.eclipse.papyrus.designer.components.FCM.impl.PortKindImpl;

/**
 * Custom implementation of PortKind stereotype
 */
public class PortKindCustomImpl extends PortKindImpl {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 */
	@Override
	public boolean isExtendedPort() {
		if (getBase_Class() != null) {
			return getBase_Class().getOwnedPorts().size() > 0;
		}
		return false;
	}
}
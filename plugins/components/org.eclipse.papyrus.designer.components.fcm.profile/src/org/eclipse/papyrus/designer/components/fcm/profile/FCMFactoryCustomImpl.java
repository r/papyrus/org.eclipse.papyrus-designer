package org.eclipse.papyrus.designer.components.fcm.profile;
import org.eclipse.papyrus.designer.components.FCM.FCMFactory;
import org.eclipse.papyrus.designer.components.FCM.PortKind;
import org.eclipse.papyrus.designer.components.FCM.TemplatePort;
import org.eclipse.papyrus.designer.components.FCM.impl.FCMFactoryImpl;


/**
 * Customized factory that returns specific elements for TemplatePort and PortKind
 *
 */
public class FCMFactoryCustomImpl extends FCMFactoryImpl implements FCMFactory {

	/**
	 * @see FCMFactoryImpl#createTemplatePort()
	 *
	 * @return the TemplatePort
	 */
	@Override
	public TemplatePort createTemplatePort() {
		return new TemplatePortCustomImpl();
	}
	


	/**
	 * @see FCMFactoryImpl#createPortKind()
	 *
	 * @return the PortKind
	 */
	@Override
	public PortKind createPortKind() {
		return new PortKindCustomImpl();
	}
	
}
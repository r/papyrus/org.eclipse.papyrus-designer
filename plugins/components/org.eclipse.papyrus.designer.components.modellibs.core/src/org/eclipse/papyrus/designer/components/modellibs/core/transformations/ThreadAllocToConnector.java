/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.core.transformations;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.SRM.SW_Concurrency.SwSchedulableResource;
import org.eclipse.papyrus.designer.deployment.tools.AllocUtils;
import org.eclipse.papyrus.designer.deployment.tools.DepUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.StructuralFeature;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Add a connection for allocation relationships with a SW thread
 */
public class ThreadAllocToConnector implements IM2MTrafoCDP {

	@Override
	public void applyTrafo(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {
		for (InstanceSpecification is : DepUtils.getInstances(deploymentPlan)) {
			InstanceSpecification target = AllocUtils.getNodeOrThread(is);
			if (target == null) {
				continue;
			}

			Classifier sourceCl = DepUtils.getClassifier(is);
			Classifier targetCl = DepUtils.getClassifier(target);
			SwSchedulableResource alloc = UMLUtil.getStereotypeApplication(targetCl, SwSchedulableResource.class);
			if (alloc != null) {
				// allocation to tread, call run method.
				Class sourceClass = (Class) sourceCl;
				Class targetClass = (Class) targetCl;
				Port lcPortSrc = (Port) ElementUtils.getNamedElementFromList(sourceClass.getAllAttributes(), "run"); //$NON-NLS-1$
				Port lcPortTrg = (Port) ElementUtils.getNamedElementFromList(targetClass.getAllAttributes(), "rRun"); //$NON-NLS-1$

				EList<Slot> lcSlotsSrc = DepUtils.getReferencingSlots(is);
				EList<Slot> lcSlotsTrg = DepUtils.getReferencingSlots(target);
				if ((lcSlotsSrc.size() > 0) && (lcSlotsTrg.size() > 0)) {
					Slot lcSlotSrc = lcSlotsSrc.get(0);
					Slot lcSlotTrg = lcSlotsTrg.get(0);

					StructuralFeature featureSrc = lcSlotSrc.getDefiningFeature();
					StructuralFeature featureTrg = lcSlotTrg.getDefiningFeature();
					if (featureSrc instanceof Property && featureTrg instanceof Property) {
						Class composite = (Class) featureSrc.getFeaturingClassifiers().get(0);
						Connector conn = composite.createOwnedConnector(
								String.format("alloc %s to %s", featureSrc.getName(), featureTrg.getName())); //$NON-NLS-1$
						ConnectorEnd end1 = conn.createEnd();
						ConnectorEnd end2 = conn.createEnd();
						end1.setRole(lcPortSrc);
						end2.setRole(lcPortTrg);
						end1.setPartWithPort((Property) featureSrc);
						end2.setPartWithPort((Property) featureTrg);
					}
				}
			}
		}
	}
}

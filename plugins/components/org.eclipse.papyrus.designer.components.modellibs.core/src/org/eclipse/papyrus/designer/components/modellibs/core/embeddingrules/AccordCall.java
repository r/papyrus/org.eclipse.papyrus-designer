/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.core.embeddingrules;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.designer.components.FCM.Connector;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.ConnectorTypeUtil;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.FCMUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.ConnectableElement;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Embedding rule
 * TODO: currently unused
 *
 */
// @unused
public class AccordCall extends ConnectorTypeUtil {

	private static final String CONNECTOR = "connector"; //$NON-NLS-1$
	private static final String RTU = "rtu"; //$NON-NLS-1$
	private static final String SERVER = "server"; //$NON-NLS-1$
	private static final String CLIENT = "client"; //$NON-NLS-1$
	
	private ConnectableElement clientRole = null;
	private ConnectableElement serverRole = null;
	private ConnectableElement rtuRole = null;
	private ConnectableElement connectorRole = null;

	@Override
	public FCMUtil.RoleBindingTable getRoleBindings(Connector connector) {
		super.getRoleBindings(connector);

		clientRole = bindingTable.getRoleKeyByName(CLIENT);
		serverRole = bindingTable.getRoleKeyByName(SERVER);
		rtuRole = bindingTable.getRoleKeyByName(RTU);
		connectorRole = bindingTable.getRoleKeyByName(CONNECTOR);

		for (org.eclipse.uml2.uml.ConnectorEnd end : connector.getBase_Connector().getEnds()) {
			if (end.getRole() instanceof org.eclipse.uml2.uml.Port) {
				org.eclipse.uml2.uml.Port port = (org.eclipse.uml2.uml.Port) end.getRole();
				org.eclipse.uml2.uml.Property part = end.getPartWithPort();
				if (StereotypeUtil.isApplied(port, org.eclipse.papyrus.designer.components.FCM.Port.class)) {
					org.eclipse.papyrus.designer.components.FCM.Port fcmPort = UMLUtil.getStereotypeApplication(port, org.eclipse.papyrus.designer.components.FCM.Port.class);
					if (fcmPort.getKind().getBase_Class().getName().equals("UseInterfaceWithRtf")) { //$NON-NLS-1$
						// => elements associated with the connector end play the client role
						List<NamedElement> clientActors = new ArrayList<NamedElement>();
						clientActors.add(port);
						clientActors.add(part);
						bindingTable.addEntry(clientRole, clientActors);
					}
					else if (fcmPort.getKind().getBase_Class().getName().equals("ProvideInterface")) { //$NON-NLS-1$
						// => elements associated with the connector end play the server role
						List<NamedElement> serverActors = new ArrayList<NamedElement>();
						serverActors.add(port);
						serverActors.add(part);
						bindingTable.addEntry(serverRole, serverActors);
						// the property playing the server role must also play the rtu role
						port = ((org.eclipse.uml2.uml.Class) part.getType()).getOwnedPort(RTU, null);
						if (port == null) {
							if (((org.eclipse.uml2.uml.Class) part.getType()).getInheritedMember(RTU) != null &&
									((org.eclipse.uml2.uml.Class) part.getType()).getInheritedMember(RTU) instanceof org.eclipse.uml2.uml.Port) {
								port = (org.eclipse.uml2.uml.Port) ((org.eclipse.uml2.uml.Class) part.getType()).getInheritedMember(RTU);
							}
							else {
								System.out.println(String.format(
										"Could not find a port rtu on part %s : %s", part.getName(), part.getType())); //$NON-NLS-1$
							}
						}
						if (port != null) {
							List<NamedElement> rtuActors = new ArrayList<NamedElement>();
							rtuActors.add(port);
							rtuActors.add(part);
							bindingTable.addEntry(rtuRole, rtuActors);
						}
					}
				}
			}
		}
		List<NamedElement> connectorActors = new ArrayList<NamedElement>();
		connectorActors.add(connector.getBase_Connector());
		bindingTable.addEntry(connectorRole, connectorActors);
		return bindingTable;
	}
}

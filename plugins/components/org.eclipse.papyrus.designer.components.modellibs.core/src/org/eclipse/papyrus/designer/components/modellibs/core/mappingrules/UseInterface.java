/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.components.modellibs.core.mappingrules;

import org.eclipse.papyrus.designer.components.FCM.Port;
import org.eclipse.papyrus.designer.components.fcm.profile.IMappingRule;
import org.eclipse.papyrus.designer.components.fcm.profile.utils.PortMapUtil;
import org.eclipse.papyrus.designer.uml.tools.utils.RealizationUtils;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Type;


public class UseInterface implements IMappingRule
{
	@Override
	public Type calcDerivedType(Port p, boolean update) {
		Type type = p.getType();
		if (type instanceof Interface) {
			Class useType = PortMapUtil.getDerivedClass(p, "Use_", update); //$NON-NLS-1$
			// markNoCodeGen(useType);
			RealizationUtils.addUsage(useType, (Interface) type);
			return useType;
		}
		return null;
	}

	@Override
	public boolean needsUpdate(Port p) {
		return false;
	}
}

/*******************************************************************************
 * Copyright (c) 2013 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher - ansgar.radermacher@cea.fr CEA LIST - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.jdt.texteditor.sync;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IParent;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.ArrayType;
import org.eclipse.jdt.core.dom.Block;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclaration;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.infra.base.StringUtils;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageCodegen;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.ListHint;
import org.eclipse.papyrus.designer.languages.java.codegen.utils.JavaGenUtils;
import org.eclipse.papyrus.designer.languages.java.jdt.texteditor.Activator;
import org.eclipse.papyrus.designer.languages.java.jdt.texteditor.TextEditorConstants;
import org.eclipse.papyrus.designer.languages.java.jdt.texteditor.listener.ModelListener;
import org.eclipse.papyrus.designer.languages.java.library.JavaLibUriConstants;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Array;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Final;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Import;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Variadic;
import org.eclipse.papyrus.designer.uml.tools.utils.BehaviorUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.ParameterUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.ui.IEditorInput;
import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLPackage;

public class SyncJDTtoModel implements Runnable {

	// standard name used for return types

	// special treatment of void in case of return parameters
	public static final String VOID_TYPE = "void"; //$NON-NLS-1$

	public static final String REGISTER = "register"; //$NON-NLS-1$

	public static final String CONST = "const"; //$NON-NLS-1$

	public static final String VOLATILE = "volatile"; //$NON-NLS-1$

	public static final String sAtParam = "@param"; //$NON-NLS-1$

	public static final String sAtReturn = "@return"; //$NON-NLS-1$

	public static final String ansiCLib = "AnsiCLibrary"; //$NON-NLS-1$

	public static final URI defaultLibs[] = {
		JavaLibUriConstants.LIBRARY_PATH_URI,
		JavaLibUriConstants.PT_LIBRARY_PATH_URI
	};
	
	ICompilationUnit icu;

	CompilationUnit astCU;

	/**
	 * input of the CDT editor. Used to obtain code within editor.
	 */
	protected IEditorInput m_input;

	/**
	 * The classifier (class) that is currently edited
	 */
	protected Classifier m_classifier;

	/**
	 * name of CDT project in which the generated code is stored.
	 */
	protected String m_projectName;

	/**
	 * reference to code generator
	 */
	protected ILangCodegen m_codegen;

	public SyncJDTtoModel(IEditorInput input, Classifier classifier, String projectName, String generatorID) {
		m_input = input;
		m_classifier = classifier;
		m_projectName = projectName;
		m_codegen = LanguageCodegen.getGenerator(TextEditorConstants.JAVA, generatorID);
	}

	public void syncJDTtoModel(ITypeRoot root) {
		if (root instanceof ICompilationUnit) {
			icu = (ICompilationUnit) root;
			CommandSupport.exec(m_classifier, "update model from JDT", this); //$NON-NLS-1$
		}
	}

	@Override
	public void run() {
		ModelListener.syncFromEditor = true;

		// In 2020-06, get operation AST.getJLSLatest() is not available, therefore
		// JLS14 is used (but causes a deprecation warning on newer Eclipse versions).
		// TODO: replace when 2020-06 support is dropped
		ASTParser parser = ASTParser.newParser(AST.JLS14);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setSource(icu);
		parser.setResolveBindings(true);
		astCU = (CompilationUnit) parser.createAST(null);

		try {
			examineChildren(icu);
			updateJavaImport();
		} catch (JavaModelException e) {
			Activator.log.error(e);
		}

		ModelListener.syncFromEditor = false;
	}

	/**
	 * Examine the children of a translation unit in order to extract the
	 * methods that are defined within the unit
	 *
	 * @param parent
	 *            a Java model parent
	 * @throws JavaModelException
	 */
	public void examineChildren(IParent parent)
			throws JavaModelException {

		for (IJavaElement child : parent.getChildren()) {
			if (child instanceof IParent) {
				examineChildren((IParent) child);
			}
			if (child instanceof IType) {
				IType clazz = (IType) child;
				TypeDeclaration astClazz = (TypeDeclaration) astCU.findDeclaringNode(clazz.getKey());
				List<Property> refAttributes = new ArrayList<Property>();
				List<Operation> refOperations = new ArrayList<Operation>();
				List<Property> existingAttributes = new ArrayList<Property>();
				List<Operation> existingOperations = new ArrayList<Operation>();
				existingAttributes.addAll(m_classifier.getAttributes());
				existingOperations.addAll(m_classifier.getOperations());
				
				int position = 0;
				for (FieldDeclaration field : astClazz.getFields()) {
					updateAttribute(field, refAttributes, position);
					position++;
				}
				position = 0;
				for (MethodDeclaration method : astClazz.getMethods()) {
					updateMethod(method, refOperations, position);
					position++;
				}
				
				// delete attributes and operations that are no longer referenced from the code
				for (Property attribute : existingAttributes) {
					if (!refAttributes.contains(attribute)) {
						attribute.destroy();
					}
				}
				for (Operation operation : existingOperations) {
					if (!refOperations.contains(operation)) {
						// destroy operation and associated behaviors
						List<Behavior> behaviors = new ArrayList<Behavior>();
						behaviors.addAll(operation.getMethods());
						for (Behavior behavior: behaviors) {
							behavior.destroy();
						}
						operation.destroy();
					}
				}
			}
		}
	}

	/**
	 * update the contents of the JavaImport stereotype
	 *
	 * @throws JavaModelException
	 */
	public void updateJavaImport() throws JavaModelException {
		String body = StringConstants.EMPTY;

		for (IImportDeclaration import_ : icu.getImports()) {
			body += import_.getSource();
		}
		if (body.length() > 0) {
			Import importSt = StereotypeUtil.applyApp(m_classifier, Import.class);
			if (importSt != null) {
				importSt.setManualImports(body);
			}
		}
	}

	/**
	 * Update a method in the model based on the qualified name.
	 *
	 * @param position
	 *            The position of the method within the file. Used to identify
	 *            renaming operations
	 * @param method
	 *            the AST method declaration
	 * @return the operation or the behavior within the model that got updated. The latter is returned in
	 *         case of behaviors that do not have a specification (e.g. the effect of a transition).
	 * @throws JavaModelException
	 */
	public NamedElement updateMethod(MethodDeclaration method, List<Operation> refOperations, int position) throws JavaModelException {

		String qualifiedName = method.getName().getFullyQualifiedName();

		String names[] = qualifiedName.split("\\."); //$NON-NLS-1$
		String name = names[names.length - 1];

		Operation operation = m_classifier.getOperation(name, null, null);
		if (operation == null) {
			if (m_classifier instanceof Interface) {
				operation = ((Interface) m_classifier).createOwnedOperation(name, null, null);
			} else if (m_classifier instanceof Class) {
				operation = ((Class) m_classifier).createOwnedOperation(name, null, null);
			}
		}
		if (operation != null) {
			updateParameters(method, operation);
			updateComment(method, operation);
			Behavior behavior = null;
			if (operation.getMethods().size() > 0) {
				behavior = operation.getMethods().get(0);
			} else if (m_classifier instanceof Class) {
				behavior = ((Class) m_classifier).createOwnedBehavior(name, UMLPackage.eINSTANCE.getOpaqueBehavior());
				operation.getMethods().add(behavior);
			}
			if (behavior instanceof OpaqueBehavior) {
				// updateParameters(method, behavior);
				String body = getBody(method);
				OpaqueBehavior ob = (OpaqueBehavior) behavior;
				BehaviorUtils.set(ob, body, "Java"); //$NON-NLS-1$
			}
			refOperations.add(operation);
		}
		return operation;
	}

	/**
	 * update an attribute from an AST field
	 * 
	 * @param field
	 *            an AST field representing the attribute
	 * @param position
	 *            position of the attribute within the file (handle rename - currently unsupported)
	 */
	protected void updateAttribute(FieldDeclaration field, List<Property> refAttributes, int position) {
		VariableDeclaration varDecl = ASTUtils.getVarDeclaration(field);
		String name = varDecl.getName().toString();
		Property attribute = m_classifier.getAttribute(name, null);
		if (attribute == null) {
			if (m_classifier instanceof Class) {
				attribute = ((Class) m_classifier).createOwnedAttribute(name, null);
			}
		}
		if (attribute != null) {
			// set type
			String typeName = ASTUtils.getTypeName(field.getType());
			NamedElement ne = getJavaType(typeName);
			if (ne instanceof Type) {
				attribute.setType((Type) ne);
			}
			refAttributes.add(attribute);
		}
	}

	/**
	 * Update the parameters of an operation or a behavior
	 * 
	 * @param method
	 *            a JDT (AST) method declaration
	 * @param opOrBehavior
	 *            either an operation or a behavior of
	 */
	protected void updateParameters(MethodDeclaration method, NamedElement opOrBehavior) {
		List<Parameter> paramList = new ArrayList<Parameter>();
		List<Parameter> existingParamList = ParameterUtils.getOwnedParametersCopy(opOrBehavior);
		for (Object parObj : method.parameters()) {
			if (parObj instanceof SingleVariableDeclaration) {
				SingleVariableDeclaration parameter = (SingleVariableDeclaration) parObj;
				String paramName = parameter.getName().toString();
				Parameter umlParameter = ParameterUtils.getParameterViaName(existingParamList, paramName);
				if (umlParameter == null) {
					// does not exist, create
					org.eclipse.jdt.core.dom.Type type = parameter.getType();
					if (type instanceof ArrayType) {
						type = ((ArrayType) type).getElementType();
					}
					String typeName = ASTUtils.getTypeName(type);
					NamedElement ne = getJavaType(typeName);
					Type umlType = null;
					if (ne instanceof Type) {
						umlType = (Type) ne;
					}
					umlParameter = ParameterUtils.createOwnedParameter(opOrBehavior, paramName, umlType);
					applyParameterModifiers(parameter, umlParameter);
				}
				paramList.add(umlParameter);
			}
		}
		org.eclipse.jdt.core.dom.Type type = method.getReturnType2();
		if (type != null) {
			String typeName = ASTUtils.getTypeName(type);
			NamedElement ne = getJavaType(typeName);
			Type umlType = null;
			if (ne instanceof Type) {
				umlType = (Type) ne;
			}
			Parameter umlRetParam = ParameterUtils.getParameterViaName(existingParamList, null);
			if (umlRetParam == null && umlType != null) {
				umlRetParam = ParameterUtils.createReturnResult(opOrBehavior, umlType);
			}
			if (umlRetParam != null) {
				paramList.add(umlRetParam);
			}
		}

		for (Parameter existingParam : existingParamList) {
			if (!paramList.contains(existingParam)) {
				existingParam.destroy();
			}
		}

		// reorder parameter list
		ParameterUtils.resetParameters(opOrBehavior, paramList);
	}

	
	/**
	 * Update the multiplicity from an existing parameter
	 * 
	 * @param parameter
	 *            a new parameter (may be null)
	 * @param existingParameter
	 *            an existing parameter (may be null)
	 */
	protected void updateMultiplicity(Parameter parameter, Parameter existingParameter) {
		if (parameter != null && existingParameter != null) {
			parameter.setLower(existingParameter.getLower());
			parameter.setUpper(existingParameter.getUpper());
		}
	}

	/**
	 * Obtain the type of a parameter, checking (and loading) ANSI-C as well.
	 * 
	 * @param parameterName
	 * @param typeName
	 *            the name of a type (might be qualified, might be a simple type name)
	 * @param existingParameters
	 * @return
	 */
	protected Type getParameterType(String typeName, Parameter existingParameter) {
		return null;
	}

	/**
	 * Apply the modifiers for a parameter, notably the stereotypes of the Java profile
	 * 
	 * @param parameter
	 *            the CDT AST parameter specification
	 * @param umlParameter
	 *            the UML parameter (to which a stereotype should be applied)
	 */
	public void applyParameterModifiers(SingleVariableDeclaration parameter, Parameter umlParameter) {
		for (Object mod : parameter.modifiers()) {
			if (mod.toString().equals("final")) { //$NON-NLS-1$
				StereotypeUtil.apply(umlParameter, Final.class);
			}
		}
		if (parameter.isVarargs()) { 
			StereotypeUtil.apply(umlParameter, Variadic.class);
		}
	
		IVariableBinding binding = parameter.resolveBinding();
		if (binding != null && binding.getType().isArray()) {
			// use multiplicity by default, unless a listHint is present. Otherwise a != 1 multiplicity
			// might be mapped eventually a non-array type. Use Array stereotype in this case
			if (GenUtils.hasStereotypeTree(umlParameter, ListHint.class)) {
				StereotypeUtil.applyApp(umlParameter, Array.class);
			}
			else {
				umlParameter.setLower(0);
				umlParameter.setUpper(-1);
			}
		}
	}

	/**
	 * Obtain an operation from the model by using the name of a CDT method.
	 * If an operation of the given name does not exist, it might indicate that
	 * the method has been renamed.
	 * 
	 * @param name
	 *            the operation name within CDT
	 * @param parent
	 *            the parent of the CDT method within CDT editor model
	 * @param position
	 *            the position within the other methods. This information is used to locate methods
	 *            within the model that might have been renamed in the CDT editor.
	 * @return a UML operation
	 */
	public Operation getModelOperationFromName(String name, IParent parent, int position) {
		Operation operation = m_classifier.getOperation(name, null, null);

		if (operation == null) {
			// operation is not found via name in the model. try to locate the operation in the model at the same
			// "position" as the method in the file and
			// verify that this method does not have the same name as any method
			// in the CDT file.
			if (position < m_classifier.getOperations().size()) {
				operation = m_classifier.getOperations().get(position);
				String modelName = operation.getName();
				try {
					for (IJavaElement child : parent.getChildren()) {
						if (child instanceof IMethod) {
							String cdtName = ((IMethod) child).getElementName();
							if (cdtName.equals(modelName)) {
								// an existing operation in the CDT file already
								// has this name
								operation = null;
								break;
							}
						}
					}
				} catch (JavaModelException e) {
				}
			}
		}
		return operation;
	}

	public String getBody(MethodDeclaration method) throws JavaModelException {
		Block body = method.getBody();
		String source = getSource(body);
		// body contains enclosing { } which we need to remove (+1, -1).
		// TODO remove indentation will not be correct for methods in nested classes
		if (source.length() > 2) {
			return StringUtils.decreaseIndent(source.toCharArray(), 1, source.length() - 1, 8).trim();
		}
		return StringConstants.EMPTY;
	}

	/**
	 * update a comment of a named element. Besides the comment of the element itself, comments on contained
	 * parameters are handled.
	 * 
	 * @param method
	 *            a method declaration
	 * @param ne
	 *            a named element that is either an operation or a behavior (in order to update parameters)
	 * @throws JavaModelException
	 */
	public void updateComment(MethodDeclaration method, NamedElement ne) throws JavaModelException {
		String comment = getSource(method.getJavadoc());
		if (comment.startsWith(StringConstants.COMMENT_START)) {
			comment = comment.substring(3);
		}
		comment = comment.replaceAll("\n\\s+\\* ", StringConstants.EOL).//$NON-NLS-1$
				replace(StringConstants.COMMENT_END, StringConstants.EMPTY).trim();
		if (comment.length() > 0) {
			// filter @param
			int atParam = comment.indexOf(sAtParam);
			int atReturn = comment.indexOf(sAtReturn);
			// does atReturn appear before @atParam?
			int atParamOrReturn = (atReturn != -1 && (atReturn < atParam || atParam == -1)) ? atReturn : atParam;
			String commentMethodOnly = (atParamOrReturn != -1) ? comment.substring(0, atParamOrReturn).trim() : comment;

			while (atParam != -1) {
				int currentAtParam = atParam;
				atParam = comment.indexOf(sAtParam, atParam + 1);
				String commentParam = (atParam != -1) ? comment.substring(currentAtParam, atParam)
						: comment.substring(currentAtParam);
				Comment commentParamUML;
				int atParamName = sAtParam.length();

				while ((atParamName < commentParam.length())
						&& Character.isWhitespace(commentParam.charAt(atParamName))) {
					atParamName++;
				}
				int atParamNameEnd = atParamName;
				while ((atParamNameEnd < commentParam.length())
						&& !Character.isWhitespace(commentParam.charAt(atParamNameEnd))) {
					atParamNameEnd++;
				}
				if (atParamNameEnd < commentParam.length() - 1) {
					String parameterName = commentParam.substring(atParamName, atParamNameEnd);
					String commentParamText = commentParam.substring(atParamNameEnd).trim();
					Parameter parameter = null;
					if (ne instanceof BehavioralFeature) {
						parameter = ((BehavioralFeature) ne).getOwnedParameter(parameterName, null, false, false);
					} else if (ne instanceof Behavior) {
						parameter = ((Behavior) ne).getOwnedParameter(parameterName, null, false, false);
					}
					if (parameter != null) {
						EList<Comment> commentsParamUML = parameter.getOwnedComments();
						if (commentsParamUML.size() == 0) {
							commentParamUML = parameter.createOwnedComment();
							commentParamUML.getAnnotatedElements().add(commentParamUML);
						} else {
							commentParamUML = commentsParamUML.get(0);
						}
						commentParamUML.setBody(commentParamText);
					} else {
						// parameter is not found in model, e.g. either renamed
						// or not yet existing
						// store comment in operation comment
						commentMethodOnly += "\n " + sAtParam + parameterName + //$NON-NLS-1$
								" not found(!) " + commentParamText; //$NON-NLS-1$
					}
				}
			}
			if (commentMethodOnly.equals(StringConstants.STAR)) {
				commentMethodOnly = StringConstants.EMPTY;
			}
			// update/create comment, if a non-empty comment context has been detected.
			EList<Comment> commentsUML = ne.getOwnedComments();
			if (commentMethodOnly.length() > 0) {
				Comment commentUML;
				if (commentsUML.size() == 0) {
					commentUML = ne.createOwnedComment();
					commentUML.getAnnotatedElements().add(commentUML);
				} else {
					commentUML = commentsUML.get(0);
				}
				commentUML.setBody(commentMethodOnly);
			} else if (commentsUML.size() > 0) {
				// destroy first comment
				commentsUML.get(0).destroy();
			}
		}
	}

	/**
	 * Accessor
	 * 
	 * @return value of codegen attribute
	 */
	public ILangCodegen getCodeGen() {
		return m_codegen;
	}

	public String getSource(ASTNode node) throws JavaModelException {
		if (node != null) {
			int start = node.getStartPosition();
			int len = node.getLength();
			String source = icu.getSource();
			if (start + len < source.length()) {
				return source.substring(start, start + len);
			}
		}
		return StringConstants.EMPTY;
	}
	
	protected NamedElement getJavaType(String qualifiedName) {
		if (qualifiedName.equals(VOID_TYPE)) {		// specific handling of void
			return null;
		}
		NamedElement ne = ElementUtils.getQualifiedElementFromRS(m_classifier, defaultLibs, qualifiedName);
		if (ne == null) {
			String prefix = JavaGenUtils.JAVA_LANG.replace(StringConstants.DOT, Namespace.SEPARATOR);
			ne = ElementUtils.getQualifiedElementFromRS(m_classifier, defaultLibs, prefix + qualifiedName);
		}
		return ne;
	}
}

/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) Ansgar.Radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.languages.java.jdt.texteditor.editor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.ISourceReference;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.internal.ui.javaeditor.CompilationUnitEditor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.source.IOverviewRuler;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.papyrus.designer.languages.java.jdt.texteditor.Activator;
import org.eclipse.papyrus.designer.languages.java.jdt.texteditor.sync.ObtainIJavaElement;
import org.eclipse.papyrus.designer.languages.java.jdt.texteditor.sync.SyncJDTtoModel;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.infra.ui.lifecycleevents.DoSaveEvent;
import org.eclipse.papyrus.infra.ui.lifecycleevents.ILifeCycleEventsProvider;
import org.eclipse.papyrus.infra.ui.lifecycleevents.ISaveEventListener;
import org.eclipse.papyrus.infra.ui.services.EditorLifecycleEventListener;
import org.eclipse.papyrus.infra.ui.services.EditorLifecycleManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.NamedElement;

/**
 * A specialization of the JDT (Class) editor made for integration into Papyrus
 *
 */
@SuppressWarnings("restriction")
public class SyncJDTEditor extends CompilationUnitEditor {

	protected ILifeCycleEventsProvider lifeCycleEvents;

	protected ISaveEventListener preSaveEvent;

	/** the text listener */
	protected FocusListener focusListener;

	protected SyncJDTtoModel syncJava;

	// a bar in different colors indicating whether synchronization
	// is active.
	protected Composite syncBar;

	public static final RGB ORANGE = new RGB(240, 150, 100);

	public static final RGB GREEN = new RGB(150, 230, 100);

	public SyncJDTEditor() {
		super();
	}

	/**
	 *
	 * initialize editor data
	 *
	 * @param registry
	 *            Papyrus service registry
	 * @param syncJava
	 *            reference to class that synchronizes the editor content with the model
	 */
	public void setEditorData(final ServicesRegistry registry, final SyncJDTtoModel syncJava) {
		try {
			// synchronize, before the UML model is saved.
			this.syncJava = syncJava;
			lifeCycleEvents = registry.getService(ILifeCycleEventsProvider.class);
			syncBar.setBackground(new Color(Display.getDefault(), GREEN));
			preSaveEvent = new ISaveEventListener() {

				@Override
				public void doSaveAs(DoSaveEvent event) {
				}

				@Override
				public void doSave(DoSaveEvent event) {
					ITypeRoot root = getInputJavaElement();
					SyncJDTEditor.this.syncJava.syncJDTtoModel(root);
				}
			};
			lifeCycleEvents.addAboutToDoSaveListener(preSaveEvent);

			EditorLifecycleManager editorlifeCycle = registry.getService(EditorLifecycleManager.class);
			EditorLifecycleEventListener closeListener = new EditorLifecycleEventListener() {

				@Override
				public void postInit(IMultiDiagramEditor editor) {
				}

				@Override
				public void postDisplay(IMultiDiagramEditor editor) {
				}

				@Override
				public void beforeClose(IMultiDiagramEditor editor) {
					SyncJDTEditor.this.syncJava = null;
					syncBar.setBackground(new Color(Display.getDefault(), ORANGE));
				}
			};
			editorlifeCycle.addEditorLifecycleEventsListener(closeListener);

		} catch (ServiceException e) {
			Activator.log.error(e);
		}
	}

	/**
	 *
	 * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createSourceViewer(org.eclipse.swt.widgets.Composite, org.eclipse.jface.text.source.IVerticalRuler, int)
	 *
	 * @param parent
	 * @param ruler
	 * @param styles
	 * @return a source viewers
	 */
	@Override
	public ISourceViewer createJavaSourceViewer(Composite editorComposite, IVerticalRuler ruler, IOverviewRuler overviewRuler, boolean isOverviewRulerVisible, int styles, IPreferenceStore store) {

		Composite parent = editorComposite.getParent();
		// parent.setLayout(new GridLayout(2, false));
		syncBar = new Composite(parent, SWT.NONE);

		GridData barGD = new GridData();
		barGD.heightHint = 5;
		barGD.horizontalAlignment = GridData.FILL;
		syncBar.setLayoutData(barGD);

		final ISourceViewer viewer = super.createJavaSourceViewer(editorComposite, ruler, overviewRuler, isOverviewRulerVisible, styles, store);
		GridData editorGD = new GridData();
		editorGD.horizontalAlignment = GridData.FILL;
		editorGD.verticalAlignment = GridData.FILL;
		editorGD.grabExcessHorizontalSpace = true;
		editorGD.grabExcessVerticalSpace = true;
		// parent.getChildren()[1].setLayoutData(editorGD);

		syncBar.setBackground(new Color(Display.getDefault(), ORANGE));
		parent.pack();
		// Composite composite = parent;
		// while (composite != null) {
		// if (composite instanceof CTabFolder) {
		// CTabFolder tabFolder = (CTabFolder) composite;
		// CTabItem t = tabFolder.getItem(tabFolder.getItemCount() - 1);
		// Control tabFolders[] = tabFolder.getParent().getChildren();
		// if (tabFolders.length == 1) {
		// // TODO: create a 2nd tabFolder and move element. Unclear side-effects => not done
		// CTabFolder right = new CTabFolder(folder.getParent(), SWT.RIGHT);
		// }
		// }
		// composite = composite.getParent();
		// }

		focusListener = new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// potential problem for undo/redo!!
				if (isDirty() && syncJava != null) {
					// by saving, we assure that the editor automatically updates when the file is re-generated due
					// to model changes. Otherwise, the user will get a popup whether he wants to load changed the changed
					// file (and loose editor contents).
					// Currently, the is-dirty flag is used to detect whether contents need to be synchronized to the model.
					// If we stop saving automatically on focus lost, we would need to add another detection.
					//
					doSave(new NullProgressMonitor());
					ITypeRoot root = getInputJavaElement();
					syncJava.syncJDTtoModel(root);
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		};
		// register focus listener
		viewer.getTextWidget().addFocusListener(focusListener);

		return viewer;
	}

	/**
	 * Goto a specific element within the text editor. Currently, only methods are supported.
	 *
	 * @param element
	 *            a named UML element
	 */
	public void gotoElement(NamedElement element) {
		IJavaElement inputJE = getInputJavaElement();
		if (inputJE instanceof ICompilationUnit) {
			ICompilationUnit icu = (ICompilationUnit) inputJE;
			IJavaElement icElement = ObtainIJavaElement.getIJavaElement(syncJava.getCodeGen(), icu, element);
			if (icElement instanceof ISourceReference) {
				try {
					ISourceRange range = ((ISourceReference) icElement).getSourceRange();

					ISourceViewer viewer = getSourceViewer();
					viewer.revealRange(range.getOffset(), 1);
					viewer.setSelectedRange(range.getOffset(), range.getLength());
					return;
				} catch (CoreException e) {
					Activator.log.error(e);
				}
			}
		}
	}

	/**
	 *
	 * @see org.eclipse.ui.editors.text.TextEditor#dispose()
	 *
	 */
	@Override
	public void dispose() {
		// we remove the listener
		StyledText st = getSourceViewer().getTextWidget();
		if (st != null && focusListener != null) {
			st.removeFocusListener(focusListener);
		}

		// remove save event listener
		if (lifeCycleEvents != null) {
			lifeCycleEvents.removeAboutToDoSaveListener(preSaveEvent);
		}
	}
}

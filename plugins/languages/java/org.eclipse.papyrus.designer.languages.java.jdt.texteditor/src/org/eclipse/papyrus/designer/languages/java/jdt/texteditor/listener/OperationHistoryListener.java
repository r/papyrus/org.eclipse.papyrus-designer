/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) Ansgar.Radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.java.jdt.texteditor.listener;

import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.commands.operations.OperationHistoryEvent;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.languages.java.jdt.texteditor.sync.SyncModelToJDT;
import org.eclipse.uml2.uml.Classifier;

public class OperationHistoryListener implements IOperationHistoryListener {

	public static void init() {
		if (ohl == null) {
			ohl = new OperationHistoryListener();
			OperationHistoryFactory.getOperationHistory().addOperationHistoryListener(ohl);
		}
	}

	private static OperationHistoryListener ohl = null;

	@Override
	public void historyNotification(OperationHistoryEvent event) {
		int eventType = event.getEventType();
		if (eventType == OperationHistoryEvent.OPERATION_ADDED || eventType == OperationHistoryEvent.REDONE
				|| eventType == OperationHistoryEvent.UNDONE) {

			EList<Classifier> regenListCopy = new BasicEList<Classifier>(ModelListener.regenList);
			ModelListener.regenList.clear();

			// re-generate files from list - if they are still in a resource
			for (Classifier cl : regenListCopy) {
				if (cl != null && cl.eResource() != null) {
					// sync. assumes that standard C++ generator
					SyncModelToJDT.syncModelToJDT(cl, null);
				}
			}
		}
	}
}

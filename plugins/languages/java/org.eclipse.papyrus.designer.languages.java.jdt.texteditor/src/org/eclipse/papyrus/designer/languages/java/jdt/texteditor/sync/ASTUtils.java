package org.eclipse.papyrus.designer.languages.java.jdt.texteditor.sync;

import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.PrimitiveType;
import org.eclipse.jdt.core.dom.SimpleType;
import org.eclipse.jdt.core.dom.VariableDeclaration;

public class ASTUtils {
	public static final String JAVA_LIB_PREFIX = "JavaLibrary::"; //$NON-NLS-1$

	/**
	 * @param field
	 *            an AST field
	 * @return the associated variable declaration (assuming that the first fragment
	 *         of type VariableDeclaration is the the right one) or null
	 */
	public static VariableDeclaration getVarDeclaration(FieldDeclaration field) {
		for (Object obj : field.fragments()) {
			if (obj instanceof VariableDeclaration) {
				return (VariableDeclaration) obj;
			}
		}
		return null;
	}

	/**
	 * @param type
	 *            an AST type
	 * @return the (qualified) type name as string
	 * 
	 */
	public static String getTypeName(org.eclipse.jdt.core.dom.Type type) {
		Name name = null;
		if (type instanceof SimpleType) {
			name = ((SimpleType) type).getName();
			return name.toString();
		}
		else if (type instanceof PrimitiveType) {
			return JAVA_LIB_PREFIX + type.toString();
		}
		return ""; //$NON-NLS-1$
	}
}

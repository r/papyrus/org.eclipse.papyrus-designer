/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher - ansgar.radermacher@cea.fr CEA LIST - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.jdt.texteditor.sync;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IParent;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.ISourceReference;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;


public class RevealCurrentOperation {

	public RevealCurrentOperation(IEditorInput input, Classifier classifier, String projectName) {
		m_input = input;
		m_classifier = classifier;
		m_projectName = projectName;
	}

	public Element obtainSelectedElement(ITextSelection selection) {
		IJavaElement ice = null; // CDTUITools.getEditorInputCElement(m_input);

		if (ice instanceof ICompilationUnit) {
//			IJavaProject project = CoreModel.getDefault().getCModel().getCProject(m_projectName);
//
//			IIndex index = null;
//			try {
//				index = CCorePlugin.getIndexManager().getIndex(project);
//				index.acquireReadLock();
//
//				// index = CCorePlugin.getIndexManager().getIndex(project);
//				ICompilationUnit itu = (ICompilationUnit) ice;
//				// hack: force re-evaluation of AST node, requires modified CDT!
//				// Seems to be no longer required.
//				// ASTProvider.getASTProvider().fCache.setActiveElement(itu);
//
//				IASTTranslationUnit ast = itu.getAST(index, ICompilationUnit.AST_SKIP_INDEXED_HEADERS);
//				IASTNodeSelector selector = ast.getNodeSelector(null);
//
//				String opName = findOperation(itu, selector, itu, selection);
//				if (opName != null) {
//					int sep = opName.lastIndexOf("::"); //$NON-NLS-1$
//					if (sep != -1) {
//						opName = opName.substring(sep + 2);
//					}
//					Operation operation = m_classifier.getOperation(opName, null, null);
//					if (operation != null) {
//						return operation;
//					}
//				}
//
//			} catch (CModelException e) {
//				Activator.getDefault().getLog().log(e.getStatus());
//			} catch (Exception e) {
//				System.err.println(e);
//			} finally {
//				if (index != null) {
//					index.releaseReadLock();
//				}
//			}
		}
		return m_classifier;
	}

	/**
	 * Examine the children of a translation unit in order to extract the methods that are defined within
	 * the unit
	 *
	 * @param icu
	 * @param selector
	 * @param parent
	 * @throws CModelException
	 */
	public String findOperation(ICompilationUnit icu, IParent parent, ITextSelection selection) throws JavaModelException {

		for (IJavaElement child : parent.getChildren()) {
			if (child instanceof IParent) {
				// return findOperation(icu, selector, (IParent) child, selection);
			}
			ISourceRange range = null;
			if (child instanceof ISourceReference) {
				range = ((ISourceReference) child).getSourceRange();
			}
			if (child instanceof IMethod) {
				// function declaration is a superclass for method declaration (but need to trace functions differently?)
				String name = ((IMethod) child).getElementName();
				int pos = selection.getOffset();
				if ((pos >= range.getOffset()) && (pos < range.getOffset() + range.getLength())) {
					return name;
				}
			}
		}
		return null;
	}

	protected IEditorInput m_input;

	protected Classifier m_classifier;

	protected String m_projectName;
}

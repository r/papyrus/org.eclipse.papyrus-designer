/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.languages.java.jdt.project;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.internal.ui.JavaPlugin;
import org.eclipse.jdt.internal.ui.wizards.JavaProjectWizard;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jdt.ui.PreferenceConstants;
import org.eclipse.jdt.ui.wizards.NewJavaProjectWizardPageOne;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.m2e.core.ui.internal.wizards.MavenProjectWizard;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.AbstractSettings;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.MavenProject;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Supports the creation and configuration of JDT projects
 */
@SuppressWarnings("restriction")
public class JavaProjectSupport implements ILangProjectSupport {
	private int dialogStatus;
	private MavenProject mavenProjectDetails;

	/**
	 * Key managed by JavaUI, indicates whether module info should be generated
	 * Code may break, if String of key changes.
	 */
	protected String LAST_MODULE_NFO = JavaUI.ID_PLUGIN + ".last.selected.create.moduleinfo"; //$NON-NLS-1$

	/**
	 * Create a Java project. Caller should test before calling, whether the
	 * project exists already. The project disables the option to generate module-info.
	 * The code proposes to change the JDT preference SRCBIN_FOLDERS_IN_NEWPROJ, if it is configured to
	 * use project folders.
	 *
	 * @param projectName
	 * @return the created project or null (in case of Cancel)
	 */
	@Override
	public IProject createProject(String projectName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		IProject project = root.getProject(projectName);
		dialogStatus = 0;
		try {
			// assure that we are running an UI thread
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					boolean useSrcBin = PreferenceConstants.getPreferenceStore().getBoolean(PreferenceConstants.SRCBIN_FOLDERS_IN_NEWPROJ);
					if (!useSrcBin) {
						boolean changePrefs = MessageDialog.openQuestion(Display.getCurrent().getActiveShell(), "Java preference", //$NON-NLS-1$
								Messages.JavaProjectSupport_PREF_WARN);
						if (changePrefs) {
							PreferenceConstants.getPreferenceStore().setValue(PreferenceConstants.SRCBIN_FOLDERS_IN_NEWPROJ, true);
						}
					}

					final JavaProjectWizard wiz = new JavaProjectWizard();
					wiz.init(PlatformUI.getWorkbench(), null);

					WizardDialog wizDiag = new WizardDialog(Display.getCurrent().getActiveShell(), wiz);
					wizDiag.create();
					// disable module_info generation
					IDialogSettings ds = JavaPlugin.getDefault().getDialogSettings();
					ds.put(LAST_MODULE_NFO, false);

					final IWizardPage page = wiz.getPages()[0];
					if (page instanceof NewJavaProjectWizardPageOne) {
						((NewJavaProjectWizardPageOne) page).setProjectName(projectName);
					}
					dialogStatus = wizDiag.open();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			project = null;
		}
		if (dialogStatus == 1) {
			// corresponds to Cancel
			return null;
		}
		if ((project == null) || !project.exists()) {
			throw new RuntimeException(
					Messages.JavaProjectSupport_COULD_NOT_CREATE);
		}
		return project;
	}

	@Override
	public IProject createProject(String projectName, Package modelRoot) {
		mavenProjectDetails = UMLUtil.getStereotypeApplication(modelRoot, MavenProject.class);
		if (mavenProjectDetails == null) {
			return createProject(projectName);
		} else {
			return createMavenProject(projectName);
		}
	}

	/**
	 * Create a Maven instead of JDT project
	 * TODO: untested
	 * 
	 * @param projectName
	 *            the project name
	 * @return the created project or null
	 */
	protected IProject createMavenProject(String projectName) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		try {
			// Create Maven wizard
			final MavenProjectWizard mavenWiz = new MavenProjectWizard();
			mavenWiz.setWindowTitle(projectName);
			Display.getDefault().syncExec(new Runnable() {
				@Override
				public void run() {
					IWorkbench wb = PlatformUI.getWorkbench();
					mavenWiz.init(wb, new StructuredSelection());
					Shell shell = wb.getActiveWorkbenchWindow().getShell();
					WizardDialog dialog = new WizardDialog(shell, mavenWiz);
					dialog.create();
					dialog.open();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		IProject project = root.getProject(projectName);

		return project;
	}

	@Override
	public void setSettings(IProject project, AbstractSettings settings) {
		if (project != null && mavenProjectDetails != null) {
			CustomizePOMFile customizePOMFile = new CustomizePOMFile(project, mavenProjectDetails);
			customizePOMFile.execute();
		}
	}

	@Override
	public AbstractSettings initialConfigurationData() {
		return null;
	}

	@Override
	public void gatherConfigData(Classifier implementation, AbstractSettings settings) {
	}
}
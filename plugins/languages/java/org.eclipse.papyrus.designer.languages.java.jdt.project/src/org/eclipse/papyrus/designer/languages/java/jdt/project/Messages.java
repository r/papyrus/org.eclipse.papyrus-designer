package org.eclipse.papyrus.designer.languages.java.jdt.project;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = Messages.class.getPackageName() + ".messages"; //$NON-NLS-1$
	public static String JavaProjectSupport_COULD_NOT_CREATE;
	public static String JavaProjectSupport_PREF_WARN;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}

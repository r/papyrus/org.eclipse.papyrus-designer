/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/
 
package org.eclipse.papyrus.designer.languages.java.codegen.xtend

import org.eclipse.papyrus.designer.languages.java.codegen.utils.Modifier
import org.eclipse.uml2.uml.AttributeOwner
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Property
import static extension org.eclipse.papyrus.designer.languages.java.codegen.xtend.JavaTypedElement.javaType

class JavaAttribute {

	// return a list of owned attributes, empty set, if null
	static def getOwnedAttributes(Classifier cl) {
		val attributes = getOwnedAttributesWNull(cl)
		if (attributes === null) {
			emptySet
		}
		else {
			attributes
		}
	}
	
	/**
	 * @return a list of owned attributes, since this is not supported directly on a classifier, null if not available
	 */
	static def getOwnedAttributesWNull(Classifier cl) {	
		if (cl instanceof AttributeOwner) {
			(cl as AttributeOwner).ownedAttributes
		} else {
			//Sequence{}
		    return null
		}
	}

	static def defaultValue(Property attribute) {
		if (attribute.defaultValue !== null) {
			" =" + attribute.defaultValue.stringValue()
		}
	}

	static def javaAttributeDeclaration(Property attribute) '''
		«JavaDocumentation.javaElementDoc(attribute)»
		«Modifier.attributeModifiers(attribute)»«attribute.javaType» «attribute.name»«defaultValue(attribute)»;
	'''
}

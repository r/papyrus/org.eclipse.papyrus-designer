/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.utils;

import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.java.codegen.xtend.JavaOperations;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Default;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Final;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Native;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.StaticClassifier;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Strictfp;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Synchronized;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Transient;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Variadic;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.Volatile;
import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Feature;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.VisibilityKind;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Utility functions managing the "modifier" of an element
 */
public class Modifier {
	private static final String SYNCHRONIZED = "synchronized "; //$NON-NLS-1$
	private static final String STRICTFP = "strictfp "; //$NON-NLS-1$
	private static final String DEFAULT = "default "; //$NON-NLS-1$
	private static final String NATIVE = "native "; //$NON-NLS-1$
	private static final String ABSTRACT = "abstract "; //$NON-NLS-1$
	private static final String VOLATILE = "volatile "; //$NON-NLS-1$
	private static final String FINAL = "final ";  //$NON-NLS-1$
	private static final String TRANSIENT = "transient ";  //$NON-NLS-1$
	private static final String PRIVATE = "private "; //$NON-NLS-1$
	private static final String PUBLIC = "public "; //$NON-NLS-1$
	private static final String STATIC = "static "; //$NON-NLS-1$

	
	public static String modVariadic(Parameter parameter) {
		if (parameter != null) {
			if (UMLUtil.getStereotypeApplication(parameter, Variadic.class) != null) {
				return "..."; //$NON-NLS-1$
			}
		}
		
		return StringConstants.EMPTY;
	}
	
	public static String attributeModifiers(Property attribute) {
		String result = StringConstants.EMPTY;
		
		if (attribute.getInterface() == null) {
			result += visibilityModifier(attribute);
			result += staticModifier(attribute);
			result += finalModifier(attribute);
			result += transientModifier(attribute);
			result += volatileModifier(attribute);
		} else {
			result += PUBLIC;
			result += staticModifier(attribute);
			result += finalModifier(attribute);
		}
		
		return result;
	}
	
	public static String methodModifiers(Operation operation) {
		String result = StringConstants.EMPTY;
		
		if (JavaOperations.isConstructor(operation)) {
			if (operation.getInterface() == null) {
				if (operation.getOwner() instanceof Enumeration) {
					result += PRIVATE;
				} else {
					result += visibilityModifier(operation);
				}
			}
		} else {
			// Visibility
			if (!JavaOperations.isAbstract(operation)) { // Operation is not abstract (not part of interface and not abstract in abstract class)
				// Normal case
				// ==> public OR protected OR private OR static OR final OR synchronized OR (native XOR strictfp)
				result += visibilityModifier(operation);
				result += staticModifier(operation);
				result += finalModifier(operation);
				result += synchronizedModifier(operation);
				result += nativeModifier(operation);
				if (nativeModifier(operation).isEmpty()) {
					result += strictfpModifier(operation);
				}
			} else if (operation.getInterface() == null) { // Operation is abstract and in an abstract class (unless it is static which means it shouldn't pass validation)
				if (operation.isStatic()) {
					// Invalid abstract operation case: static and also abstract operation
					// ==> we consider only the static modifier and handle it as a normal case instead of abstract operation case
					result += visibilityModifier(operation);
					result += staticModifier(operation);
					result += finalModifier(operation);
					result += synchronizedModifier(operation);
					result += nativeModifier(operation);
					if (nativeModifier(operation).isEmpty()) {
						result += strictfpModifier(operation);
					}
				} else {
					// Abstract operation case
					// ==> only (public XOR protected) OR abstract modifiers are allowed
					if (operation.getVisibility() == VisibilityKind.PROTECTED_LITERAL) {
						result += visibilityModifier(operation);
					} else {
						result += PUBLIC;
					}
					result += abstractModifier(operation);
				}
			} else if (operation.getInterface() != null) { // Operation is in an interface
				// Interface case
				// ==> public OR ((static XOR default) OR (strictfp if static XOR default)) modifiers are allowed (note that abstract is useless)
				result += PUBLIC;
				if (operation.isStatic()) {
					result += staticModifier(operation);
					result += strictfpModifier(operation);
				} else {
					result += defaultModifier(operation);
					if (!defaultModifier(operation).isEmpty()) {
						result += strictfpModifier(operation);
					}
				}
			}
		}
		
		return result;
	}
	
	public static String parameterModifiers(Parameter parameter) {
		if (UMLUtil.getStereotypeApplication(parameter, Final.class) != null) {
			return FINAL;
		}
		
		return StringConstants.EMPTY;
	}
	
	private static String visibilityModifier(NamedElement element) {
		if (element.getVisibility() == VisibilityKind.PACKAGE_LITERAL) {
			return StringConstants.EMPTY; // UML package visibility is default visibility in Java
		}
		return element.getVisibility().toString().toLowerCase() + StringConstants.SPACE;
	}
	
	private static String transientModifier(Element element) {
		if (UMLUtil.getStereotypeApplication(element, Transient.class) != null) {
			return TRANSIENT;
		}
		
		return StringConstants.EMPTY;
	}
	
	private static String staticModifier(Feature element) {
		if (element.isStatic()) {
			if (element.getOwner() != null) {
				if (element.getOwner().getOwner() instanceof Classifier) { // element is a feature of an inner classifier
					if (UMLUtil.getStereotypeApplication(element.getOwner(), StaticClassifier.class) != null) {
						return STATIC;
					}
				} else { // element is a feature of a normal classifier
					return STATIC;
				}
			}
		}
		
		return StringConstants.EMPTY;
	}
	
	private static String finalModifier(Feature element) {
		if (element.isLeaf()) {
			return FINAL;
		}
		return StringConstants.EMPTY;
	}
	
	private static String volatileModifier(Element element) {
		if (element instanceof Feature && ((Feature) element).isLeaf()) {
			return StringConstants.EMPTY;
		}
		
		if (UMLUtil.getStereotypeApplication(element, Volatile.class) != null) {
			return VOLATILE;
		}
		
		return StringConstants.EMPTY;
	}
	
	private static String abstractModifier(BehavioralFeature element) {
		if (element.isAbstract()) {
			return ABSTRACT;
		}
		return StringConstants.EMPTY;
	}
	
	private static String synchronizedModifier(Element element) {
		if (UMLUtil.getStereotypeApplication(element, Synchronized.class) != null) {
			return SYNCHRONIZED;
		}
		return StringConstants.EMPTY;
	}
	
	private static String nativeModifier(Element element) {
		if (UMLUtil.getStereotypeApplication(element, Native.class) != null) {
			return NATIVE;
		}
		return StringConstants.EMPTY;
	}
	
	private static String strictfpModifier(Element element) {
		if (UMLUtil.getStereotypeApplication(element, Strictfp.class) != null) {
			return STRICTFP;
		}
		return StringConstants.EMPTY;
	}
	
	private static String defaultModifier(Element element) {
		if (UMLUtil.getStereotypeApplication(element, Default.class) != null) {
			return DEFAULT;
		}
		return StringConstants.EMPTY;
	}
}

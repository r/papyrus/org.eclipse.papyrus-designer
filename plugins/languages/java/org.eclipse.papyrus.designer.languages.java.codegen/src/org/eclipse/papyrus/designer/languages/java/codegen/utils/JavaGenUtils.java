/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.External;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen;
import org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.ExternLibrary;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.ClassifierTemplateParameter;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.ParameterableElement;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameter;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Some utilities specific to Java code generation
 */
public class JavaGenUtils {

	public static final String JAVA_LANG = "java.lang."; //$NON-NLS-1$

	private static NamedElement currentNE;

	/**
	 * Map that stores the existing imports. Used to avoid duplicate imports
	 * and assure that elements with the same short-name are only done for one of these
	 */
	public static Map<NamedElement, EList<String>> imports;

	public static void resetImports() {
		imports = new HashMap<NamedElement, EList<String>>();
	}

	/**
	 * The standard UML and MARTE libraries provide some primitives types that need to be be translated to Java
	 *
	 * @param type
	 *            a primitive type
	 * @return the name of a standard type, or null if the type does not need specific translation
	 */
	public static String getStdtypes(PrimitiveType type) {
		Object owner = type.getOwner();
		String owningPkgName = StringConstants.EMPTY;
		if (owner instanceof Package) {
			owningPkgName = ((Package) owner).getName();
		}
		if (owningPkgName.equals("PrimitiveTypes") || // used in UML >= 2.4 //$NON-NLS-1$
				owningPkgName.equals("UMLPrimitiveTypes") || // used in UML < 2.4 //$NON-NLS-1$
				owningPkgName.equals("MARTE_PrimitivesTypes")) { //$NON-NLS-1$
			String td = StringConstants.EMPTY;
			String name = type.getName();

			if (name.equalsIgnoreCase("Integer")) { //$NON-NLS-1$
				td = "int"; //$NON-NLS-1$
			} else if (name.equalsIgnoreCase("Unlimited Natural")) { //$NON-NLS-1$
				td = "long"; //$NON-NLS-1$
			} else if (name.equalsIgnoreCase("Real")) { //$NON-NLS-1$
				td = "float"; //$NON-NLS-1$
			} else if (name.equalsIgnoreCase("String")) { //$NON-NLS-1$
				td = "String"; //$NON-NLS-1$
			} else {
				td = name.toLowerCase();
			}
			return td;
		}
		return null;
	}

	/**
	 * Return a kind of qualifiedName, except if
	 * - The named element has the stereotype External or NoCodeGen
	 * - The named element is a primitive type that has no further definition via a stereotype
	 * - The passed named element is defined in the current namespace (package)
	 *
	 * @param ne
	 *            a name element
	 * @param context
	 *            scope in which ne is used (classifier for which code is generated)
	 * @return the qualified name
	 */
	public static String javaQualifiedName(NamedElement ne) {
		if (ne == null) {
			return "undefined"; //$NON-NLS-1$
		}
		if (GenUtils.hasStereotypeTree(ne, NoCodeGen.class)) {
			return ne.getName();
		} else if (ne instanceof PrimitiveType) {
			// always return short name for primitive type, special treatment for
			// some standard UML types.
			String stdType = getStdtypes((PrimitiveType) ne);
			if (stdType != null) {
				return stdType;
			} else {
				return ne.getName();
			}
		} else if (ne.getOwner() instanceof ClassifierTemplateParameter) {
			// return short name for template in Type
			return ne.getName();
		} else if (ne instanceof Classifier && !((Classifier) ne).getTemplateBindings().isEmpty()) {
			TemplateBinding templateBinding = ((Classifier) ne).getTemplateBindings().get(0);
			TemplateSignature signature = templateBinding.getSignature();

			String specializedTypeName = StringConstants.EMPTY;

			if (signature != null && signature.getTemplate() instanceof Classifier) {
				specializedTypeName = javaQualifiedName((Classifier) signature.getTemplate(), ne);

				List<TemplateParameter> templateParameters = signature.getOwnedParameters();
				Map<TemplateParameter, NamedElement> parameterToClassMap = new HashMap<TemplateParameter, NamedElement>();

				List<TemplateParameterSubstitution> substitutions = templateBinding.getParameterSubstitutions();
				for (TemplateParameterSubstitution substitution : substitutions) {
					if (substitution.getFormal() != null
							&& templateParameters.contains(substitution.getFormal())
							&& substitution.getActual() instanceof NamedElement) {
						parameterToClassMap.put(substitution.getFormal(), (NamedElement) substitution.getActual());
					}
				}

				if (parameterToClassMap.size() == templateParameters.size()) {
					specializedTypeName += "<"; //$NON-NLS-1$

					for (TemplateParameter templateParameter : templateParameters) {
						NamedElement substitutionType = parameterToClassMap.get(templateParameter);
						specializedTypeName += javaQualifiedName(substitutionType, ne) + ", "; //$NON-NLS-1$
					}

					specializedTypeName = specializedTypeName.substring(0, specializedTypeName.length() - 2);
					specializedTypeName += ">"; //$NON-NLS-1$
				}

				return specializedTypeName;
			}
		}

		// Get qualified name and remove root name if root is a <<Project>>
		String qName = GenUtils.getFullName(ne, StringConstants.DOT, false);
		ExternLibrary el = GenUtils.getApplicationTree(ne, ExternLibrary.class);
		if (el != null && el.getPrefix() != null) {
			// if a prefix is specified on an external library, calculate the qualified name
			// rather than relying on the UML qualified name
			qName = el.getPrefix() + StringConstants.DOT + ne.getName();
		}
		External extJava = UMLUtil.getStereotypeApplication(ne, org.eclipse.papyrus.designer.languages.java.profile.PapyrusJava.External.class);
		External extCommon = UMLUtil.getStereotypeApplication(ne, External.class);
		// Use <<External>> from Java profile with priority compared to stereotype from common profile
		// (if multiple External stereotypes for different languages are applied)
		if (extJava != null && extJava.getName() != null) {
			qName = extJava.getName();
		}
		else if (extCommon != null && extCommon.getName() != null) {
			qName = extJava.getName();
		}
		return qName;
	}

	/**
	 * Return a qualifiedName or short name, depending on the context. Return
	 * a shortName for
	 * - elements imported
	 * - elements in the same package
	 * - elements in the special package java.lang
	 *
	 * @param ne
	 *            a name element
	 * @param context
	 *            scope in which ne is used (classifier for which code is generated)
	 * @return a short or qualified name
	 */
	public static String javaQualifiedName(NamedElement ne, Element context) {
		String qName = javaQualifiedName(ne);
		// First check that the ne is not a direct inner class of ns
		// Also check that ne does not have the same short name as a direct inner class of ns
		if (context instanceof Classifier) {
			for (Element directlyOwnedElement : context.getOwnedElements()) {
				if (directlyOwnedElement instanceof Enumeration
						|| directlyOwnedElement instanceof Interface
						|| directlyOwnedElement.eClass().equals(UMLFactory.eINSTANCE.getUMLPackage().getClass_())) {
					if (((Classifier) directlyOwnedElement).getQualifiedName().equals(ne.getQualifiedName())) {
						return ne.getName();
					} else if (((Classifier) directlyOwnedElement).getName().equals(ne.getName())) {
						return qName;
					}
				}
			}
		}

		// is the NE defined in the same namespace as the context?
		if (context == null || ne.getOwner() == context.getOwner() || isJavaLang(qName)) {
			return ne.getName();
		}

		// Then check that ne hasn't been imported
		EList<String> importsOfCurrentNs = imports.get(currentNE);
		if (importsOfCurrentNs != null) {
			for (String importOfCurrentNs : importsOfCurrentNs) {
				if (importOfCurrentNs.equals(qName)) {
					// ne is imported in the current ns, so we use its short name
					return ne.getName();
				}
			}
		}

		// We return the qualified name of ne otherwise
		return qName;
	}

	/**
	 * Do not create an import for elements in the java.lang package
	 * 
	 * @param qName
	 *            a qualified name
	 * @return true, if within java.lang
	 */
	public static boolean isJavaLang(String qName) {
		return qName.startsWith(JAVA_LANG) &&
				qName.indexOf(StringConstants.DOT, JAVA_LANG.length()) == -1; // check whether not a sub-package
	}

	/**
	 * Returns the string that is used within a Java template declaration, e.g. the "Class XY" in template<class XY>.
	 *
	 * @return the template type formated as string
	 */
	public static String getTemplateTypeName(TemplateParameter templateParam) {
		String name = StringConstants.EMPTY;

		// Retrieve name of the ParameteredElement (when possible = it is a NamedElement
		ParameterableElement pElt = templateParam.getParameteredElement();
		if ((pElt != null) && (pElt instanceof NamedElement)) {
			name = ((NamedElement) pElt).getName();
		} else {
			name = "undefined"; //$NON-NLS-1$
		}

		return (name);
	}


	/**
	 * Return a Java namespace definition for a named element
	 *
	 * @param ne
	 *            a named element
	 * @return a Java namespace definition for a named element
	 */
	public static String getNamespace(NamedElement ne) {
		String namespace = StringConstants.EMPTY;
		for (Namespace ns : ne.allNamespaces()) {
			if (ns.getOwner() != null) {
				String nsName = ns.getName();
				if (!namespace.equals(StringConstants.EMPTY)) {
					nsName += "::"; //$NON-NLS-1$
				}
				namespace = nsName + namespace;
			}
		}
		if (!namespace.equals(StringConstants.EMPTY)) {
			namespace = "\n" + "using namespace " + namespace + ";\n"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		}
		return namespace;
	}

	/**
	 * Update the currentNS (used to organize imports and types)
	 * 
	 * @param ne
	 *            a named element
	 */
	public static void openNS(NamedElement ne) {
		currentNE = ne;
	}
}

/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.preferences;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.papyrus.designer.languages.java.codegen.Activator;

/**
 * Utility class that returns the preference values
 */
public class JavaCodeGenUtils {

	protected static IEclipsePreferences prefs = null;

	public static String getHeaderSuffix() {
		initPreferenceStore();
		return prefs.get(JavaCodeGenConstants.P_JAVA_SUFFIX_KEY, JavaCodeGenConstants.P_JAVA_SUFFIX_DVAL);
	}

	public static String getCommentHeader() {
		initPreferenceStore();
		return prefs.get(JavaCodeGenConstants.P_COMMENT_HEADER_KEY, JavaCodeGenConstants.P_COMMENT_HEADER_DVAL);
	}

	public static void initPreferenceStore() {
		if (prefs == null) {
			prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);

		}
	}
}

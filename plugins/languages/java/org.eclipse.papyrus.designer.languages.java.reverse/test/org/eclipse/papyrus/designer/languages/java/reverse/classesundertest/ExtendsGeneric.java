/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.java.reverse.classesundertest;

import java.util.ArrayList;

/**
 * @author dumoulin
 *
 */
public class ExtendsGeneric extends ArrayList<String> {

	/**
	 * Constructor.
	 *
	 */
	public ExtendsGeneric() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Extends with generic
	 * @author dumoulin
	 *
	 * @param <T>
	 */
	class A<T> extends ArrayList<T> {
		
	}
}

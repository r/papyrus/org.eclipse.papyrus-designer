/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.java.reverse.exception;

/**
 * @author cedric dumoulin
 *
 */
public class ImportNotFoundException extends NotFoundException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 *
	 * @param name The name for which no import is found.
	 */
	public ImportNotFoundException(String name) {
		super(name);
	}

	/**
	 * @see java.lang.Throwable#getMessage()
	 *
	 * @return the error message
	 */
	@Override
	public String getMessage() {
		return String.format("No import found for '%s'", super.getMessage()); //$NON-NLS-1$
	}
}

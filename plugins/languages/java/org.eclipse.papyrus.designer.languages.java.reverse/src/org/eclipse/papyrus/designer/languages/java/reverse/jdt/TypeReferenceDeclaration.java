/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.java.reverse.jdt;

import org.eclipse.jdt.core.dom.FieldDeclaration;

/**
 * Class carrying data about the Type of a Property, a Parameter,  ...
 * 
 * @author cedric dumoulin
 *
 */
public class TypeReferenceDeclaration {

	public enum MultiplicityKind {
		simple, array, collection;
	}
	
	MultiplicityKind multiplicityKind = MultiplicityKind.simple;
	
	/**
	 * Constructor.
	 *
	 * @param fieldDeclaration
	 */
	public TypeReferenceDeclaration(FieldDeclaration fieldDeclaration) {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the multiplicity kkin
	 */
	public MultiplicityKind getMultiplicityKind() {
		// TODO Auto-generated method stub
		return multiplicityKind;
	}

}

/*******************************************************************************
 * Copyright (c) 2013 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.java.codegen.ui.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.papyrus.designer.languages.java.codegen.Activator;
import org.eclipse.papyrus.designer.languages.java.codegen.preferences.JavaCodeGenConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Group;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;


/**
 * This class represents a preference page that is contributed to the
 * Preferences dialog. By subclassing <samp>FieldEditorPreferencePage </samp>,
 * we can use the field support built into JFace that allows us to create a page
 * that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the preference store that belongs to the main plug-in class. That way, preferences can be accessed directly via the preference store.
 */

public class CodegenPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	private Document pCommentDoc = new Document();

	public CodegenPreferencePage() {
		super(GRID);
		IPreferenceStore store = new ScopedPreferenceStore(InstanceScope.INSTANCE, Activator.PLUGIN_ID);
		setPreferenceStore(store);
		setDescription("This preferences page allows to customize Papyrus Java code generation"); //$NON-NLS-1$
	}

	public void addTextField(String name, String label, Document currentDoc) {
		// ///////////////////////////////////////////////////////////////////////
		// Create a Group for the text
		// ///////////////////////////////////////////////////////////////////////
		Group txtGroup = new Group(getFieldEditorParent(), SWT.RESIZE);
		txtGroup.setLayout(new FillLayout());
		txtGroup.setText(label);
		GridData gd = new GridData(GridData.FILL_BOTH /* FILL_HORIZONTAL */);
		// gd.heightHint = 250;
		gd.horizontalSpan = 2;
		txtGroup.setLayoutData(gd);

		// Text area
		SourceViewer txtViewer = new SourceViewer(txtGroup, null, SWT.MULTI
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);

		txtViewer.setDocument(currentDoc);

		// Retrieving existing preference
		String content = getPreferenceStore().getString(name);

		// Loading preference in txt zone
		currentDoc.set(content);

	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common
	 * GUI blocks needed to manipulate various types of preferences. Each field
	 * editor knows how to save and restore itself.
	 */

	@Override
	public void createFieldEditors() {

		addField(new StringFieldEditor(JavaCodeGenConstants.P_JAVA_SUFFIX_KEY,
				"Suffix for Java files ", getFieldEditorParent())); //$NON-NLS-1$
		addField(new StringFieldEditor(JavaCodeGenConstants.P_PROJECT_PREFIX_KEY,
				"Prefix for generated Java (JDT) projects ", getFieldEditorParent())); //$NON-NLS-1$

		// Add P_COMMENT_HEADER
		addTextField(JavaCodeGenConstants.P_COMMENT_HEADER_KEY, "Header for generated files", //$NON-NLS-1$
				pCommentDoc);
	}

	@Override
	public void init(IWorkbench workbench) {
	}

	@Override
	public void performDefaults() {

		// initialize comment field from default
		pCommentDoc.set(getPreferenceStore().getDefaultString(JavaCodeGenConstants.P_COMMENT_HEADER_KEY));
		super.performDefaults();
	}

	@Override
	public boolean performOk() {

		// Store txt for P_COMMENT_HEADER
		String currentComment = pCommentDoc.get();
		getPreferenceStore().setValue(JavaCodeGenConstants.P_COMMENT_HEADER_KEY, currentComment);

		return super.performOk();
	}
}

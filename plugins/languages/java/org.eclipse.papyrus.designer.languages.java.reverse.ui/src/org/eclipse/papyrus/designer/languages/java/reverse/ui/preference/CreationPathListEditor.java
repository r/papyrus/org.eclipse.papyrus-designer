/*****************************************************************************
 * Copyright (c) 2014 Jonathan Geoffroy
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * Jonathan Geoffroy	geoffroy.jonathan@gmail.com		initial implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.languages.java.reverse.ui.preference;

import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * A ListEditor implementation. When user push "add" button, open a dialog box which can validate/invalidate the input value for Creation Path. <br>
 * Used to add a Creation Path value into Eclipse preferences
 * 
 * @author Jonathan Geoffroy
 *
 */
public class CreationPathListEditor extends ListEditor {

	private static final String ADD_CREATION_PATH = "Add creation path"; //$NON-NLS-1$
	public static final String SPLIT_STRING = ":"; //$NON-NLS-1$

	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param labelText
	 * @param parent
	 */
	public CreationPathListEditor(String name, String labelText, Composite parent) {
		super(name, labelText, parent);
	}

	@Override
	protected String createList(String[] items) {
		StringBuffer str = new StringBuffer();
		for(String item : items) {
			str.append(item + SPLIT_STRING);
		}
		return str.toString();
	}

	@Override
	protected String getNewInputObject() {
		InputDialog dialog = new InputDialog(getShell(), ADD_CREATION_PATH,
				ADD_CREATION_PATH, "", new CreationPathValidator()); //$NON-NLS-1$
		dialog.open();
		
		// If user clicked on "cancel" button, return null to NOT add empty value
		if(dialog.getReturnCode() == InputDialog.CANCEL) {
			return null;
		}
		return dialog.getValue();
	}

	@Override
	protected String[] parseString(String stringList) {
		return stringList.split(SPLIT_STRING);
	}

	/**
	 * Validator for creation path value
	 * 
	 * @author Jonathan Geoffroy
	 *
	 */
	private class CreationPathValidator implements IInputValidator {

		@Override
		/**
		 * Validate the input value if it contains 3 parts, separated by ';' character
		 * @see org.eclipse.jface.dialogs.IInputValidator#isValid(java.lang.String)
		 *
		 * @param newText
		 * @return
		 */
		public String isValid(String newText) {
			String[] splittedText = newText.split(";"); //$NON-NLS-1$
			if(! (splittedText.length == 3 || (splittedText.length == 2 && newText.charAt(newText.length() - 1) == ';'))) {
				return "pattern: includePath ; excludePath ; destination (use ';' as separator)"; //$NON-NLS-1$
			}
			return null;
		}

	}
}

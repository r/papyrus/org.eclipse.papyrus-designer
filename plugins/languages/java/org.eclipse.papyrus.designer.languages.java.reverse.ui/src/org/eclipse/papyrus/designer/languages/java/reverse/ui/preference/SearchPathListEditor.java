/*****************************************************************************
 * Copyright (c) 2014 Jonathan Geoffroy
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 * Jonathan Geoffroy	geoffroy.jonathan@gmail.com		initial implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.languages.java.reverse.ui.preference;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.swt.widgets.Composite;

/**
 * A ListEditor implementation. When user push "add" button, open a dialog box which can validate/invalidate the input value for Search Path. <br>
 * Used to add a Search Path value into Eclipse preferences
 * 
 * @author Jonathan Geoffroy
 *
 */
@SuppressWarnings("restriction")
public class SearchPathListEditor extends ListEditor {

	private static final String SEMI = ";"; //$NON-NLS-1$

	/**
	 * Constructor.
	 * 
	 * @param name
	 * @param labelText
	 * @param parent
	 */
	public SearchPathListEditor(String name, String labelText, Composite parent) {
		super(name, labelText, parent);
	}

	@Override
	/**
	 * Functionality for New button.
	 * Shows a dialog to select a search path entry and return it.
	 */
	protected String getNewInputObject() {
		InputDialog dialog = new InputDialog(getShell(), "Search path", "Enter path entry", "", null);
		dialog.open();
		return dialog.getValue();
	}

	@Override
	protected String createList(String[] items) {
		StringBuffer str = new StringBuffer();
		for(String item : items) {
			str.append(item + SEMI);
		}
		return str.toString();
	}

	/*
	 * (non-Javadoc)
	 * initialize list of items
	 * 
	 * @see org.eclipse.jface.preference.ListEditor#parseString(java.lang.String)
	 */
	@Override
	protected String[] parseString(String stringList) {
		return stringList.split(SEMI);
	}

}

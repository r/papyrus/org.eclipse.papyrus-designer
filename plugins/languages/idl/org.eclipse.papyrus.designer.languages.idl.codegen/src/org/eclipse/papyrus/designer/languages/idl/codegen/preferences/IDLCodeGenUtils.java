/*******************************************************************************
 * Copyright (c) 2016, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.idl.codegen.preferences;

import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.papyrus.designer.languages.idl.codegen.Activator;

/**
 * Utility class that returns the preference values
 * 
 */

public class IDLCodeGenUtils {

	protected static IEclipsePreferences prefs = null;

	public static String getIDLSuffix() {
		initPreferenceStore();
		return prefs.get(IDLCodeGenConstants.P_IDL_SUFFIX_KEY, IDLCodeGenConstants.P_IDL_SUFFIX_DVAL);
	}

	public static String getCommentHeader() {
		initPreferenceStore();
		return prefs.get(IDLCodeGenConstants.P_IDL_COMMENT_HEADER_KEY, IDLCodeGenConstants.P_IDL_COMMENT_HEADER_DVAL);
	}

	public static void initPreferenceStore() {
		if(prefs  == null) {
			prefs = DefaultScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		}
	}
}

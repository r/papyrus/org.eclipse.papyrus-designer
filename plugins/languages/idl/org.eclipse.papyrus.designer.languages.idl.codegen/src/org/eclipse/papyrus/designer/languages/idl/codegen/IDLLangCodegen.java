/*******************************************************************************
 * Copyright (c) 2006 - 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - ansgar.radermacher@cea.fr   initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.idl.codegen;

import org.eclipse.core.resources.IProject;
import org.eclipse.papyrus.designer.languages.common.base.CommonLangCodegen;
import org.eclipse.papyrus.designer.languages.common.base.ModelElementsCreator;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.MethodInfo;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.SyncInformation;
import org.eclipse.papyrus.designer.languages.idl.codegen.preferences.IDLCodeGenUtils;
import org.eclipse.papyrus.designer.languages.idl.codegen.transformation.IDLModelElementsCreator;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * IDL language support
 *
 */
public class IDLLangCodegen extends CommonLangCodegen implements ILangCodegen {

	@Override
	public String getDescription() {
		return "An IDL generator."; //$NON-NLS-1$
	}

	@Override
	public boolean isEligible(Element modelElement) {
		if (modelElement instanceof Classifier) {
			return true;
		}
		return false;
	}

	@Override
	public String getSuffix(FILE_KIND fileKind) {
		return IDLCodeGenUtils.getIDLSuffix();
	}
	
	@Override
	public IProject getTargetProject(PackageableElement pe, boolean createIfMissing) {
		//return LocateCProject.getTargetProject(pe, createIfMissing);
		return null;
	}
	
	protected ModelElementsCreator newCreator(IProject project, PackageableElement pe) {
		return new IDLModelElementsCreator(project);
	}

	// TODO: provide a suitable implementation
	@Override
	public SyncInformation getSyncInformation(String methodName, String body) {
		return null;
	}

	// TODO: provide a suitable implementation
	@Override
	public MethodInfo getMethodInfo(NamedElement operationOrBehavior) {
		return null;
	}
}

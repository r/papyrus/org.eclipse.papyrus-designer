/*******************************************************************************
 * Copyright (c) 2016 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.idl.codegen.opensplice.preferences;


public class IDLCodeGenConstants {
	
	/**
	 * Suffix for generated IDL files
	 */
	public static final String P_IDL_SUFFIX = "idlSuffix"; //$NON-NLS-1$

	/**
	 * User defined comment header in generated files
	 */
	public static final String P_IDL_COMMENT_HEADER = "idlCommentHeader"; //$NON-NLS-1$

}

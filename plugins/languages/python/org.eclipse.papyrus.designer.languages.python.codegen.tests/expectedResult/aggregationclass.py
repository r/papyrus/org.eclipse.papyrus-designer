from .elementclass2 import ElementClass2
from .elementclass1 import ElementClass1

class AggregationClass:

	def __init__(self):
		self.elementclass2: ElementClass2 = None
		self.elementclass1: ElementClass1 = None


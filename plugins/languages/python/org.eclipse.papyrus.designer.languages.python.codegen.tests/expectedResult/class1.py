class Class1:

	static_var1: set = set()

	def __init__(self):
		self.var1: int = 0
		self.__var2: float = 0.0
		self._var3: boolean = None
		self.var4: str = ''

	def __op1(self, a):
		return a + 1

	def op2(self, a, b):
		return a * b

	def _op3(self):
		pass

	@staticmethod
	def static_op():
		print('print something...')


### Interface ###
from abc import ABC, abstractmethod

class InterfaceTest(ABC):
	Property3: int = 2
	Property4: int = 0

	@abstractmethod
	def printHi(self):
		pass

	@abstractmethod
	def printHello(self):
		pass

from .elementclass1 import ElementClass1
from .elementclass2 import ElementClass2

class CompositionClass:

	def __init__(self):
		self.elementclass1 = [ElementClass1(), ElementClass1(), ElementClass1(), ElementClass1(), ElementClass1(), ElementClass1()]
		self.elementclass2 = ElementClass2()


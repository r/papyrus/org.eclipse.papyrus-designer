/*
 * Copyright (c) 2021 CEA.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.languages.python.codegen.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.common.base.TestInfo;
import org.eclipse.papyrus.designer.languages.common.testutils.FileComparison;
import org.eclipse.papyrus.designer.languages.common.testutils.RecursiveCopy;
import org.eclipse.papyrus.designer.languages.common.testutils.TestConstants;
import org.eclipse.papyrus.infra.services.openelement.service.OpenElementService;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.junit.utils.rules.HouseKeeper;
import org.eclipse.papyrus.junit.utils.rules.PapyrusEditorFixture;
import org.eclipse.papyrus.junit.utils.rules.PluginResource;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.uml2.uml.Package;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

@SuppressWarnings("nls")
@PluginResource("resources/PythonCodegenTest.di")
public class PythonCodegenTest {

	static {
		// This system property avoids opening dialogs during Papyrus operations. It must
		// be set before trying to load any of the Papyrus classes.
		TestInfo.runHeadless();
	}

	private static final String GENERATE_COMMAND_ID = "org.eclipse.papyrus.designer.languages.common.codegen.command";

	private static final String ModelName = "PythonCodegenTest.uml";

	private static final String GenProjectName = "org.eclipse.papyrus.pygen.PythonCodegenTest";

	private static final String GenFolderName = "pythoncodegentest";

	private static final String Class1_fragment = "_zi_sgLzBEeyQFo3GZXaVyQ";

	private static final String Class2_fragment = "_z2iTkLzBEeyQFo3GZXaVyQ";

	private static final String SpecialisedClass1_fragment = "_5z8GILzBEeyQFo3GZXaVyQ";

	private static final String SpecialisedClass2_fragment = "_5hPzoLzBEeyQFo3GZXaVyQ";

	private static final String AbstractClass_fragment = "__VtFkLzBEeyQFo3GZXaVyQ";

	private static final String SuperClass1_fragment = "_HIezcLzEEeymLfF7IeW-NA";

	private static final String SuperClass2_fragment = "_IJpVgLzEEeymLfF7IeW-NA";

	private static final String SuperClass3_fragment = "_JA4fgLzEEeymLfF7IeW-NA";

	private static final String DaughterClass_fragment = "_J4cZoLzEEeymLfF7IeW-NA";

	private static final String package1_fragment = "_WBLtELzEEeymLfF7IeW-NA";

	private static final String Pack1Class1_fragment = "_ZD9hkLzEEeymLfF7IeW-NA";

	private static final String Pack1Class2_fragment = "_fHnGELzEEeymLfF7IeW-NA";

	private static final String Pack1Enumeration1_fragment = "_g3YakLzEEeymLfF7IeW-NA";

	private static final String package1_1_fragment = "_jrObMLzEEeymLfF7IeW-NA";

	private static final String pack1_1class1_fragment = "_fM29A7zFEeymLfF7IeW-NA";

	private static final String Pack1_1Class2_fragment = "_ulPRsLzEEeymLfF7IeW-NA";

	private static final String Pack1_1Enumeration1_fragment = "_x0hRILzEEeymLfF7IeW-NA";

	private static final String package1_1_1_fragment = "_AIee8LzFEeymLfF7IeW-NA";

	private static final String Pack1_1_1Class1_fragment = "_FygCgLzFEeymLfF7IeW-NA";

	private static final String Pack1_1_1Class2_fragment = "_HT15cLzFEeymLfF7IeW-NA";

	private static final String DependencyTest_fragment = "_L5jgULzFEeymLfF7IeW-NA";

	private static final String CompositionClass_fragment = "_sx5PULzFEeymLfF7IeW-NA";

	private static final String AssociationClass_fragment = "_waQdILzFEeymLfF7IeW-NA";

	private static final String AggregationClass_fragment = "_xd0y8LzFEeymLfF7IeW-NA";

	private static final String ElementClass1_fragment = "_0TZ48LzFEeymLfF7IeW-NA";

	private static final String ElementClass2_fragment = "_2Br_sLzFEeymLfF7IeW-NA";

	private static final String InterfaceTest_fragment = "_GcLmwLzHEeymLfF7IeW-NA";

	private static final String EnumerationTest1_fragment = "_qBOzoLzHEeymLfF7IeW-NA";

	private static final String EnumerationTest2_fragment = "_STxNsCzyEe2vkqc4zQtyTg";

	private static final String EnumerationTestClass_fragment = "_rrIJoLzHEeymLfF7IeW-NA";

	private static final String DataType1_fragment = "_tBEwwMe7Eey0OLBTqXqG0A";

	private static final String modulepackage_fragment = "_AURQIO1VEeyoTvQE7Aqjjg";

	private static final String module1_fragment = "_evhZAO1SEeyPk7TeCGA9EQ";

	// private static final String MClass1_fragment = "_gk3KAO1SEeyPk7TeCGA9EQ";

	// private static final String MClass2_fragment = "_hWNucO1SEeyPk7TeCGA9EQ";

	private static final String Testmodule_fragment = "_iKPzcO1SEeyPk7TeCGA9EQ";

	private static IProject modelProject;

	private static IHandlerService handlerService;

	private static URI modelUri;

	private static URI genCodeUri;

	private static IMultiDiagramEditor multiEditor;

	private static OpenElementService elementActivator;

	private static Package model;

	@ClassRule
	public static HouseKeeper.Static houseKeeper = new HouseKeeper.Static();

	@ClassRule
	/** The model set fixture. */
	public final static PapyrusEditorFixture modelSetFixture = new PapyrusEditorFixture();

	@BeforeClass
	public static void loadProject() throws Exception {

		handlerService = (IHandlerService) PlatformUI.getWorkbench().getService(IHandlerService.class);

		// Obtain model project
		modelProject = modelSetFixture.getProject().getProject();

		String modelProjectName = modelProject.getName();

		// copy expected results folder into model project
		Bundle srcBundle = FrameworkUtil.getBundle(PythonCodegenTest.class);
		RecursiveCopy copier = new RecursiveCopy(houseKeeper);
		copier.copy(srcBundle, TestConstants.EXPECTED_RESULT, modelProject, "");

		// Setup the base modelUri for convenience in the test cases.
		modelUri = URI.createPlatformResourceURI("/" + modelProjectName + '/' + ModelName, true);
		assertNotNull(modelUri);

		model = modelSetFixture.getModel();
		assertNotNull(model);

		multiEditor = modelSetFixture.getEditor();
		assertNotNull(multiEditor);

		// Model elements are selected with the appropriate service.
		elementActivator = multiEditor.getServicesRegistry().getService(OpenElementService.class);
		assertNotNull(elementActivator);

		elementActivator.startService();

		// Setup the base genCodeUri for convenience in the test cases.
		genCodeUri = URI.createPlatformPluginURI("/" + modelProjectName + '/' + TestConstants.EXPECTED_RESULT, true);
		assertNotNull(genCodeUri);
	}

	@Test
	public void testGenerateClass1() throws Exception {
		assertGenerate(Class1_fragment);
		assertGeneratedMatchesExpected("class1.py");
	}

	@Test
	public void testGenerateClass2() throws Exception {
		assertGenerate(Class2_fragment);
		assertGeneratedMatchesExpected("class2.py");
	}

	@Test
	public void testGenerateSpecialisedClass1() throws Exception {
		assertGenerate(SpecialisedClass1_fragment);
		assertGeneratedMatchesExpected("specialisedclass1.py");
	}

	@Test
	public void testGenerateSpecialisedClass2() throws Exception {
		assertGenerate(SpecialisedClass2_fragment);
		assertGeneratedMatchesExpected("specialisedclass2.py");
	}

	@Test
	public void testGenerateAbstractClass() throws Exception {
		assertGenerate(AbstractClass_fragment);
		assertGeneratedMatchesExpected("abstractclass.py");
	}

	@Test
	public void testGenerateSuperClass1() throws Exception {
		assertGenerate(SuperClass1_fragment);
		assertGeneratedMatchesExpected("superclass1.py");
	}

	@Test
	public void testGenerateSuperClass2() throws Exception {
		assertGenerate(SuperClass2_fragment);
		assertGeneratedMatchesExpected("superclass2.py");
	}

	@Test
	public void testGenerateSuperClass3() throws Exception {
		assertGenerate(SuperClass3_fragment);
		assertGeneratedMatchesExpected("superclass3.py");
	}

	@Test
	public void testGenerateDaughterClass() throws Exception {
		assertGenerate(DaughterClass_fragment);
		assertGeneratedMatchesExpected("daughterclass.py");
	}

	@Test
	public void testGeneratepackage1_fragment() throws Exception {
		assertGenerate(package1_fragment);
		assertGeneratedMatchesExpected("__init__.py", "package1");
	}

	@Test
	public void testGeneratePack1Class1() throws Exception {
		assertGenerate(Pack1Class1_fragment);
		assertGeneratedMatchesExpected("pack1class1.py", "package1");
	}

	@Test
	public void testGeneratePack1Class2() throws Exception {
		assertGenerate(Pack1Class2_fragment);
		assertGeneratedMatchesExpected("pack1class2.py", "package1");
	}

	@Test
	public void testGeneratePack1Enumeration1() throws Exception {
		assertGenerate(Pack1Enumeration1_fragment);
		assertGeneratedMatchesExpected("pack1enumeration1.py", "package1");
	}

	@Test
	public void testGeneratepackage1_1() throws Exception {
		assertGenerate(package1_1_fragment);
		assertGeneratedMatchesExpected("__init__.py", "package1", "package1_1");
	}

	@Test
	public void testGeneratepack1_1class1() throws Exception {
		assertGenerate(pack1_1class1_fragment);
		assertGeneratedMatchesExpected("pack1_1class1.py", "package1", "package1_1");
	}

	@Test
	public void testGeneratePack1_1Class2() throws Exception {
		assertGenerate(Pack1_1Class2_fragment);
		assertGeneratedMatchesExpected("pack1_1class2.py", "package1", "package1_1");
	}

	@Test
	public void Pack1_1Enumeration1() throws Exception {
		assertGenerate(Pack1_1Enumeration1_fragment);
		assertGeneratedMatchesExpected("pack1_1enumeration1.py", "package1", "package1_1");
	}

	@Test
	public void testGeneratepackage1_1_1() throws Exception {
		assertGenerate(package1_1_1_fragment);
		assertGeneratedMatchesExpected("__init__.py", "package1", "package1_1", "package1_1_1");
	}

	@Test
	public void testGeneratePack1_1_1Class1() throws Exception {
		assertGenerate(Pack1_1_1Class1_fragment);
		assertGeneratedMatchesExpected("pack1_1_1class1.py", "package1", "package1_1", "package1_1_1");
	}

	@Test
	public void testGeneratePack1_1_1Class2() throws Exception {
		assertGenerate(Pack1_1_1Class2_fragment);
		assertGeneratedMatchesExpected("pack1_1_1class2.py", "package1", "package1_1", "package1_1_1");
	}

	@Test
	public void testGenerateDependencyTest() throws Exception {
		assertGenerate(DependencyTest_fragment);
		assertGeneratedMatchesExpected("dependencytest.py", "package1");
	}

	@Test
	public void testGenerateCompositionClass() throws Exception {
		assertGenerate(CompositionClass_fragment);
		assertGeneratedMatchesExpected("compositionclass.py");
	}

	@Test
	public void testGenerateAssociationClass() throws Exception {
		assertGenerate(AssociationClass_fragment);
		assertGeneratedMatchesExpected("associationclass.py");
	}

	@Test
	public void testGenerateAggregationClass() throws Exception {
		assertGenerate(AggregationClass_fragment);
		assertGeneratedMatchesExpected("aggregationclass.py");
	}

	@Test
	public void testGenerateElementClass1() throws Exception {
		assertGenerate(ElementClass1_fragment);
		assertGeneratedMatchesExpected("elementclass1.py");
	}

	@Test
	public void testGenerateElementClass2() throws Exception {
		assertGenerate(ElementClass2_fragment);
		assertGeneratedMatchesExpected("elementclass2.py");
	}

	@Test
	public void testGenerateInterfaceTest() throws Exception {
		assertGenerate(InterfaceTest_fragment);
		assertGeneratedMatchesExpected("interfacetest.py");
	}

	@Test
	public void testGenerateEnumerationTest1() throws Exception {
		assertGenerate(EnumerationTest1_fragment);
		assertGeneratedMatchesExpected("enumerationtest1.py");
	}

	@Test
	public void testGenerateEnumerationTest2() throws Exception {
		assertGenerate(EnumerationTest2_fragment);
		assertGeneratedMatchesExpected("enumerationtest2.py");
	}

	@Test
	public void testGenerateClassM() throws Exception {
		assertGenerate(EnumerationTestClass_fragment);
		assertGeneratedMatchesExpected("enumerationtestclass.py");
	}

	@Test
	public void testGeneratemodulepackage() throws Exception {
		assertGenerate(modulepackage_fragment);
		assertGeneratedMatchesExpected("__init__.py", "module_package");
	}

	@Test
	public void testGeneratemodule1() throws Exception {
		assertGenerate(module1_fragment);
		assertGeneratedMatchesExpected("module1.py", "module_package");
	}

	@Test
	public void testGenerateDataType1() throws Exception {
		assertGenerate(DataType1_fragment);
		assertGeneratedMatchesExpected("datatype1.py");
	}

	@Test
	public void testGenerateTestmodule() throws Exception {
		assertGenerate(Testmodule_fragment);
		assertGeneratedMatchesExpected("testmodule.py");
	}

	private void assertGenerate(String fragment) throws Exception {
		selectSemanticElement(fragment);
		handlerService.executeCommand(GENERATE_COMMAND_ID, null);
	}

	private void selectSemanticElement(String uriFragment) throws Exception {
		URI elementUri = modelUri.appendFragment(uriFragment);
		EObject semantic = model.eResource().getResourceSet().getEObject(elementUri, true);

		// #openSemanticElement returns the multiEditor if successful and null otherwise

		// the open often fails if pages are passed in, so we first try to open without specifying
		// pages
		IMultiDiagramEditor editor = elementActivator.openSemanticElement(semantic);
		assertNotNull(editor);

		try {
			// wait (max 1 second) until editor becomes available
			int i = 0;
			while (editor.getActiveEditor() == null) {
				editor = elementActivator.openSemanticElement(semantic);
				Thread.sleep(10);
				if (i++ > 100) {
					fail("Timeout during wait for editor to become active");
				}
			}
		} catch (InterruptedException e) {
		}

		// make sure there is an active multiEditor so that the selection will be available
		assertNotNull(editor.getActiveEditor());
	}

	/**
	 * Compare the files in folder with what we expect to see. When comparing
	 * file content, filter out whitespace which will replace all whitespace
	 * with a single space in the actual file content and the expected file
	 * content in order to avoid problems with differences caused by code
	 * formatting options where the test suite is run. Then a simple string
	 * comparison is done.
	 */
	private void assertGeneratedMatchesExpected(String fileName, String... depthSegments) throws Exception {
		IFolder srcGenFolder = FileComparison.getGeneratedProject(GenProjectName).getFolder(GenFolderName);
		assertTrue("src-gen folder \"" + srcGenFolder + "\" does not exist", srcGenFolder.exists());
		// IFolder generatedFolder = srcGenFolder.getFolder(GenFolderName);
		FileComparison.assertGeneratedMatchesExpected(srcGenFolder, modelProject, fileName, depthSegments);
	}
}

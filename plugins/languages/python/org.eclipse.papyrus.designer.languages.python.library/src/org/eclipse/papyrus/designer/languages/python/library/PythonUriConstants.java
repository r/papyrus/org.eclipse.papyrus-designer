/*****************************************************************************
 * Copyright (c) 2016, 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.python.library;

import org.eclipse.emf.common.util.URI;

/**
 * URI constants for libraries defined in this plug-in
 */
public class PythonUriConstants {
	
	public static final String PATHMAP = "pathmap://PapyrusPython_LIBRARIES/"; //$NON-NLS-1$
	
	public static final String PSL_LIB_PATH = PATHMAP + "pythonStandardLibrary.uml"; //$NON-NLS-1$

	public static final URI PSL_LIB_URI = URI.createURI(PSL_LIB_PATH);
}

/**
 */
package org.eclipse.papyrus.designer.languages.python.profile.python.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.designer.languages.python.profile.python.External;
import org.eclipse.papyrus.designer.languages.python.profile.python.PythonPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>External</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ExternalImpl extends org.eclipse.papyrus.designer.languages.common.profile.Codegen.impl.ExternalImpl implements External {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExternalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PythonPackage.Literals.EXTERNAL;
	}

} //ExternalImpl

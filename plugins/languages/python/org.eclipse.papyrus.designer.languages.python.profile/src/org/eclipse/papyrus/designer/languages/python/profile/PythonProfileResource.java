package org.eclipse.papyrus.designer.languages.python.profile;

import org.eclipse.emf.common.util.URI;

/**
 * Utility class to get informations on Python profile resources
 */
public class PythonProfileResource {

	public static final String PROFILE_PATHMAP = "pathmap://PapyrusPython_PROFILES/"; //$NON-NLS-1$

	public static final String PROFILE_PATH = PROFILE_PATHMAP + "python.profile.uml"; //$NON-NLS-1$

	public static final URI PROFILE_PATH_URI = URI.createURI(PROFILE_PATH);

	public static final String PROFILE_URI = "http://www.eclipse.org/papyrus/python/1"; //$NON-NLS-1$
}

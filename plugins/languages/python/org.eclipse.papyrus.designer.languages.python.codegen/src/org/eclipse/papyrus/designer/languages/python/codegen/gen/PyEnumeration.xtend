/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mohamed Harkat - Initial API and implementation
 *   Ansgar Radermacher - Integration and bug fixes
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.gen

import org.eclipse.papyrus.designer.languages.python.codegen.transformation.PythonCodeGenUtils
import org.eclipse.uml2.uml.Enumeration

class PyEnumeration {
	/**
	 * A simple code for enumeration generation. It assigns for each literal a number starting from zero.
	 */
	def static genEnumeration(Enumeration enumeration, boolean useRelativeImports) '''
		### Enumeration ###
		from enum import Enum
		«PythonCodeGenUtils.writeImports(enumeration, useRelativeImports)»


		«var int i = 0»
		class «enumeration.name»(«IF enumeration.generals.empty»Enum«ELSE»«FOR superEnumeration : enumeration.generals SEPARATOR ', '»«superEnumeration.name»«ENDFOR»«ENDIF»):
			«IF enumeration.ownedLiterals.empty»
				pass
			«ENDIF»

			«FOR literal : enumeration.ownedLiterals»
				«IF literal.specification !== null»
					«literal.name» = «literal.specification.integerValue.toString»
				«ELSE»
					«while(checkValueParentEnum(enumeration, i)){
						i++
					}»
					«literal.name» = «i++»
				«ENDIF»
		«ENDFOR»
	'''

	/**
	 * A method that assigns a value to the literal. If the value of the literal is supplied by the user, then return
	 * that value, else assign a different value to the literal. This method needs to check all the assigned values
	 * to generate a new one.
	 */
	 def static int assignLiteralValue(){
	 	return 0
	 }
	 
	 def static checkValueParentEnum(Enumeration enumeration, int value){
	 	for(general : enumeration.generals){
	 		if(general instanceof Enumeration){
	 			for(literal : general.ownedLiterals){
	 				if(literal.specification !== null && value === literal.specification.integerValue)
	 					return true
	 			}
	 		}	
	 	}
	 	return false
	}
}
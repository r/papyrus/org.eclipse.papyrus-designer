/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mohamed Harkat - Initial API and implementation
 *   Ansgar Radermacher - Integration and bug fixes
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.gen

import org.eclipse.papyrus.designer.languages.python.codegen.transformation.PythonCodeGenUtils
import org.eclipse.uml2.uml.Interface

import static extension org.eclipse.papyrus.designer.languages.python.codegen.gen.PyAttributes.*
import static extension org.eclipse.papyrus.designer.languages.python.codegen.gen.PyMethods.method

class PyInterface {
	/**
	 * A  simple method to write an interface.
	 */
	def static genInterface(Interface intf, boolean useRelativeImports) '''
		### Interface ###
		from abc import ABC, abstractmethod
		«PythonCodeGenUtils.writeImports(intf, useRelativeImports)»

		class «intf.name»(ABC):
			«FOR attribute : intf.attributes»
				«IF attribute.isComposite»
					«attribute.writeAttributeVisibility»«attribute.name»«attribute.assignCompositeAttributes»
				«ELSE»
					«attribute.writeAttributeVisibility»«attribute.name»«attribute.assignType»«attribute.assignAttributeValue»
				«ENDIF»
			«ENDFOR»
			«FOR op : intf.ownedOperations»

				@abstractmethod
				«op.method»
					pass
			«ENDFOR»
			'''
}
package org.eclipse.papyrus.designer.languages.python.codegen;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.languages.python.codegen.messages"; //$NON-NLS-1$
	public static String LocatePythonProjectTitle;
	public static String LocatePythonProjectInfo;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}

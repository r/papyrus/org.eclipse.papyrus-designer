/*******************************************************************************
 * Copyright (c) 2022 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.transformation;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.common.base.ModelElementsCreator;
import org.eclipse.papyrus.designer.languages.common.base.file.FileSystemAccessFactory;
import org.eclipse.papyrus.designer.languages.common.base.file.IPFileSystemAccess;
import org.eclipse.papyrus.designer.languages.python.codegen.gen.PyClasses;
import org.eclipse.papyrus.designer.languages.python.codegen.gen.PyDatatype;
import org.eclipse.papyrus.designer.languages.python.codegen.gen.PyEnumeration;
import org.eclipse.papyrus.designer.languages.python.codegen.gen.PyInterface;
import org.eclipse.papyrus.designer.languages.python.profile.python.Module;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.ValueSpecification;
//import python.Module;


public class PythonModelElementsCreator extends ModelElementsCreator {

	private static final String PYTHON_LANG = "Python"; //$NON-NLS-1$

	/**
	 * Constructor.
	 * 
	 * @param project
	 *            the Eclipse project for which to construct a create
	 */
	public PythonModelElementsCreator(IProject project) {
		this(project, FileSystemAccessFactory.create(project));
	}

	/**
	 * Constructor. Pass caller defined file system access
	 * 
	 * @param project
	 *            the Eclipse project for which to construct a create
	 * @param fileSystemAccess
	 *            a a fileSystemAccess instance
	 * @param commentHeader
	 *            commentHeader. If null, take from preferences
	 */
	public PythonModelElementsCreator(IProject project, IPFileSystemAccess fileSystemAccess) {
		super(fileSystemAccess, new PythonLocStrategy(), PYTHON_LANG);
		this.project = project;
		pythonExt = "py"; //$NON-NLS-1$
	}

	protected String pythonExt;

	@Override
	protected boolean noCodeGen(Element element) {
		return false;
	}

	@Override
	protected void createPackageableElementFile(PackageableElement pe, IProgressMonitor monitor) {
		if (pe instanceof Classifier && !StereotypeUtil.isApplied(pe.getNearestPackage(), Module.class)) {
			Classifier classifier = (Classifier) pe;

			final String fileName = getFileName(classifier) + "." + pythonExt; //$NON-NLS-1$
			String result = StringConstants.EMPTY;

			if (classifier instanceof PrimitiveType || classifier instanceof OpaqueBehavior ||
					classifier instanceof Usage || classifier instanceof ValueSpecification) {
				// do nothing
			} else if (classifier instanceof Class) {
				result = PyClasses.genClassWithNS((Class) classifier, useRelativeImports()).toString();
				generateFile(fileName, result);
			} else if (classifier instanceof Interface) {
				result = PyInterface.genInterface((Interface) classifier, useRelativeImports()).toString();
				generateFile(fileName, result);
			} else if (classifier instanceof Enumeration) {
				result = PyEnumeration.genEnumeration((Enumeration) classifier, useRelativeImports()).toString();
				fileSystemAccess.generateFile(fileName, result);
			} else if (classifier instanceof DataType) {
				result = PyDatatype.genDatatype((DataType) classifier).toString();
				generateFile(fileName, result);
			}
		} else if (pe instanceof Package) {
			Package pkg = (Package) pe;
			if (StereotypeUtil.isApplied(pe, Module.class)) {
				final String fileName = getFileName(pe.getNearestPackage()) + StringConstants.DOT + pythonExt;
				String result = StringConstants.EMPTY;
				for (Element element : pkg.allOwnedElements()) {
					if (element instanceof Classifier) {
						if (element instanceof Class && !(element instanceof OpaqueBehavior))
							result += PyClasses.genClassWithNS((Class) element, useRelativeImports()).toString();
					}
				}
				generateFile(fileName, result);
			} else if (pkg.getOwnedMembers().size() > 0) {
				// create __init__ for non-empty package
				final String fileName = getFileName(pkg) + "/__init__." + pythonExt; //$NON-NLS-1$
				generateFile(fileName, StringConstants.EMPTY);
			}
		}
	}

	protected boolean useRelativeImports() {
		return true;
	}

	protected void generateFile(String fileName, String content) {
		fileSystemAccess.generateFile(fileName, content);
	}
}

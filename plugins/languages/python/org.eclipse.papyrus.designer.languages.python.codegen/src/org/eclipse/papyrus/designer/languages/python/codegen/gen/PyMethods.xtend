/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mohamed Harkat - Initial API and implementation
 *   Ansgar Radermacher - Integration and bug fixes
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.gen

import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.profile.standard.Destroy

import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.*

class PyMethods {
	/**
	 *
	 */
	def static method(Operation method){
		if (method.isStatic) {
			return method.staticMethod
		} else{
			return method.normalMethod
		}
	}
	/**
	 * Write the header of a static method
	 */
	def static staticMethod(Operation method) '''
		@staticmethod
		def «method.writeMethodVisibility»«method.name»(«FOR parameter : method.nonRetParams SEPARATOR ', '»«parameter.name»«ENDFOR»):
	'''
	/**
	 * Write the header of a normal method, the self keyword should be added as a parameter
	 */
	def static normalMethod(Operation method) '''
		def «method.writeMethodVisibility»«method.mName»(self«FOR parameter : method.nonRetParams», «parameter.name»«ENDFOR»):
	'''
	/**
	 * Write the method's name. if the method is a constructor or a destroyer, then replace the name with "__init__" or
	 * "__del__" respectively. Else, write the method's name
	 */
	def static mName(Operation method){
		if (method.isApplied(Create)){
			"__init__"
		}
		else if (method.isApplied(Destroy)){
			"__del__"
		}
		else {
			method.name
		}
	}
	/**
	 * Write the mehtod's body. It ensures that the body is written in Python.
	 */
	def static mBody(Operation operation){
		for(method : operation.methods){
			if(method instanceof OpaqueBehavior) {
				for (var i=0; i < method.bodies.size; i++) {
					if (method.languages.get(i) == "Python") {
						return method.bodies.get(i)
					}
				}
			}
		}
	}
	/**
	 *
	 */
	def static nonRetParams(Operation method) {
		method.ownedParameters.filter[it.direction != ParameterDirectionKind.RETURN_LITERAL]
	}
	/**
	 * Check if the method is a Constructor
	 */
	def static isConstructor(Operation method){
		if (method.isApplied(Create)) {
			return true
		}
		else
			return false
	}
	/**
	 * Check if the method is a destroyer
	 */
	def static isDestroyer(Operation method){
		if (method.isApplied(Destroy)) {
			return true
		}
		else
			return false
	}
	/**
	 * Write the visibility of the method.
	 * __ for private
	 * _ for protected
	 * nothing is adding for public visibility
	 */
	def static writeMethodVisibility(Operation method){
		switch(method.visibility.name()){
			case "PRIVATE_LITERAL": return '''__'''
			case "PROTECTED_LITERAL": return '''_'''
			default: return ''''''
		}
	}
}
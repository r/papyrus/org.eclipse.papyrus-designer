/*******************************************************************************
 * Copyright (c) 2022 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Ansgar Radermacher - Initial API and implementation
 *   Mohamed Harkat
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.python.codegen.transformation

import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Package
import org.eclipse.papyrus.designer.languages.python.profile.python.Module
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Namespace
import org.eclipse.papyrus.designer.infra.base.StringConstants
import org.eclipse.papyrus.designer.languages.common.base.ClassUtils
import static extension org.eclipse.papyrus.designer.languages.common.base.GenUtils.isUMLPrimitiveType
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.External

/**
 * Utility method for Python code generation
 */
class PythonCodeGenUtils {

	/**
	 * A method that returns the qualified name of the element in form of a python
	 * pathname (lowercase, dot as path separator).
	 */
	def static getPythonQName(NamedElement element) {
		val external = UMLUtil.getStereotypeApplication(element, External)
		if (external !== null && external.name !== null) {
			return external.name	
		}
		else {
			return element.qualifiedName.toLowerCase.replace(Namespace.SEPARATOR, StringConstants.DOT)
		}
	}

	/**
	 * Get the relative path of the target Classifier to the source classifier
	 * (The classifier type was chosen here because classes, interfaces
	 * enumerations and datatypes are all specializations of the classifier).
	 * 
	 * @param src the importing classifier
	 * @param target the element to import
	 */
	def static getRelativePath(Classifier src, NamedElement target) {
		// find common namespace element
		var dot = StringConstants.DOT;
		var ns = src.namespace;
		while (!target.allNamespaces.contains(ns) && ns !== null) {
			dot += StringConstants.DOT;
			ns = ns.namespace;
		}
		if (ns !== null) {
			// use relative path to common element + remove path to this element from qualified name.
			return dot + getPythonQName(target).replaceFirst(getPythonQName(ns) + StringConstants.DOT, '')

		} else {
			// no parent packages in common, return the whole package branch of the target
			return getPythonQName(target)
		}
	}

	/**
	 * Returns an array list which contains all the packages that the classifier depends on.
	 */
	def static getPackageDep(Classifier src) {
		var packDp = newArrayList
		for (dependency : src.clientDependencies) {
			for (target : dependency.targets.filter[it instanceof Package]) {
				packDp.add(target as Package)
			}
		}
		return packDp
	}

	/**
	 * 
	 */
	def static pyRequiredClassifiers(Classifier classifier) {
		return ClassUtils.requiredClassifiers(classifier).filter[!it.isUMLPrimitiveType && !it.isPythonPrimitiveType]
	}

	def static isPythonPrimitiveType(Classifier type) {
		val owningPkgName = type.nearestPackage.name;
		return "PythonStdLib".equals(owningPkgName)
	}

	/**
	 * Write all the necessary importations for the classifier for all the relations.
	 */
	def static writeImports(Classifier classifier, boolean useRelativeImports) '''
		«IF classifier.isAbstract»
			from abc import ABC, abstractmethod
		«ENDIF»
		«FOR pkg : classifier.getPackageDep»
««« No relative imports on package level
			import «pkg.pythonQName»
		«ENDFOR»
		«FOR dependencyClassifier : classifier.pyRequiredClassifiers»
			«val mp = dependencyClassifier.modulePkg»
			«val extPython = UMLUtil.getStereotypeApplication(dependencyClassifier, org.eclipse.papyrus.designer.languages.python.profile.python.External)»
			«val extCommon = UMLUtil.getStereotypeApplication(dependencyClassifier, External)»
«««	Use <<External>> from Python profile with priority compared to stereotype from common profile
«««	(if multiple External stereotypes for different languages are applied)
			«IF extPython !== null»
				«extPython.externalImport»
			«ELSEIF extCommon !== null»
				«extCommon.externalImport»
			«ELSEIF useRelativeImports»
				from «classifier.getRelativePath(mp)» import «dependencyClassifier.name»
			«ELSE»
				from «mp.pythonQName» import «dependencyClassifier.name»
			«ENDIF»
		«ENDFOR»
	'''

	/**
	 * Import an external classifier, always assuming that it is in a module
	 */
	def static externalImport(External external) {
		var name = external.name
		if (name === null) {
			name = external.base_Classifier.getPythonQName
		}
		// if UML separator is used, replace with a dot
		name = name.replace(Namespace.SEPARATOR, StringConstants.DOT)
		val index = name.lastIndexOf(StringConstants.DOT)
		if (index != -1) {
			val from = name.substring(0, index).toLowerCase
			val shortName = name.substring(index + 1)
			return '''from «from» import «shortName»'''
		}
		else {
			// no qualified name, assume that nothing needs to be imported
			return ""
		}
	}
	
	/**
	 * Module = multiple classifiers in same file (which has the name of the package)
	 * If a classifier is defined in a "module" package, return this package, otherwise
	 * return the classifier itself.
	 */
	def static modulePkg(NamedElement ne) {
		if (StereotypeUtil.isApplied(ne.nearestPackage, Module)) {
			return ne.nearestPackage
		}
		return ne
	}
}

/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mohamed Harkat - Initial API and implementation
 *   Ansgar Radermacher - Integration and bug fixes
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.python.codegen.gen

import org.eclipse.papyrus.designer.languages.python.codegen.transformation.PythonCodeGenUtils
import org.eclipse.papyrus.designer.languages.python.profile.python.Main
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.util.UMLUtil

import static extension org.eclipse.papyrus.designer.languages.python.codegen.gen.PyAttributes.*
import static extension org.eclipse.papyrus.designer.languages.python.codegen.gen.PyMethods.*

class PyClasses {
	/**
	 * The method that writes the whole class, it kind of acts as a main method here
	 * 
	 * @param clazz the class to generate code for
	 * @param useRelativeImports if true, write imports in a relative way
	 */
	def static genClassWithNS(Class clazz, boolean useRelativeImports)'''
		«val imports = PythonCodeGenUtils.writeImports(clazz, useRelativeImports)»
		«imports»
		«IF !imports.toString.isBlank» ««« add a blank line, if imports not empty


		«ENDIF»
		«clazz.genClass»
	'''

	/**
	 * Generate the header and the body of the class
	 */
	def static genClass(Class clazz)'''
		class «clazz.name»«IF clazz.generals.size > 0»(«IF clazz.isAbstract»ABC«ENDIF»«
			FOR superClass : clazz.generals SEPARATOR ', '»«superClass.name»«ENDFOR»)«ELSE»«IF clazz.isAbstract»(ABC)«ENDIF»«ENDIF»:

			«IF !clazz.writeStaticAttributes.toString.empty/*Add a blank line for pretty printing*/»
				«clazz.writeStaticAttributes»

			«ENDIF»
			«IF !clazz.hasConstructor && clazz.checkIfNotStaticAttributes»
				def __init__(self):
					«clazz.writeNotStaticAttributes»

			«ENDIF»
			«FOR op : clazz.ownedOperations»
				«IF clazz.isAbstract && op.isAbstract»@abstractmethod«ENDIF»
				«op.method.toString.trim»
					«IF op.isConstructor»
						«clazz.writeNotStaticAttributes»
					«ENDIF»
					«IF op.isAbstract || op.mBody === null »
						pass

					«ELSE»
						«op.mBody»

					«ENDIF»
			«ENDFOR»
			«IF clazz.ownedOperations.isEmpty && clazz.ownedAttributes.isEmpty»
				def __init__(self):
					pass

			«ENDIF»
		«IF StereotypeUtil.isApplied(clazz, Main)»
			«val main = UMLUtil.getStereotypeApplication(clazz, Main)»

			def main():
				«IF main.body === null || main.body.isEmpty»
					pass
				«ELSE»
					«main.body»
				«ENDIF»


			if __name__ == '__main__':
				main()
		«ENDIF»
	'''

	/**
	 * A method to write static attributes of the class. the attributes should be declared outside the constructor.
	 */
	def static writeStaticAttributes(Class clazz)'''
		«FOR attribute : clazz.ownedAttributes.filter[it.isStatic && !it.isComposite && !it.isReadOnly]»
			«attribute.writeAttributeVisibility»«attribute.name»«attribute.assignType»«attribute.assignAttributeValue»
		«ENDFOR»
		«FOR attribute : clazz.ownedAttributes.filter[it.isStatic && it.isComposite && !it.isReadOnly]»
			«attribute.writeAttributeVisibility»«attribute.name»«attribute.assignCompositeAttributes»
		«ENDFOR»
		'''
	/**
	 * A method to write non static attributes of the class. the attributes should be declared inside a constructor.
	 */
	def static writeNotStaticAttributes(Class clazz)'''
		«FOR attribute : clazz.ownedAttributes.filter[!it.isStatic && !it.isComposite && !it.isReadOnly]»
			self.«attribute.writeAttributeVisibility»«attribute.name»«attribute.assignType»«attribute.assignAttributeValue»
		«ENDFOR»
		«FOR attribute : clazz.ownedAttributes.filter[!it.isStatic && it.isComposite && !it.isReadOnly]»
			self.«attribute.writeAttributeVisibility»«attribute.name»«attribute.assignCompositeAttributes»
		«ENDFOR»
		'''
	/**
	 * A method to write read only attributes.
	 */
	def static writeReadOnlyAttributes(Class clazz)'''
		«FOR attribute : clazz.ownedAttributes.filter[it.readOnly]»
			def «attribute.name»(«IF !attribute.isStatic»self«ENDIF»):
				«IF !attribute.isStatic»self.«ENDIF»«attribute.name»«attribute.assignType» = «attribute.assignAttributeValue»
		«ENDFOR»
	'''
	/**
	 * a checker that returns true if the class has no static attributes
	 * else it returns false
	 **/
	def static checkIfNotStaticAttributes(Class clazz){
		for(attribute : clazz.ownedAttributes){
			if(!attribute.isStatic)
				return true
		}
		return false
	}
	/**
	 * Check if the class has a constructor. The constructor would be necessary to declare non static variables
	 * returns false if there is a constructor, else true.
	 */
	def static hasConstructor(Class clazz){
		for(op: clazz.ownedOperations){
			if(op.isConstructor)
				return true
		}
		return false
	}
	/**
	 *Check if the class has a static main operation .
	 */
	 def static hasMainOp(Class clazz){
	 	for(op : clazz.ownedOperations)
	 		if(op.name.equals('main') && op.static)
	 			return true
	 	return false
	 }
}
/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.extensionpoints;

/**
 * Abstract superclass for project settings. It is empty to allow for arbitrary
 * project specific settings
 */
public abstract class AbstractSettings {
	/**
	 * the operating system for which code should be produced (will have a
	 * project specific effect, e.g. in case of CDT it will result in a #define)
	 */
	public String targetOS;
}

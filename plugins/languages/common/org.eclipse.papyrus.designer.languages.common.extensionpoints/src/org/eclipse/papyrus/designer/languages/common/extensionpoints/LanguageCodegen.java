/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - ansgar.radermacher@cea.fr   initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.extensionpoints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.uml2.uml.Element;

/**
 * Common interface to generate code. Supports for multiple target languages via
 * the Eclipse extension mechanism
 */
public class LanguageCodegen {

	private static final String CLASS = "class"; //$NON-NLS-1$

	private static final String LANGUAGE = "language";//$NON-NLS-1$

	private static final String ID = "id";//$NON-NLS-1$

	public static final String ILANG_SUPPORT_ID = Activator.PLUGIN_ID + ".languageCodegen"; //$NON-NLS-1$

	public static final Pattern MATCH_ALL = Pattern.compile(".*");  //$NON-NLS-1$

	/**
	 * Get a code generator via language and ID.
	 * 
	 * @param language
	 *            a string with the supported language
	 * @return a code generator
	 */
	public static ILangCodegen getGenerator(String language) {
		// compile language into a pattern, escape "+"
		return getGenerator(language, null); 
	}

	/**
	 * Get a code generator via language and ID.
	 * 
	 * @param language
	 *            a string with the supported language
	 * @return a code generator
	 */
	public static ILangCodegen getGenerator(String language, String id) {
		return getGenerator(Pattern.compile(language.replace("+", "\\+")), id); //$NON-NLS-1$//$NON-NLS-2$
	}

	/**
	 * Get a code generator via language and ID.
	 * 
	 * @param languagePattern
	 *            a pattern containing a list of supported languages
	 * @param id
	 *            a string determining the id of the code generator. If null
	 *            or empty, the first available generator is returned.
	 * @return a code generator
	 */
	public static ILangCodegen getGenerator(Pattern languagePattern, String id) {
		List<ILangCodegen> generators = getCodegenList(languagePattern);
		for (ILangCodegen generator : generators) {
			if ((id == null) || id.length() == 0 || generatorIDs.get(generator).equals(id)) {
				return generator;
			}
		}
		throw new RuntimeException(String.format(Messages.LanguageSupport_LanguageNotSupported, languagePattern));
	}

	/**
	 * Get a list of generators that conform a language patterns and are
	 * eligible for a passe classifier
	 * 
	 * @param languagePattern
	 *            a language pattern
	 * @param element
	 *            a UML element for which we want to choose a code generator
	 * @return a list of code generators that comply with the language pattern
	 */
	public static List<ILangCodegen> getEligibleGeneratorList(Pattern languagePattern, Element element) {
		List<ILangCodegen> eligibleGenerators = new ArrayList<ILangCodegen>();
		for (ILangCodegen generator : getCodegenList(languagePattern)) {
			if (generator.isEligible(element)) {
				eligibleGenerators.add(generator);
			}
		}
		return eligibleGenerators;
	}

	/**
	 * Retrieve a list of code generators for a given language pattern. The language may
	 * be a regular expression
	 * 
	 * @param languagePattern a language pattern, e.g. C++|C
	 * @return a list of code generators that comply with the past language patterns
	 */
	public static List<ILangCodegen> getCodegenList(Pattern languagePattern) {
		IExtensionRegistry reg = Platform.getExtensionRegistry();
		IConfigurationElement[] configElements = reg.getConfigurationElementsFor(ILANG_SUPPORT_ID);
		List<ILangCodegen> generators = new ArrayList<ILangCodegen>();

		for (IConfigurationElement configElement : configElements) {
			try {
				final String id = configElement.getAttribute(ID);
				final String extLanguage = configElement.getAttribute(LANGUAGE);
				Matcher m = languagePattern.matcher(extLanguage);
				if (m.matches()) {
					final Object obj = configElement.createExecutableExtension(CLASS);
					if (obj instanceof ILangCodegen) {
						ILangCodegen generator = (ILangCodegen) obj;
						if (!generatorIDs.containsKey(generator)) {
							generatorIDs.put(generator, id);
						}
						if (!generatorLanguages.containsKey(generator)) {
							generatorLanguages.put(generator, extLanguage);
						}
						generators.add((ILangCodegen) obj);
					}
				}
			} catch (CoreException exception) {
				exception.printStackTrace();
			}
		}
		return generators;
	}

	/**
	 * @param generator
	 *            a generator
	 * @return the ID for the passed generator
	 */
	public static String getID(ILangCodegen generator) {
		return generatorIDs.get(generator);
	}

	/**
	* @param generator
	 *            a generator
	 * @return the language name for the passed generator
	 */
	public static String getLanguage(ILangCodegen generator) {
		return generatorLanguages.get(generator);
	}

	static Map<ILangCodegen, String> generatorIDs = new HashMap<ILangCodegen, String>();

	static Map<ILangCodegen, String> generatorLanguages = new HashMap<ILangCodegen, String>();
}

<?xml version="1.0" encoding="UTF-8"?>
<ecore:EPackage xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:ecore="http://www.eclipse.org/emf/2002/Ecore" name="Codegen" nsURI="http://www.eclipse.org/papyrus/Codegen/1" nsPrefix="Codegen">
  <eClassifiers xsi:type="ecore:EClass" name="Project">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Control how code is generated:&#xA;- name of project to generate code into&#xA;- target folder within that project (e.g. src-gen)&#xA;- Batch vs. incremental&#xA;- Prefix: namespace prefix of generated code (similar to prefix option in genmodel)"/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Model" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Model"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="generationMode" ordered="false"
        lowerBound="1" eType="#//GenerationModeKind"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="projectName" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="sourceFolder" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="prefix" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EEnum" name="GenerationModeKind">
    <eLiterals name="Batch"/>
    <eLiterals name="Incremental" value="1"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="GeneratorHint">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Information about the generator that should be used."/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Element" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Element"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="language" ordered="false"
        lowerBound="1" eType="#//Language"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="generatorID" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Language">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="&lt;p>Indicate programming language. The idea is that the GeneratorHint::language attribute is not an arbitrary string (which would be error prone), but can be selected from a list of available programming languages (i.e. a library with supported languages is required).&lt;/p>&#xA;"/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Class" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Class"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="NoCodeGen">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Do not generate code  for this element."/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Element" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Element"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ListHint">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="A hint how to generate code for elements with upper != 1. fixed: lower=upper, variable: lower=0, upper=-1, bounded: lower&lt;upper. [typeName] in String will be replaced by type name."/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="fixed" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="variable" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Package" ordered="false"
        eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Package"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="bounded" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="TraceHint">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Customize how to trace an element. For complex types such as lists or pointers"/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="declaration" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="params" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Element" ordered="false"
        eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Element"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="prepare" ordered="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="AOverride">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="Annotation Indicating that a method overrides a superclass method (for C++ and Java). Use &quot;A&quot; prefix to avoid clash with Java annotation"/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Operation" ordered="false"
        eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Operation"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="External">
    <eAnnotations source="http://www.eclipse.org/emf/2002/GenModel">
      <details key="documentation" value="An annotation indicating that the classifier is external, i.e. no code should be generated for it and it can be referenced via its name"/>
    </eAnnotations>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="name" ordered="false" unique="false"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Classifier" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Classifier"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MavenProject" eSuperTypes="#//Project">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="groupId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="artifactId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="version" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="archeType" ordered="false"
        lowerBound="1" eType="#//ArcheType"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="dependencies" ordered="false"
        upperBound="-1" eType="#//MavenDependency"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="parent" ordered="false"
        lowerBound="1" eType="#//ParentArtifact"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="properties" ordered="false"
        upperBound="-1" eType="#//Property"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Package" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Package"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ArcheType">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="groupId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="artifactId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="version" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="repository" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Class" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Class"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="MavenDependency">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="groupId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="artifactId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="version" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Class" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Class"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="exclusions" ordered="false"
        upperBound="-1" eType="#//ExcludedDependency"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ExcludedDependency">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="groupId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="artifactId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Class" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Class"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="ParentArtifact">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="groupId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="artifactId" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="version" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Class" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Class"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="Property">
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="name" ordered="false" lowerBound="1"
        eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EAttribute" name="value" ordered="false"
        lowerBound="1" eType="ecore:EDataType platform:/plugin/org.eclipse.uml2.types/model/Types.ecore#//String"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_Class" ordered="false"
        lowerBound="1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Class"/>
  </eClassifiers>
  <eClassifiers xsi:type="ecore:EClass" name="TemplateBinding">
    <eStructuralFeatures xsi:type="ecore:EReference" name="base_TypedElement" ordered="false"
        eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//TypedElement"/>
    <eStructuralFeatures xsi:type="ecore:EReference" name="actuals" ordered="false"
        upperBound="-1" eType="ecore:EClass platform:/plugin/org.eclipse.uml2.uml/model/UML.ecore#//Type"/>
  </eClassifiers>
</ecore:EPackage>

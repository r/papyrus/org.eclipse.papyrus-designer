/*****************************************************************************
 * Copyright (c) 2016 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base;

import org.eclipse.emf.common.util.URI;

/**
 * URI constants for UML standard profile and library
 */
public class StdUriConstants {
	public static final String UML_PRIM_TYPES_PATH = "pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml"; //$NON-NLS-1$
	public static final String UML_STD_PROFILE_PATH = "pathmap://UML_PROFILES/Standard.profile.uml"; //$NON-NLS-1$

	public static final URI UML_PRIM_TYPES_URI = URI.createURI(UML_PRIM_TYPES_PATH);
	public static final URI UML_STD_PROFILE_URI = URI.createURI(UML_STD_PROFILE_PATH);
}

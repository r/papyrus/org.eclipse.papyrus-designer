/*******************************************************************************
 * Copyright (c) 2006 - 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.common.base;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Interface;

/**
 * A set of utility functions related to classes.
 */
public class ClassUtils {
	/**
	 * Calculate the list of classifiers that are required by another classifier
	 *
	 * @param currentClass
	 * @return a list of classifiers that are required
	 */
	public static EList<Classifier> requiredClassifiers(Classifier currentClass) {
		// Retrieve package used by current package (dependencies)
		// use a unique list to avoid duplicates
		EList<Classifier> usedClasses = new UniqueEList<Classifier>();

		// class attributes dependencies
		usedClasses.addAll(GenUtils.getTypesViaAttributes(currentClass));
		// operation parameters dependencies
		usedClasses.addAll(GenUtils.getTypesViaOperations(currentClass));
		// inner classifier dependencies
		usedClasses.addAll(GenUtils.getInnerClassifierTypes(currentClass));
		// realized interface dependencies
		if (currentClass instanceof org.eclipse.uml2.uml.Class) {
			org.eclipse.uml2.uml.Class clazz = (org.eclipse.uml2.uml.Class) currentClass;
			EList<Interface> implementedInterfaces = clazz.getImplementedInterfaces();
			usedClasses.addAll(implementedInterfaces);
		}
		// dependencies and associations
		usedClasses.addAll(GenUtils.getTypesViaRelationshipsNoDeps(currentClass));
		usedClasses.addAll(GenUtils.getTypesViaDependencies(currentClass));
		
		// template parameters are declared locally (if owned) and do not correspond to a file
		// that can be included
		usedClasses.removeAll(GenUtils.getTemplateParameteredElements(currentClass));
		return usedClasses;
	}
}

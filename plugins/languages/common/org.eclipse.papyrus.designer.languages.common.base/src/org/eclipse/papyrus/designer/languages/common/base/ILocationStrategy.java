/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.languages.common.base;

import org.eclipse.uml2.uml.NamedElement;

/**
 * A location strategy defines the location of a file associated with a model element
 * depending on the namespaces of this model element (package hierarchy).
 */
public interface ILocationStrategy {
	
	public String getFolder(NamedElement element);

	public String getFileName(NamedElement element);
}

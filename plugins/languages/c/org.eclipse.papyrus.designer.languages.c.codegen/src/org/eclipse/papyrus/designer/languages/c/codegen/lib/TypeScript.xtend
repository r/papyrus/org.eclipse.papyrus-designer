/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.uml2.uml.Type

class TypeScript {
	def static genIntegerType(Type type) {
		'''int'''
	}

	def static genStringType(Type type) {
		'''char*'''
	}


	def static genBooleanType(Type type) {
		'''/*bool*/ char'''
	}
}

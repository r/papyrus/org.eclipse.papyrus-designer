/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.header

import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.VisibilityKind

import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.variableScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.services.UmlCommentServices.*
import org.eclipse.uml2.uml.CallEvent
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.SignalEvent
import org.eclipse.uml2.uml.FinalState
import org.eclipse.uml2.uml.State

import static extension org.eclipse.papyrus.designer.uml.tools.utils.StateMachineUtils.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.FunctionScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.DataTypeScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.ClassScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.ImportScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.header.CommonHeaderScript.*

class ClassHeaderScript {
	def static classHeaderScript(Class clazz) '''
		««« This template is called by the main module file 
		«clazz.genHeading»
		
		«clazz.genHeadingHeader»
		
		«clazz.genHeaderIncludes»
		
		«clazz.classHeaderBody»
		
		«clazz.genEndHeader»
	'''

	def static classHeaderBody(Class clazz) '''

			«IF (clazz.classifierBehavior !== null && !clazz.ownedBehaviors.filter(StateMachine).isEmpty)»
				// ----------------------------------Enum State declaration ----------------------------------
					«clazz.partComment('Enum State declaration')»
					«clazz.ownedBehaviors.filter(StateMachine).head.genStateEnumPrototype»
				// ----------------------------------Structure State_t declaration ----------------------------------
					«clazz.partComment('State_t declaration')»
					«clazz.ownedBehaviors.filter(StateMachine).head.genStatetStructure»
			«ENDIF»

			«IF (clazz.visibility == VisibilityKind.PUBLIC_LITERAL)»
				«clazz.partComment('Public Class Description')»
				//  Structure 
				«clazz.genClassStructDeclarations()»
				
				// Constructor and destructor declarations 
				«clazz.genDynamicInstanciationOperationPrototypes()»
				
				// Property initialisation declarations
				«clazz.genDefaultInitialisationProtoype()»
				
				// Class methods declarations
				«clazz.genNonStaticFunctionDeclarations()»
				
				// Class receptions declarations
				«clazz.genReceptionDeclarations()»
			«ENDIF»
			
			// ----------------------------------Public Global VariableDescription----------------------------------
			
			«IF (clazz.ownedAttributes.filter[isStatic && visibility == VisibilityKind.PUBLIC_LITERAL && type !== null].size > 0)»
				«clazz.partComment('Global Public Variable Declarations')»
			«ENDIF»
			«FOR attribute : clazz.ownedAttributes»
				«IF (attribute.isStatic && attribute.visibility == VisibilityKind.PUBLIC_LITERAL && attribute.type !== null)»
					// global variable declaration
					«attribute.genVariableDeclaration»
				«ENDIF»
			«ENDFOR»
			
			// ----------------------------------Global Public Functions----------------------------------
			«IF (clazz.operations.filter[isStatic && visibility == VisibilityKind.PUBLIC_LITERAL].size > 0)»
				«clazz.partComment('Global Public Function Declarations')»
			«ENDIF»
			«FOR operation : clazz.operations + clazz.interfaceRealizations.map[contract.ownedOperations].flatten»
				«IF operation.isStatic && operation.visibility == VisibilityKind.PUBLIC_LITERAL»
					«operation.genFunctionPrototype»
				«ENDIF»
			«ENDFOR»
		
		// ----------------------------------Signal Event Process Functions Implementations--------------------------------
		«IF (clazz.classifierBehavior !== null)»
			«FOR signalEvent : clazz.model.allOwnedElements.filter(SignalEvent)»		
				«signalEvent.genFunctionPrototype(clazz)»
			«ENDFOR»
		«ENDIF»
		
			// ----------------------------------Call Event Process Functions ----------------------------------
		«IF (clazz.classifierBehavior !== null)»
			«FOR callevent : clazz.model.allOwnedElements.filter(CallEvent)»		
				«IF (callevent.operation !== null && clazz.operations.contains(callevent.operation))»
					«callevent.genFunctionPrototype»
				«ENDIF»
			«ENDFOR»
		«ENDIF»
		
		
		«IF (clazz.classifierBehavior !== null && !clazz.ownedBehaviors.filter(StateMachine).isEmpty)»
			«var sm = clazz.ownedBehaviors.filter(StateMachine).head»	
			// ----------------------------------Entry Exit and DoActivity Prototypes for each state ----------------------------------
			«IF !sm.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)].isEmpty»
				«var states = sm.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)]»
				«FOR state : states»		
					«IF (state.entry.isBehaviorExist())»
						«state.genEntryPrototype»
					«ENDIF»
					«IF (state.exit.isBehaviorExist())»
						«state.genExitPrototype»
					«ENDIF»
					«IF (state.doActivity.isBehaviorExist())»
						«state.genDoActivityPrototype»
					«ENDIF»
				«ENDFOR»
			«ENDIF»
			«IF(sm.hasTriggerlessTransition())»
			//--------------------process Completion Event for completion transition-------------------//
			void ProcessCompletionEvent(«clazz.genName()»* self);
			«ENDIF»
		«ENDIF»
	'''
}

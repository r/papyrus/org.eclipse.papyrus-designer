/*******************************************************************************
 * Copyright (c) 2006 - 2012 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.preferences;

import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_COMMENT_HEADER_DVAL;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_COMMENT_HEADER_KEY;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_FORMAT_CODE_DVAL;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_FORMAT_CODE_KEY;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_HEADER_SUFFIX_DVAL;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_HEADER_SUFFIX_KEY;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_IMPLEM_SUFFIX_DVAL;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_IMPLEM_SUFFIX_KEY;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_OUT_INOUT_OP_DVAL;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_OUT_INOUT_OP_KEY;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_PROJECT_PREFIX_DVAL;
import static org.eclipse.papyrus.designer.languages.c.codegen.preferences.CCodeGenConstants.P_PROJECT_PREFIX_KEY;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.papyrus.designer.languages.c.codegen.Activator;


public class CCodeGenPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	public void initializeDefaultPreferences() {
		IEclipsePreferences prefs = DefaultScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		prefs.put(P_HEADER_SUFFIX_KEY, P_HEADER_SUFFIX_DVAL);
		prefs.put(P_IMPLEM_SUFFIX_KEY, P_IMPLEM_SUFFIX_DVAL);
		prefs.put(P_OUT_INOUT_OP_KEY, P_OUT_INOUT_OP_DVAL);
		prefs.put(P_PROJECT_PREFIX_KEY, P_PROJECT_PREFIX_DVAL);
		prefs.put(P_COMMENT_HEADER_KEY, P_COMMENT_HEADER_DVAL);
		prefs.putBoolean(P_FORMAT_CODE_KEY, P_FORMAT_CODE_DVAL);
	}
}

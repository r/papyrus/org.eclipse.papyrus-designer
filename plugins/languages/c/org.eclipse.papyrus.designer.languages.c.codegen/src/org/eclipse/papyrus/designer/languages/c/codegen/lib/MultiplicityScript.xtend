/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.uml2.uml.MultiplicityElement

class MultiplicityScript {
	def static genMultiplicity(MultiplicityElement multiplicityElement) '''
		«IF (multiplicityElement.upper == -1)»
			[]
		«ELSEIF (multiplicityElement.upper > 1)» 
			[«multiplicityElement.upper.toString()»]
		«ENDIF»'''
}

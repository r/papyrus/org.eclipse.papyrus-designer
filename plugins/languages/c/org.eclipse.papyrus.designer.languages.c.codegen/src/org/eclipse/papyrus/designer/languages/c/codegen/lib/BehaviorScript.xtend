package org.eclipse.papyrus.designer.languages.c.codegen.lib

/*******************************************************************************
 * Copyright (c) 2014 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

import org.eclipse.papyrus.designer.languages.c.codegen.utils.CGenUtils
import org.eclipse.papyrus.designer.languages.c.codegen.utils.Modifier
import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Inline
import org.eclipse.uml2.uml.Behavior
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Parameter
import org.eclipse.uml2.uml.ParameterDirectionKind

class BehaviorScript {
	static def returnSpec(Behavior behavior) '''
		«IF (GenUtils.returnResult(behavior) === null)»void «ELSE»«Modifier.modCVQualifier(GenUtils.returnResult(behavior))» «CGenUtils.cppQualifiedName(GenUtils.returnResult(behavior).type)»«Modifier.modPtr(GenUtils.returnResult(behavior))»«Modifier.modRef(GenUtils.returnResult(behavior))» «ENDIF»
	'''

	static def behaviorImplementation(String name,OpaqueBehavior behavior) '''
		«CommonScript.genComment(behavior)»
		«BehaviorScript.returnSpec(behavior)»«name»(«org.eclipse.papyrus.designer.languages.c.codegen.lib.BehaviorScript.behaviorParameters(behavior, false)»)«Modifier.modCVQualifier(behavior)» {
			«GenUtils.getBodyFromOB(behavior, FunctionScript.CLANGUAGE)»
		}
	'''

	static def InlineTxt(Element element) {
		if (GenUtils.hasStereotype(element, Inline)) 'inline '
	}

	static def behaviorDeclaration(String name, Behavior behavior) '''
		«CommonScript.genComment(behavior)»
		«InlineTxt(behavior)»«BehaviorScript.returnSpec(behavior)»«name»(«org.eclipse.papyrus.designer.languages.c.codegen.lib.BehaviorScript.behaviorParameters(behavior, true)»)«Modifier.modCVQualifier(behavior)»;
	'''

	/**  
	 * comment signature for a given behavior (e.g. effect within state machine)
	 */ 
	static def behaviorParameters(Behavior behavior, boolean showDefault) '''
		«FOR ownedParameter : behavior.ownedParameters.filter[it.direction != ParameterDirectionKind.RETURN_LITERAL] SEPARATOR ', '»«org.eclipse.papyrus.designer.languages.c.codegen.lib.BehaviorScript.parameter(ownedParameter, showDefault)»«ENDFOR»
	'''

	/**
	 * C++ parameter. Default values are added, if parameter showDefault is true (implementation signature
	 */ 
	static def parameter(Parameter parameter, boolean showDefault) {
		Modifier.modCVQualifier(parameter) + Modifier.modSCQualifier(parameter) + CGenUtils.cppQualifiedName(parameter.type) +
			Modifier.modPtr(parameter) + Modifier.modRef(parameter) + Modifier.dirInfo(parameter) + " " + parameter.name +
			Modifier.modArray(parameter) + {if (showDefault) defaultValue(parameter) else ""}
	}

	static def defaultValue(Parameter parameter) {
		if (parameter.defaultValue !== null)  " = " + parameter.defaultValue.stringValue() else ""
	}
}
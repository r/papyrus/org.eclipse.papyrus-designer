/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.header

import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.VisibilityKind

import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.variableScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.services.UmlCommentServices.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.DataTypeScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.ImportScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.header.CommonHeaderScript.*

class DataTypeHeaderScript {
	def static dataTypefill(DataType dataType) '''
	'''

	def static dataTypeHeaderScript(DataType dataType) '''
		// This template is called by the main module file 
		«dataType.genHeading»
		
		«dataType.genHeadingHeader»
		
		// Explicit import of the class
		«dataType.genHeaderIncludes»
		
		// header body
		«dataType.dataTypeHeaderBody»
		
		«dataType.genEndHeader»
	'''

	def static dataTypeHeaderBody(DataType dataType) '''
		«dataType.partComment('Public Class Description')»
		//  Structure 
		«dataType.genDataTypeStructDeclarations()»

		// Property initialisation declarations
		«dataType.genDefaultInitialisationProtoype()»

		// ----------------------------------Public Global VariableDescription----------------------------------
		«IF (dataType.ownedAttributes.filter[isStatic && visibility == VisibilityKind.PUBLIC_LITERAL && type !== null].size> 0)»
			«dataType.partComment('Global Public Variable Declarations')»
		«ENDIF»
		«FOR attribute : dataType.ownedAttributes.filter[!isStatic]»
			«IF (attribute.visibility == VisibilityKind.PUBLIC_LITERAL && attribute.type !== null)»
				// global variable declaration
				«attribute.genVariableDeclaration()»
			«ENDIF»
		«ENDFOR»
	'''
}
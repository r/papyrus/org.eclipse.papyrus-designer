/*******************************************************************************
 * Copyright (c) 2007 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.c.codegen.services;

import java.util.ArrayList;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Type;

public class UmlAssociationServices {

	/**
	 * Select all associations from a node which have class aClass in their
	 * members.
	 * 
	 * @param rootNode
	 *            EObject node at which to begin the search.
	 * @param aClass
	 *            Class the class to select.
	 * @return ArrayList with associations of the class.
	 */
	public ArrayList<Association> getClassAssociation(EObject rootNode, Class aClass) {
		ArrayList<Association> resultList = new ArrayList<Association>();
		for (TreeIterator<EObject> iterAssoc = rootNode.eAllContents(); iterAssoc.hasNext();) {
			Object obj = iterAssoc.next();
			if (obj instanceof Association) {
				// if obj is an association object
				Association assoc = (Association) obj;
				for (Type type : assoc.getEndTypes()) {
					if (type instanceof Class) {
						Class associationClass = (Class) type;
						if (associationClass == aClass) {
							resultList.add(assoc);
						}
					}
				}
			}
		}
		return resultList;
	}
}

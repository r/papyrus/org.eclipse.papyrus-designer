/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.uml2.uml.Constraint
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.OpaqueExpression
import org.eclipse.uml2.uml.Transition
import java.util.regex.Pattern

class AcslScript {
	public static String ACSL = "ACSL"

	def static getACSLConstraint(Constraint constraint) {
		var constraintText = ""
		if (constraint.specification instanceof OpaqueExpression) {
			var opaqueExpression = constraint.specification as OpaqueExpression
			if (opaqueExpression.bodies.size == opaqueExpression.languages.size) {
				for (var i = 0; i < opaqueExpression.languages.size; i++) {
					if (opaqueExpression.languages.get(i).equalsIgnoreCase(ACSL)) {
						constraintText += GenUtils.cleanCR(opaqueExpression.bodies.get(i)) + "\n"
					}
				}
			}
		}

		return constraintText
	}

	def static genPrePostConditions(Operation operation) {
		var specificationText = ""; // $NON-NLS-1$
		for (Constraint constraint : operation.preconditions) {
			specificationText += constraint.ACSLConstraint
		}

		for (Constraint constraint : operation.postconditions) {
			specificationText += constraint.ACSLConstraint
		}

		if (!specificationText.empty) {
			return getACSLComment(specificationText)
		} else {
			""
		}
	}

	def static String getACSLComment(String acslSpecification) '''
		/*@
			«acslSpecification»
		 */
	'''

	def static String getGuard(Transition transition) {
		val selectedLanguages = Pattern.compile(ACSL)
		val guard = StateMachineScript.getGuard(transition, selectedLanguages)
		if (!guard.empty) {
			return getACSLComment(guard)
		}
		return ""
	}
}

/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.module

import org.eclipse.uml2.uml.NamedElement
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.genName

class CommonModuleScript {
	def static genHeadingModule(NamedElement namedElement) '''
		#ifndef «(namedElement.genName() as String).toUpperCase()»_C_
		#define «(namedElement.genName() as String).toUpperCase()»_C_
	'''


	def static genEndModule(NamedElement namedElement) '''
		#endif /*«(namedElement.genName() as String).toUpperCase()»_C_*/
	'''
}

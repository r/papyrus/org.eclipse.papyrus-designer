/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.lib

import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Typedef
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.PrimitiveType

import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.variableScript.*
import static extension org.eclipse.papyrus.designer.languages.c.codegen.services.UmlCommentServices.*
import static extension org.eclipse.papyrus.designer.languages.common.base.GenUtils.*
import static extension org.eclipse.uml2.uml.util.UMLUtil.getStereotypeApplication
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.FinalState
import static extension org.eclipse.papyrus.designer.languages.c.codegen.lib.CommonScript.*

class DataTypeScript {
	def static genDefaultInitialisationProtoype(DataType dataType) '''
		«IF (dataType.ownedAttributes.filter[defaultValue !== null].size > 0)»
			«dataType.partComment('Default value initialization prototypes')»
			/*
			 * Default value initialization
			 * @param «dataType.genName» structure instance pointer
			 * @return void
			 */
			void «dataType.genName»_init(«dataType.genName»* self);
		«ENDIF»
		
	'''


	def static genDefaultIntialisationOperation(DataType dataType) '''
		«IF (dataType.ownedAttributes.filter[defaultValue !== null].size > 0)»
			«dataType.partComment('Default value initialization Implementation')»
			void «dataType.genName()»_init(«dataType.genName()»* self){
			«FOR attribute : dataType.ownedAttributes»
				«IF (attribute.defaultValue !== null && !attribute.isStatic && attribute.type !== null)»
					«attribute.genInitDefaultValue()»
				«ENDIF»
			«ENDFOR»
			}
		«ENDIF»
	'''


	def static genDataTypeStructDeclarations(DataType dataType) '''
		«FOR comment : dataType.ownedComments»
			 «comment.genComment()»
		«ENDFOR»
		typedef struct «dataType.genName» «dataType.genName»;
		
		struct «dataType.genName» {
		«FOR attribute : dataType.getAllAttributes().filter[!isStatic]»
			«attribute.genVariableDeclaration»
		«ENDFOR»
		};
	'''


	def static genDataTypeStructDeclarations(Enumeration enumeration) '''
		«FOR comment : enumeration.ownedComments»
			 «comment.genComment()»
		«ENDFOR»
		typedef enum «enumeration.genName()» «enumeration.genName()»;
		
		enum «enumeration.genName()»{
			«FOR literal : enumeration.ownedLiterals SEPARATOR(',\n')»
				«literal.genName()»
			«ENDFOR»
		};
	'''
	
	def static genStateEnumPrototype(StateMachine sm) '''
		typedef enum StateIDEnum StateIDEnum;
		
		enum StateIDEnum {
			«FOR state : sm.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)]»
				«state.genName()»,
			«ENDFOR»
		};
	'''

	def static genStatetStructure(StateMachine sm) '''
		typedef struct State_t State_t;
		
		struct State_t {
			void (*entry)();
			void (*exit)();
			void (*doActivity)();
			
		};
	'''

	def static genDataTypeStructDeclarations(PrimitiveType primitiveType) '''
		«FOR comment : primitiveType.ownedComments»
			 «comment.genComment()»
		«ENDFOR»
		«IF (primitiveType.hasStereotype(Typedef))»
			typedef «primitiveType.getStereotypeApplication(Typedef).definition» «primitiveType.genName()»;
		«ELSE»
			typedef struct «primitiveType.genName()» «primitiveType.genName()»;
			struct «primitiveType.genName()»{
				«FOR attribute : primitiveType.ownedAttributes»
					«IF (!attribute.isStatic)»
						«attribute.genVariableDeclaration()»
					«ENDIF»
				«ENDFOR»
			};
		«ENDIF»
	'''
}

/*******************************************************************************
 * Copyright (c) 2006 - 2012 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.preferences;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.papyrus.designer.languages.c.codegen.Activator;

/**
 * Utility class that returns the C preference values
 * 
 */
public class CCodeGenUtils {

	protected static IEclipsePreferences prefs = null;

	public static String getHeaderSuffix() {
		initPreferenceStore();
		return prefs.get(CCodeGenConstants.P_HEADER_SUFFIX_KEY, CCodeGenConstants.P_HEADER_SUFFIX_DVAL);
	}

	public static String getBodySuffix() {
		initPreferenceStore();
		return prefs.get(CCodeGenConstants.P_IMPLEM_SUFFIX_KEY, CCodeGenConstants.P_IMPLEM_SUFFIX_DVAL);
	}

	public static String getOutInoutOp() {
		initPreferenceStore();
		return prefs.get(CCodeGenConstants.P_OUT_INOUT_OP_KEY, CCodeGenConstants.P_OUT_INOUT_OP_DVAL);
	}

	public static String getCommentHeader() {
		initPreferenceStore();
		return prefs.get(CCodeGenConstants.P_COMMENT_HEADER_KEY, CCodeGenConstants.P_COMMENT_HEADER_DVAL);
	}

	public static boolean getFormatCode() {
		initPreferenceStore();
		return prefs.getBoolean(CCodeGenConstants.P_FORMAT_CODE_KEY, CCodeGenConstants.P_FORMAT_CODE_DVAL);
	}

	public static void initPreferenceStore() {
		if(prefs == null) {
			prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		}
	}
}

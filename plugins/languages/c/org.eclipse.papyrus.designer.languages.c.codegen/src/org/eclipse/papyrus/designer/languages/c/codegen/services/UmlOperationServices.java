/*******************************************************************************
 * Copyright (c) 2007 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.languages.c.codegen.services;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;

public class UmlOperationServices {

	/**
	 * Removes the return parameter of the operation parameter list
	 * 
	 * @return operation parameters without the return parameter
	 */
	public static EList<Parameter> getArguments(Operation operation) {
		Parameter retParam = operation.getReturnResult();
		EList<Parameter> params = new BasicEList<Parameter>();
		params.addAll(operation.getOwnedParameters());
		params.remove(retParam);
		return params;
	}
}

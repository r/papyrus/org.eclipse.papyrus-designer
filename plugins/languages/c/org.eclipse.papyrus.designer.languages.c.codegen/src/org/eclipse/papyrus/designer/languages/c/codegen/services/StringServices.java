/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.c.codegen.services;

import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.TypedElement;

public class StringServices {

	private static final String NAME_SEP = "::"; //$NON-NLS-1$

	/**
	 * get the relative path between two elements in a model format : ./P1/P2 or
	 * ../../P1/
	 */
	@SuppressWarnings("nls")
	public String getRelativePath(NamedElement src, NamedElement dest)
		throws ArrayIndexOutOfBoundsException {
		try {
			String[] qualifiedNameSrcParts = src.getQualifiedName().split(NAME_SEP);
			String[] qualifiedNameDestParts = dest.getQualifiedName().split(
				NAME_SEP);
			String tmp_src = src.getQualifiedName();
			String tmp_dest = dest.getQualifiedName();
			String relativePath = StringConstants.EMPTY;

			//System.out.println("---");
			//System.out.println("src : "+src.getQualifiedName());
			//System.out.println("dest : "+dest.getQualifiedName());

			for(int i = 0; i < Math.min(qualifiedNameSrcParts.length, qualifiedNameDestParts.length); i++) {
				if(qualifiedNameSrcParts[i]
					.equalsIgnoreCase(qualifiedNameDestParts[i])) {
					tmp_src = tmp_src.replaceFirst(qualifiedNameDestParts[i]
						+ NAME_SEP, StringConstants.EMPTY);
					tmp_dest = tmp_dest.replaceFirst(qualifiedNameDestParts[i]
						+ NAME_SEP, StringConstants.EMPTY);
				}
			}
			qualifiedNameSrcParts = tmp_src.split(NAME_SEP);
			qualifiedNameDestParts = tmp_dest.split(NAME_SEP);

			if(qualifiedNameSrcParts.length < 2
				&& qualifiedNameDestParts.length > 1) {
				//System.out.println("Src and Dest in the same Package");
				relativePath = relativePath + "./";
			} else {
				for(int i = 0; i < (qualifiedNameSrcParts.length - 1); i++) {
					relativePath = relativePath + "../";
				}
			}

			//System.out.println("Lenght dest	parts"+qualifiedNameDestParts.length);
			for(int i = 0; i < (qualifiedNameDestParts.length - 1); i++) {
				//System.out.println("qualifiedNameDestParts[i]"+qualifiedNameDestParts[i]);
				relativePath = relativePath + qualifiedNameDestParts[i] + StringConstants.SLASH;
			}
			//System.out.println("relativePath :"+relativePath);
			return relativePath;
		} catch (ArrayIndexOutOfBoundsException e) {
			return StringConstants.EMPTY;
		}
	}

	/**
	 * return a string with quote if the property, the parameter or the slot
	 * defining feature are typed as Strings
	 */
	@SuppressWarnings("nls")
	public String LiteralStringValue(LiteralString lst)
	{
		if(lst.getOwner() instanceof Property || lst.getOwner() instanceof Parameter) {
			TypedElement tmp = (TypedElement)lst.getOwner();
			if(tmp.getType() == null) {
				return "\" \"";
			}
			if(tmp.getType().getName().equalsIgnoreCase("char")) {
				if(lst.getValue().toString().equalsIgnoreCase(StringConstants.EMPTY)) {
					return "' '";
				} else {
					return withSimpleQuote(lst.getValue().toString());
				}
			}
			if(tmp.getType().getName().equalsIgnoreCase("String")) {
				if(lst.getValue().toString().equalsIgnoreCase(StringConstants.EMPTY)) {
					return "\" \"";
				} else {
					return withDoubleQuote(lst.getValue().toString());
				}
			}
		}
		if(lst.getOwner().eClass().getName().equalsIgnoreCase("Slot")) {
			Slot tmp = (Slot)lst.getOwner();
			if(tmp.getDefiningFeature() == null
				|| tmp.getDefiningFeature().getType() == null) {
				return StringConstants.SPACE;
			}
			if(tmp.getDefiningFeature().getName().equalsIgnoreCase("char")) {
				if(lst.getValue().toString().equalsIgnoreCase(StringConstants.EMPTY)) {
					return "' '";
				} else {
					return withSimpleQuote(lst.getValue().toString());
				}
			}
			if(tmp.getDefiningFeature().getType().getName().equalsIgnoreCase(
				"String")) {
				if(lst.getValue().toString().equalsIgnoreCase(StringConstants.EMPTY)) {
					return "\" \"";
				} else {
					return withDoubleQuote(lst.getValue().toString());
				}
			}

		}
		return lst.getValue();
	}

	/**
	 * Transform a String with notation aa.bb.cc to aa/bb/cc<br/>
	 * Usefull for package translating.
	 */
	public String toPath(String packageName) {
		return packageName.trim().replaceAll("\\.", StringConstants.SLASH); //$NON-NLS-1$
	}

	/**
	 * Round value with double quote if is not done (abcd to "abcd")<br/>
	 * Replace double quote with \" (ab"cd to "ab\"cd")<br/>
	 * Remove simple
	 * quote starting and ending in value ('abcd' to "abcd")<br/>
	 * Usefull for
	 * default string value.
	 */
	public String withDoubleQuote(String value) {
		if(value.endsWith(StringConstants.SINGLE_QUOTE) && value.startsWith(StringConstants.SINGLE_QUOTE)) {
			value = value.substring(1, value.length() - 1);
		}
		if(value.endsWith(StringConstants.QUOTE) && value.startsWith(StringConstants.QUOTE)) {
			value = value.substring(1, value.length() - 1);
		}
		// if null return null without "
		// if (value.equalsIgnoreCase("null")) {
		// return value;
		// }
		// replace " with /"
		return StringConstants.QUOTE + value.trim().replaceAll(StringConstants.QUOTE, "\\\\\\\"") + StringConstants.QUOTE; //$NON-NLS-1$
	}

	/**
	 * Round value with double quote if is not done (a to 'a')<br/>
	 * Replace
	 * double quote with \" (ab"cd to "ab\"cd")<br/>
	 * Remove simple quote
	 * starting and ending in value ('abcd' to "abcd")<br/>
	 * Usefull for default
	 * string value.
	 */
	public String withSimpleQuote(String value) {
		if(value.endsWith(StringConstants.SINGLE_QUOTE) && value.startsWith(StringConstants.SINGLE_QUOTE)) {
			value = value.substring(1, value.length() - 1);
		}
		if(value.endsWith(StringConstants.QUOTE) && value.startsWith(StringConstants.QUOTE)) {
			value = value.substring(1, value.length() - 1);
		}
		// if null return null without "
		if(value.equalsIgnoreCase("null")) { //$NON-NLS-1$
			return value;
		}
		// replace " with /"
		return StringConstants.SINGLE_QUOTE + value.trim() + StringConstants.SINGLE_QUOTE;
	}
}

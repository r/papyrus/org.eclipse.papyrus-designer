package org.eclipse.papyrus.designer.languages.c.codegen.utils;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.languages.c.codegen.utils.messages"; //$NON-NLS-1$
	public static String LocateCProject_ApplyCNatureTitle;
	public static String LocateCProject_ApplyCNatureDesc;
	public static String LocateCProject_CreateTargetProjectTitle;
	public static String LocateCProject_CreateTargetProjectDesc;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}

/*******************************************************************************
 * Copyright (c) 2013 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher - ansgar.radermacher@cea.fr CEA LIST - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.sync;

/**
 * Simple grouping of modifier related parameters
 */
public class ParameterModifiers {
	public ParameterModifiers() {
		isPointer = false;
		isRef = false;
		isRegister = false;
		array = ""; //$NON-NLS-1$
	}
	
	/**
	 * true, if parameter is a pointer
	 */
	public boolean isPointer;
	
	/**
	 * true, if parameter is a reference
	 */
	public boolean isRef;
	
	/**
	 * true, if parameter is a register
	 */
	public boolean isRegister;
	
	/**
	 * value of array modifiers (e.g. [2])
	 */
	public String array ;
}

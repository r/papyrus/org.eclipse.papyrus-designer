/*******************************************************************************
 * Copyright (c) 2015 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Ansgar Radermacher - ansgar.radermacher@cea.fr CEA LIST - initial API and implementation
 *
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor;

import org.eclipse.papyrus.designer.languages.common.extensionpoints.GenerationConstants;

/**
 * Collection of utility operations that are used by the CDT editor.
 */
public class Utils {

	/**
	 * Remove generated code from a body (everything between the GENERATED_START flag
	 * 
	 * @param bodyStr
	 * @return the code without the start and end tags 
	 */
	public static String removeGenerated(String bodyStr) {
		for (;;) {
			int startPos = bodyStr.indexOf(GenerationConstants.GENERATED_START);
			if (startPos == -1) {
				break;
			}
			// search line break of previous line (if any)
			while ((startPos > 0) && bodyStr.charAt(startPos) != '\r' && bodyStr.charAt(startPos) != '\n') {
				startPos--;
			}
			int endPos = bodyStr.indexOf(GenerationConstants.GENERATED_END, startPos);
			if (endPos == -1) {
				break;
			}
			endPos += GenerationConstants.GENERATED_END.length();
			// stop at first non white-space character after comment.
			while ((endPos < bodyStr.length()) && Character.isWhitespace(bodyStr.charAt(endPos))) {
				endPos++;
			}
			bodyStr = bodyStr.substring(0, startPos) + bodyStr.substring(endPos);
		}
		return bodyStr;
	}
}

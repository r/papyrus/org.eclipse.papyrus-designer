/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) Ansgar.Radermacher@cea.fr - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.editor;

import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.ISourceRange;
import org.eclipse.cdt.core.model.ISourceReference;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.cdt.internal.ui.editor.CEditor;
import org.eclipse.cdt.ui.CDTUITools;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.Activator;
import org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.sync.ObtainICElement;
import org.eclipse.papyrus.designer.languages.cpp.cdt.texteditor.sync.SyncCDTtoModel;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.infra.ui.lifecycleevents.DoSaveEvent;
import org.eclipse.papyrus.infra.ui.lifecycleevents.ILifeCycleEventsProvider;
import org.eclipse.papyrus.infra.ui.lifecycleevents.ISaveEventListener;
import org.eclipse.papyrus.infra.ui.services.EditorLifecycleEventListener;
import org.eclipse.papyrus.infra.ui.services.EditorLifecycleManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.NamedElement;

/**
 * A specialization of the CDT editor made for integration into Papyrus
 *
 */
@SuppressWarnings("restriction")
public class SyncCDTEditor extends CEditor {

	protected ILifeCycleEventsProvider lifeCycleEvents;

	protected ISaveEventListener preSaveEvent;

	/** the text listener */
	protected FocusListener focusListener;

	protected SyncCDTtoModel syncCpp;

	// a bar in different colors indicating whether synchronization
	// is active.
	protected Composite syncBar;

	public static final RGB ORANGE = new RGB(240, 150, 100);

	public static final RGB GREEN = new RGB(150, 230, 100);

	public SyncCDTEditor() {
		super();
	}

	/**
	 *
	 * initialize editor data
	 *
	 * @param registry
	 *            Papyrus service registry
	 * @param syncCpp
	 *            reference to class that synchronizes the editor content with the model
	 */
	public void setEditorData(final ServicesRegistry registry, final SyncCDTtoModel syncCpp) {
		try {
			// synchronize, before the UML model is saved.
			this.syncCpp = syncCpp;
			lifeCycleEvents = registry.getService(ILifeCycleEventsProvider.class);
			syncBar.setBackground(new Color(Display.getDefault(), GREEN));
			preSaveEvent = new ISaveEventListener() {

				@Override
				public void doSaveAs(DoSaveEvent event) {
				}

				@Override
				public void doSave(DoSaveEvent event) {
					SyncCDTEditor.this.syncCpp.syncCDTtoModel();
				}
			};
			lifeCycleEvents.addAboutToDoSaveListener(preSaveEvent);

			EditorLifecycleManager editorlifeCycle = registry.getService(EditorLifecycleManager.class);
			EditorLifecycleEventListener closeListener = new EditorLifecycleEventListener() {

				@Override
				public void postInit(IMultiDiagramEditor editor) {
				}

				@Override
				public void postDisplay(IMultiDiagramEditor editor) {
				}

				@Override
				public void beforeClose(IMultiDiagramEditor editor) {
					SyncCDTEditor.this.syncCpp = null;
					syncBar.setBackground(new Color(Display.getDefault(), ORANGE));
				}
			};
			editorlifeCycle.addEditorLifecycleEventsListener(closeListener);

		} catch (ServiceException e) {
			Activator.log.error(e);
		}
	}

	/**
	 *
	 * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createSourceViewer(org.eclipse.swt.widgets.Composite, org.eclipse.jface.text.source.IVerticalRuler, int)
	 *
	 * @param parent
	 * @param ruler
	 * @param styles
	 * @return a source viewers
	 */
	@Override
	public ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {

		parent.setLayout(new GridLayout(2, false));
		syncBar = new Composite(parent, SWT.NONE);

		GridData barGD = new GridData();
		barGD.widthHint = 5;
		barGD.verticalAlignment = GridData.FILL;
		syncBar.setLayoutData(barGD);

		final ISourceViewer viewer = super.createSourceViewer(parent, ruler, styles);
		GridData editorGD = new GridData();
		editorGD.horizontalAlignment = GridData.FILL;
		editorGD.verticalAlignment = GridData.FILL;
		editorGD.grabExcessHorizontalSpace = true;
		editorGD.grabExcessVerticalSpace = true;
		parent.getChildren()[1].setLayoutData(editorGD);

		// parent.setBackground(new Color(Display.getDefault(), ORANGE));
		syncBar.setBackground(new Color(Display.getDefault(), ORANGE));
		parent.pack();
		// Composite composite = parent;
		// while (composite != null) {
		// if (composite instanceof CTabFolder) {
		// CTabFolder tabFolder = (CTabFolder) composite;
		// CTabItem t = tabFolder.getItem(tabFolder.getItemCount() - 1);
		// Control tabFolders[] = tabFolder.getParent().getChildren();
		// if (tabFolders.length == 1) {
		// // TODO: create a 2nd tabFolder and move element. Unclear side-effects => not done
		// CTabFolder right = new CTabFolder(folder.getParent(), SWT.RIGHT);
		// }
		// }
		// composite = composite.getParent();
		// }

		focusListener = new FocusListener() {

			@Override
			public void focusLost(FocusEvent e) {
				// potential problem for undo/redo!!
				if (isDirty() && syncCpp != null) {
					// by saving, we assure that the editor automatically updates when the file is re-generated due
					// to model changes. Otherwise, the user will get a popup whether he wants to load changed the changed
					// file (and loose editor contents).
					// Currently, the is-dirty flag is used to detect whether contents need to be synchronized to the model.
					// If we stop saving automatically on focus lost, we would need to add another detection.
					//
					doSave(new NullProgressMonitor());
					syncCpp.syncCDTtoModel();
				}
			}

			@Override
			public void focusGained(FocusEvent e) {
			}
		};
		// register focus listener
		viewer.getTextWidget().addFocusListener(focusListener);

		return viewer;
	}

	/**
	 * Goto a specific element within the text editor. Currently, only methods are supported.
	 *
	 * @param element
	 *            a named UML element
	 */
	public void gotoElement(NamedElement element) {
		ICElement ice = CDTUITools.getEditorInputCElement(getEditorInput());

		if (ice instanceof ITranslationUnit) {
			ITranslationUnit itu = (ITranslationUnit) ice;
			ICElement icElement = ObtainICElement.getICElement(syncCpp.getCodeGen(), itu, element);
			if (icElement instanceof ISourceReference) {
				try {
					ISourceRange range = ((ISourceReference) icElement).getSourceRange();

					ISourceViewer viewer = getSourceViewer();
					viewer.revealRange(range.getStartPos(), 1);
					viewer.setSelectedRange(range.getStartPos(), range.getLength());
					return;
				} catch (CoreException e) {
					Activator.log.error(e);
				}
			}
		}
	}

	/**
	 *
	 * @see org.eclipse.ui.editors.text.TextEditor#dispose()
	 *
	 */
	@Override
	public void dispose() {
		// we remove the listener
		StyledText st = getSourceViewer().getTextWidget();
		if (st != null && focusListener != null) {
			st.removeFocusListener(focusListener);
		}

		// remove save event listener
		if (lifeCycleEvents != null) {
			lifeCycleEvents.removeAboutToDoSaveListener(preSaveEvent);
		}
	}
}

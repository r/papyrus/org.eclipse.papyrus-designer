/*******************************************************************************
 * Copyright (c) 2016 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   Van Cam Pham (CEA LIST) <vancam.pham@cea.fr> - Reverse implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.reverse

import java.util.ArrayList
import java.util.Collections
import java.util.HashMap
import java.util.List
import java.util.Map
import java.util.logging.Logger
import org.eclipse.cdt.core.CCorePlugin
import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit
import org.eclipse.cdt.core.model.ICContainer
import org.eclipse.cdt.core.model.ICElement
import org.eclipse.cdt.core.model.ICProject
import org.eclipse.cdt.core.model.IDeclaration
import org.eclipse.cdt.core.model.IEnumeration
import org.eclipse.cdt.core.model.IEnumerator
import org.eclipse.cdt.core.model.IField
import org.eclipse.cdt.core.model.IMethodDeclaration
import org.eclipse.cdt.core.model.IParent
import org.eclipse.cdt.core.model.IStructure
import org.eclipse.cdt.core.model.ITranslationUnit
import org.eclipse.cdt.core.model.ITypeDef
import org.eclipse.core.resources.IResourceDelta
import org.eclipse.core.runtime.IProgressMonitor
import org.eclipse.emf.common.notify.Notification
import org.eclipse.emf.common.util.UniqueEList
import org.eclipse.papyrus.designer.languages.common.base.StdUriConstants
import org.eclipse.papyrus.designer.languages.cpp.profile.CppProfileResource
import org.eclipse.papyrus.designer.languages.cpp.reverse.change.CElementChange
import org.eclipse.papyrus.designer.languages.cpp.reverse.change.ChangeMapStore
import org.eclipse.papyrus.designer.languages.cpp.reverse.change.CppChangeObject
import org.eclipse.papyrus.designer.languages.cpp.reverse.change.ModelChangeObject
import org.eclipse.papyrus.designer.languages.cpp.reverse.utils.FileWatcher
import org.eclipse.papyrus.designer.languages.cpp.reverse.utils.ModelManagement
import org.eclipse.papyrus.designer.languages.cpp.reverse.utils.RoundtripCppUtils
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.EnumerationLiteral
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.ParameterDirectionKind
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.Type

import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ASTUtils.*
import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ReverseUtils.*
import org.eclipse.cdt.core.model.IBinary
import org.eclipse.core.resources.IResource
import org.eclipse.cdt.core.model.ISourceRoot
import org.eclipse.core.runtime.SubMonitor

/**
 * The main reverser class that will reverse C++ to UML
 * 
 */
class ReverseCpp2Uml extends ReverseData {
	enum ReverseMode {
		BATCH,
		INCREMENTAL
	}

	enum ConflictResolutionMode {
		FROM_MODEL,
		FROM_CODE,
		UI_INTERACTION
	}

	ConflictResolutionMode conflictResolveMode = ConflictResolutionMode.FROM_CODE

	public static String C_LangID = "C"
	public static String Cpp_LangID = "C++"

	String langID = "C++"

	static final String TEMPLATE_PARAMETER_SIGNATURE_NAME = "template_paremeter_signature"
	public static final String REVERSE_FOLDER = "reversed_models"
	public static final String MODEL_POSTFIX = ".uml"

	final static Logger LOGGER = Logger.getLogger(ReverseCpp2Uml.getName());

	String path;

	static long timestamp = 0

	ReverseMode reverseMode = ReverseMode.BATCH

	int pass
	
	// this list is used for incremental reverse engineering
	List<CppChangeObject> changeList = new ArrayList
	static List<ModelChangeObject> modelChangeList = new ArrayList

	final static String projectPrefix = "org.eclipse.papyrus.cppgen"

	static def addModelChange(ModelChangeObject change) {
		modelChangeList.add(change)
	}

	static def clearModelChange() {
		modelChangeList.clear
	}

	new(ITranslationUnit unit, IProgressMonitor monitor, String langID) {
		this(unit.CProject, monitor, langID)
		this.unit = unit
	}

	new(ITranslationUnit unit, IProgressMonitor monitor) {
		this(unit.CProject, monitor, Cpp_LangID)
	}

	new(ICProject project, IProgressMonitor monitor, String langID) {
		this(project, monitor, langID, null)
	}

	new(ICProject project, IProgressMonitor monitor) {
		this(project, monitor, Cpp_LangID, null)
	}

	new(ICProject project, IProgressMonitor monitor, String langID, String path) {
		this.project = project
		this.monitor = monitor
		this.langID = langID
		this.path = path
		current = this;
		// initialize standard data
		index = CCorePlugin.getIndexManager().getIndex(project)
		tuToASTtuMap = new HashMap<ITranslationUnit, IASTTranslationUnit>()
		includesMap = new HashMap<String, Boolean>()
		containers = new UniqueEList<ICContainer>
		map = new HashMap
		analyzeMap = new HashMap
	}

	def reverse() {
		var projectName = unit.CProject.elementName
		var iSync = new BatchReverseFunctionBody(unit, projectName)
		iSync.run
	}

	/**
	 * Check whether the passed container is a source folder, in the sense that it
	 * contains translation units (directly or indirectly)
	 */
	private def boolean isSourceFolder(ICContainer container) {
		for (child : container.children) {
			if (child instanceof ITranslationUnit) {
				return true
			} else if (child instanceof ICContainer) {
				if ((child as ICContainer).isSourceFolder) {
					return true
				}
			}
		}
		return false
	}

	def reverseProject(boolean reset) {
		reverseProject(reset, null)
	}

	def reverseProject(boolean reset, IResource fileOrFolder) {
		
		var ICContainer[] sourceRoots
		if (fileOrFolder !== null) {
			sourceRoots = Collections.singleton(project.findElement(fileOrFolder.fullPath) as ICContainer)
		}
		else {
			sourceRoots = project.sourceRoots.filter[it.isSourceFolder]
		}
		if (sourceRoots.size < 1) {
			throw new Exception("No source folder")
		}

		containers.clear
		var sourceRootGenerated = sourceRoots.filter[it.elementName.contains(projectPrefix)].head
		if (sourceRootGenerated !== null) {
			containers.addAll(sourceRootGenerated.children.filter(typeof(ICContainer)).filter[it.sourceFolder])
		} else {
			containers.addAll(sourceRoots)
		}

		modelManager = new ModelManagement
		var String umlFilePath
	
		if (path === null || path.equals("")) {
			var fileName = project.elementName
			if (fileOrFolder !== null) {
				// file name is a concatenation with folder name
				fileName = String.format("%s-%s", project.elementName, fileOrFolder.name)
			} 
			fileName += MODEL_POSTFIX;
			umlFilePath = modelManager.getPath(
				project.project,
				REVERSE_FOLDER,
				fileName
			)
		} else {
			umlFilePath = path
		}

		modelManager.createOrgetModel(project.elementName, umlFilePath, !reset, reset)

		models = modelManager.models
		for (model : models) {
			RoundtripCppUtils.applyProfile(model, CppProfileResource.PROFILE_PATH)
			RoundtripCppUtils.applyProfile(model, StdUriConstants.UML_STD_PROFILE_PATH)
			model.setXmlID
		}

		var count = 0;
		for (container : containers) {
			count += container.countTUs
		}
		monitor.beginTask("Reverse source files", count * 2)

		for (pass = 1; pass <= 2; pass++) {
			includesMap.clear
			for (container : containers) {
				reverseProject(container)
			}
		}

		for (subModel : models) {
			subModel.eResource.save(ModelManagement.getDefaultSaveOptions)
		}
		// modelManager.modesaveModel(Collections.singleton(umlFilePath).toList);
		timestamp = models.last.eResource.timeStamp / 1000
		clearRawChangeList
		modelManager.dispose
	}

	def void syncIncrementalProject() {
		reverseMode = ReverseMode.INCREMENTAL
		var sourceRoots = project.sourceRoots.filter[it.sourceFolder]
		if (sourceRoots.size < 1) {
			throw new Exception("No source folder")
		}

		containers.clear
		var sourceRootGenerated = sourceRoots.filter[it.elementName.contains(projectPrefix)].head
		if (sourceRootGenerated !== null) {
			containers.addAll(sourceRootGenerated.children.filter(typeof(ICContainer)).filter[it.sourceFolder])
		} else {
			containers.addAll(sourceRoots)
		}

		modelManager = new ModelManagement

		var String umlFilePath
		if (path === null || path.equals("")) {
			umlFilePath = modelManager.getPath(
				project.project,
				REVERSE_FOLDER,
				project.elementName + MODEL_POSTFIX
			)

		} else {
			umlFilePath = path
		}

		modelManager.createOrgetModel(project.elementName, umlFilePath, false, false)

		models = modelManager.models
		models.forEach [
			RoundtripCppUtils.applyProfile(it, CppProfileResource.PROFILE_PATH)
			RoundtripCppUtils.applyProfile(it, StdUriConstants.UML_STD_PROFILE_PATH)
		]

		if (rawChangeList === null || rawChangeList.size == 0) {
			var fileWatcher = new FileWatcher(project, timestamp)
			var modifiedItus = fileWatcher.getModifiledTranslationUnits(project)
			for (modified : modifiedItus) {
				modified.syncTranslationUnit
			}
			modelManager.saveModel(Collections.singleton(umlFilePath).toList);
			timestamp = models.last.eResource.timeStamp / 1000
		} else {
			reverseIncrementalChanges
			modelManager.saveModel(Collections.singleton(umlFilePath).toList);
		}

		modelManager.dispose

	/*CommandSupport.executeCmd(modelManager.resourceSet, new Runnable() {
	 * 	
	 * 	override run() {
	 * 		m_models = modelManager.models
	 * 		// m_model.name = containers.head.elementName
	 * 		m_models.forEach [
	 * 			RoundtripCppUtils.applyProfile(it, RoundtripCppUtils.cppProfileUri)
	 * 		]
	 * 
	 * 		if (rawChangeList === null || rawChangeList.size == 0) {
	 * 			var fileWatcher = new FileWatcher(project, timestamp)
	 * 			var modifiedItus = fileWatcher.getModifiledTranslationUnits(project)
	 * 			for(modified:modifiedItus) {
	 * 				modified.syncTranslationUnit
	 * 			}
	 * 			modelManager.saveModel(paths);
	 * 			timestamp = m_models.last.eResource.timeStamp/1000
	 * 		} else {
	 * 			reverseIncrementalChanges
	 * 			modelManager.saveModel(paths);
	 * 		}
	 * 		
	 * 		modelManager.dispose
	 * 	}			
	 })*/
	}

	/**
	 * Count the number of translation units within a parent, in order
	 * to configure the progess monitor
	 */
	private def int countTUs(IParent parent) {
		var count = 0
		if (!(parent instanceof ITranslationUnit)) {
			for (child : parent.children) {
				if (child instanceof ITranslationUnit) {
					count++;
				}
				else if (child instanceof IBinary) {
					// do nothing
				}
				else if (child instanceof IParent) {
					count += child.countTUs
				}
			}
		}
		return count
	}

	/**
	 * reverse a project
	 */	
	private def void reverseProject(IParent parent) {
		for (child : parent.children) {
			if (monitor.isCanceled) return;
			if (child instanceof ITranslationUnit) {
				(child as ITranslationUnit).reverseHeader
				if (false && !child.isHeaderUnit) {
					// TODO: do not reverse implementations for the moment
					monitor.subTask("Parsing method implementations in " + child.elementName)
					child.reverseSource
				}
			}
			else if (child instanceof IBinary) {
				// do nothing
			}
			else if (child instanceof IParent) {
				reverseProject(child)
			}
		}
	}

	/**
	 * Reverse the source code of a translation unit (that is not a header unit)
	 */
	private def reverseSource(ITranslationUnit unit) {
		// getOrCreateClassifier(unit.correspondingModel, unit)
		var sync = new BatchReverseFunctionBody(unit, project.elementName, unit.correspondingModel)
		try {
			sync.run
		} catch (Exception e) {
			e.printStackTrace;
		}
	}

	def reverseHeader(ITranslationUnit headerUnit) {
		if (!includesMap.containsKey(headerUnit.path.toString())) {
			includesMap.put(headerUnit.path.toString(), true)
			
			monitor.subTask(String.format("Pass %d: parsing types in %s", pass, headerUnit.elementName))
			monitor.worked(1);
			if (pass == 1) {
				GetOrCreateP1.getOrCreateClassifiers(headerUnit)
			} else if (pass == 2) {
				GetOrCreateP2.getOrCreateClassifiers(headerUnit)
			}
		}
	}

	/**
	 * provide access to instance
	 */
	def static currentCpp2Uml() {
		return current as ReverseCpp2Uml;
	}
	
	private def syncTranslationUnit(ITranslationUnit itu) {
		// check if a structure is created/deleted/renamed
		var structures = itu.getAllIStructures(false, true, project).filter(typeof(IStructure))
		for (structure : structures) {
			var classifier = GetOrCreateP2.getClassifier(structure)
			if (classifier instanceof Classifier) {
				syncIStructureToModel(classifier, structure)
			}
		}
	}

	private def syncIStructureToModel(Classifier classifier, IStructure istructure) {
		var fields = istructure.children.filter(typeof(IField)).toList
		var attributes = classifier.ownedElements.filter(typeof(Property)).toList
		mergeAttributes(classifier, attributes, fields)
		var methods = istructure.children.filter(typeof(IMethodDeclaration)).toList
		var operations = classifier.ownedElements.filter(typeof(Operation)).toList
		mergeOperations(classifier, operations, methods)
		var declarations = istructure.children.filter(typeof(IDeclaration)).toList
		var nestedTypes = classifier.ownedElements.filter(typeof(Type)).toList
		mergeNestedTypes(classifier, nestedTypes, declarations)
	}

	private def mergeAttributes(Classifier parent, List<Property> attributes, List<IField> fields) {
		var foundFieldList = new UniqueEList<IField>
		var notFoundFieldList = new UniqueEList<IField>
		var foundAttrList = new UniqueEList<Property>
		var notFoundAttrList = new UniqueEList<Property>
		for (field : fields) {
			val attrFound = attributes.filter[it.name.equals(field.elementName)].head
			if (attrFound === null) {
				notFoundFieldList.add(field)
			} else {
				foundAttrList.add(attrFound)
				foundFieldList.add(field)
				var isModelObjectChanged = (modelChangeList.filter[it.eObject == attrFound].head !== null)
				if (isModelObjectChanged) {
					syncAttributeWithMode(parent, attrFound, field, conflictResolveMode)
				} else {
					syncAttributeWithMode(parent, attrFound, field, ConflictResolutionMode.FROM_CODE)
				}
			}
		}

		for (attr : attributes) {
			if (!foundAttrList.contains(attr)) {
				notFoundAttrList.add(attr)
			}
		}

		var addChanges = modelChangeList.filter[it.eventType == Notification.ADD].toList
		var tobeRemovedsInNotFound = new UniqueEList<Property>
		for (attr : notFoundAttrList) {
			var modelChangeObj = addChanges.filter[it.eObject == attr].head
			if (modelChangeObj !== null) {
				tobeRemovedsInNotFound.add(attr)
			} else {
			}
		}

		for (i : tobeRemovedsInNotFound) {
			notFoundAttrList.remove(i)
		}

		var processedFields = new UniqueEList<IField>
		var remainingFields = new UniqueEList<IField>
		var remainingAttributes = new UniqueEList<Property>
		for (notFoundField : notFoundFieldList) {
			if (!processedFields.contains(notFoundField)) {
				val umlType = notFoundField.declSpecifier.getUMLType(notFoundField)
				val typeName = notFoundField.typeName.cppTypeName
				var sameTypeNameFields = notFoundFieldList.filter[it.typeName.cppTypeName.equals(typeName)].toList
				var sameTypeAttrs = notFoundAttrList.filter[it.type.name.equals(umlType.name)].toList
				var i = 0
				for (i = 0; i < sameTypeNameFields.size; i++) {
					if (i >= sameTypeAttrs.size) {
						remainingFields.add(sameTypeNameFields.get(i))
					} else {
						val tobeProcessedField = sameTypeNameFields.get(i)
						val tobeProcessedAttr = sameTypeAttrs.get(i)
						var isModelObjectChanged = (modelChangeList.filter[it.eObject == tobeProcessedAttr].head !==
							null)
						if (isModelObjectChanged) {
							syncAttributeWithMode(parent, tobeProcessedAttr, tobeProcessedField,
								ConflictResolutionMode.FROM_MODEL)
						} else {
							syncAttributeWithMode(parent, tobeProcessedAttr, tobeProcessedField,
								ConflictResolutionMode.FROM_CODE)
						}
						processedFields.add(tobeProcessedField)
						notFoundAttrList.remove(tobeProcessedAttr)
					}
				}
				if (i < sameTypeAttrs.length) {
					for (var j = i; j < sameTypeAttrs.length; j++) {
						remainingAttributes.add(sameTypeAttrs.get(j))
					}
				}
			}
		}

		for (notFoundAttr : notFoundAttrList) {
			if (!remainingAttributes.contains(notFoundAttr)) {
				remainingAttributes.add(notFoundAttr)
			}
		}

		for (remaining : remainingFields) {
			PropertyUtils.createProperty(remaining, parent)
		}

		var remainingChanges = modelChangeList.filter [
			it.eventType == Notification.ADD || it.eventType == Notification.SET
		].toList
		for (remaining : remainingAttributes) {
			if (remainingChanges.filter[it.eObject == remaining].empty) {
				if (parent instanceof Class) {
					(parent as Class).ownedAttributes -= remaining
				} else if (parent instanceof DataType) {
					(parent as DataType).ownedAttributes -= remaining
				}
			}
		}
	}

	private def syncAttributeWithMode(Classifier parent, Property attribute, IField field,
		ConflictResolutionMode resolveMode) {
		switch (resolveMode) {
			case FROM_MODEL: {
				// TODO: sync from model to code by regenerate the file after sync from code to model
			}
			case FROM_CODE: {
				var type = field.declSpecifier.getUMLType(field)
				attribute.type = type
				attribute.name = field.elementName
				PropertyUtils.updateProperty(field, attribute)
				var changeObj = modelChangeList.filter[it.eObject == attribute].head
				if (changeObj !== null) {
					modelChangeList.remove(changeObj)
				}
			}
			case UI_INTERACTION: {
				// TODO: display UI
			}
		}
	}

	private def syncOperationWithMode(Classifier parent, Operation op, IMethodDeclaration method,
		ConflictResolutionMode resolveMode) {
		switch (resolveMode) {
			case FROM_MODEL: {
				// TODO: sync from model to code by regenerate the file after sync from code to model
			}
			case FROM_CODE: {
				op.name = method.elementName
				MethodUtils.updateMethod(parent as Class, op, method)
				var changeObj = modelChangeList.filter[it.eObject == op].head
				if (changeObj !== null) {
					modelChangeList.remove(changeObj)
				}
			}
			case UI_INTERACTION: {
				// TODO: display UI
			}
		}
	}

	private def isSameOperation(Operation op, IMethodDeclaration method) {
		var ret = true
		if (!op.name.equals(method.elementName)) {
			ret = false
		} else {
			ret = isSameSignature(op, method)
		}
		return ret
	}

	private def isSameSignature(Operation op, IMethodDeclaration method) {
		var ret = true
		var params = op.ownedParameters.filter[it.direction != ParameterDirectionKind.RETURN_LITERAL].toList
		if (method.numberOfParameters != params.size) {
			ret = false
		} else {
			for (var i = 0; i < method.numberOfParameters; i++) {
				if (!method.parameterTypes.get(i).cppTypeName.equals(params.get(i).type.name)) {
					ret = false
				}
			}
			if (ret != false) {
				var returnParam = op.ownedParameters.filter[it.direction == ParameterDirectionKind.RETURN_LITERAL].head
				if (returnParam === null) {
					if (!method.returnType.cppTypeName.equals("void")) {
						ret = false
					}
				} else if (returnParam.type !== null && !method.returnType.cppTypeName.equals(returnParam.type.name)) {
					ret = false
				}
			}
		}

		return ret
	}

	private def isSameMethodDeclaration(IMethodDeclaration method1, IMethodDeclaration method2) {
		var result = true;

		result = result && method1.signature.equals(method2.signature);
		result = result && (method1.visibility.convertVisibility == method2.visibility.convertVisibility)
		result = result && (method1.isStatic == method2.isStatic)
		result = result && (method1.isVirtual == method2.isVirtual)
		result = result && (method1.virtual == method2.virtual)
		result = result && (method1.inline == method2.inline)
		result = result && (method1.friend == method2.friend)
		result = result && (method1.volatile == method2.volatile)
		result = result && (method1.constructor == method2.constructor)
		result = result && (method1.destructor == method2.destructor)
		result = result && (method1.returnType !== null && method2.returnType !== null)
		if (method1.returnType !== null && method2.returnType !== null) {
			result = result && method1.returnType.equals(method2.returnType)
		}
		result = result && (method1.numberOfParameters == method2.numberOfParameters)

		if (result == false) {
			return result
		}

		try {
			var declarator1 = method1.declarator
			var declarator2 = method2.declarator

			result = result && (declarator1.rawSignature.equals(declarator2.rawSignature))
		} catch (Exception e) {
			e.printStackTrace
			return result
		}

		return result
	}

	private def getNumberOfSameParameters(Operation op, IMethodDeclaration method) {
		var ret = 0
		var params = op.ownedParameters.filter[it.direction != ParameterDirectionKind.RETURN_LITERAL].toList
		for (var i = 0; i < method.numberOfParameters; i++) {
			if (i <= params.size) {
				if (params.get(i).type !== null &&
					method.parameterTypes.get(i).cppTypeName.equals(params.get(i).type.name)) {
					ret++
				}
			}
		}
		return ret
	}

	private def findMostSameOperation(List<Operation> ops, IMethodDeclaration method) {
		if(ops.empty) return {
			null
		}
		var ret = ops.head
		for (op : ops) {
			if (ret != op) {
				if (op.getNumberOfSameParameters(method) > ret.getNumberOfSameParameters(method)) {
					ret = op
				}
			}
		}
		return ret
	}

	private def mergeOperations(Classifier parent, List<Operation> operations, List<IMethodDeclaration> methods) {
		var foundMethodList = new UniqueEList<IMethodDeclaration>
		var notFoundMethodList = new UniqueEList<IMethodDeclaration>
		var foundOperationList = new UniqueEList<Operation>
		var notFoundOperationList = new UniqueEList<Operation>
		for (method : methods) {
			val opFound = operations.filter[it.isSameOperation(method)].head
			if (opFound === null) {
				notFoundMethodList.add(method)
			} else {
				foundOperationList.add(opFound)
				foundMethodList.add(method)
				var isModelObjectChanged = (modelChangeList.filter[it.eObject == opFound].head !== null)
				if (isModelObjectChanged) {
					// syncAttributeWithMode(parent, attrFound, field, conflictResolveMode)
				} else {
					// syncAttributeWithMode(parent, attrFound, field, ConflictResolutionMode.FROM_CODE)
				}
			}
		}

		for (op : operations) {
			if (!foundOperationList.contains(op)) {
				notFoundOperationList.add(op)
			}
		}

		var addChanges = modelChangeList.filter[it.eventType == Notification.ADD].toList
		var tobeRemovedsInNotFound = new UniqueEList<Operation>
		for (op : notFoundOperationList) {
			var modelChangeObj = addChanges.filter[it.eObject == op].head
			if (modelChangeObj !== null) {
				tobeRemovedsInNotFound.add(op)
			} else {
			}
		}

		for (i : tobeRemovedsInNotFound) {
			notFoundOperationList.remove(i)
		}

		var processedMethods = new UniqueEList<IMethodDeclaration>
		var remainingMethods = new UniqueEList<IMethodDeclaration>
		var remainingOperations = new UniqueEList<Operation>
		for (notFoundMethod : notFoundMethodList) {
			if (!processedMethods.contains(notFoundMethod)) {
				val functionName = notFoundMethod.elementName.split("::").last
				val sameSignatureOperation = notFoundOperationList.filter[it.isSameSignature(notFoundMethod)].head
				if (sameSignatureOperation !== null) {
					var isModelObjectChanged = (modelChangeList.filter[it.eObject == sameSignatureOperation].head !==
						null)
					{
						if (isModelObjectChanged) {
							syncOperationWithMode(parent, sameSignatureOperation, notFoundMethod,
								ConflictResolutionMode.FROM_MODEL)
						} else {
							syncOperationWithMode(parent, sameSignatureOperation, notFoundMethod,
								ConflictResolutionMode.FROM_CODE)
						}
					}
					notFoundOperationList.remove(sameSignatureOperation)
					processedMethods.add(notFoundMethod)
				} else {
					// look for function with same name
					val sameNameMethods = notFoundMethodList.filter[it.elementName.equals(functionName)].toList
					val sameNameOperations = notFoundOperationList.filter[it.name.equals(functionName)].toList
					if (sameNameOperations.size > 0) {
						var i = 0
						for (i = 0; i < sameNameMethods.size; i++) {
							if (i >= sameNameOperations.size) {
								remainingMethods.add(sameNameMethods.get(i))
							} else {
								val tobeProcessedMethod = sameNameMethods.get(i)
								val tobeProcessedOp = sameNameOperations.get(i)
								var isModelObjectChanged = (modelChangeList.filter[it.eObject == tobeProcessedOp].
									head !== null)
								if (isModelObjectChanged) {
									syncOperationWithMode(parent, tobeProcessedOp, tobeProcessedMethod,
										ConflictResolutionMode.FROM_MODEL)
								} else {
									syncOperationWithMode(parent, tobeProcessedOp, tobeProcessedMethod,
										ConflictResolutionMode.FROM_CODE)
								}
								processedMethods.add(tobeProcessedMethod)
								notFoundOperationList.remove(tobeProcessedOp)
							}
						}
						if (i < sameNameOperations.length) {
							for (var j = i; j < sameNameOperations.length; j++) {
								remainingOperations.add(sameNameOperations.get(j))
							}
						}
					} else {
						val mostSameOp = findMostSameOperation(notFoundOperationList, notFoundMethod)
						if (mostSameOp !== null) {
							var isModelObjectChanged = (modelChangeList.filter[it.eObject == mostSameOp].head !== null)
							if (isModelObjectChanged) {
								syncOperationWithMode(parent, mostSameOp, notFoundMethod,
									ConflictResolutionMode.FROM_MODEL)
							} else {
								syncOperationWithMode(parent, mostSameOp, notFoundMethod,
									ConflictResolutionMode.FROM_CODE)
							}
							processedMethods.add(notFoundMethod)
							notFoundOperationList.remove(mostSameOp)
						}
					}

				}

			}
		}

		for (notFoundOp : notFoundOperationList) {
			if (!remainingOperations.contains(notFoundOp)) {
				remainingOperations.add(notFoundOp)
			}
		}

		for (remaining : remainingMethods) {
			MethodUtils.createMethod(remaining, parent as Class)
		}

		var remainingChanges = modelChangeList.filter [
			it.eventType == Notification.ADD || it.eventType == Notification.SET
		].toList
		for (remaining : remainingOperations) {
			if (remainingChanges.filter[it.eObject == remaining].empty) {
				if (parent instanceof Class) {
					(parent as Class).ownedOperations -= remaining
				} else if (parent instanceof DataType) {
					(parent as DataType).ownedOperations -= remaining
				}
			}
		}
	}

	private def mergeNestedTypes(Classifier parent, List<Type> nestedTypes, List<IDeclaration> declarations) {
	}

	private def reverseIncrementalChanges() {
		changeList.clear
		optimizeChangeList

		for (change : changeList) {
			switch (change.changeKind) {
				case IResourceDelta.ADDED: {
					addToModel(change)
				}
				case IResourceDelta.CHANGED: {
					updateToModel(change)
				}
				case IResourceDelta.REMOVED: {
					removeFromModel(change)
				}
			}
		}
		changeList.clear
	}

	private def getRawChangeList() {
		if (ChangeMapStore.changesMap !== null && ChangeMapStore.changesMap.get(project.elementName) !== null &&
			ChangeMapStore.changesMap.get(project.elementName).size > 0) {
			return ChangeMapStore.changesMap.get(project.elementName)
		}
		return null;

	}

	private def clearRawChangeList() {
		if (ChangeMapStore.changesMap !== null && project !== null &&
			ChangeMapStore.changesMap.get(project.elementName) !== null) {
			ChangeMapStore.changesMap.get(project.elementName).clear
		}
	}

	private def optimizeChangeList() {
		var rawChangeList = getRawChangeList
		changeList.clear
		var List<CElementChange> processedList = new UniqueEList
		var List<CElementChange> remainList = new UniqueEList

		// remove immediate events
		for (var i = 0; i < rawChangeList.size; i++) {
			var change = rawChangeList.get(i)

			if (isInSourceContainers(change.element)) {
				if (!processedList.contains(change)) {
					switch (change.changeKind) {
						case IResourceDelta.ADDED: {
							var inter = findIntermediateEvent(rawChangeList, i, change)
							if (inter.size > 0) {
								processedList.add(change)
								processedList.addAll(inter)
							}
						}
						case IResourceDelta.CHANGED: {
							var inter = findIntermediateEvent(rawChangeList, i, change)
							if (inter.size > 0) {
								processedList.addAll(inter)
							}
						}
						case IResourceDelta.REMOVED: {
						}
						default: {
							processedList.add(change)
						}
					}
				}

				if (!processedList.contains(change)) {
					remainList.add(change)
				}
			}
		}

		val Map<ICElement, List<CppChangeObject>> doubtChangeListMap = new HashMap

		for (var i = 0; i < remainList.size; i++) {
			var change = remainList.get(i)
			var CppChangeObject cppChangeObject = null
			switch (change.changeKind) {
				case IResourceDelta.ADDED: {
					var found = false
					var j = i + 1

					while (j < remainList.size && !found) {
						// TODO BUG: ADD(3a, 4a, 5a) then REMOVE(5, 3, 4), the first element of same type as 3a is 5 so we create a change object where 3a is the newElement and 5 is the oldElement although it's wrong
						// There is no way to know if 3a corresponds exactly to 3
						if (remainList.get(j).changeKind == IResourceDelta.REMOVED &&
							change.element.elementType == remainList.get(j).element.elementType &&
							remainList.get(j).parent == change.parent) {
							cppChangeObject = new CppChangeObject(remainList.get(j).element, change.element,
								change.element.getTranslationUnitFromElement(), change.parent, IResourceDelta.CHANGED)
							if (doubtChangeListMap.get(change.parent) === null) {
								doubtChangeListMap.put(change.parent, new ArrayList)
							}
							if (change.element instanceof IField || change.element instanceof IMethodDeclaration ||
								change.element instanceof IEnumerator) {
								doubtChangeListMap.get(change.parent).add(cppChangeObject)
							}

							remainList.remove(j)
							j--

							found = true
						}

						j++
					}

					if (!found) {
						cppChangeObject = new CppChangeObject(null, change.element,
							change.element.getTranslationUnitFromElement(), change.parent, IResourceDelta.ADDED)
					}
				}
				case IResourceDelta.CHANGED: {
					cppChangeObject = new CppChangeObject(remainList.get(i).element, change.element,
						change.element.getTranslationUnitFromElement(), change.parent, IResourceDelta.CHANGED)
				}
				case IResourceDelta.REMOVED: {
					var found = false
					var j = i + 1

					while (j < remainList.size && !found) {
						if (remainList.get(j).changeKind == IResourceDelta.ADDED &&
							change.element.elementType == remainList.get(j).element.elementType) {
							cppChangeObject = new CppChangeObject(change.element, remainList.get(j).element,
								change.element.getTranslationUnitFromElement(), change.parent, IResourceDelta.CHANGED)
							if (doubtChangeListMap.get(change.parent) === null) {
								doubtChangeListMap.put(change.parent, new ArrayList)
							}
							if (change.element instanceof IField || change.element instanceof IMethodDeclaration ||
								change.element instanceof IEnumerator) {
								doubtChangeListMap.get(change.parent).add(cppChangeObject)
							}
							remainList.remove(j)
							j--

							found = true
						}

						j++
					}

					if (!found) {
						cppChangeObject = new CppChangeObject(null, change.element,
							change.element.getTranslationUnitFromElement(), change.parent, IResourceDelta.REMOVED)
					}
				}
			}
			if (cppChangeObject !== null) {
				changeList.add(cppChangeObject)
			}
		}

		for (doubtMap : doubtChangeListMap.entrySet) {
			if (doubtMap.value.size > 1) {
				alignChangedElements(doubtMap.key, doubtMap.value)
			}
		}

		clearRawChangeList
	}

	private def alignChangedElements(ICElement parent, List<CppChangeObject> changes) {
		// This method is used to exactly verify the removed element associated to the added element in the correspond cpp change object
		// this function only checks structure members (enumerators, fields and methods)
		// The verification is based on the relative position of structure members in the code to those in the model
		val oldNamedElementsList = new ArrayList<NamedElement>
		val Map<NamedElement, CppChangeObject> oldNamedElementToChangeMap = new HashMap
		changes.forEach [
			var oldNamedElement = it.parent.getNamedElement(it.oldElement)
			oldNamedElementToChangeMap.put(oldNamedElement, it)
			oldNamedElementsList.add(oldNamedElement)
		]
		val namedElementParent = oldNamedElementsList.head.eContainer
		val alignedOldNamedElements = new ArrayList<NamedElement>
		namedElementParent.eContents.filter(typeof(NamedElement)).forEach [
			if (oldNamedElementsList.contains(it)) {
				alignedOldNamedElements.add(it)
			}
		]
		val newICElements = new ArrayList<ICElement>
		if (parent instanceof IParent) {
			// get all new elements in changes list ordered as in its parent
			parent.children.forEach [
				val childInParent = it
				val founded = changes.filter[it.newElement == childInParent].head
				if (founded !== null) {
					newICElements.add(founded.newElement)
				}
			]
		}

		if (alignedOldNamedElements.size == newICElements.size) {
			for (var i = 0; i < alignedOldNamedElements.size; i++) {
				var change = oldNamedElementToChangeMap.get(alignedOldNamedElements.get(i))
				if (change.newElement != newICElements.get(i)) {
					var sameElementType = change.newElement instanceof IField && newICElements.get(i) instanceof IField
					sameElementType = sameElementType ||
						(change.newElement instanceof IMethodDeclaration &&
							newICElements.get(i) instanceof IMethodDeclaration)
					sameElementType = sameElementType ||
						(change.newElement instanceof IEnumerator && newICElements.get(i) instanceof IEnumerator)
					if (sameElementType) {
						change.newElement = newICElements.get(i)
					}
				}
			}
		}

	}

	private def getNamedElement(ICElement parent, ICElement child) {
		var NamedElement ret
		switch (child) {
			case child instanceof IField: {
				if (parent instanceof IStructure) {
					var parentStructure = parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						if (classifier.isExist(child.elementName)) {
							var prop = classifier.ownedElements.filter(typeof(Property)).filter [
								it.name.equals(child.elementName)
							].head
							ret = prop
						}
					}
				}
			}
			case child instanceof IMethodDeclaration: {
				if (parent instanceof IStructure) { // Method declaration
					var parentStructure = parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						if (classifier.isExist(child.elementName)) {
							var samenames = classifier.ownedElements.filter(typeof(Operation)).filter [
								it.name.equals(child.elementName)
							].toList
							val oldMethod = child as IMethodDeclaration
							var op = samenames.filter[it.isSameSignature(oldMethod)].head
							if (op !== null) {
								ret = op
							}
						}
					}
				}
			}
			case child instanceof IEnumerator: {
				var cppEnumeration = parent as IEnumeration
				var enumeration = GetOrCreateP2.getClassifier(cppEnumeration)

				if (enumeration !== null && enumeration instanceof Enumeration) {
					var enumerator = (enumeration as Enumeration).ownedLiterals.filter(typeof(EnumerationLiteral)).
						filter[it.name == child.elementName].head
					if (enumerator !== null) {
						ret = enumerator
					}
				}
			}
		}
		return ret
	}

	private def boolean isInSourceContainers(ICElement element) {
		if (element === null) {
			return false
		}

		if (element instanceof ICContainer && containers.contains(element)) {
			return true
		}

		return isInSourceContainers(element.parent)
	}

	private def findIntermediateEvent(List<CElementChange> list, int position, CElementChange change) {
		var List<CElementChange> ret = new UniqueEList
		for (var i = position + 1; i < list.size; i++) {
			if (change.changeKind == IResourceDelta.ADDED && list.get(i).changeKind == IResourceDelta.REMOVED &&
				change.element.isSameICElement(list.get(i).element)) {
				if (change.element instanceof IMethodDeclaration && list.get(i).element instanceof IMethodDeclaration) {
					val method = change.element as IMethodDeclaration
					if (isSameMethodDeclaration(method, list.get(i).element as IMethodDeclaration)) {
						ret.add(list.get(i))
					}
				} else {
					ret.add(list.get(i))
				}
			} else if (change.changeKind == IResourceDelta.CHANGED &&
				change.element.isSameICElement(list.get(i).element)) {
				ret.add(list.get(i))
			}
		}
		var changed = ret.filter[it.changeKind == IResourceDelta.CHANGED].toList
		if (changed.size > 1) {
			ret.remove(changed.last)
		}

		return ret
	}

	private def isSameICElement(ICElement e1, ICElement e2) {
		var boolean ret = false
		if (e1.elementName.equals(e2.elementName) && e1.elementType == e2.elementType &&
			e1.parent.elementName.equals(e2.parent.elementName)) {
			ret = true
		}
		return ret
	}

	private def isExist(NamedElement parent, String childName) {
		return !parent.ownedElements.filter(typeof(NamedElement)).filter[it.name.equals(childName)].empty
	}

	private def addToModel(CppChangeObject change) {
		switch (change.newElement) {
			case change.newElement instanceof IField: {
				val field = change.newElement as IField
				if (field.parent instanceof IStructure) {
					var parentStructure = field.parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						if (!classifier.isExist(field.elementName)) {
							PropertyUtils.createProperty(field, classifier as Classifier)
						}
					}
				}
			}
			case change.newElement instanceof IMethodDeclaration: {
				val method = change.newElement as IMethodDeclaration
				if (method.parent instanceof IStructure) {
					var parentStructure = method.parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						var samenames = classifier.ownedElements.filter(typeof(Operation)).filter [
							it.name.equals(method.elementName)
						].toList
						if (samenames.filter[it.isSameSignature(method)].empty) {
							MethodUtils.createMethod(method, classifier as Class)
						}

					}
				} else if (method.parent instanceof ITranslationUnit) {
					// TODO a single function declaration (or definition) in a cpp file ==> possible strategy: add as a method of a class? which class?
				}
			}
			case change.newElement instanceof IStructure,
			case change.newElement instanceof IEnumeration,
			case change.newElement instanceof ITypeDef: {
				var declaration = change.newElement as IDeclaration
				GetOrCreateP1.getOrCreateClassifier(declaration)
			}
			case change.newElement instanceof IEnumerator: {
				var enumerator = change.newElement as IEnumerator
				var cppEnumeration = enumerator.path as IEnumeration
				var enumeration = GetOrCreateP2.getClassifier(cppEnumeration)

				if (enumeration instanceof Enumeration) {
					enumeration.createOwnedLiteral(enumerator.elementName).setXmlID
				}
			}
			case change.newElement instanceof ITranslationUnit: {
				var itu = change.newElement as ITranslationUnit
				GetOrCreateP1.getOrCreateClassifiers(itu)
			}
		}

	}

	private def updateToModel(CppChangeObject change) {
		switch (change.newElement) {
			case change.oldElement instanceof IField: {
				val field = change.newElement as IField
				if (field.parent instanceof IStructure) {
					var parentStructure = field.parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						if (classifier.isExist(change.oldElement.elementName)) {
							var prop = classifier.ownedElements.filter(typeof(Property)).filter [
								it.name.equals(change.oldElement.elementName)
							].head
							if (prop !== null) {
								PropertyUtils.updateProperty(change.newElement as IField, prop)
							}
						}
					}
				}
			}
			case change.oldElement instanceof IMethodDeclaration: {
				val method = change.newElement as IMethodDeclaration
				if (method.parent instanceof IStructure) { // Method declaration
					var parentStructure = method.parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						if (classifier.isExist(change.oldElement.elementName)) {
							var samenames = classifier.ownedElements.filter(typeof(Operation)).filter [
								it.name.equals(change.oldElement.elementName)
							].toList
							val oldMethod = change.oldElement as IMethodDeclaration
							var op = samenames.filter[it.isSameSignature(oldMethod)].head
							if (op !== null) {
								MethodUtils.updateMethod(classifier as Class, op,
									change.newElement as IMethodDeclaration)
							}
						}
					}
				} else if (method.parent instanceof ITranslationUnit) { // Method definition (implementation in cpp)
					reverseSource(method.parent as ITranslationUnit)
				}
			}
			case change.oldElement instanceof IStructure,
			case change.oldElement instanceof IEnumeration,
			case change.oldElement instanceof ITypeDef: {
				var declaration = change.newElement as IDeclaration
				var umlClassifier = GetOrCreateP2.getClassifier(change.oldElement as IDeclaration)
				if (umlClassifier !== null) {
					umlClassifier.name = declaration.elementName
				}
			}
			case change.oldElement instanceof IEnumerator: {
				val cppEnumerator = change.newElement as IEnumerator
				var cppEnumeration = cppEnumerator.parent as IEnumeration
				var enumeration = GetOrCreateP2.getClassifier(cppEnumeration)

				if (enumeration !== null && enumeration instanceof Enumeration) {
					var enumerator = (enumeration as Enumeration).ownedLiterals.filter(typeof(EnumerationLiteral)).
						filter[it.name == change.oldElement.elementName].head
					if (enumerator !== null) {
						enumerator.name = cppEnumerator.elementName
					}
				}
			}
			case change.oldElement instanceof ITranslationUnit: {
				var itu = change.newElement as ITranslationUnit
				if (itu.isSourceUnit) {
					reverseSource(itu)
				} else if (itu.isHeaderUnit) {
					reverseHeader(itu)
				}

			// getOrCreateClassifier(itu.correspondingModel, itu)
			}
		}
	}

	private def removeFromModel(CppChangeObject change) {
		switch (change.oldElement) {
			case change.oldElement instanceof IField: {
				val field = change.oldElement as IField
				if (field.parent instanceof IStructure) {
					var parentStructure = field.parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						if (classifier.isExist(change.oldElement.elementName)) {
							var prop = classifier.ownedElements.filter(typeof(Property)).filter [
								it.name.equals(change.oldElement.elementName)
							].head
							if (prop !== null) {
								if (classifier instanceof Class) {
									classifier.ownedAttributes -= prop
								} else if (classifier instanceof DataType) {
									classifier.ownedAttributes -= prop
								}
							}
						}
					}
				}
			}
			case change.oldElement instanceof IMethodDeclaration: {
				val method = change.oldElement as IMethodDeclaration
				if (method.parent instanceof IStructure) {
					var parentStructure = method.parent as IStructure
					var classifier = GetOrCreateP2.getClassifier(parentStructure)
					if (classifier !== null) {
						if (classifier.isExist(change.oldElement.elementName)) {
							var samenames = classifier.ownedElements.filter(typeof(Operation)).filter [
								it.name.equals(method.elementName)
							].toList
							var op = samenames.filter[it.isSameSignature(method)].head
							if (op !== null) {
								if (classifier instanceof Class) {
									classifier.ownedOperations -= op
								}
							}
						}

					}
				}
			}
			case change.oldElement instanceof IStructure,
			case change.oldElement instanceof IEnumeration,
			case change.oldElement instanceof ITypeDef: {
				val declaration = change.oldElement as IDeclaration
				if (change.parent instanceof IStructure) {
					var parentStructure = change.parent as IStructure
					var parentClassifier = GetOrCreateP2.getClassifier(parentStructure)
					var childClassifier = parentClassifier.ownedElements.filter(typeof(Type)).filter [
						it.name == declaration.elementName
					].head
					if (childClassifier !== null && parentClassifier instanceof Class) {
						(parentClassifier as Class).nestedClassifiers -= childClassifier as Classifier
					}
				} else {
					var containerPackage = declaration.translationUnit.correspondingModel
					var childClassifier = containerPackage.ownedElements.filter(typeof(Type)).filter [
						it.name == declaration.elementName
					].head
					containerPackage.ownedTypes -= childClassifier
				}
			}
			case change.oldElement instanceof IEnumerator: {
				var cppEnumerator = change.oldElement as IEnumerator
				var cppEnumeration = cppEnumerator.path as IEnumeration
				var enumeration = GetOrCreateP2.getClassifier(cppEnumeration)

				if (enumeration !== null && enumeration instanceof Enumeration) {
					var enumerator = (enumeration as Enumeration).getOwnedLiteral(change.oldElement.elementName)
					if (enumerator !== null) {
						(enumeration as Enumeration).ownedLiterals -= enumerator
					}
				}
			}
			case change.oldElement instanceof ITranslationUnit: {
				var itu = change.newElement as ITranslationUnit
				GetOrCreateP1.getOrCreateClassifiers(itu)
			}
		}
	}
}

/*******************************************************************************
 * Copyright (c) 2016 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   Van Cam Pham (CEA LIST) <vancam.pham@cea.fr> - Reverse implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.reverse

import java.util.List
import java.util.logging.Level
import java.util.logging.Logger
import java.util.regex.Matcher
import java.util.regex.Pattern
import org.eclipse.cdt.core.dom.ast.IASTDeclSpecifier
import org.eclipse.cdt.core.dom.ast.IASTDeclarator
import org.eclipse.cdt.core.dom.ast.IASTEqualsInitializer
import org.eclipse.cdt.core.dom.ast.IASTNode
import org.eclipse.cdt.core.dom.ast.IASTPointerOperator
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTArrayDeclarator
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTFunctionDefinition
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTReferenceOperator
import org.eclipse.cdt.core.model.CoreModel
import org.eclipse.cdt.core.model.CoreModelUtil
import org.eclipse.cdt.core.model.ICContainer
import org.eclipse.cdt.core.model.ICElement
import org.eclipse.cdt.core.model.ICProject
import org.eclipse.cdt.core.model.IEnumeration
import org.eclipse.cdt.core.model.IInclude
import org.eclipse.cdt.core.model.IMethod
import org.eclipse.cdt.core.model.IMethodDeclaration
import org.eclipse.cdt.core.model.IParent
import org.eclipse.cdt.core.model.ISourceReference
import org.eclipse.cdt.core.model.ISourceRoot
import org.eclipse.cdt.core.model.IStructure
import org.eclipse.cdt.core.model.IStructureDeclaration
import org.eclipse.cdt.core.model.IStructureTemplate
import org.eclipse.cdt.core.model.ITranslationUnit
import org.eclipse.cdt.core.model.ITypeDef
import org.eclipse.cdt.core.model.IVariableDeclaration
import org.eclipse.emf.common.util.UniqueEList
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.xmi.XMLResource
import org.eclipse.papyrus.designer.languages.common.base.StdUriConstants
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.External
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Array
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Const
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ref
import org.eclipse.papyrus.designer.languages.cpp.profile.CppProfileResource
import org.eclipse.papyrus.designer.languages.cpp.reverse.utils.RoundtripCppUtils
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.Model
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Namespace
import org.eclipse.uml2.uml.OpaqueExpression
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.Parameter
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.TypedElement
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.Usage
import org.eclipse.uml2.uml.ValueSpecification

import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ASTUtils.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.*
import static extension org.eclipse.uml2.uml.util.UMLUtil.*

/**
 * Utility methods for the reverse
 * 
 */
class ReverseUtils {

	public final static Logger LOGGER = Logger.getLogger(ReverseCpp2Uml.getName());

	public static final String Cpp_LangID = "C++"

	public static final ICElement[] EMPTY_CHILDS = #[]

	public static final String TEMPLATE_PARAMETER_SIGNATURE_NAME = "template_paremeter_signature"

	/**
	 * Analyze a declaration and apply suitable stereotypes from the C/C++ profile
	 * Used for operations and attributes
	 * @param declarators a list of declarators
	 * @param type the UML type of the element
	 * @param typedElement the parameter or attribute that holds a type.
	 */
	 static def analyzeDeclaration(List<IASTDeclarator> declarators, Type type, TypedElement typedElement,
		String langID) {
		for (declarator : declarators) {
			declarator.analyzeDeclaration(type, typedElement, langID)
		}
	}

	/**
	 * Analyze a declaration and apply suitable stereotypes from the C/C++ profile
	 * Used for operations and attributes
	 */
	static def analyzeDeclaration(IASTDeclarator declarator, Type type, TypedElement typedElement, String langID) {
		if (declarator instanceof ICPPASTArrayDeclarator) {
			var arrayDeclarator = declarator as ICPPASTArrayDeclarator
			var arrays = arrayDeclarator.arrayModifiers
			if (arrays.size > 0) {
				val array = typedElement.applyApp(Array)
				array.definition = "";
				arrays.forEach [
					val expr = it.constantExpression
					if (expr !== null) {
						array.definition = array.definition + "[" + expr.toString + "]"
					}
				]
			}
		}

		if (declarator.pointerOperators !== null) {
			if (typedElement.isApplied(Ptr)) {
				typedElement.getStereotypeApplication(Ptr).declaration = ""
			}
			for (pointerOperator : declarator.pointerOperators) {
				if (pointerOperator instanceof ICPPASTReferenceOperator) {
					val reference = pointerOperator as ICPPASTReferenceOperator
					val refStereo = typedElement.applyApp(Ref);
					refStereo.declaration = reference.syntax.toString
				} else if (pointerOperator instanceof IASTPointerOperator) {
					val pointer = pointerOperator as IASTPointerOperator
					val ptrStereo = typedElement.applyApp(Ref);
					if (ptrStereo !== null) {
						ptrStereo.declaration = ptrStereo.declaration + pointer.syntax.toString
					}
					else {
						System.err.println("Hallo???");
					}
				}
			}

			// reset declaration, if equal to default value
			if (typedElement.isApplied(Ptr)) {
				val ptrStereo = typedElement.getStereotypeApplication(Ptr);
				if (ptrStereo.declaration !== null && ptrStereo.declaration.trim.equals("*")) {
					ptrStereo.declaration = null
				}
			}

			if (typedElement.isApplied(Ref)) {
				val refStereo = typedElement.getStereotypeApplication(Ref);
				if (refStereo.declaration !== null && refStereo.declaration.trim.equals("&")) {
					refStereo.declaration = null
				}
			}
			var Pattern pattern = Pattern.compile("(\\*)([\\s]*)(const)");
			var Matcher matcher = pattern.matcher(declarator.rawSignature);
			if (matcher.find()) {
				typedElement.applyApp(Const);
			}
		}

		// parse initial values
		var initilizer = declarator.initializer
		var valueExisting = typedElement.ownedElements.filter(typeof(ValueSpecification)).filter [
			it.name.equals("defaultValue")
		].head
		if (valueExisting !== null) {
			valueExisting.destroy
		}
		if (initilizer !== null) {
			var ValueSpecification vs = null
			if (typedElement instanceof Property) {
				vs = (typedElement as Property).createDefaultValue("defaultValue", typedElement.type,
					UMLPackage.Literals.OPAQUE_EXPRESSION)
			} else if (typedElement instanceof Parameter) {
				vs = (typedElement as Parameter).createDefaultValue("default", typedElement.type,
					UMLPackage.Literals.OPAQUE_EXPRESSION)
			}

			if (vs === null) {
				return
			}
			var oe = vs as OpaqueExpression
			oe.getLanguages().add(langID);
			if (initilizer instanceof IASTEqualsInitializer) {
				var equalsInitialiser = initilizer as IASTEqualsInitializer
				var clause = equalsInitialiser.initializerClause
				if (clause !== null) {
					oe.bodies.add(clause.rawSignature)
				}
			}
		}
	}

	static def String getCppTypeName(String name) {
		var trimName = name.trim
		trimName = trimName.replace("*", "")
		trimName = trimName.replace("&", "")
		trimName = trimName.replace("[", "")
		trimName = trimName.replace("]", "")
		trimName = trimName.replace("const ", "")
		trimName = trimName.replace(" const", "")
		trimName = trimName.replace("volatile", "")
		trimName = trimName.replace(" volatile", "")
		trimName = trimName.trim
		return trimName
	}

	/**
	 * TODO might disappear
	 * Currently reads (includes forward definitions)
	 */
	static def List<ICElement> getAllIStructures(IParent parent, boolean allIncludes, boolean lookNestedTypes,
		ICProject project) { // including structure included
		var List<ICElement> ret = new UniqueEList
		var ICElement[] children = parent.safeGetChilds
		for (var i = 0; i < children.length; i++) {
			var child = children.get(i)
			switch (child) {
				case child instanceof IStructure: {
					ret.add(child as IStructure)
					if (lookNestedTypes) {
						ret.addAll(getAllIStructures(child as IStructure, allIncludes, lookNestedTypes, project))
					}

				}
				case child instanceof IEnumeration: {
					ret.add(child as IEnumeration)
				}
				case child instanceof IParent: {
					ret.addAll(getAllIStructures(child as IParent, allIncludes, lookNestedTypes, project))
				}
				case child instanceof IInclude: {
					if (allIncludes) {
						var unit = getTranslationUnitFromInclude(child as IInclude, project)
						if (unit !== null) {
							ret.addAll(getAllIStructures(unit, allIncludes, lookNestedTypes, project))
						}
					}

				}
				case child instanceof ITypeDef: {
					ret.add(child)
				}
				case child instanceof IStructureDeclaration: {
					ret.add(child)
				}
			}
		}
		return ret
	}

	/**
	 * Obtain the translation unit from an IInclude instance.
	 * Check whether examined header file is within project. If yes, translate fileName
	 * to a project relative fileName and obtain translation unit
	 */
	static def ITranslationUnit getTranslationUnitFromInclude(IInclude include, ICProject cproject) {
		if (include.fullFileName !== null) {
			var tu = getTranslationUnitFromPath(include.fullFileName, include.CProject)
			if (tu === null) {
				// include is not in current cproject, might be form another cProject in the workspace
				val cModel = CoreModel.getDefault().CModel
				for (otherCProject : cModel.CProjects) {
					tu = getTranslationUnitFromPath(include.fullFileName, otherCProject)
					if (tu !== null) {
						if (tu.correspondingModel === null) {
							// no registered model, add it
							// TODO: common code with ReverseCpp2Uml
							val modelManager = ReverseData.current.modelManager
							var umlFilePath = modelManager.getPath(
								cproject.project,
								ReverseCpp2Uml.REVERSE_FOLDER,
								otherCProject.elementName + ReverseCpp2Uml.MODEL_POSTFIX)
							val reset = false
							modelManager.createOrgetModel(otherCProject.elementName, umlFilePath, !reset, reset)
							val model = tu.correspondingModel
							RoundtripCppUtils.applyProfile(model, CppProfileResource.PROFILE_PATH)
							RoundtripCppUtils.applyProfile(model, StdUriConstants.UML_STD_PROFILE_PATH)
							model.setXmlID
						}
						return tu
					}
				}
			}
			return tu;
		}
		LOGGER.log(Level.WARNING,
			String.format("path of \"%s\" include cannot be resolved", include.elementName))
		return null;
	}

	/**
	 * Obtain the translation unit from an absolute path
	 */
	static def ITranslationUnit getTranslationUnitFromPath(String fullFileName, ICProject cproject) {
		val fullProjectPath = cproject.project.location;
		if (!fullFileName.contains(fullProjectPath.toString)) {
			// include is outside project, don't reverse it
			return null
		}
		var ITranslationUnit ret = null
		// translate fullFileName to a project relative path
		var fileIncludePath = fullFileName.split(fullProjectPath.toString).last
		var file = cproject.project.getFile(fileIncludePath)
		if (file !== null) {
			var ITranslationUnit unit = CoreModelUtil.findTranslationUnit(file);
			if (unit === null) {
				unit = CoreModel.getDefault().createTranslationUnitFrom(cproject, file.getLocation());
			}
			ret = unit
		}
		return ret
	}

	/**
	 * Get the translationUnit from an element
	 */
	static def getTranslationUnitFromElement(ICElement element) {
		var owner = element
		while (owner !== null && !(owner instanceof ITranslationUnit)) {
			owner = owner.parent
		}
		return owner as ITranslationUnit
	}

	static def unapplyAllStereotypes(Element element) {
		val stereotypeAppList = element.stereotypeApplications
		stereotypeAppList.forEach [
			val stereotype = it.getStereotype
			element.unapplyStereotype(stereotype)
		]
	}

	static def applyStereotype(Element element, boolean doApply, Class<? extends EObject> clazz) {
		if (doApply) {
			element.apply(clazz);
		}
	}

	/**
	 * Used in the context of parameter type declarations
	 */
	static def Type getUMLType(IASTDeclSpecifier declSpecifier, ICElement context) {
		var Type ret = null
		ret = declSpecifier.qualifiedName.getUMLType(context)
		return ret
	}

	/**
	 * Get a UML type from a given context (translation Unit)
	 * @param typeName a qualified type name (except for basic types and template parameters)
	 * @param context the ICElement that is related with the type lookup.
	 *      Used to resolved non-qualified names, e.g. template parameters
	 */
	static def Type getUMLType(String typeName, ICElement context) {
		val itu = context.translationUnitFromElement
		if (RoundtripCppUtils.isPrimitiveCppType(typeName)) {
			return RoundtripCppUtils.getPrimitiveType(typeName, itu.correspondingModel)
		}
		if (typeName === null) {
			return null;
		}
		val containerPkg = itu.correspondingModel
		// add wildcard for source-folder name
		val strippedTypeName = typeName.stripTemplate
		val qName = containerPkg.name + "::*::" + strippedTypeName
		val candidate1 = ElementUtils.getQualifiedElement(containerPkg, qName)
		if (candidate1 instanceof Type) {
			return candidate1
		}

		var Type ret = null
		val project = ReverseData.current.project;

		var token = strippedTypeName.split(Namespace.SEPARATOR)
		val simpleTypeName = token.get(token.length - 1).trim

		// Looking in structures in the same file
		val nestedStructures = itu.getAllIStructures(false, true, project)
		val sameNameType = nestedStructures.filter[it.elementName.trim.equals(simpleTypeName.trim)].head
		if (sameNameType === null) {
			// check whether a template parameterI
			var structureTemplates = nestedStructures.filter(typeof(IStructureTemplate))
			val match = structureTemplates.filter [
				it.templateParameterTypes.filter [
					it.equals(simpleTypeName)
				].size > 0
			].head

			// TODO choose which one?
			if (match !== null) {
				var templateClassifier = GetOrCreateP2.getClassifier(match)
				ret = GetOrCreateP1.getOrCreateTemplateParameter(templateClassifier as Classifier, simpleTypeName,
					"class")
			}

		}
//		else {
//			if (sameNameType instanceof IStructureDeclaration && !(sameNameType instanceof IStructure)) {
//				ret = lookForATypeByName(sameNameType.elementName)
//			} else {
//				// TODO potential stack overflow
//				// solve following issue: referencing a type that has not been (completely) reversed yet
//				// (e.g. attribute within struct references struct itself).
//				ret = GetOrCreateP1.getClassifier(sameNameType)
//			}
//		}

		if (ret === null) {
			if (typeName.startsWith("ecl")) {
				System.err.println("what happened")
			}
			// now, check, if in external package
			val externalPkg = RoundtripCppUtils.getOrcreateExternalPackage(itu.correspondingModel, true)
			val containerQName = typeName.stripLastQName
			var Namespace container
			if (containerQName !== null) {
				container = getContainer(externalPkg, containerQName)
			} else {
				container = externalPkg
			}
			val candidate = container.getMember(simpleTypeName)
			if (candidate instanceof Type) {
				ret = container.getMember(simpleTypeName) as Type
			}
			else if (candidate !== null) {
				System.err.println("A non type member of same name exists, This should not happen")
				return null
			}
			if (ret === null) {
				LOGGER.log(Level.WARNING, typeName + " is not found, it will be created dynamically")
				if (!(container instanceof Package)) {
					// container can be a datatype in case of nested types, since external elements are always
					// created as data types
					container = container.nearestPackage
				}
				ret = (container as Package).createOwnedType(simpleTypeName, UMLPackage.Literals.DATA_TYPE)
				ret.apply(External)
			}
		}

		return ret
	}

	/**
	 * Return the qualified name of the owner of aa variable or method definition.
	 * Used during body reverse
	 */
	static def Classifier getTypeOfVariableOrMethod(ICElement element, ITranslationUnit translationUnit) {
		if (element instanceof IVariableDeclaration || element instanceof IMethod) {
			// get the qualified name of the function or attribute via AST, strip last segment
			val qName = (element as ISourceReference).getQualifiedName
			if (qName !== null) {
				val type = qName.stripTemplate.getUMLType(translationUnit)
				if (type instanceof Classifier) {
					return type
				}
			}
		}
		return null
	}

	/**
	 * return true, if the element is active, i.e. not disabled by
	 * condititional directives such as #ifdef
	 */
	static def isActive(ICElement element) {
		if (element instanceof ISourceReference) {
			return element.isActive
		}
		return false
	}

	/**
	 * Remove a template signature from a name, i.e. replace MyName<...> with MyName 
	 */
	static def String stripTemplate(String name) {
		if (name !== null) {
			val char LT = '<'
			val char GT = '>'
			val openIdx = name.indexOf(LT)
			if (openIdx != -1) {
				var count = 1
				var closeIdx = openIdx + 1
				for (; count > 0 && closeIdx < name.length; closeIdx++) {
					val ch = name.charAt(closeIdx)
					if (ch == LT) count++
					else if (ch == GT) count--
				}
				if (count == 0) {
					return stripTemplate(name.substring(0, openIdx) + name.substring(closeIdx))
				}
			}
		}
		return name
	}

	/**
	 * Remove the last segment of a qualified name. Returns null
	 * if no separator is found, i.e. name is not qualified
	 */
	static def stripLastQName(String qName) {
		if (qName !== null) {
			val idx = qName.lastIndexOf(Namespace.SEPARATOR)
			if (idx != -1) {
				return qName.substring(0, idx)
			}
		}
		return null
	}

	/**
	 * Get the model corresponding to a translation unit (model map)
	 */
	static def Model getCorrespondingModel(ITranslationUnit unit) {
		var Model ret = null
		for (model : ReverseData.current.models) {
			if (unit.path.toString.contains(model.name)) {
				ret = model
			}
		}
		return ret
	}

	/**
	 * Get a container (UML) for a given ICElement, it returns either a package
	 * or a class. The method will always created packages, if they do not exist yet.
	 * Currently, the location of the source folder is used in addition of C++ name-spaces
	 * This has the disadvantage that UML namespaces and packages diverge. On the other
	 * hand, some projects might use source folders as structuring elements
	 */
	static def getContainer(ICElement element) {
		return getContainer(element, false)
	}

	static def getContainer(ICElement element, boolean forAlias) {
		var tu = element.translationUnitFromElement
		var String qName
		if (tu.sourceFolder === null) {
			qName = "external"
		} else {
			qName = tu.sourceFolder.elementName; // source folder name
		}
		var String qNameSR = null
		if (forAlias) {
			// as an alias has no representation on the ICElement level, the caller
			// passes the container (namespace or struct), use full qualified name of parent
			qNameSR = (element as ISourceReference).qualifiedName
		} else if (element instanceof ITypeDef) {
			// a typedef does not have a qualified AST name, use full qualified name of parent
			// instead
			qNameSR = (element.parent as ISourceReference).qualifiedName
		} else {
			// use qualified name without last segment of element itself
			qNameSR = (element as ISourceReference).qualifiedName.stripLastQName
		}
		if (qNameSR !== null) {
			qName = qName + Namespace.SEPARATOR + qNameSR
		}
		return getContainer(tu.correspondingModel, qName)
	}

	/**
	 * get a container from a qualified name
	 */
	static def getContainer(Package parent, String qName) {
		if (parent === null) {
			return null
		}
		var Namespace parentPack = parent
		for (name : qName.split(Namespace.SEPARATOR)) {
			if (parentPack.getMember(name) === null) {
				if (parentPack instanceof Package) {
					if (name.equals("UmsConfig")) {
						// TODO remove debug code
						System.err.println("debug")
					}
					val pkg = parentPack.createNestedPackage(name)
					pkg.setXmlID
				} else {
					System.err.println("should not happen");
					return parentPack
				}
			}
			parentPack = parentPack.getMember(name) as Namespace
		}
		return parentPack
	}

	static def getSourceFolder(ICElement parent) {
		var sf = parent;
		while (sf !== null) {
			if (sf instanceof ISourceRoot) {
				return sf
			}
			sf = sf.getParent()
		}
		return null
	}

	static def getExcludedIncludesMapKey(ITranslationUnit translationUnit, Element umlElement) {
		return "" + translationUnit.hashCode + umlElement.hashCode
	}

	static def getElementsSameName(String name) {
		ReverseData.current.map.entrySet.filter[it.key.elementName.equals(name)].toList
	}

	static def ICElement lookTypeInContainer(ICContainer container, String typeName) {
		var ICElement ret = null
		for (child : container.safeGetChilds) {
			if (child instanceof ITranslationUnit) {
				var nesteds = child.getAllIStructures(false, true, ReverseData.current.project)
				ret = nesteds.filter[it instanceof IStructure || it instanceof IEnumeration || it instanceof ITypeDef].
					filter[it.elementName.equals(typeName)].head
			} else if (child instanceof ICContainer) {
				ret = lookTypeInContainer(child, typeName)
			}
			if (ret !== null) {
				return ret
			}
		}
		return ret
	}

	static def lookForATypeByName(String typeName) {
		var Type ret = null
		var ICElement cType = null
		for (container : ReverseData.current.containers) {
			if (cType === null) {
				cType = lookTypeInContainer(container, typeName)
			}
		}
		if (cType !== null) {
			ret = GetOrCreateP2.getClassifier(cType)
		}
		return ret
	}

	static def isSimpleAssociation(Property prop) {
		if ((prop.isApplied(Ptr) || prop.isApplied(Ref)) && !prop.isApplied(Array)) {
			return true
		}

		return false
	}

	static def isAggregation(Property prop) {
		if ((prop.isApplied(Ptr) || prop.isApplied(Ref)) && prop.isApplied(Array)) {
			return true
		}
		return false
	}

	static def isComposition(Property prop) {
		if ((!prop.isApplied(Ptr) && !prop.isApplied(Ref))) {
			return true
		}
		return false
	}

	static def String getMemberInit(IMethodDeclaration method) {
		return getMemberInit(method.findEnclosingNode)
	}

	static def String getMemberInit(IASTNode methodNode) {
		var ret = ""

		if (methodNode instanceof ICPPASTFunctionDefinition) {
			val inits = methodNode.memberInitializers
			if (inits !== null) {
				ret = '''«FOR i : inits SEPARATOR ", "»«i.memberInitializerId»«i.initializer.rawSignature»«ENDFOR»'''
			}
		}

		return ret.toString
	}

	/**
	 * Write an XML id based on its name (without the top-level package name).
	 * This enables a pragmatical re-use of XML-IDs (e.g. in a Papyrus diagram) when the project
	 * is re-generated. OF course, a good synchronization would avoid re-creating UML
	 * elements (and the associated IDs)
	 * @param ne a named element
	 */
	static def void setXmlID(NamedElement ne) {
		val root = PackageUtil.getRootPackage(ne)
		var String qName
		if (ne instanceof Usage) {
			qName = "U_" + ne.suppliers.get(0).qualifiedName
		}
		else {
			qName = ne.qualifiedName
		}
		if (qName !== null) {
			setXmlID(
				ne,
				"ID" +
					qName.replaceFirst(root.getName(), "").replaceAll(NamedElement.SEPARATOR, "_").replaceAll("#", "_").
						replaceAll(" ", "_").replaceAll("<", "_").replaceAll(">", "_").replaceAll("&", "_")
			);
		}
	}

	/**
	 * Set the XML ID of an eObject
	 * 
	 * @param eObject
	 * @param uniqueID
	 */
	static def void setXmlID(EObject eObject, String id) {
		val eObjectRes = eObject.eResource()

		if (eObjectRes instanceof XMLResource) {
			val xmlResource = eObjectRes as XMLResource
			var uniqueID = id
			var counter = 0
			while (xmlResource.getEObject(uniqueID) !== null) {
				uniqueID = id + counter;
				counter++;
			}
			xmlResource.setID(eObject, uniqueID)
		}
	}

	/**
	 * It seems that the call to getChildren can cause an stack-overflow in some cases.
	 * Use this function to capture it (in this case, an empty array is returned)
	 */
	static def safeGetChilds(IParent parent) {
		var ICElement[] children
		try {
			children = parent.getChildren()
		} catch (StackOverflowError e) {
			LOGGER.log(Level.WARNING,
				String.format("Exception <%s> during fetching of children of parent <%s>", e.message, parent.toString))
			children = EMPTY_CHILDS
		}
		return children
	}
}

package org.eclipse.papyrus.designer.languages.cpp.reverse;

import org.eclipse.cdt.core.model.ICElement;

/**
 * Qualified name information. In addition to the resolved qualified name, it contains
 * the translation unit, in which the name has been found.
 *
 */
public class QNInfo {
	String qName;
	ICElement source;
}

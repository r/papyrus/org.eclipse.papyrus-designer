/*******************************************************************************
 * Copyright (c) 2016, 2022 CEA LIST
 * 
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   Van Cam Pham (CEA LIST) <vancam.pham@cea.fr> - Reverse implementation
 *   Ansgar Radermacher (CEA LIST) - Larger refactoring
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.reverse

import java.util.regex.Matcher
import java.util.regex.Pattern
import org.eclipse.cdt.core.dom.ast.ExpansionOverlapsBoundaryException
import org.eclipse.cdt.core.dom.ast.IASTMacroExpansionLocation
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTAliasDeclaration
import org.eclipse.cdt.core.dom.ast.cpp.ICPPASTTemplateDeclaration
import org.eclipse.cdt.core.model.ICElement
import org.eclipse.cdt.core.model.IEnumeration
import org.eclipse.cdt.core.model.IEnumerator
import org.eclipse.cdt.core.model.IInclude
import org.eclipse.cdt.core.model.IMethodDeclaration
import org.eclipse.cdt.core.model.IParent
import org.eclipse.cdt.core.model.ISourceReference
import org.eclipse.cdt.core.model.IStructure
import org.eclipse.cdt.core.model.IStructureDeclaration
import org.eclipse.cdt.core.model.IStructureTemplate
import org.eclipse.cdt.core.model.IStructureTemplateDeclaration
import org.eclipse.cdt.core.model.ITypeDef
import org.eclipse.cdt.core.model.IUsing
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Template
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Typedef
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Using
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.ClassifierTemplateParameter
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.EnumerationLiteral
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.Namespace
import org.eclipse.uml2.uml.OpaqueExpression
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.RedefinableTemplateSignature
import org.eclipse.uml2.uml.TemplateSignature
import org.eclipse.uml2.uml.UMLFactory
import org.eclipse.uml2.uml.UMLPackage

import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ASTUtils.*
import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.ReverseUtils.*
import static extension org.eclipse.papyrus.designer.languages.cpp.reverse.CommentUtils.*
import org.eclipse.cdt.core.dom.ast.IASTSimpleDeclaration
import org.eclipse.uml2.uml.Type

/**
 * Pass1: build all classifiers (and packages) on UML level, but do not fill them with contents (attributes and methods, etc.)
 * The objective of having two passes is that all type references (set in pass2) and forward declared elements are already
 * available in the UML model
 */
class GetOrCreateP1 {
	/**
	 * Create or get the classifiers within a parent (called initially with a translation unit, then recursively)
	 * @param model the model to get/create classifiers in
	 * @param parent the CDT model parent
	 * @return a list of created UML types
	 */
	static def void getOrCreateClassifiers(IParent parent) throws Exception {
		var ICElement[] children = parent.safeGetChilds
		// var model = (parent as ICElement).translationUnitFromElement.correspondingModel
		for (var i = 0; i < children.length; i++) {
			var child = children.get(i)
			if (child.isActive) {
				// don't analyze inactive code, as AST is no available (TODO: clarify options/requirements)
				switch (child) {
					case child instanceof IStructure: {
						getOrCreateClassifier(child)
						// structures can have nested classifiers
						getOrCreateClassifiers(child as IParent)
					}
					case child instanceof IEnumeration: {
						getOrCreateClassifier(child)
					}
					case child instanceof IParent: {
						getOrCreateClassifiers(child as IParent)
					}
					case child instanceof ITypeDef: {
						getOrCreateClassifier(child)
					}
					case child instanceof IUsing: {
						// IUsing is only used for "using namespace", handled internally by CDT
					}
					case child instanceof IInclude: {
						val headerUnit = (child as IInclude).getTranslationUnitFromInclude(ReverseData.current.project)
						if (headerUnit !== null && headerUnit.sourceFolder !== null) {
							ReverseCpp2Uml.currentCpp2Uml.reverseHeader(headerUnit)
						}
					}
				}
			}
		}
		if (parent instanceof ISourceReference) {
			getOrCreateAlias(parent)
		}
	}

	/*
	 * Create classifier for a forward declaration, either a (template) class, struct/union
	 *  @param container the owner to create a classifier in, must be a class or package
	 * @return the associated uml type
	 */
	static def getOrCreateClassifier(ICElement ideclaration) {

		val map = ReverseData.current.map

		if (ideclaration.elementType != ICElement.C_CLASS && ideclaration.elementType != ICElement.C_TEMPLATE_CLASS &&
			ideclaration.elementType != ICElement.C_ENUMERATION && ideclaration.elementType != ICElement.C_STRUCT &&
			ideclaration.elementType != ICElement.C_UNION && ideclaration.elementType != ICElement.C_TEMPLATE_STRUCT &&
			!(ideclaration instanceof ITypeDef) && !(ideclaration instanceof IUsing)) {
			return null
		}

		var Classifier existing = null
		var container = getContainer(ideclaration)
		if (container instanceof DataType) {
			container = container.nearestPackage
		}

		if (map.get(ideclaration) instanceof Classifier) {
			existing = map.get(ideclaration) as Classifier
		}
		if (existing !== null) {
			return existing
		}

		var boolean structIsClassLike = ideclaration.isClassLike
		var name = ideclaration.elementName.stripTemplate
		val astNode = (ideclaration as ISourceReference).findEnclosingNode
		if ("".equals(name)) {
			// anonymous declarations (within typedef)
			if (astNode.parent instanceof IASTSimpleDeclaration) {
				var simpleDecl = astNode.parent as IASTSimpleDeclaration
				if (simpleDecl.getDeclarators.length > 0) {
					name = simpleDecl.declarators.get(0).name.toString
				}
			}
		}

		if (ideclaration instanceof ITypeDef) {
			val typedef = createTypedef(container, ideclaration)
			if (typedef !== null) {
				map.put(ideclaration, typedef)
			}
			return typedef
		} else if (ideclaration.elementType == ICElement.C_CLASS ||
			ideclaration.elementType == ICElement.C_TEMPLATE_CLASS || structIsClassLike) {
			val IStructureDeclaration iStructure = ideclaration as IStructure
			var Class classifier = null
			if (existing !== null) {
				classifier = existing as Class
			} else {
				if (container instanceof Package) {
					classifier = (container as Package).createOwnedClass(name, false)
				} else {
					classifier = (container as Class).createNestedClassifier(name, UMLPackage.Literals.CLASS) as Class
				}
				classifier.setXmlID
			}

			map.put(ideclaration, classifier)
			classifier.addComment(astNode)

			if (iStructure.elementType == ICElement.C_TEMPLATE_CLASS) {
				var istructureTemplate = iStructure as IStructureTemplate
				val template = StereotypeUtil.applyApp(classifier, Template)
				template.declaration = istructureTemplate.templateSignature
			}
			return classifier

		} else if (!structIsClassLike && (
				ideclaration.elementType == ICElement.C_STRUCT || ideclaration.elementType == ICElement.C_UNION ||
			ideclaration.elementType == ICElement.C_TEMPLATE_STRUCT)) {
			var DataType dataType = null
			if (existing !== null) {
				dataType = existing as DataType
			} else {
				if (container instanceof Package) {
					dataType = (container as Package).createOwnedType(name, UMLPackage.Literals.DATA_TYPE) as DataType
				} else {
					dataType = (container as Class).createNestedClassifier(name,
						UMLPackage.Literals.DATA_TYPE) as DataType
				}
				dataType.setXmlID
			}

			map.put(ideclaration, dataType)
			dataType.addComment(astNode)

			return dataType

		} else if (ideclaration.elementType == ICElement.C_ENUMERATION) {
			var Enumeration enumeration = null
			val iEnum = ideclaration as IEnumeration
			if (existing !== null) {
				enumeration = existing as Enumeration
			} else {
				if (container instanceof Package) {
					enumeration = (container as Package).createOwnedEnumeration(name)
				} else {
					enumeration = (container as Class).createNestedClassifier(name,
						UMLPackage.Literals.ENUMERATION) as Enumeration
				}
				enumeration.setXmlID
			}
			map.put(ideclaration, enumeration)
			enumeration.addComment(astNode)

			// useful for enumerations?
			iEnum.getOrCreateClassifiers

			for (child : iEnum.children) {
				if (child instanceof IEnumerator) {
					val EnumerationLiteral literal = enumeration.createOwnedLiteral(child.elementName)
					literal.setXmlID

					if (child.constantExpression !== null && !child.constantExpression.equals("")) {
						val valueSpecification = literal.createSpecification("defaultVal-ue", literal.enumeration,
							UMLPackage.Literals.OPAQUE_EXPRESSION) as OpaqueExpression
						valueSpecification.getLanguages().add(ReverseCpp2Uml.Cpp_LangID);
						valueSpecification.getBodies().add(child.constantExpression);
					}
				}
			}
			return enumeration
		}

		return null
	}

	/**
	 * A C++ struct can be "class-like", e.g. have nested definitions and operations. However, a
	 * datatype in UML is more constrained (no nested classifier). Therefore, we need to know
	 * whether to map a C++ struct to a UML class instead of a datatype.
	 */
	static def isClassLike(ICElement ideclaration) {
		if (ideclaration.elementType == ICElement.C_STRUCT || ideclaration.elementType == ICElement.C_UNION ||
			ideclaration.elementType == ICElement.C_TEMPLATE_STRUCT) {
			val iStructure = ideclaration as IStructure

			for (child : iStructure.children) {
				if (child instanceof IMethodDeclaration || child instanceof IStructure ||
					child instanceof IStructureTemplateDeclaration) {
					return true
				}
			}
			// check if the declaration contains using statements (they are not
			// children on the ideclaration level)
			var node = (ideclaration as ISourceReference).findEnclosingNode();
			if (ideclaration instanceof IStructureTemplate) {
				// A template does not contain the alias/using statements directly, we need to navigate
				// to the declSpecifier first
				node = node.declSpecifier
			}
			for (child : node.getChildren()) {
				if (child instanceof ICPPASTAliasDeclaration) {
					return true
				}
			}
		}
		return false
	}

	/**
	 * Create a type definition
	 * @param container the owner to create the typedef in, should be either
	 *  a class, package or datatype. In case of datatype, nearest package is used
	 * 
	 */
	static def createTypedef(Namespace container, ITypeDef typedef) {

		if (typedef.typeName == "struct" || typedef.typeName == "class" || typedef.typeName == "enum") {
			var pos = (typedef.parent as IParent).children.indexOf(typedef)
			if (pos > 0) {
				var childStruct = (typedef.parent as IParent).children.get(pos - 1)
				if ((childStruct instanceof IStructure || childStruct instanceof IEnumeration) &&
					childStruct.elementName == "") {
					// case: typedef enum { ... } name => programming idiom caused by restrictions of C
					// (can not reference a struct or enum without typedef simply via its name).
					// On UML level, we do not want a typedef + separate enum definition
					// (or worse, a textual definition in the typedef stereotype)
					return null;
				}
			}
		}

		// check, if type already exist. This is possible since users might declare a typedef at several
		// places (if not in a common include file). TODO: should that imply a warning?
		var primitiveType = container.getMember(typedef.elementName) as Type
		if (primitiveType !== null) {
			return primitiveType
		}
		primitiveType = UMLFactory.eINSTANCE.createPrimitiveType => [
			it.name = typedef.elementName
		]
		primitiveType.addComment(typedef)
		if (container instanceof Package) {
			container.ownedTypes += primitiveType
		} else if (container instanceof Class && primitiveType instanceof Classifier) {
			(container as Class).nestedClassifiers += primitiveType as Classifier
		} else {
			container.nearestPackage.ownedTypes += primitiveType
		}
		val typedefSt = StereotypeUtil.applyApp(primitiveType, Typedef)
		var typedefNode = typedef.findEnclosingNode
		var String rawSignature = typedefNode.rawSignature.replaceAll("\\n", "").replaceAll("\\r", "").
			replaceAll(";", "").replaceAll("\\s+", " ").trim
		// Unescaped regex: (\()(\s*)(\*)(.*)(\))(\s*)(\()(.*)(\))
		var Pattern pattern = Pattern.compile("(\\()(\\s*)(\\*)(.*)(\\))(\\s*)(\\()(.*)(\\))");
		var Matcher matcher = pattern.matcher(rawSignature);
		if (matcher.find()) {
			val String typeName = rawSignature.replaceFirst(Pattern.quote(typedef.elementName), "typeName")
			typedefSt.definition = typeName
		} else {
			typedefSt.definition = typedef.typeName
		}
		ReverseData.current.map.put(typedef, primitiveType)
		return primitiveType
	}

	/**
	 * Search for alias declarations in a structure and create them, if they
	 * do not exist yet. This function works on the AST level, as alias declaration (via Using)
	 * do not exist on the ICElement level
	 */
	static def getOrCreateAlias(ISourceReference sourceRef) {
		val container = (sourceRef as ICElement).getContainer(true)
		// if (container instanceof Namespace) {
		var node = sourceRef.findEnclosingNode();
		if (node === null) {
			return
		}
		if (sourceRef instanceof IStructureTemplate) {
			// A template does not contain the alias/using statements directly, we need to navigate
			// to the declSpecifier first
			val declSpecifier = node.declSpecifier
			if (declSpecifier !== null) {
				node = declSpecifier
			} else if (!(node instanceof IASTMacroExpansionLocation)) {
				System.err.println("not in macro?? should not happen")
			}
		}

		for (child : node.getChildren()) {
			if (child instanceof ICPPASTAliasDeclaration) {
				getOrCreateAlias(child, container)
			}
			if (child instanceof ICPPASTTemplateDeclaration) {
				// handle alias definition with a template signature, e.g. template<...> using ...
				val declaration = child.declaration
				if (declaration instanceof ICPPASTAliasDeclaration) {
					getOrCreateAlias(declaration, container)
				}
			}
		}
	// }
	}

	/**
	 * Create or get an alias declaration from an ASTAliasDeclaration
	 */
	static def getOrCreateAlias(ICPPASTAliasDeclaration aliasDecl, Namespace container) {
		var definition = ""
		try {
			var token = aliasDecl.mappingTypeId.syntax
			while (token !== null) {
				while (token.offset > definition.length) {
					definition += " "
				}
				definition += token.toString
				token = token.next
			}
		} catch (ExpansionOverlapsBoundaryException e) {
			// getSyntax fails, since part of macro expansion => get right hand side from declSpec
			// (declSpec does not contain whole right hand side, e.g. the typename keyword is missing)
			definition = aliasDecl.mappingTypeId.declSpecifier.toString();
		}
		val aliasName = aliasDecl.alias.toString()
		var aliasPT = container.getMember(aliasName)
		if (aliasPT === null) {
			if (container instanceof Package) {
				aliasPT = container.createOwnedPrimitiveType(aliasName)
			} else if (container instanceof Class) {
				aliasPT = container.createNestedClassifier(aliasName, UMLPackage.eINSTANCE.primitiveType)
			} else {
				System.err.println("should not happen")
			}
			// does not make sense to put into a map, as only represented on AST level
			aliasPT.setXmlID
			val usingSt = StereotypeUtil.applyApp(aliasPT, Using)
			usingSt.definition = definition
		}

	}

	/**
	 * Create a template parameter within a template signature. If the
	 * signature does not exist, it will be created.
	 */
	static def Classifier getOrCreateTemplateParameter(NamedElement element, String parameterTypeName, String keyWord) {
		var Classifier ret = null
		var TemplateSignature templateSignature
		if (element instanceof Classifier) {
			var classifier = element as Classifier
			if (classifier.ownedTemplateSignature === null ||
				!(classifier.ownedTemplateSignature instanceof RedefinableTemplateSignature)) {
				templateSignature = classifier.createOwnedTemplateSignature(
					UMLPackage.Literals.REDEFINABLE_TEMPLATE_SIGNATURE) as RedefinableTemplateSignature
				(templateSignature as RedefinableTemplateSignature).name = TEMPLATE_PARAMETER_SIGNATURE_NAME
			}
			templateSignature = classifier.ownedTemplateSignature as RedefinableTemplateSignature
		} else if (element instanceof Operation) {
			var operation = element as Operation
			if (operation.ownedTemplateSignature === null ||
				!(operation.ownedTemplateSignature instanceof TemplateSignature)) {
				templateSignature = operation.createOwnedTemplateSignature(
					UMLPackage.Literals.TEMPLATE_SIGNATURE) as TemplateSignature
			}
			templateSignature = operation.ownedTemplateSignature as TemplateSignature
		} else {
			return null
		}

		var classifierTemplates = templateSignature.ownedParameters.filter(typeof(ClassifierTemplateParameter))
		var classifierTemplatesContainClassifier = classifierTemplates.filter [
			it.ownedParameteredElement instanceof Classifier
		]
		var containedClassifiers = classifierTemplatesContainClassifier.map[it.ownedParameteredElement]
		ret = containedClassifiers.filter(typeof(Classifier)).filter[it.name.equals(parameterTypeName)].head
		if (ret === null) {
			var classifierTemplate = templateSignature.createOwnedParameter(
				UMLPackage.Literals.CLASSIFIER_TEMPLATE_PARAMETER) as ClassifierTemplateParameter
			ret = classifierTemplate.createOwnedParameteredElement(UMLPackage.Literals.CLASS) as Classifier
			ret.name = parameterTypeName
			classifierTemplate.addKeyword(keyWord)
		}

		return ret
	}
}

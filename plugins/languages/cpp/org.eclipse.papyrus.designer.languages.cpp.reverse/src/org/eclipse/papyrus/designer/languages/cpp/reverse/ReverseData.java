package org.eclipse.papyrus.designer.languages.cpp.reverse;

import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.dom.ast.IASTTranslationUnit;
import org.eclipse.cdt.core.index.IIndex;
import org.eclipse.cdt.core.model.ICContainer;
import org.eclipse.cdt.core.model.ICElement;
import org.eclipse.cdt.core.model.ICProject;
import org.eclipse.cdt.core.model.ITranslationUnit;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.cpp.reverse.utils.ModelManagement;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;

public class ReverseData {
	ITranslationUnit unit;
	ICProject project;
	ModelManagement modelManager;
	List<Model> models;

	Package dependencyPkg;

	/**
	 * The map associates elements of the CDT internal model with UML elements
	 * (in form of EObjects)
	 * The map is used for type lookup
	 */
	Map<ICElement, EObject> map;

	/**
	 * map that contains completely analyzed element
	 */
	Map<ICElement, Boolean> analyzeMap;

	IProgressMonitor monitor;
	IIndex index;
	Map<ITranslationUnit, IASTTranslationUnit> tuToASTtuMap;

	/**
	 * The map stores the information whether the translation unit
	 * (header file) has already been processed
	 */
	Map<String, Boolean> includesMap;
	List<ICContainer> containers;

	static ReverseData current;

	static ReverseData current() {
		return current;
	}
}

package org.eclipse.papyrus.designer.languages.cpp.reverse

class ReverseFromHeader {
	/*
	private def List<Type> getOrCreateClassifier(Model model, IParent parent) throws Exception {
		var List<Type> ret = new UniqueEList<Type>
		var ICElement[] childrend = parent.getChildren();
		for (var i = 0; i < childrend.length; i++) {
			var child = childrend.get(i)
			switch (child) {
				case child instanceof IStructure,
				case child instanceof IEnumeration: {
					if (!(child as IDeclaration).elementName.equals("")) {
						ret.addAll(getOrcreateTypes(child as IDeclaration, parent, model))	
					}
				}
				case child instanceof IMethod: {
					//println("getOrCreateClassifier: child is instance of IMethod")
					if ((child as IMethod).translationUnit.isHeaderUnit && !(child.parent instanceof IStructure)) {
						val IMethod method = child as IMethod
						
						var List<Classifier> types = getTypeByQualifiedName(method, method.translationUnit, method.contextNamespaces)
						
						if (types.size > 0) {
							val Classifier type = types.get(0)
							var batchReverser = new BatchReverseFunctionBody(method.translationUnit, method.CProject.elementName, this, type)
							
							// function declaration is a superclass for method declaration
							// (but need to trace functions differently?)
							var String name = method.elementName
							var IASTNode node = method.findEnclosingNode
							if (node instanceof IASTFunctionDefinition) {
								var IASTFunctionDefinition definition = node as IASTFunctionDefinition
								var IASTFunctionDeclarator declarator = definition.getDeclarator();
								var String body = getBody(method);
								
								var Operation operation = batchReverser.updateMethod(method, node, 0, parent, name, body, declarator)
								if (operation !== null) {
									new DependencyAnalysis(operation, definition, method.translationUnit, this).analyzeDependencies()
								}
							}
						}
					}
				}
				case child instanceof IFunction: {
					//println("getOrCreateClassifier: child is instance of IFunction")
//					var func = child as IFunction
//					var parentPack = func.translationUnit.containerPackage
//					var Class classifier = null
//					if (parentPack.getOwnedMember(func.translationUnit.elementName) === null) {
//						parentPack.createOwnedClass(func.translationUnit.elementName, false)
//					}
//					classifier = parentPack.getOwnedMember(func.translationUnit.elementName) as Class
//					classifier.createOwnedOperation(func.elementName, null, null)
				}
				case child instanceof IParent: {
					ret.addAll(getOrCreateClassifier(model, (child as IParent)))
				}
				case child instanceof ITypeDef: {
					var parentPack = getContainerPackage((child as ITypeDef).translationUnit)
					var Classifier tempType
					if (child.parent instanceof IStructure) {
						var parentClass = getUMLType(child.parent.elementName, (child as ITypeDef).translationUnit,
							child.contextNamespaces) as Classifier
						if (parentClass.getOwnedMember(child.elementName) === null) {
							createOrgetClassifier(parentClass, child, true)
						}
						tempType = parentClass.getOwnedMember(child.elementName) as Classifier
					} else {
						if (parentPack.getOwnedType(child.elementName) === null) {
							createOrgetClassifier(parentPack, child, true)
						}
						tempType = parentPack.getOwnedMember(child.elementName) as Classifier
					}
					ret.add(tempType)
				}
				case child instanceof IUsing: {
					// [ansgar]
					val using = child as IUsing
					System.err.println(using)
				}
			}
		}

		return ret;

	}
	*/
}
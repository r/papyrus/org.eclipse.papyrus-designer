/*******************************************************************************
 * Copyright (c) 2022 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
 
 package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Pragma
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.uml2.uml.Classifier

/**
 * Get a pragma pre or post declaration form a pragma reference.
 * The reference points to a class stereotyped with PragmaOption
 */
class CppPragma {
	def static prePragma(Classifier cl) {
		val pragmaRef = UMLUtil.getStereotypeApplication(cl, Pragma)
		if (pragmaRef !== null) {
			val option = pragmaRef.pragma
			if (option !== null) {
				return option.preDecl
			}
		}
		return ""
	}

	def static postPragma(Classifier cl) {
		val pragmaRef = UMLUtil.getStereotypeApplication(cl, Pragma)
		if (pragmaRef !== null) {
			val option = pragmaRef.pragma
			if (option !== null) {
				return option.preDecl
			}
		}
		return ""
	}
}
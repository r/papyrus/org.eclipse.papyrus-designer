/*******************************************************************************
 * Copyright (c) 2006 - 2012 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.preferences;

import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_COMMENT_HEADER_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_COMMENT_HEADER_KEY;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_CPP11_CLASS_ENUM_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_CPP11_CLASS_ENUM_KEY;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_FORMAT_CODE_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_FORMAT_CODE_KEY;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_HEADER_SUFFIX_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_HEADER_SUFFIX_KEY;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_IMPLEM_SUFFIX_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_IMPLEM_SUFFIX_KEY;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_OUT_INOUT_OP_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_OUT_INOUT_OP_KEY;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_PROJECT_PREFIX_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_PROJECT_PREFIX_KEY;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_USING_NAMESPACES_DVAL;
import static org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants.P_USING_NAMESPACES_KEY;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.papyrus.designer.languages.cpp.codegen.Activator;

public class CppCodeGenPreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences prefs = DefaultScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		prefs.put(P_HEADER_SUFFIX_KEY, P_HEADER_SUFFIX_DVAL);
		prefs.put(P_IMPLEM_SUFFIX_KEY, P_IMPLEM_SUFFIX_DVAL);
		prefs.put(P_OUT_INOUT_OP_KEY, P_OUT_INOUT_OP_DVAL);
		prefs.put(P_PROJECT_PREFIX_KEY, P_PROJECT_PREFIX_DVAL);
		prefs.put(P_COMMENT_HEADER_KEY, P_COMMENT_HEADER_DVAL);
		prefs.putBoolean(P_USING_NAMESPACES_KEY, P_USING_NAMESPACES_DVAL);
		prefs.putBoolean(P_CPP11_CLASS_ENUM_KEY, P_CPP11_CLASS_ENUM_DVAL);
		prefs.putBoolean(P_FORMAT_CODE_KEY, P_FORMAT_CODE_DVAL);
	}
}

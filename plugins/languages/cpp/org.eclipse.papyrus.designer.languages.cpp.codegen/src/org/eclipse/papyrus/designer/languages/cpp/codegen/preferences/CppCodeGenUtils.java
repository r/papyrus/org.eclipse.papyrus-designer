/*******************************************************************************
 * Copyright (c) 2006 - 2012 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.preferences;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.cpp.codegen.Activator;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.EnumStyle;
import org.eclipse.uml2.uml.Enumeration;

/**
 * Utility class that returns the C++ preference values (eventually taking
 * stereotypes into account)
 */
public class CppCodeGenUtils {

	protected static IEclipsePreferences prefs = null;

	public static String getHeaderSuffix() {
		initPreferenceStore();
		return prefs.get(CppCodeGenConstants.P_HEADER_SUFFIX_KEY, CppCodeGenConstants.P_HEADER_SUFFIX_DVAL);
	}

	public static String getBodySuffix() {
		initPreferenceStore();
		return prefs.get(CppCodeGenConstants.P_IMPLEM_SUFFIX_KEY, CppCodeGenConstants.P_IMPLEM_SUFFIX_DVAL);
	}

	public static String getOutInoutOp() {
		initPreferenceStore();
		return prefs.get(CppCodeGenConstants.P_OUT_INOUT_OP_KEY, CppCodeGenConstants.P_OUT_INOUT_OP_DVAL);
	}

	public static String getCommentHeader() {
		initPreferenceStore();
		return prefs.get(CppCodeGenConstants.P_COMMENT_HEADER_KEY, CppCodeGenConstants.P_COMMENT_HEADER_DVAL);
	}

	public static boolean getFormatCode() {
		initPreferenceStore();
		return prefs.getBoolean(CppCodeGenConstants.P_FORMAT_CODE_KEY, CppCodeGenConstants.P_FORMAT_CODE_DVAL);
	}

	public static void initPreferenceStore() {
		if (prefs == null) {
			prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		}
	}

	public static boolean getUsingNamespaces() {
		initPreferenceStore();
		return prefs.getBoolean(CppCodeGenConstants.P_USING_NAMESPACES_KEY, CppCodeGenConstants.P_USING_NAMESPACES_DVAL);
	}

	public static boolean getC11ClassEnum(Enumeration enumeration) {
		EnumStyle enumStyle = GenUtils.getApplicationTree(enumeration, EnumStyle.class);
		if (enumStyle != null) {
			return enumStyle.isClassEnum();
		}
		initPreferenceStore();
		return prefs.getBoolean(CppCodeGenConstants.P_CPP11_CLASS_ENUM_KEY, CppCodeGenConstants.P_CPP11_CLASS_ENUM_DVAL);
	}
}

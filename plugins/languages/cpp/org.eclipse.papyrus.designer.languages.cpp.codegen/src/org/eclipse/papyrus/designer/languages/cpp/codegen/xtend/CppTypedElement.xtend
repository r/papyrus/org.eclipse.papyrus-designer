/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import org.eclipse.uml2.uml.TypedElement
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.TemplateBinding
import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.CppGenUtils.cgu
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.ListHint
import org.eclipse.uml2.uml.MultiplicityElement
import org.eclipse.papyrus.designer.languages.cpp.codegen.utils.Modifier
import org.eclipse.papyrus.designer.languages.common.profile.TemplateBindingConstants

/**
 * Produce the string for a typed element, taking the ListHint and TemplateBinding stereotype
 * into account
 */
class CppTypedElement {

	/**
	 * Handle list types
	 * @see Modifier (modArray) - in addition to this method. modArray is handling
	 * 	multiplicity via modifiers after the variable declaration (e.g. A a[]) if listHint
	 *  is NOT applied
	 * TODO: refactor code that both aspects (listHint applied or default handling) are done
	 *        in the same place
	 * @param propertyOrParameter
	 *            a property or parameter (in both cases a typed element)
	 * @return a string representation of the type of a property or parameter. The function takes
	 *         list hints into account in case of non 1 multiplicity
	 */
	static def String cppType(TypedElement propertyOrParameter) {
		var String type = null
		// default: normal string representation of C++ type
		var String defaultType = propertyOrParameter.cgu.cppQualifiedName(propertyOrParameter.type)
		val binding =
				UMLUtil.getStereotypeApplication(propertyOrParameter, TemplateBinding)
		if (binding !== null) {
			defaultType += '''<«FOR actual : binding.getActuals() SEPARATOR ','»«propertyOrParameter.cgu.cppQualifiedName(actual)»«ENDFOR»>'''
		}

		// calculate type from stereotype definition
		val listHint = GenUtils.getApplicationTree(propertyOrParameter, ListHint)
		val lower = (propertyOrParameter as MultiplicityElement).getLower()
		val upper = (propertyOrParameter as MultiplicityElement).getUpper()
		if (listHint !== null) {
			if (upper == -1) {
				type = listHint.getVariable()
			} else if (upper > 1) {
				if (upper == lower) {
					type = listHint.getFixed()
				} else {
					type = listHint.getBounded()
				}
			}
		}
		if (type !== null) {
			return type.replace(TemplateBindingConstants.TYPE_NAME_TAG, defaultType).
				replace(TemplateBindingConstants.LOWER, String.format("%d", lower)).
				replace(TemplateBindingConstants.UPPER, String.format("%d", upper))
		} else {
			return defaultType
		}
	}
}

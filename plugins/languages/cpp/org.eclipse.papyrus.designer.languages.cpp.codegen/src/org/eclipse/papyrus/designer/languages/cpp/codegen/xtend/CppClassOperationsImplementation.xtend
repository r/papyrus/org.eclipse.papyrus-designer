/*******************************************************************************
 * Copyright (c) 2014 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Inline
import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen
import org.eclipse.uml2.uml.Operation
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Default
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Delete

/**
 * @author Önder GÜRCAN (onder.gurcan@cea.fr)
 */
class CppClassOperationsImplementation {

	// implementations of a classifier, parameter controls whether only inline (or only not inline) operations
	//are produced
	static def CppClassOperationsImplementation(Classifier clazz, boolean inline) '''
		«FOR ownedOperation : CppOperations.getOwnedOperations(clazz).filter[(!it.noImplDef) && (GenUtils.hasStereotype(it, Inline) == inline)] SEPARATOR "\n"»
			«CppOperations.CppOperationImplementation(ownedOperation)»
		«ENDFOR»

		«FOR nestedOperation : CppOperations.getNestedOperations(clazz).filter[(!it.noImplDef) && (GenUtils.hasStereotype(it, Inline) == inline)] SEPARATOR "\n"»
			«CppOperations.CppOperationImplementation(nestedOperation)»
		«ENDFOR»

		«FOR b : clazz.ownedMembers.filter[it instanceof OpaqueBehavior && GenUtils.hasStereotype(it, Inline)==inline]»
			«val ob = b as OpaqueBehavior»
			«IF ob.specification === null»
				// opaque behavior without specification
				«CppOperations.CppBehaviorImplementation(b as OpaqueBehavior)»
			«ENDIF»
		«ENDFOR»
	'''

	/**
	 * No implementation definition is required
	 */
	static def noImplDef(Operation op) {
		return 
			StereotypeUtil.isApplied(op, NoCodeGen) ||
			StereotypeUtil.isApplied(op, Default) ||
			StereotypeUtil.isApplied(op, Delete) ||
			op.isAbstract;
	}
}
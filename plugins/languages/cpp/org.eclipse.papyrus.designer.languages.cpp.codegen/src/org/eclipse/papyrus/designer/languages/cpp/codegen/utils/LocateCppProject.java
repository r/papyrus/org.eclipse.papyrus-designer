/*******************************************************************************
 * Copyright (c) 2006 - 2013 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.codegen.utils;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.designer.languages.common.base.TestInfo;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.LanguageProjectSupport;
import org.eclipse.papyrus.designer.languages.cpp.codegen.Activator;
import org.eclipse.papyrus.designer.languages.cpp.codegen.preferences.CppCodeGenConstants;
import org.eclipse.papyrus.designer.languages.cpp.codegen.transformation.CppModelElementsCreator;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.util.UMLUtil;

public class LocateCppProject {

	private static final boolean Headless = TestInfo.runsHeadless();

	/**
	 * Locate and return the target project for the given packageable element.
	 * Return null if no target project can be found.
	 *
	 * Ensures that the target project is correctly setup to contain generated C/C++
	 * code. Does not create a new project, but may modify existing ones.
	 *
	 * @param pe
	 *            a packageable element within a model
	 * @param createIfMissing
	 *            if true, ask the user to apply the C++ nature if
	 *            required.
	 * @return the associated project, if the C++ nature is applied.
	 */
	public static IProject getTargetProject(PackageableElement pe, boolean createIfMissing) {
		Package rootPkg = PackageUtil.getRootPackage(pe);

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		String prefix = prefs != null ? prefs.get(CppCodeGenConstants.P_PROJECT_PREFIX_KEY, CppCodeGenConstants.P_PROJECT_PREFIX_DVAL) : "bad preferences."; //$NON-NLS-1$

		org.eclipse.papyrus.designer.languages.common.profile.Codegen.Project projectStereo = UMLUtil
				.getStereotypeApplication(rootPkg,
						org.eclipse.papyrus.designer.languages.common.profile.Codegen.Project.class);

		String projectName = null;
		if (projectStereo != null) {
			projectName = projectStereo.getProjectName();
		}
		if (projectName == null) {
			projectName = prefix + rootPkg.getName();
		}
		IProject modelProject = root.getProject(projectName);

		if (!modelProject.exists()) {
			String languageName = CppModelElementsCreator.LANGUAGE_NAME;
			boolean create = createIfMissing;
			if (create && !Headless) {
				languageName = LanguageProjectSupport.UI_PREFIX + languageName;	// use UI variant of creation mechanism
				create = openQuestion(Messages.LocateCppProject_CreateTargetProjectTitle,
						String.format(Messages.LocateCppProject_CreateTargetProjectDesc, projectName));
			}
			if (create) {
				ILangProjectSupport langSupport = LanguageProjectSupport
						.getProjectSupport(languageName);
				if (langSupport != null) {
					modelProject = langSupport.createProject(projectName);
					langSupport.setSettings(modelProject, langSupport.initialConfigurationData());
					if (modelProject == null) {
						return null;
					}
				} else {
					return null;
				}
			}
		}

		// Make sure the target project is open. If it was just created, it is
		// open. However if it is an old project then it could have been closed.
		if (!modelProject.isOpen()) {
			try {
				modelProject.open(null);
			} catch (CoreException e) {
				return null;
			}
		}

		return modelProject;
	}

	private static boolean openQuestion(final String title, final String message) {
		final boolean[] ret = new boolean[] { false };
		Display.getDefault().syncExec(new Runnable() {
			@Override
			public void run() {
				ret[0] = MessageDialog.openQuestion(Display.getCurrent().getActiveShell(), title, message);
			}
		});
		return ret[0];
	}
}

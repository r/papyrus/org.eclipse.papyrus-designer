/*******************************************************************************
 * Copyright (c) 2014 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
 
 package org.eclipse.papyrus.designer.languages.cpp.codegen.xtend

import org.eclipse.uml2.uml.Classifier
import static extension org.eclipse.papyrus.designer.languages.cpp.codegen.utils.CppGenUtils.cgu

import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Friend
import org.eclipse.papyrus.designer.languages.common.base.GenUtils
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen

/**
 * @author Önder GÜRCAN (onder.gurcan@cea.fr)
 */
class CppClassFriendDeclaration {
	static def CppClassFriendDeclaration(Classifier hostingClass, Classifier friend) '''
		friend class «hostingClass.cgu.cppQualifiedName(friend)»
	'''

	static def CppClassIncludeFriendDeclaration(Classifier clazz) {
		for (uc : GenUtils.getUsedClassifiers(clazz)) {
			if (GenUtils.hasStereotype(uc, Friend) && (!GenUtils.hasStereotype(uc, NoCodeGen))) {
				clazz.CppClassFriendDeclaration(uc)
			}
		}
	}
}
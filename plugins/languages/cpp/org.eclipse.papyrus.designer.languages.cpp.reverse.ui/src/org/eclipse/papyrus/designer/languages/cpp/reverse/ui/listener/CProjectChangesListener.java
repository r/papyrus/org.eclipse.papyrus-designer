/*******************************************************************************
 * Copyright (c) 2016 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.reverse.ui.listener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.cdt.core.model.ElementChangedEvent;
import org.eclipse.cdt.core.model.ICElementDelta;
import org.eclipse.cdt.core.model.IElementChangedListener;
import org.eclipse.papyrus.designer.languages.cpp.reverse.change.CElementChange;

/**
 * A listener that detects changes on a CDT project.
 * The events and processed and the listener feeds a map
 * that will be used for reverse in merge mode.
 *
 */
public class CProjectChangesListener implements IElementChangedListener {
	private static final String CHANGED = "CHANGED"; //$NON-NLS-1$
	private static final String REMOVED = "REMOVED"; //$NON-NLS-1$
	private static final String ADDED = "ADDED"; //$NON-NLS-1$
	private static final String POST_RECONCILE = "POST_RECONCILE"; //$NON-NLS-1$
	private static final String POST_CHANGE = "POST_CHANGE"; //$NON-NLS-1$
	private Map<String, List<CElementChange>> changesMap;
	
	
	public CProjectChangesListener(Map<String, List<CElementChange>> changesMap) {
		this.changesMap = changesMap;
	}

	@Override
	public void elementChanged(ElementChangedEvent event) {
		String eventName = ""; //$NON-NLS-1$
		if (event.getType() == 1) {
			eventName = POST_CHANGE;
		} else if (event.getType() == 4) {
			eventName = POST_RECONCILE;
		} else {
			eventName = event.getType() + ""; //$NON-NLS-1$
		}
		
		System.out.println("Process event of type " + eventName); //$NON-NLS-1$
		
		if (event.getType() == ElementChangedEvent.POST_RECONCILE || event.getType() == ElementChangedEvent.POST_CHANGE) {
			List<CElementChange> affectedLeafElements = new ArrayList<CElementChange>();
			fillAffectedLeafElements(event.getDelta(), affectedLeafElements);
			
			List<CElementChange> changes = null;
			for (CElementChange cElementChange : affectedLeafElements) {
				if (cElementChange.getChangeKind() != 0) {
					if (cElementChange.getElement().getCProject() != null) {
						String projectName = cElementChange.getElement().getCProject().getElementName();
						if (changesMap != null) {
							if (changesMap.get(projectName) != null) {
								changes = changesMap.get(projectName);
							} else {
								changes = Collections.synchronizedList(new LinkedList<CElementChange>());
								changesMap.put(cElementChange.getElement().getCProject().getElementName(), changes);
							}
						}
						
						if (changes != null) {
							changes.add(cElementChange);
							
							String changeName = ""; //$NON-NLS-1$
							if (cElementChange.getChangeKind() == 1) {
								changeName = ADDED;
							} else if (cElementChange.getChangeKind() == 2) {
								changeName = REMOVED;
							} else if (cElementChange.getChangeKind() == 4) {
								changeName = CHANGED;
							} else {
								changeName = cElementChange.getChangeKind() + ""; //$NON-NLS-1$
							}
							System.out.println(String.format("---> %s of type %s has been %s ==> PUT IN MAP", //$NON-NLS-1$
									cElementChange.getElement().getElementName(), cElementChange.getElement().getElementType(), changeName));
						}
					}
				} else {
					System.out.println(String.format("---> %s of type %s has been %s ==> NOTHING", //$NON-NLS-1$
							cElementChange.getElement().getElementName(), cElementChange.getElement().getElementType(), cElementChange.getChangeKind()));
				}
			}
		}
	}
	
	private void fillAffectedLeafElements(ICElementDelta delta, List<CElementChange> affectedLeafElements) {
		if (affectedLeafElements == null) {
			return;
		}
		
		if (delta != null) {
			if (delta.getAffectedChildren().length == 0 && delta.getElement() != null) {
				affectedLeafElements.add(new CElementChange(delta));
			} else {
				for (ICElementDelta childDelta : delta.getAffectedChildren()) {
					fillAffectedLeafElements(childDelta, affectedLeafElements);
				}
			}
		}
	}

}

/*****************************************************************************
 * Copyright (c) 2016, 2020 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.languages.cpp.library;

import org.eclipse.emf.common.util.URI;

/**
 * URI constants for libraries defined in this plug-in
 */
public class CppUriConstants {
	
	public static final String PATHMAP = "pathmap://PapyrusC_Cpp_LIBRARIES/"; //$NON-NLS-1$
	
	public static final String PTHREAD_LIB_PATH = PATHMAP + "pthread.uml"; //$NON-NLS-1$
	public static final String SOCKET_LIB_PATH = PATHMAP + "socket.uml"; //$NON-NLS-1$
	public static final String ANSIC_LIB_PATH = PATHMAP + "AnsiCLibrary.uml"; //$NON-NLS-1$
	public static final String OSAL_LIB_PATH = PATHMAP + "osal.uml"; //$NON-NLS-1$
	public static final String STL_LIB_PATH = PATHMAP + "STL.uml"; //$NON-NLS-1$

	public static final URI PTHREAD_LIB_URI = URI.createURI(PTHREAD_LIB_PATH);
	public static final URI SOCKET_LIB_URI = URI.createURI(SOCKET_LIB_PATH);
	public static final URI ANSIC_LIB_URI = URI.createURI(ANSIC_LIB_PATH);
	public static final URI OSAL_LIB_URI = URI.createURI(OSAL_LIB_PATH);
	public static final URI STL_LIB_URI = URI.createURI(STL_LIB_PATH);
}

/**
 */
package org.eclipse.papyrus.designer.transformation.profile.Transformation;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationFactory
 * @model kind="package"
 * @generated
 */
public interface TransformationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Transformation"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/Transformation/1"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Transformation"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TransformationPackage eINSTANCE = org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoImpl <em>M2M Trafo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoImpl
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getM2MTrafo()
	 * @generated
	 */
	int M2M_TRAFO = 0;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO__BASE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Before</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO__BEFORE = 1;

	/**
	 * The feature id for the '<em><b>After</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO__AFTER = 2;

	/**
	 * The number of structural features of the '<em>M2M Trafo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>M2M Trafo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoChainImpl <em>M2M Trafo Chain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoChainImpl
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getM2MTrafoChain()
	 * @generated
	 */
	int M2M_TRAFO_CHAIN = 1;

	/**
	 * The feature id for the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO_CHAIN__BASE_CLASS = 0;

	/**
	 * The number of structural features of the '<em>M2M Trafo Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO_CHAIN_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>M2M Trafo Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int M2M_TRAFO_CHAIN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.DerivedElementImpl <em>Derived Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.DerivedElementImpl
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getDerivedElement()
	 * @generated
	 */
	int DERIVED_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_ELEMENT__BASE_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_ELEMENT__SOURCE = 1;

	/**
	 * The number of structural features of the '<em>Derived Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Derived Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DERIVED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ApplyTransformationImpl <em>Apply Transformation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ApplyTransformationImpl
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getApplyTransformation()
	 * @generated
	 */
	int APPLY_TRANSFORMATION = 3;

	/**
	 * The feature id for the '<em><b>Trafo</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TRANSFORMATION__TRAFO = 0;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TRANSFORMATION__BASE_ELEMENT = 1;

	/**
	 * The number of structural features of the '<em>Apply Transformation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TRANSFORMATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Apply Transformation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLY_TRANSFORMATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl <em>Execute Trafo Chain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getExecuteTrafoChain()
	 * @generated
	 */
	int EXECUTE_TRAFO_CHAIN = 4;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_TRAFO_CHAIN__BASE_PACKAGE = 0;

	/**
	 * The feature id for the '<em><b>Chain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_TRAFO_CHAIN__CHAIN = 1;

	/**
	 * The feature id for the '<em><b>Additional Trafos</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_TRAFO_CHAIN__ADDITIONAL_TRAFOS = 2;

	/**
	 * The feature id for the '<em><b>Additional Chains</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_TRAFO_CHAIN__ADDITIONAL_CHAINS = 3;

	/**
	 * The number of structural features of the '<em>Execute Trafo Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_TRAFO_CHAIN_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Execute Trafo Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTE_TRAFO_CHAIN_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl <em>Monitored</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getMonitored()
	 * @generated
	 */
	int MONITORED = 5;

	/**
	 * The feature id for the '<em><b>Base State Machine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED__BASE_STATE_MACHINE = 0;

	/**
	 * The feature id for the '<em><b>Is Monitored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED__IS_MONITORED = 1;

	/**
	 * The feature id for the '<em><b>Exclude</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED__EXCLUDE = 2;

	/**
	 * The feature id for the '<em><b>Exclusively Include</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED__EXCLUSIVELY_INCLUDE = 3;

	/**
	 * The feature id for the '<em><b>Generate Monitoring Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED__GENERATE_MONITORING_CODE = 4;

	/**
	 * The feature id for the '<em><b>Generate Exclude</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED__GENERATE_EXCLUDE = 5;

	/**
	 * The feature id for the '<em><b>Generate Exclusively Include</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED__GENERATE_EXCLUSIVELY_INCLUDE = 6;

	/**
	 * The number of structural features of the '<em>Monitored</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Monitored</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MONITORED_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo <em>M2M Trafo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>M2M Trafo</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo
	 * @generated
	 */
	EClass getM2MTrafo();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo#getBase_Class()
	 * @see #getM2MTrafo()
	 * @generated
	 */
	EReference getM2MTrafo_Base_Class();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo#getBefore <em>Before</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Before</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo#getBefore()
	 * @see #getM2MTrafo()
	 * @generated
	 */
	EReference getM2MTrafo_Before();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo#getAfter <em>After</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>After</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo#getAfter()
	 * @see #getM2MTrafo()
	 * @generated
	 */
	EReference getM2MTrafo_After();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain <em>M2M Trafo Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>M2M Trafo Chain</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain
	 * @generated
	 */
	EClass getM2MTrafoChain();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain#getBase_Class <em>Base Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Class</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain#getBase_Class()
	 * @see #getM2MTrafoChain()
	 * @generated
	 */
	EReference getM2MTrafoChain_Base_Class();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.DerivedElement <em>Derived Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Derived Element</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.DerivedElement
	 * @generated
	 */
	EClass getDerivedElement();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.DerivedElement#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.DerivedElement#getBase_Element()
	 * @see #getDerivedElement()
	 * @generated
	 */
	EReference getDerivedElement_Base_Element();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.DerivedElement#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.DerivedElement#getSource()
	 * @see #getDerivedElement()
	 * @generated
	 */
	EReference getDerivedElement_Source();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ApplyTransformation <em>Apply Transformation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Apply Transformation</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ApplyTransformation
	 * @generated
	 */
	EClass getApplyTransformation();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ApplyTransformation#getTrafo <em>Trafo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Trafo</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ApplyTransformation#getTrafo()
	 * @see #getApplyTransformation()
	 * @generated
	 */
	EReference getApplyTransformation_Trafo();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ApplyTransformation#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ApplyTransformation#getBase_Element()
	 * @see #getApplyTransformation()
	 * @generated
	 */
	EReference getApplyTransformation_Base_Element();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain <em>Execute Trafo Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Execute Trafo Chain</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain
	 * @generated
	 */
	EClass getExecuteTrafoChain();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getBase_Package <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Package</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getBase_Package()
	 * @see #getExecuteTrafoChain()
	 * @generated
	 */
	EReference getExecuteTrafoChain_Base_Package();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getChain <em>Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Chain</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getChain()
	 * @see #getExecuteTrafoChain()
	 * @generated
	 */
	EReference getExecuteTrafoChain_Chain();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getAdditionalTrafos <em>Additional Trafos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Additional Trafos</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getAdditionalTrafos()
	 * @see #getExecuteTrafoChain()
	 * @generated
	 */
	EReference getExecuteTrafoChain_AdditionalTrafos();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getAdditionalChains <em>Additional Chains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Additional Chains</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getAdditionalChains()
	 * @see #getExecuteTrafoChain()
	 * @generated
	 */
	EReference getExecuteTrafoChain_AdditionalChains();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored <em>Monitored</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Monitored</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored
	 * @generated
	 */
	EClass getMonitored();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getBase_StateMachine <em>Base State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base State Machine</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getBase_StateMachine()
	 * @see #getMonitored()
	 * @generated
	 */
	EReference getMonitored_Base_StateMachine();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isMonitored <em>Is Monitored</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Monitored</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isMonitored()
	 * @see #getMonitored()
	 * @generated
	 */
	EAttribute getMonitored_IsMonitored();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getExclude <em>Exclude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Exclude</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getExclude()
	 * @see #getMonitored()
	 * @generated
	 */
	EReference getMonitored_Exclude();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getExclusivelyInclude <em>Exclusively Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Exclusively Include</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getExclusivelyInclude()
	 * @see #getMonitored()
	 * @generated
	 */
	EReference getMonitored_ExclusivelyInclude();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isGenerateMonitoringCode <em>Generate Monitoring Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generate Monitoring Code</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isGenerateMonitoringCode()
	 * @see #getMonitored()
	 * @generated
	 */
	EAttribute getMonitored_GenerateMonitoringCode();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getGenerateExclude <em>Generate Exclude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Generate Exclude</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getGenerateExclude()
	 * @see #getMonitored()
	 * @generated
	 */
	EReference getMonitored_GenerateExclude();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getGenerateExclusivelyInclude <em>Generate Exclusively Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Generate Exclusively Include</em>'.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getGenerateExclusivelyInclude()
	 * @see #getMonitored()
	 * @generated
	 */
	EReference getMonitored_GenerateExclusivelyInclude();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TransformationFactory getTransformationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoImpl <em>M2M Trafo</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoImpl
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getM2MTrafo()
		 * @generated
		 */
		EClass M2M_TRAFO = eINSTANCE.getM2MTrafo();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference M2M_TRAFO__BASE_CLASS = eINSTANCE.getM2MTrafo_Base_Class();

		/**
		 * The meta object literal for the '<em><b>Before</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference M2M_TRAFO__BEFORE = eINSTANCE.getM2MTrafo_Before();

		/**
		 * The meta object literal for the '<em><b>After</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference M2M_TRAFO__AFTER = eINSTANCE.getM2MTrafo_After();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoChainImpl <em>M2M Trafo Chain</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.M2MTrafoChainImpl
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getM2MTrafoChain()
		 * @generated
		 */
		EClass M2M_TRAFO_CHAIN = eINSTANCE.getM2MTrafoChain();

		/**
		 * The meta object literal for the '<em><b>Base Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference M2M_TRAFO_CHAIN__BASE_CLASS = eINSTANCE.getM2MTrafoChain_Base_Class();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.DerivedElementImpl <em>Derived Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.DerivedElementImpl
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getDerivedElement()
		 * @generated
		 */
		EClass DERIVED_ELEMENT = eINSTANCE.getDerivedElement();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DERIVED_ELEMENT__BASE_ELEMENT = eINSTANCE.getDerivedElement_Base_Element();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DERIVED_ELEMENT__SOURCE = eINSTANCE.getDerivedElement_Source();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ApplyTransformationImpl <em>Apply Transformation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ApplyTransformationImpl
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getApplyTransformation()
		 * @generated
		 */
		EClass APPLY_TRANSFORMATION = eINSTANCE.getApplyTransformation();

		/**
		 * The meta object literal for the '<em><b>Trafo</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLY_TRANSFORMATION__TRAFO = eINSTANCE.getApplyTransformation_Trafo();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference APPLY_TRANSFORMATION__BASE_ELEMENT = eINSTANCE.getApplyTransformation_Base_Element();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl <em>Execute Trafo Chain</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getExecuteTrafoChain()
		 * @generated
		 */
		EClass EXECUTE_TRAFO_CHAIN = eINSTANCE.getExecuteTrafoChain();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTE_TRAFO_CHAIN__BASE_PACKAGE = eINSTANCE.getExecuteTrafoChain_Base_Package();

		/**
		 * The meta object literal for the '<em><b>Chain</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTE_TRAFO_CHAIN__CHAIN = eINSTANCE.getExecuteTrafoChain_Chain();

		/**
		 * The meta object literal for the '<em><b>Additional Trafos</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTE_TRAFO_CHAIN__ADDITIONAL_TRAFOS = eINSTANCE.getExecuteTrafoChain_AdditionalTrafos();

		/**
		 * The meta object literal for the '<em><b>Additional Chains</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTE_TRAFO_CHAIN__ADDITIONAL_CHAINS = eINSTANCE.getExecuteTrafoChain_AdditionalChains();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl <em>Monitored</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl
		 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.TransformationPackageImpl#getMonitored()
		 * @generated
		 */
		EClass MONITORED = eINSTANCE.getMonitored();

		/**
		 * The meta object literal for the '<em><b>Base State Machine</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORED__BASE_STATE_MACHINE = eINSTANCE.getMonitored_Base_StateMachine();

		/**
		 * The meta object literal for the '<em><b>Is Monitored</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORED__IS_MONITORED = eINSTANCE.getMonitored_IsMonitored();

		/**
		 * The meta object literal for the '<em><b>Exclude</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORED__EXCLUDE = eINSTANCE.getMonitored_Exclude();

		/**
		 * The meta object literal for the '<em><b>Exclusively Include</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORED__EXCLUSIVELY_INCLUDE = eINSTANCE.getMonitored_ExclusivelyInclude();

		/**
		 * The meta object literal for the '<em><b>Generate Monitoring Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MONITORED__GENERATE_MONITORING_CODE = eINSTANCE.getMonitored_GenerateMonitoringCode();

		/**
		 * The meta object literal for the '<em><b>Generate Exclude</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORED__GENERATE_EXCLUDE = eINSTANCE.getMonitored_GenerateExclude();

		/**
		 * The meta object literal for the '<em><b>Generate Exclusively Include</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MONITORED__GENERATE_EXCLUSIVELY_INCLUDE = eINSTANCE.getMonitored_GenerateExclusivelyInclude();

	}

} //TransformationPackage

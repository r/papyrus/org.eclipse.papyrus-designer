/**
 */
package org.eclipse.papyrus.designer.transformation.profile.Transformation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Execute Trafo Chain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Execute a specified transformation chain.
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getBase_Package <em>Base Package</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getChain <em>Chain</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getAdditionalTrafos <em>Additional Trafos</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getAdditionalChains <em>Additional Chains</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getExecuteTrafoChain()
 * @model
 * @generated
 */
public interface ExecuteTrafoChain extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Package</em>' reference.
	 * @see #setBase_Package(org.eclipse.uml2.uml.Package)
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getExecuteTrafoChain_Base_Package()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	org.eclipse.uml2.uml.Package getBase_Package();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getBase_Package <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Package</em>' reference.
	 * @see #getBase_Package()
	 * @generated
	 */
	void setBase_Package(org.eclipse.uml2.uml.Package value);

	/**
	 * Returns the value of the '<em><b>Chain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain</em>' reference.
	 * @see #setChain(M2MTrafoChain)
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getExecuteTrafoChain_Chain()
	 * @model ordered="false"
	 * @generated
	 */
	M2MTrafoChain getChain();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain#getChain <em>Chain</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Chain</em>' reference.
	 * @see #getChain()
	 * @generated
	 */
	void setChain(M2MTrafoChain value);

	/**
	 * Returns the value of the '<em><b>Additional Trafos</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Trafos</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Trafos</em>' reference list.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getExecuteTrafoChain_AdditionalTrafos()
	 * @model ordered="false"
	 * @generated
	 */
	EList<M2MTrafo> getAdditionalTrafos();

	/**
	 * Returns the value of the '<em><b>Additional Chains</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Chains</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Chains</em>' reference list.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getExecuteTrafoChain_AdditionalChains()
	 * @model ordered="false"
	 * @generated
	 */
	EList<M2MTrafoChain> getAdditionalChains();

} // ExecuteTrafoChain

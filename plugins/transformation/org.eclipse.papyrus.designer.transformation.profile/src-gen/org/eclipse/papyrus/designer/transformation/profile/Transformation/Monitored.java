/**
 */
package org.eclipse.papyrus.designer.transformation.profile.Transformation;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.StateMachine;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Monitored</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getBase_StateMachine <em>Base State Machine</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isMonitored <em>Is Monitored</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getExclude <em>Exclude</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getExclusivelyInclude <em>Exclusively Include</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isGenerateMonitoringCode <em>Generate Monitoring Code</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getGenerateExclude <em>Generate Exclude</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getGenerateExclusivelyInclude <em>Generate Exclusively Include</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored()
 * @model
 * @generated
 */
public interface Monitored extends EObject {
	/**
	 * Returns the value of the '<em><b>Is Monitored</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Monitored</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Monitored</em>' attribute.
	 * @see #setIsMonitored(boolean)
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored_IsMonitored()
	 * @model default="true" dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isMonitored();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isMonitored <em>Is Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Monitored</em>' attribute.
	 * @see #isMonitored()
	 * @generated
	 */
	void setIsMonitored(boolean value);

	/**
	 * Returns the value of the '<em><b>Exclude</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Element}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exclude</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exclude</em>' reference list.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored_Exclude()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Element> getExclude();

	/**
	 * Returns the value of the '<em><b>Exclusively Include</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Element}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exclusively Include</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exclusively Include</em>' reference list.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored_ExclusivelyInclude()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Element> getExclusivelyInclude();

	/**
	 * Returns the value of the '<em><b>Base State Machine</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base State Machine</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base State Machine</em>' reference.
	 * @see #setBase_StateMachine(StateMachine)
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored_Base_StateMachine()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	StateMachine getBase_StateMachine();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#getBase_StateMachine <em>Base State Machine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base State Machine</em>' reference.
	 * @see #getBase_StateMachine()
	 * @generated
	 */
	void setBase_StateMachine(StateMachine value);

	/**
	 * Returns the value of the '<em><b>Generate Monitoring Code</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate Monitoring Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generate Monitoring Code</em>' attribute.
	 * @see #setGenerateMonitoringCode(boolean)
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored_GenerateMonitoringCode()
	 * @model default="true" dataType="org.eclipse.uml2.types.Boolean" required="true" ordered="false"
	 * @generated
	 */
	boolean isGenerateMonitoringCode();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored#isGenerateMonitoringCode <em>Generate Monitoring Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generate Monitoring Code</em>' attribute.
	 * @see #isGenerateMonitoringCode()
	 * @generated
	 */
	void setGenerateMonitoringCode(boolean value);

	/**
	 * Returns the value of the '<em><b>Generate Exclude</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Element}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate Exclude</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generate Exclude</em>' reference list.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored_GenerateExclude()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Element> getGenerateExclude();

	/**
	 * Returns the value of the '<em><b>Generate Exclusively Include</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.uml2.uml.Element}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generate Exclusively Include</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generate Exclusively Include</em>' reference list.
	 * @see org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage#getMonitored_GenerateExclusivelyInclude()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Element> getGenerateExclusivelyInclude();

} // Monitored

/**
 */
package org.eclipse.papyrus.designer.transformation.profile.Transformation.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Execute Trafo Chain</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl#getBase_Package <em>Base Package</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl#getChain <em>Chain</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl#getAdditionalTrafos <em>Additional Trafos</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.ExecuteTrafoChainImpl#getAdditionalChains <em>Additional Chains</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExecuteTrafoChainImpl extends MinimalEObjectImpl.Container implements ExecuteTrafoChain {
	/**
	 * The cached value of the '{@link #getBase_Package() <em>Base Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Package()
	 * @generated
	 * @ordered
	 */
	protected org.eclipse.uml2.uml.Package base_Package;

	/**
	 * The cached value of the '{@link #getChain() <em>Chain</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChain()
	 * @generated
	 * @ordered
	 */
	protected M2MTrafoChain chain;

	/**
	 * The cached value of the '{@link #getAdditionalTrafos() <em>Additional Trafos</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalTrafos()
	 * @generated
	 * @ordered
	 */
	protected EList<M2MTrafo> additionalTrafos;

	/**
	 * The cached value of the '{@link #getAdditionalChains() <em>Additional Chains</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalChains()
	 * @generated
	 * @ordered
	 */
	protected EList<M2MTrafoChain> additionalChains;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecuteTrafoChainImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformationPackage.Literals.EXECUTE_TRAFO_CHAIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public org.eclipse.uml2.uml.Package getBase_Package() {
		if (base_Package != null && base_Package.eIsProxy()) {
			InternalEObject oldBase_Package = (InternalEObject)base_Package;
			base_Package = (org.eclipse.uml2.uml.Package)eResolveProxy(oldBase_Package);
			if (base_Package != oldBase_Package) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TransformationPackage.EXECUTE_TRAFO_CHAIN__BASE_PACKAGE, oldBase_Package, base_Package));
			}
		}
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public org.eclipse.uml2.uml.Package basicGetBase_Package() {
		return base_Package;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_Package(org.eclipse.uml2.uml.Package newBase_Package) {
		org.eclipse.uml2.uml.Package oldBase_Package = base_Package;
		base_Package = newBase_Package;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformationPackage.EXECUTE_TRAFO_CHAIN__BASE_PACKAGE, oldBase_Package, base_Package));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public M2MTrafoChain getChain() {
		if (chain != null && chain.eIsProxy()) {
			InternalEObject oldChain = (InternalEObject)chain;
			chain = (M2MTrafoChain)eResolveProxy(oldChain);
			if (chain != oldChain) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TransformationPackage.EXECUTE_TRAFO_CHAIN__CHAIN, oldChain, chain));
			}
		}
		return chain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public M2MTrafoChain basicGetChain() {
		return chain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setChain(M2MTrafoChain newChain) {
		M2MTrafoChain oldChain = chain;
		chain = newChain;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformationPackage.EXECUTE_TRAFO_CHAIN__CHAIN, oldChain, chain));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<M2MTrafo> getAdditionalTrafos() {
		if (additionalTrafos == null) {
			additionalTrafos = new EObjectResolvingEList<M2MTrafo>(M2MTrafo.class, this, TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_TRAFOS);
		}
		return additionalTrafos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<M2MTrafoChain> getAdditionalChains() {
		if (additionalChains == null) {
			additionalChains = new EObjectResolvingEList<M2MTrafoChain>(M2MTrafoChain.class, this, TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_CHAINS);
		}
		return additionalChains;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__BASE_PACKAGE:
				if (resolve) return getBase_Package();
				return basicGetBase_Package();
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__CHAIN:
				if (resolve) return getChain();
				return basicGetChain();
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_TRAFOS:
				return getAdditionalTrafos();
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_CHAINS:
				return getAdditionalChains();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)newValue);
				return;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__CHAIN:
				setChain((M2MTrafoChain)newValue);
				return;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_TRAFOS:
				getAdditionalTrafos().clear();
				getAdditionalTrafos().addAll((Collection<? extends M2MTrafo>)newValue);
				return;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_CHAINS:
				getAdditionalChains().clear();
				getAdditionalChains().addAll((Collection<? extends M2MTrafoChain>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__BASE_PACKAGE:
				setBase_Package((org.eclipse.uml2.uml.Package)null);
				return;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__CHAIN:
				setChain((M2MTrafoChain)null);
				return;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_TRAFOS:
				getAdditionalTrafos().clear();
				return;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_CHAINS:
				getAdditionalChains().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__BASE_PACKAGE:
				return base_Package != null;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__CHAIN:
				return chain != null;
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_TRAFOS:
				return additionalTrafos != null && !additionalTrafos.isEmpty();
			case TransformationPackage.EXECUTE_TRAFO_CHAIN__ADDITIONAL_CHAINS:
				return additionalChains != null && !additionalChains.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ExecuteTrafoChainImpl

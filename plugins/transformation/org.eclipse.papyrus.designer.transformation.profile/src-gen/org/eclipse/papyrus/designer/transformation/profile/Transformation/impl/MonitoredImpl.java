/**
 */
package org.eclipse.papyrus.designer.transformation.profile.Transformation.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.TransformationPackage;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.StateMachine;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Monitored</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl#getBase_StateMachine <em>Base State Machine</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl#isMonitored <em>Is Monitored</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl#getExclude <em>Exclude</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl#getExclusivelyInclude <em>Exclusively Include</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl#isGenerateMonitoringCode <em>Generate Monitoring Code</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl#getGenerateExclude <em>Generate Exclude</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.transformation.profile.Transformation.impl.MonitoredImpl#getGenerateExclusivelyInclude <em>Generate Exclusively Include</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MonitoredImpl extends MinimalEObjectImpl.Container implements Monitored {
	/**
	 * The cached value of the '{@link #getBase_StateMachine() <em>Base State Machine</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_StateMachine()
	 * @generated
	 * @ordered
	 */
	protected StateMachine base_StateMachine;

	/**
	 * The default value of the '{@link #isMonitored() <em>Is Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMonitored()
	 * @generated
	 * @ordered
	 */
	protected static final boolean IS_MONITORED_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isMonitored() <em>Is Monitored</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMonitored()
	 * @generated
	 * @ordered
	 */
	protected boolean isMonitored = IS_MONITORED_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExclude() <em>Exclude</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExclude()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> exclude;

	/**
	 * The cached value of the '{@link #getExclusivelyInclude() <em>Exclusively Include</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExclusivelyInclude()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> exclusivelyInclude;

	/**
	 * The default value of the '{@link #isGenerateMonitoringCode() <em>Generate Monitoring Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGenerateMonitoringCode()
	 * @generated
	 * @ordered
	 */
	protected static final boolean GENERATE_MONITORING_CODE_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isGenerateMonitoringCode() <em>Generate Monitoring Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isGenerateMonitoringCode()
	 * @generated
	 * @ordered
	 */
	protected boolean generateMonitoringCode = GENERATE_MONITORING_CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getGenerateExclude() <em>Generate Exclude</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerateExclude()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> generateExclude;

	/**
	 * The cached value of the '{@link #getGenerateExclusivelyInclude() <em>Generate Exclusively Include</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerateExclusivelyInclude()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> generateExclusivelyInclude;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MonitoredImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TransformationPackage.Literals.MONITORED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isMonitored() {
		return isMonitored;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIsMonitored(boolean newIsMonitored) {
		boolean oldIsMonitored = isMonitored;
		isMonitored = newIsMonitored;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformationPackage.MONITORED__IS_MONITORED, oldIsMonitored, isMonitored));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Element> getExclude() {
		if (exclude == null) {
			exclude = new EObjectResolvingEList<Element>(Element.class, this, TransformationPackage.MONITORED__EXCLUDE);
		}
		return exclude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Element> getExclusivelyInclude() {
		if (exclusivelyInclude == null) {
			exclusivelyInclude = new EObjectResolvingEList<Element>(Element.class, this, TransformationPackage.MONITORED__EXCLUSIVELY_INCLUDE);
		}
		return exclusivelyInclude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public StateMachine getBase_StateMachine() {
		if (base_StateMachine != null && base_StateMachine.eIsProxy()) {
			InternalEObject oldBase_StateMachine = (InternalEObject)base_StateMachine;
			base_StateMachine = (StateMachine)eResolveProxy(oldBase_StateMachine);
			if (base_StateMachine != oldBase_StateMachine) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TransformationPackage.MONITORED__BASE_STATE_MACHINE, oldBase_StateMachine, base_StateMachine));
			}
		}
		return base_StateMachine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateMachine basicGetBase_StateMachine() {
		return base_StateMachine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setBase_StateMachine(StateMachine newBase_StateMachine) {
		StateMachine oldBase_StateMachine = base_StateMachine;
		base_StateMachine = newBase_StateMachine;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformationPackage.MONITORED__BASE_STATE_MACHINE, oldBase_StateMachine, base_StateMachine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isGenerateMonitoringCode() {
		return generateMonitoringCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setGenerateMonitoringCode(boolean newGenerateMonitoringCode) {
		boolean oldGenerateMonitoringCode = generateMonitoringCode;
		generateMonitoringCode = newGenerateMonitoringCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TransformationPackage.MONITORED__GENERATE_MONITORING_CODE, oldGenerateMonitoringCode, generateMonitoringCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Element> getGenerateExclude() {
		if (generateExclude == null) {
			generateExclude = new EObjectResolvingEList<Element>(Element.class, this, TransformationPackage.MONITORED__GENERATE_EXCLUDE);
		}
		return generateExclude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Element> getGenerateExclusivelyInclude() {
		if (generateExclusivelyInclude == null) {
			generateExclusivelyInclude = new EObjectResolvingEList<Element>(Element.class, this, TransformationPackage.MONITORED__GENERATE_EXCLUSIVELY_INCLUDE);
		}
		return generateExclusivelyInclude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TransformationPackage.MONITORED__BASE_STATE_MACHINE:
				if (resolve) return getBase_StateMachine();
				return basicGetBase_StateMachine();
			case TransformationPackage.MONITORED__IS_MONITORED:
				return isMonitored();
			case TransformationPackage.MONITORED__EXCLUDE:
				return getExclude();
			case TransformationPackage.MONITORED__EXCLUSIVELY_INCLUDE:
				return getExclusivelyInclude();
			case TransformationPackage.MONITORED__GENERATE_MONITORING_CODE:
				return isGenerateMonitoringCode();
			case TransformationPackage.MONITORED__GENERATE_EXCLUDE:
				return getGenerateExclude();
			case TransformationPackage.MONITORED__GENERATE_EXCLUSIVELY_INCLUDE:
				return getGenerateExclusivelyInclude();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TransformationPackage.MONITORED__BASE_STATE_MACHINE:
				setBase_StateMachine((StateMachine)newValue);
				return;
			case TransformationPackage.MONITORED__IS_MONITORED:
				setIsMonitored((Boolean)newValue);
				return;
			case TransformationPackage.MONITORED__EXCLUDE:
				getExclude().clear();
				getExclude().addAll((Collection<? extends Element>)newValue);
				return;
			case TransformationPackage.MONITORED__EXCLUSIVELY_INCLUDE:
				getExclusivelyInclude().clear();
				getExclusivelyInclude().addAll((Collection<? extends Element>)newValue);
				return;
			case TransformationPackage.MONITORED__GENERATE_MONITORING_CODE:
				setGenerateMonitoringCode((Boolean)newValue);
				return;
			case TransformationPackage.MONITORED__GENERATE_EXCLUDE:
				getGenerateExclude().clear();
				getGenerateExclude().addAll((Collection<? extends Element>)newValue);
				return;
			case TransformationPackage.MONITORED__GENERATE_EXCLUSIVELY_INCLUDE:
				getGenerateExclusivelyInclude().clear();
				getGenerateExclusivelyInclude().addAll((Collection<? extends Element>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TransformationPackage.MONITORED__BASE_STATE_MACHINE:
				setBase_StateMachine((StateMachine)null);
				return;
			case TransformationPackage.MONITORED__IS_MONITORED:
				setIsMonitored(IS_MONITORED_EDEFAULT);
				return;
			case TransformationPackage.MONITORED__EXCLUDE:
				getExclude().clear();
				return;
			case TransformationPackage.MONITORED__EXCLUSIVELY_INCLUDE:
				getExclusivelyInclude().clear();
				return;
			case TransformationPackage.MONITORED__GENERATE_MONITORING_CODE:
				setGenerateMonitoringCode(GENERATE_MONITORING_CODE_EDEFAULT);
				return;
			case TransformationPackage.MONITORED__GENERATE_EXCLUDE:
				getGenerateExclude().clear();
				return;
			case TransformationPackage.MONITORED__GENERATE_EXCLUSIVELY_INCLUDE:
				getGenerateExclusivelyInclude().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TransformationPackage.MONITORED__BASE_STATE_MACHINE:
				return base_StateMachine != null;
			case TransformationPackage.MONITORED__IS_MONITORED:
				return isMonitored != IS_MONITORED_EDEFAULT;
			case TransformationPackage.MONITORED__EXCLUDE:
				return exclude != null && !exclude.isEmpty();
			case TransformationPackage.MONITORED__EXCLUSIVELY_INCLUDE:
				return exclusivelyInclude != null && !exclusivelyInclude.isEmpty();
			case TransformationPackage.MONITORED__GENERATE_MONITORING_CODE:
				return generateMonitoringCode != GENERATE_MONITORING_CODE_EDEFAULT;
			case TransformationPackage.MONITORED__GENERATE_EXCLUDE:
				return generateExclude != null && !generateExclude.isEmpty();
			case TransformationPackage.MONITORED__GENERATE_EXCLUSIVELY_INCLUDE:
				return generateExclusivelyInclude != null && !generateExclusivelyInclude.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (isMonitored: "); //$NON-NLS-1$
		result.append(isMonitored);
		result.append(", generateMonitoringCode: "); //$NON-NLS-1$
		result.append(generateMonitoringCode);
		result.append(')');
		return result.toString();
	}

} //MonitoredImpl

package org.eclipse.papyrus.designer.transformation.library.transformations;

import org.eclipse.papyrus.designer.deployment.tools.DepUtils;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationUtil;
import org.eclipse.papyrus.designer.transformation.extensions.InstanceConfigurator;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Package;

public class ApplyIConfiguratorOnTarget  implements IM2MTrafoCDP {

	@Override
	public void applyTrafo(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {
		InstanceConfigurator.onNodeModel = true;
		for (InstanceSpecification instance : DepUtils.getTopLevelInstances(deploymentPlan)) {
			TransformationUtil.applyInstanceConfigurators(instance, null, null);
		}
	}

}

/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 *****************************************************************************/
 
 package org.eclipse.papyrus.designer.transformation.library.xtend

import org.eclipse.uml2.uml.Operation
import static extension org.eclipse.papyrus.designer.transformation.library.xtend.CppUtils.cppType
import static extension org.eclipse.papyrus.designer.uml.tools.utils.OperationUtils.parametersInInout

// TODO: original model code does effectively do more than marshalling
class StructBasedMarshalling {
	def static marshall(Operation operation) '''
		struct ParamData {
			«FOR parameter : operation.parametersInInout»
				«parameter.type.cppType» «parameter.name»;
			«ENDFOR»
		};
		Event event;
		event.ID = [operation.name/];
		event.kind = CallEvent;
		ParamData * data = &event.params;
		«FOR parameter : operation.parametersInInout»
		data->[parameter.name/] = [parameter.name/];
		«ENDFOR»
		out->dispatch(event);
	'''
	
}
/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.designer.transformation.library.xtend

import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Class
import static extension org.eclipse.papyrus.designer.transformation.library.xtend.CppUtils.*
import static extension org.eclipse.papyrus.designer.transformation.library.xtend.Marshalling.*

/**
 * text template to execute asynchrounous calls in the context of a component-based
 * implementation.
 * TODO: This class is referenced from the component library, but is not registered in the plugin.xml
 *   It's likely broken or deprecated
 */
class AsyncCalls {
	def asyncCall(Operation operation) '''
		// TODO: need suitable constant dimensioning
		pBuffer = &buffer[500];  // grows backwards
		«operation.marshall»
		int operationID = ID_[operation.name/];
		BEncAsnContent (&amp;pBuffer, &amp;operationID);

		pthread_t pt;
		pthread_create (&amp;pt, NULL, staticDispatch, (void *) this);
		// TODO: add semaphore which assures that subsequent calls to «operation.name» are not executed before dispatch
		// has removed the parameters from the pBuffer stack (pBuffer can be corrupted).
	'''

	def dispatch_(Class clazz) '''
		int operationID;
		BDecAsnContent (&pBuffer, operationID);
		switch (operationID) {
			«FOR operation : clazz.ownedOperations»
			case ID_«operation.name»
			{
				
				// delegate call to executor
				rconn->«operation.cppCall»;
				break;
			}
			«ENDFOR»
		}
 	'''

	def dispatchWithThreadPool(Class clazz) '''
		int operationID;
		BDecAsnContent (&pBuffer, operationID);
		switch (operationID) {
			«FOR operation : clazz.ownedOperations»
			case ID_«operation.name»
			{
				«operation.unmarshall»
				// delegate call to executor
				rconn->«operation.cppCall»;
				«IF operation.type !== null»«operation.type.cppType» ret = «ENDIF»rconn->«operation.cppCall»;
				«operation.marshallOutInout»
				resultsReady = 1;
				break;
			}
			«ENDFOR»
		}
	}
	'''
}
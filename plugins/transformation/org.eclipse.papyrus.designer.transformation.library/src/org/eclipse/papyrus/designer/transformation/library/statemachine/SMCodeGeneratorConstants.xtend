/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.designer.transformation.library.statemachine

/**
 * SM Code generator constants that are common for C++ and Java
 */
class SMCodeGeneratorConstants {
	public static final String ACTIVE_SUB_STATE = "activeSubState"
	public static final String PREVIOUS_SUB_STATE = "previouSubState"
	public static final String RESTORE_DEEP_HISTORY_FUNC_NAME = "restoreDeepHistory"
	public static final String RESTORE_SHALLOW_HISTORY_FUNC_NAME = "restoreShallowHistory"
	public static final String ON_ENTRY_ACTION_FUNC_NAME = "onEntryAction"
	public static final String ON_EXIT_ACTION_FUNC_NAME = "onExitAction"
	public static final String SET_INIDEFAULT_STATE_FUNC_NAME = "setInitDefaultState"
	public static final String SET_SHALLOW_HISTORY_DEFAULT_STATE_FUNC_NAME = "setShallowHistoryDefaultState"
	public static final String SET_DEEP_HISTORY_DEFAULT_STATE_FUNC_NAME = "setDeepHistoryDefaultState"
	public static final String CONTEXT = "context"
	public static final String ANCESTOR = "ancestor"
	public static final String ASSIGNMENT_OPERATOR = "operator="
	public static final String ORTHOGONAL_STATE_CLASS_NAME = "OrthogonalState"
	public static final String ENTRY_COMPOSITE_ON_HISTORY = "entryCompositeOnHistory"
	public static final String ENTRY_COMPOSITE_ON_PARTICULAR_SUBSTATE = "entryCompositeOnSubState"
	public static final String TIMED_STATE_INTERFACE_NAME = "TimedState"
	public static final String TIMER_CLASS_NAME = "Timer"
	public static final String TIMEOUT_NAME = "timeout"
	public static final String TIMEOUT_MILISECOND_NAME = "milisec"
	public static final String TIMED_STATE_ATTR_NAME = "timedState"
	public static final String START_THREAD_FUNC_NAME = "startThread"
	public static final String WAIT_THREAD_FUNC_NAME = "waitThread"
	public static final String START_INTERNAL_THREAD_NAME = "startInternalThread"
	public static final String INTERNAL_THREAD_ENTRY_NAME = "startInternalThreadEntry"
	public static final String PTHREAD_ATTR_NAME = "mThread"
	public static final String EVENT_POOL_NAME = "eventPool"
	public static final String TIMER_POOL_NAME = "timerPool"
	public static final String MAX_NUMER_EVENT = "50"
	public static final String MAX_NUMBER_TIME_EVENT = "50"
	public static final String MAX_NUMBER_EVENT_NAME = "MAX_EVENT"
	public static final String MAX_NUMBER_TIME_EVENT_NAME = "MAX_TIME_EVENT"
	public static final String NUMBER_EVENT_NAME = "numberEvent"
	public static final String NUMBER_TIME_EVENT_NAME = "numberTimeEvent"
	public static final String EVENT_READ_POS_NAME = "eventReadPos"
	public static final String TIME_EVENT_READ_POS_NAME = "timeEventReadPos"
	public static final String EVENT_WRITE_POS_NAME = "eventWritePos"
	public static final String TIME_EVENT_WRITE_POS_NAME = "timeEventWritePos"
	public static final String POP_EVENT = "popEvent"
	public static final String POP_TIME_EVENT = "popTimeEvent"
	public static final String PROCESSTIMEEVENT = "processTimeEvent"
	public static final String SIGNAL_EVENT_NAME = "signal_name"
	public static final String CALL_OPERATION_NAME = "operation_name"
	public static final String ENTRY_COMPOSITE_ON_POINT = "entryOn_"
	public static final String EXIT_COMPOSITE_ON_POINT = "exitOn_"
	public static final String TRANSITION_ON_EXP = "transitionOn_"
	public static final String ENTRY_ON_FORK = "entryOn_"
	public static final String DO_ACTIVITY_NAME = "doActivity"
	public static final String ON_ACTIVITY_THREAD = "onActivityThread"
	public static final String TRIGGERLESS_TRANSITION = "triggerlessTransition"
	public static final String ENTRY_NAME = "entry"
	public static final String EXIT_NAME = "exit"
	public static final String STATE_STRUCT_NAME = "State_t"

	public static final String ON_ACTIVITY_ACTION_NAME = "onActivityAction"
	public static final String ON_EXIT_ACTIVITY = "onExitActivity"
	public static final String cpp11ThreadAttributeName = "theThread"
	public static final String STATE_ARRAY_ATTRIBUTE = "states"
	public static final String ACTIVE_ROOT_STATE_ID = "activeStateID"
	public static final String STATE_MAX = "STATE_MAX"
	public static final String PREVIOUS_STATES = "previousStates"
	public static final String ACTIVE_SUB_STATES = "actives"
	public static final String COMPLETION_EVENT = "CompletionEvent"
	public static final String THREADS = "threads"
	public static final String THREADS_TIME_EVENT = "timeEventThreads"
	public static final String THREADS_CHANGE_EVENT = "changeEventThreads"
	public static final String FLAGS_ACTIVITY = "flags"
	public static final String FLAGS_TIME_EVENT = "timeEventFlags"
	
	public static final String MUTEXES = "mutexes"
	public static final String MUTEXES_TIME_EVENT = "timeEventMutexes"
	public static final String CONDITIONS = "conds"
	public static final String CONDITIONS_TIME_EVENT = "timeEventConds"
	public static final String STRUCT_FOR_THREAD_SHORTN = "StructForThread_t"
	public static final String THREAD_STRUCTS = "threadStructs"
	public static final String THREAD_STRUCTS_FOR_TIMEEVENT = "timeEventThreadStructs"
	public static final String THREAD_STRUCTS_FOR_CHANGEEVENT = "changeEventThreadStructs"
	public static final String THREAD_FUNC_WRAPPER = "thread_func_wrapper"
	public static final String FPT_POINTER_FOR_TABLE = "FptPointer"
	public static final String DO_ACTIVITY_TABLE = "doActivityTable"
	public static final String TIME_EVENT_TABLE = "timeEventTable"
	public static final String REGION_TABLE = "regionTable"
	public static final String PARALLEL_TRANSITION_TABLE = "transitionTable"
	public static final String REGION_TABLE_EXIT = "regionExitTable"
	public static final String REGION_FUNCTION_PTR = "RegionFunctionPtr_t"
	public static final String CHANGE_EVENT_TABLE = "changeEventTable"
	public static final String DO_CALL_ACTIVITY = "doCallActivity"
	public static final String SET_FLAG = "setFlag"
	public static final String STATE_ID_ENUM = "StateIDEnum"
	public static final String FORK_NAME = "pthread_create"
	public static final String JOIN_NAME = "pthread_join"
	public static final String EVENT_ID = "EventId_t"

	public static final String TIME_EVENT_LISTEN_FUNCTION = "listenTimeEvent"
	public static final String CHANGE_EVENT_LISTEN_FUNCTION = "listenChangeEvent"
	public static final String SYSTEM_STATE_ATTR = "systemState"
	public static final String EVENT_DISPATCH = "dispatchEvent"
	public static final String EVENT_QUEUE = "eventQueue"
	public static final String RUN_TO_COMPLETION_MUTEX = "runToCompletionMutex"
	public static final String RUN_TO_COMPLETION_COND = "runToCompletionCond"
	public static final String ENTER_MODE_PARAM = "enter_mode"
}

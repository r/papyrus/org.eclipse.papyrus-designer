/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.library;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.eclipse.papyrus.designer.transformation.library.messages"; //$NON-NLS-1$

	public static String MergeClass_CannotApply0;
	public static String BootLoaderGen_AtLeastOneBlockingCall;
	public static String CompleteStatemachine_NoTransformationFound;

	public static String CreateAndConfigureProject_CANNOT_CREATE_PROJECT;

	public static String CreateAndConfigureProject_CHANGE_PROJECT_MAPPING;

	public static String DeployToNodes_CantFindDefaultNode;

	public static String DeployToNodes_CantFindImplementation;

	public static String DeployToNodes_CouldNotCreateProject;

	public static String InstantiateDepPlan_InfoDeployingForNode;

	public static String InstantiateDepPlan_InfoGeneratingModel;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}

/*****************************************************************************
 * Copyright (c) 2022 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 *****************************************************************************/
 
 package org.eclipse.papyrus.designer.transformation.library.xtend

import org.eclipse.uml2.uml.Operation
import static extension org.eclipse.papyrus.designer.transformation.library.xtend.CppUtils.cppCall

class MultipleReceptacle {
	def operation_(Operation operation) '''
		for (int i=0; i&lt;9; i++) {
			if (rconn[i] != 0) {
				rconn[i]->«operation.cppCall»;
			}
		}
	'''
}
/*****************************************************************************
 * Copyright (c) 2019 CIL4Sys Engineering
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Yoann Farré - yoann.farre@cil4sys.com
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.library.transformations;

import java.text.Normalizer;
import java.util.Map.Entry;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;

/**
 * This class enables the normalization of named elements, i.e. the replacement of
 * special characters by code compatible characters
 * This transformation can be applied to all "NamedElement"
 */
public class NamedElementNormalization implements IM2MTrafoCDP {

	@Override
	public void applyTrafo(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {
		for (Entry<EObject, EObject> e : TransformationContext.current.copier.entrySet()) {
			EObject copy = e.getValue();
			if (copy instanceof NamedElement) {
				normalizeName((NamedElement) copy);
			}
		}
	}

	/**
	 * Replace special chars in the name of a named element by code compatible characters
	 * @param namedElement
	 */
	public static void normalizeName(NamedElement namedElement){
		String oldName = namedElement.getName();
		if (oldName != null && !oldName.isEmpty()) {
			String newName = Normalizer.normalize(namedElement.getName(), Normalizer.Form.NFD)
					.replaceAll("[^\\p{ASCII}]", "")
					.replaceAll(" ", "_")
					.replaceAll("'", "_");
	
			namedElement.setName(newName);
		}
	}
}

/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.export;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.ProfileApplication;

/**
 * Variant of LazyCopier that will only copy a single element (and the contained contents)
 *
 */
public class SelectiveLazyCopier extends LazyCopier {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1592795273952957239L;

	/**
	 * Constructor.
	 *
	 * @param source
	 * @param target
	 * @param copyExtResources
	 * @param copyID
	 */
	public SelectiveLazyCopier(Package source, Package target, CopyExtResources copyExtResources, boolean copyID) {
		super(source, target, copyExtResources, copyID);
	}

	protected EObject filterObject;
	
	/**
	 * Set the filter object, i.e. the only object that should get copied (including its children)
	 * @param filterObject
	 */
	public void setFilter(EObject filterObject) {
		this.filterObject = filterObject;
	}
	
	@Override
	public boolean copyResource(EObject sourceEObj) {
		if (isChild(sourceEObj)) {
			return super.copyResource(sourceEObj);
		}
		else if (sourceEObj instanceof Package || sourceEObj instanceof ProfileApplication || (!(sourceEObj instanceof Element))) {
			// copy all packages, profile applications and stereotype applications (non-element objects) 
			return super.copyResource(sourceEObj);
		}
		return false;
	}
	
	/**
	 * @param sourceEObj
	 * @return true, if the passed object is a child of the the filterObject
	 */
	protected boolean isChild(EObject sourceEObj) {
		if (sourceEObj == filterObject) {
			return true;
		}
		EObject container = sourceEObj.eContainer();
		if (container != null) {
			return isChild(container);
		}
		return false;
	}
}

/*******************************************************************************
 * Copyright (c) 2011, 2014 AtoS, CEA LIST and other.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Anass RADOUANI (AtoS)
 *    Gabriel Pascual (ALL4TEC) gabriel.pascual@all4tec.net - Bug 440754
 *    Fred Eckertson (Cerner) - fred.eckertson@cerner.com - Bug 502705
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.export.wizards;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier.CopyExtResources;
import org.eclipse.papyrus.designer.transformation.export.Activator;
import org.eclipse.papyrus.designer.transformation.export.Messages;
import org.eclipse.papyrus.designer.transformation.export.SelectiveLazyCopier;
import org.eclipse.papyrus.infra.emf.utils.EMFHelper;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * Export wizard for Export all diagrams feature.
 */
public class ExportElementWizard extends Wizard implements IExportWizard {

	/**
	 * wizard page to export an element from a Papyrus model.
	 */
	private ExportElementPage page;

	protected EList<PackageableElement> selectedElements;

	/**
	 * Constructor.
	 *
	 */
	public ExportElementWizard() {
		super();
		setWindowTitle(Messages.ExportElements_EXPORT_SELECTED_ELEMENT_NEW_UML);
	}

	/**
	 * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
	 *
	 * @param workbench a workbench reference
	 * @param selection_ a selection (ignored)
	 */
	public void init(IWorkbench workbench, IStructuredSelection selection_) {
		// ignore passed selection (since it does not relate to model explorer or diagram)
		ISelectionService selectionService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
		ISelection selection = selectionService.getSelection();
		selectedElements = new BasicEList<PackageableElement>();

		// Get first element if the selection is an IStructuredSelection
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			for (Object selectedObject : structuredSelection.toList()) {
				EObject selectedEObject = EMFHelper.getEObject(selectedObject);
				if (selectedEObject instanceof PackageableElement) {
					selectedElements.add((PackageableElement) selectedEObject);
				}
			}
		}
		if (selectedElements.size() > 0) {
			page = new ExportElementPage(selectedElements);
			addPage(page);
		}
		else {
			addErrorPage();
		}
	}

	private void addErrorPage() {
		WizardPage pageError = new WizardPage("Error") { //$NON-NLS-1$
			/**
			 * Create contents of the wizard.
			 *
			 * @param parent
			 */
			public void createControl(Composite parent) {
				Label label = new Label(parent, SWT.NONE);
				label.setText(Messages.ExportElements_ERROR_MSG);
				setControl(label);
			}
		};
		addPage(pageError);
	}

	/**
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 *
	 * @return true, if successful
	 */
	@Override
	public boolean performFinish() {
		page.setErrorMessage(null);
		ModelManagement newModel;
		try {
			newModel = ModelManagement.createNewModel(page.getModelName());
			newModel.setURI(page.getOutputURI());
			Package rootPkg = PackageUtil.getRootPackage(selectedElements.get(0));
			if (page.useTransitiveClosure()) {
				LazyCopier copier = new LazyCopier(rootPkg, newModel.getModel(), CopyExtResources.NONE, true);
				for (PackageableElement element : selectedElements) {
					copier.getCopy(element);
				}
			}
			else {
				SelectiveLazyCopier copier = new SelectiveLazyCopier(rootPkg, newModel.getModel(), CopyExtResources.NONE, true);
				for (PackageableElement element : selectedElements) {
					copier.setFilter(element);
					copier.getCopy(element);
				}
			}
			newModel.save();
			newModel.dispose();
		}
		catch (TransformationException e) {
			Activator.log.error(e);
		}
		return true;
	}
}

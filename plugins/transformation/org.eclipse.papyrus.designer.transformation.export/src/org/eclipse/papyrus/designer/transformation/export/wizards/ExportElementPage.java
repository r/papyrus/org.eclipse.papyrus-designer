/*******************************************************************************
 * Copyright (c) 2011 AtoS
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Anass RADOUANI (AtoS)
 *    Fred Eckertson (Cerner) - fred.eckertson@cerner.com - Bug 502705
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.export.wizards;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.papyrus.designer.transformation.export.Messages;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ContainerSelectionDialog;
import org.eclipse.uml2.uml.PackageableElement;

/**
 * wizard page to export all diagram from a Papyrus model
 */
public class ExportElementPage extends WizardPage {

	private final EList<PackageableElement> selectedElements;

	// SWT Objects
	private Text outputPathTxt;

	protected Text fileNameTxt;

	protected Text modelNameTxt;

	private Button outputDirectoryBtn;

	private Button btnCheckButton;

	protected boolean useClosure = false;

	/**
	 * Create the wizard.
	 * 
	 * @since 2.0
	 */
	public ExportElementPage(EList<PackageableElement> selectedElements) {
		super(Messages.ExportElements_EXPORT_SELECTED_ELEMENT);
		this.selectedElements = selectedElements;
		
		setDescription(Messages.ExportElements_EXPORT_SELECTED_ELEMENT_NEW_UML);
	}

	/**
	 * Create contents of the wizard.
	 *
	 * @param parent
	 */
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		
		PackageableElement firstElement = selectedElements.get(0);
		Label modelNameLbl = new Label(composite, SWT.NONE);
		modelNameLbl.setText(Messages.ExportElements_NEW_MODEL_NAME);
		modelNameTxt = new Text(composite, SWT.BORDER);
		modelNameTxt.setText(PackageUtil.getRootPackage(firstElement).getName());
		modelNameTxt.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		Label fileNameLbl = new Label(composite, SWT.NONE);
		fileNameLbl.setText(Messages.ExportElements_FILE_NAME);
		fileNameTxt = new Text(composite, SWT.BORDER);
		fileNameTxt.setText(firstElement.getName() + ".uml"); //$NON-NLS-1$
		fileNameTxt.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		Label editorConfLbl = new Label(composite, SWT.NONE);
		editorConfLbl.setText(Messages.ExportElements_OUTPUT_DIRECTORY);
		Composite output = new Composite(composite, SWT.NONE);
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		output.setLayout(layout);
		output.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		outputPathTxt = new Text(output, SWT.BORDER);
		outputPathTxt.setEnabled(false);
		outputPathTxt.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		outputDirectoryBtn = new Button(output, SWT.NONE);
		outputDirectoryBtn.setText(Messages.ExportElements_SELECT);

		btnCheckButton = new Button(composite, SWT.CHECK);
		btnCheckButton.setText(Messages.ExportElements_TRANSITIVE_CLOSURE);

		GridLayout dialogLayout = new GridLayout();
		GridData dialogLayoutData = new GridData(GridData.FILL_BOTH);
		composite.setLayout(dialogLayout);
		composite.setLayoutData(dialogLayoutData);

		setControl(composite);

		hookListeners();
		setPageComplete(false);
	}

	private void hookListeners() {
		btnCheckButton.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				useClosure = btnCheckButton.getSelection();
			}
		});

		outputDirectoryBtn.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				ContainerSelectionDialog csDialog = new ContainerSelectionDialog(
						Display.getCurrent().getActiveShell(), ResourcesPlugin.getWorkspace().getRoot(), true,
						Messages.ExportElements_OUTPUT_DIRECTORY);

				if (csDialog.open() == Window.OK) {
					Object[] results = csDialog.getResult();
					if (results.length == 1 && results[0] instanceof IPath) {
						outputPathTxt.setText(results[0].toString());
						setPageComplete(true);
					}
				}
			}
		});
	}

	/**
	 * return the output directory where exported diagrams will be saved
	 *
	 * @return an URI corresponding to the output directory
	 */
	public URI getOutputURI() {
		String path = outputPathTxt.getText() + "/" + fileNameTxt.getText(); //$NON-NLS-1$
		return URI.createPlatformResourceURI(path, true);
	}

	/**
	 * @return a boolean indicating whether to use the transitive closure
	 */
	public boolean useTransitiveClosure() {
		return useClosure;
	}
	
	public String getModelName() {
		return modelNameTxt.getText();
	}
}

/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.transformations.filters;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.papyrus.designer.transformation.core.copylisteners.PreCopyListener;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;


/**
 * Load models that are referenced into the model-management resource set, i.e. the
 * resource set that can be modified.
 */
public class FilterLoadReferencedModels implements PreCopyListener {

	public static FilterLoadReferencedModels getInstance() {
		if (instance == null) {
			instance = new FilterLoadReferencedModels();
		}
		return instance;
	}

	@Override
	public EObject preCopyEObject(LazyCopier copier, EObject sourceEObj) {
		final Resource sourceRes = sourceEObj.eResource();
		if (sourceRes != copier.source.eResource()) {
			if (sourceRes == null) {
				GenUtils.checkProxy(sourceEObj);
			}
			Resource targetRes = ModelManagement.getResourceSet().getResource(sourceRes.getURI(), true);
			return targetRes.getEObject(sourceRes.getURIFragment(sourceEObj));
		}
		return sourceEObj;
	}

	private static FilterLoadReferencedModels instance = null;
}

/*****************************************************************************
 * Copyright (c) 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.transformations.filters;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.core.copylisteners.PreCopyListener;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;


/**
 * Treat all elements within a containment, i.e. ignore (USE_SOURCE) elements thast
 * are outside
 */
public class FilterContainment implements PreCopyListener {

	public static FilterContainment getInstance() {
		if (instance == null) {
			instance = new FilterContainment();
		}
		return instance;
	}

	protected EObject root;
	
	/**
	 * set the root object of the containment
	 * @param root
	 */
	public void setRoot(EObject root) {
		this.root = root;
	}
	
	@Override
	public EObject preCopyEObject(LazyCopier copier, EObject sourceEObj) {
		if (!withinContainment(copier, sourceEObj)) {
			return LazyCopier.USE_SOURCE_OBJECT;
		}
		return sourceEObj;	
	}

	/**
	 * Check whether the passed element (within the source model) is within a
	 * a template, i.e. one of is owners is mapped towards the bound package in
	 * the target model. Note that multiple owners in the source model may be
	 * mapped to the same bound package.
	 *
	 * @param element
	 * @return true, if within containment
	 */
	public boolean withinContainment(LazyCopier copier, EObject element) {
		if (root != null) {
			EObject owner = element;
			if ((element.eContainer() == null) &&
					!(element instanceof Element)) { // has no eContainer and is not a UML element => likely to be a be a stereotype application.
				// it is important not to call getBaseElement for all EObjects, since its execution can take
				// quite a while (in particular, if not called on a stereotype application)
				Element base = UMLUtil.getBaseElement(owner);
				if (base != null) {
					owner = base; // containment check is done with base element
				}
				// check case that owner is already bound package (which can only be the case for a stereotype)
				if (copier.get(owner) == root) {
					return true;
				}
			}
			while (owner != null) {
				if (copier.get(owner) == root) {
					return true;
				}
				owner = owner.eContainer();
			}
		}
		return false;
	}

	private static FilterContainment instance = null;
}

/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.transformation.core.generate;

/**
 * Constants for generation options (must be powers of 2)
 *
 * @author ansgar
 *
 */
public interface GenerationOptions {

	final public static int REWRITE_SETTINGS = 1;

	final public static int ONLY_CHANGED = 2;

	final public static int MODEL_ONLY = 4;

	final public static int CAC_ONLY = 8;

};

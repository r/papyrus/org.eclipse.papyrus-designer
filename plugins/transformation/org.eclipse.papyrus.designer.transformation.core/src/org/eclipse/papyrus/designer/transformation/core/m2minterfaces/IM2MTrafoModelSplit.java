/**
 * Copyright (c) 2016 CEA LIST
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License 2.0 which
 * accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - Initial API and implementation
 */

package org.eclipse.papyrus.designer.transformation.core.m2minterfaces;

import java.util.List;

import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.transformation.extensions.IM2MTrafo;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.uml2.uml.Package;

public interface IM2MTrafoModelSplit extends IM2MTrafo {
	/**
	 * Split a model into several sub-models (on which subsequent transformations are applied)
	 * @param trafo
	 * @param deploymentPlan
	 * @return a list of new transformation contexts
	 * @throws TransformationException
	 */
	public List<TransformationContext> splitModel(M2MTrafo trafo, Package deploymentPlan) throws TransformationException;
}

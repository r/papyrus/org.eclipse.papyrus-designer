/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.transformations;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangProjectSupport;
import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Package;

/**
 * Return information about the current values in the context of a transformation
 * e.g. the current instance, the current port (container transformation), ...
 * A part of these data changes during a single transformation chain, such as the
 * copier and the current modelRoot, since each model split updates these. These
 * variables are available on the instance level via the "current" attribute.
 */
public class TransformationContext {

	public Package deploymentPlan;

	public ModelManagement mm;

	public IProject project;

	public ILangProjectSupport projectSupport;

	public Package modelRoot;

	public LazyCopier copier;

	// the element, to which transformations are currently applied.
	public Classifier classifier;

	public InstanceSpecification node;

	/**
	 * List of files to keep during codeCleanup (needed, if additional files are
	 * produced that are not created by code creation) 
	 */
	public List<String> keepFiles = new ArrayList<String>();

	/**
	 * Initialize a transformation context, but do not create a new context
	 */
	public static void init(IProgressMonitor monitor, Package initialSourceRoot, Package initialDeploymentPlan) {
		TransformationContext.monitor = monitor;
		TransformationContext.initialSourceRoot = initialSourceRoot;
		TransformationContext.initialDeploymentPlan = initialDeploymentPlan;
		current = null;
		chainContexts = new ArrayList<TransformationContext>();
	}

	/**
	 * Set a new active context. It is added to the list of available contexts
	 * 
	 * @param tc
	 *            a transformation context
	 */
	public static void setContext(TransformationContext tc) {
		current = tc;
		chainContexts.add(tc);
	}

	/**
	 * Reset the active context. Called at the end of a transformation chain
	 */
	public static void resetContext() {
		current = null;
		chainContexts = null;
	}

	/**
	 * Remove the last context from the list of current contexts.
	 * ALso updates the current context (to the previous context)
	 */
	public static void popContext() {
		int size = chainContexts.size();
		if (size > 0) {
			size--;
			chainContexts.remove(size);
			if (size > 0) {
				current = chainContexts.get(size - 1);
			} else {
				current = null;
			}
		}
	}
	
	public static List<TransformationContext> chainContexts() {
		return chainContexts;
	};

	/**
	 * Current transformation context
	 * Use only for read access (use setActiveContext, popContext or resetContenxt for changing
	 * the current context)
	 */
	public static TransformationContext current;

	/**
	 * keep a list of contexts used by a transformation chain
	 */
	protected static List<TransformationContext> chainContexts;

	public static IProgressMonitor monitor;

	public static Package initialSourceRoot;

	public static Package initialDeploymentPlan;

}

/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.core.templates;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.Messages;
import org.eclipse.papyrus.designer.transformation.core.copylisteners.PostCopyListener;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier.CopyExtResources;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier.CopyStatus;
import org.eclipse.papyrus.designer.transformation.core.transformations.filters.FilterContainment;
import org.eclipse.papyrus.designer.transformation.core.transformations.filters.FilterSignatures;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.TemplateUtils;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.ParameterableElement;
import org.eclipse.uml2.uml.TemplateBinding;
import org.eclipse.uml2.uml.TemplateParameterSubstitution;
import org.eclipse.uml2.uml.TemplateSignature;
import org.eclipse.uml2.uml.TemplateableElement;

/**
 * This class takes care of (package) template instantiation,
 * i.e. the creation of a new package in which the formal parameters
 * of the package template are bound to the actual parameters provided
 * by a binding.
 */
public class TemplateInstantiation {

	/**
	 * reuse the same copier for the same bound package to enable the
	 * option that a package template has only be partly copied (copier
	 * is lazy) without creating duplicates.	 */
	protected static Map<Package, LazyCopier> copierSet;

	protected static Map<EObject, Boolean> appliedPost;
	
	/**
	 * initialize the copier set.
	 */
	public static void init() {
		copierSet = new WeakHashMap<Package, LazyCopier>();
	}
	/**
	 * Create a new template instantiation from a given copier and binding
	 * 
	 * @param binding
	 *            a template binding (formal->actual mapping)
	 * @throws TransformationException
	 */
	public TemplateInstantiation(TemplateBinding binding) throws TransformationException {
		if (binding == null) {
			throw new TransformationException("Passed binding is null"); //$NON-NLS-1$
		}
		signature = binding.getSignature();
		if (signature == null) {
			throw new TransformationException("Passed template binding does not have a signature"); //$NON-NLS-1$
		}
		packageTemplate = (Package) signature.getOwner();

		Package boundPackage = (Package) binding.getBoundElement();
		// set template instantiation parameter. Used by xtend templates to get relation between
		// formal and actual parameters
		context = new InstantiationContext(this);
		
		this.binding = binding;
		copier = copierSet.get(boundPackage);
		if (copier == null) {
			// not in map yet, create
			copier = new LazyCopier(packageTemplate, boundPackage, CopyExtResources.ALL, true);
			copierSet.put(boundPackage, copier);
			
			// add copy listeners ---
			copier.preCopyListeners.add(FilterContainment.getInstance());
			FilterContainment.getInstance().setRoot(boundPackage);
			// remove template signature
			copier.preCopyListeners.add(FilterSignatures.getInstance());
			// special treatment for elements stereotyped with template parameter
			copier.preCopyListeners.add(PreTemplateInstantiationListener.getInstance());
			PreTemplateInstantiationListener.getInstance().init();
		}
		
		if (boundPackage.getPackagedElements().size() > 0) {
			// bound package is not empty, but copier does not know about it. Fill copyMap with information about the relation
			// This happens, if the original model already contains template instantiations, e.g. for template ports
			if (copier.keySet().size() == 0) {
				sourceResource = packageTemplate.eResource();
				targetResource = boundPackage.eResource();
				syncCopyMap(packageTemplate, boundPackage);
			}
			// syncCopy map would not be necessary, if ports are not updated (by a listener?) during copy)
		}

		// register a combination of formal/actual in the hashmap
		// => copier will replace actual with formal
		for (TemplateParameterSubstitution substitution : binding.getParameterSubstitutions()) {
			ParameterableElement formal = substitution.getFormal().getParameteredElement();
			ParameterableElement actual = substitution.getActual();
			copier.putPair(formal, actual);
		}
		appliedPost = new HashMap<EObject, Boolean>();
	}

	/**
	 * Synchronize the copy map, i.e. put the correspondences between existing source and target elements into the map.
	 * Otherwise, a new binding would produce duplicates.
	 * TODO: A more efficient way would be to cache the copy function and only re-sync, if a new model has been loaded.
	 * On the other hand, the bound package is normally not very large
	 *
	 * @param source
	 *            An element of the source model
	 * @param targetPkg
	 *            The associated element of the target model
	 */
	public void syncCopyMap(EObject source, EObject targetPkg) {
		if ((copier.get(source) == null) || copier.getStatus(targetPkg) != CopyStatus.FULL) {
			copier.put(source, targetPkg);
			if (source instanceof Package) {
				copier.setStatus(targetPkg, CopyStatus.SHALLOW);
			} else {
				copier.setStatus(targetPkg, CopyStatus.FULL);
			}
			for (EObject targetSubElem : targetPkg.eContents()) {
				EObject sourceSubElement = null;
				if (targetSubElem instanceof NamedElement) {
					String targetName = ((NamedElement) targetSubElem).getName();
					sourceSubElement = ElementUtils.getNamedElementFromList(source.eContents(), targetName);
				}
				if (sourceSubElement == null) {
					// no source element found, try to find via XML URI (copier synchronizes on demand)
					String uriFragment = targetResource.getURIFragment(targetSubElem);
					sourceSubElement = sourceResource.getEObject(uriFragment);
				}
				if (sourceSubElement != null) {
					syncCopyMap(sourceSubElement, targetSubElem);
				}
			}
		}
	}

	Package packageTemplate;

	public TemplateBinding binding;

	protected LazyCopier copier;

	protected TemplateSignature signature;

	protected Resource sourceResource;

	protected Resource targetResource;
	
	/**
	 * Map to identify target objects when given source objects
	 */
	protected Map<EObject, EObject> templateMap;

	/**
	 * Set of maps for template instantiations
	 */
	protected Map<EObject, Map<EObject, EObject>> templateMapInfo;

	/**
	 * Bind a named element. Besides of binding the passed element, this
	 * operation will bind all elements that are referenced (required) by the
	 * passed element.
	 *
	 * In consequence, typically only a small part of a package template is
	 * actually created within the bound package. We call this mechanism lazy
	 * instantiation/binding
	 *
	 * @param namedElement
	 *            A member within the package template which should be bound,
	 *            i.e. for which template instantiation should be performed.
	 */
	@SuppressWarnings("unchecked")
	public <T extends Element> T bindElement(T namedElement) throws TransformationException {
		if (namedElement == null) {
			// user should never see this exception
			throw new TransformationException(Messages.TemplateInstantiation_TemplateIsNull);
		}

		EList<Namespace> path = PkgTemplateUtils.relativePathWithMerge(namedElement, packageTemplate);

		if (path != null) {
			// register owning package template (template can be defined in
			// multiple packages)
			Element owner = TemplateUtils.getTemplateOwner(namedElement, signature);
			if (owner != null) {
				// put root of the template (which may be different from the packageTemplate which
				// is referenced via package merge relationship)
				Package boundPackage = (Package) binding.getBoundElement();
				copier.put(owner, boundPackage);
			}
		} 
		else {
			// element is not part of the package template referenced by the binding
			if (namedElement instanceof TemplateableElement) {
				// check whether the referenced element is part of another
				// package template,
				// (for which we allow for implicit binding with the first
				// template parameter)
				TemplateSignature signatureOfNE = TemplateUtils.getSignature((TemplateableElement) namedElement);
				if ((signatureOfNE != null) && (signature != signatureOfNE)) {
					TemplateBinding subBinding = PkgTemplateUtils.getSubBinding(copier.target, (TemplateableElement) namedElement, binding);
					TemplateInstantiation ti = new TemplateInstantiation(subBinding);
					Element ret = ti.bindElement(namedElement);
					return (T) ret;
				}
			}
			// => nothing to do with respect to template instantiation, but
			// since the template is potentially instantiated in another model,
			// the referenced element might need to be copied.
			return copier.getCopy(namedElement);
		}

		// element is contained in the template package, examine whether it
		// already exists in the bound package.
		NamedElement existingMember = (NamedElement) copier.get(namedElement);

		if (existingMember == null) {
			T copiedElement = copier.getCopy(namedElement);
			// apply additional transformation on element, notably text-template instantiations
			applyPost(copier.target, Collections.singletonList(PostTemplateInstantiationListener.getInstance()));

			return copiedElement;
		}
		return (T) existingMember;
	}

	/**
	 * Apply post copy listeners
	 * @param element
	 * @param listeners
	 */
	protected void applyPost(EObject element, List<PostCopyListener> listeners) {
		if (!appliedPost.containsKey(element)) {
			appliedPost.put(element, true);
		for (PostCopyListener listener : listeners) {
			listener.postCopyEObject(copier, element);
		}
		for (EObject subElement : element.eContents()) {
			applyPost(subElement, listeners);
		}
		}
	}
	
	public static InstantiationContext context;
}

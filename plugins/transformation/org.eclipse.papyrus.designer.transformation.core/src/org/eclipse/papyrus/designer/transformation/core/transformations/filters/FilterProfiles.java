/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.transformations.filters;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.core.copylisteners.PreCopyListener;
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier;
import org.eclipse.uml2.uml.Profile;


/**
 * Do not copy copy profiles during target model transformation (avoid duplicate profiles
 * in designer resource set). This class is required, as the lazy copier does no longer
 * avoid copying profiles in general (to assure that a profile does not remain in the Papyrus
 * model set only with implications on threading).
 */
public class FilterProfiles implements PreCopyListener {

	public static FilterProfiles getInstance() {
		if (instance == null) {
			instance = new FilterProfiles();
		}
		return instance;
	}

	@Override
	public EObject preCopyEObject(LazyCopier copier, EObject sourceEObj) {
		if (sourceEObj instanceof Profile) {
			return LazyCopier.USE_SOURCE_OBJECT;
		}
		return sourceEObj;
	}

	private static FilterProfiles instance = null;
}

/*****************************************************************************
 * Copyright (c) 2013, 2022 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.core.utils;

import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageableElement;
import org.eclipse.uml2.uml.Type;

public class DependencyUtils {
	/**
	 * Declares a dependency from the current classifier which is produced by template instantiation
	 * to the element (enum) "OperationIDs", if found within the passed package.
	 *
	 * @param pkg
	 */
	public static void declareDependencyToOperationIDs(Package pkg) {
		PackageableElement type = pkg.getPackagedElement("OperationIDs"); //$NON-NLS-1$
		if (type instanceof Type) {
			ElementUtils.declareDependency(TransformationContext.current.classifier, (Type) type);
		}
	}

	/**
	 * Declares a dependency from the current classifier which is produced by template instantiation
	 * to the element (enum) "SignalIDs", if found within the passed package.
	 *
	 * @param pkg
	 *            the package in which an enumeration is looked up
	 */
	public static void declareDependencyToSignalIDs(Package pkg) {
		NamedElement type = ElementUtils.getQualifiedElement(pkg, "globalenums::" + SIGNAL_ENUM); //$NON-NLS-1$
		if (type instanceof Type) {
			ElementUtils.declareDependency(TransformationContext.current.classifier, (Type) type);
		}
	}
	
	static final String SIGNAL_ENUM = "SignalIDs"; //$NON-NLS-1$
}


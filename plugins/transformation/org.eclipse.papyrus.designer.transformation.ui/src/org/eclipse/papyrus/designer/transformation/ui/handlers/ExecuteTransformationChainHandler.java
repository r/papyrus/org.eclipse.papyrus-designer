/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.ui.handlers;

import java.util.regex.Pattern;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.papyrus.designer.infra.ui.UIProjectManagement;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.designer.languages.common.codegen.ui.ChooseGenerator;
import org.eclipse.papyrus.designer.languages.common.extensionpoints.ILangCodegen;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.GeneratorHint;
import org.eclipse.papyrus.designer.transformation.core.transformations.ExecuteTransformationChain;
import org.eclipse.papyrus.designer.transformation.ui.Messages;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Package;

/**
 * handler for instantiating a deployment plan
 */
public class ExecuteTransformationChainHandler extends CmdHandler {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();
		// only one model is selected
		if (selectedEObject instanceof Package) {
			selectedCDP = (Package) selectedEObject;

			project = UIProjectManagement.getCurrentProject();

			Job job = new Job(Messages.ExecuteTransformationChainHandler_EXECUTE_CHAIN) {

				@Override
				public IStatus run(IProgressMonitor monitor) {
					boolean hasHint = GenUtils.hasStereotypeTree(selectedCDP, GeneratorHint.class);
					if (!hasHint) {
						ILangCodegen generator = ChooseGenerator.choose(Pattern.compile(".*"), selectedCDP); //$NON-NLS-1$
						if (generator == null) {
							return Status.CANCEL_STATUS;
						}
					}
					// execute the task ...
					new ExecuteTransformationChain(selectedCDP, project).executeTransformation(monitor, 0);
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}

		return null;
	}

	private Package selectedCDP;

	private IProject project;
}

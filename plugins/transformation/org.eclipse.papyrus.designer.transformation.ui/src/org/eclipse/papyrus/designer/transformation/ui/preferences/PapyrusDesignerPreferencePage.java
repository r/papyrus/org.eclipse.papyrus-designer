/*****************************************************************************
 * Copyright (c) 2012 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.ui.preferences;

import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.papyrus.designer.transformation.base.Activator;
import org.eclipse.papyrus.designer.transformation.base.preferences.TransfoBasePreferenceConstants;
import org.eclipse.papyrus.designer.transformation.ui.Messages;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.preferences.ScopedPreferenceStore;

/**
 * This class represents the EC3M preference page
 * <p>
 * This page is used to modify preferences only. They are stored in the preference store that belongs to the main plug-in class. That way, preferences can be accessed directly via the preference store.
 */

public class PapyrusDesignerPreferencePage
		extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	public PapyrusDesignerPreferencePage() {
		super(GRID);
		IPreferenceStore store = new ScopedPreferenceStore(InstanceScope.INSTANCE, Activator.PLUGIN_ID);
		setPreferenceStore(store);
		setDescription("Papyrus designer options (for component-based design)"); //$NON-NLS-1$
	}

	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	@Override
	public void createFieldEditors() {
		addField(new BooleanFieldEditor(
				TransfoBasePreferenceConstants.P_SHOW_PROV_REQ_AS_PORT_ICONS_KEY,
				Messages.PapyrusDesignerPreferencePage_SHOW_ICONS,
				getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				TransfoBasePreferenceConstants.P_TREAT_NONE_AS_COMPOSITE_KEY,
				Messages.PapyrusDesignerPreferencePage_NONE_AS_COMPOSITE,
				getFieldEditorParent()));

		addField(new BooleanFieldEditor(
				TransfoBasePreferenceConstants.P_ALL_ATTRIBUTES_ARE_CONFIG_ATTRIBUTES_KEY,
				Messages.PapyrusDesignerPreferencePage_ALL_CONFIG_ATTRIBUTES,
				getFieldEditorParent()));

		addField(new StringFieldEditor(
				TransfoBasePreferenceConstants.P_CODE_GEN_PREFIX_KEY,
				Messages.PapyrusDesignerPreferencePage_GENERATION_PREFIX, getFieldEditorParent()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	@Override
	public void init(IWorkbench workbench) {
	}
}

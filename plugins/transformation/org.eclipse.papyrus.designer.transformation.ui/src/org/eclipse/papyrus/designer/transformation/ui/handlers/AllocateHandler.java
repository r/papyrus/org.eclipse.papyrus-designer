/*******************************************************************************
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher (CEA LIST) ansgar.radermacher@cea.fr - Initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.designer.deployment.tools.DepUtils;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.infra.base.RunnableWithResult;
import org.eclipse.papyrus.designer.transformation.ui.Messages;
import org.eclipse.papyrus.designer.transformation.ui.dialogs.AllocationDialog;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Package;

/**
 * Implementation class for ClassAction action
 */
public class AllocateHandler extends CmdHandler {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();
		if (!(selectedEObject instanceof Package)) {
			return null;
		}

		final Package cdp = (Package) selectedEObject;
		final Shell shell = Display.getCurrent().getActiveShell();

		// org.eclipse.papyrus.designer.components.vsl.ParseVSL.test();
		if (DepUtils.getTopLevelInstances(cdp).isEmpty()) {
			MessageDialog.openInformation(shell, "Error", //$NON-NLS-1$
					Messages.AllocateHandler_NO_INSTANCES);
			return null;
		}

		// 1. select possible connectors according to port types
		// (only show compatible connectors check-box?)
		// 2. select implementation group according to connector type

		// container dialog: either extension, rule or interceptor
		// howto select? which? (and howto add/remove?) - Std - dialog is good?
		CommandSupport.exec(cdp, Messages.AllocateHandler_INSTANCE_ALLOCATION, new RunnableWithResult() {

			@Override
			public IStatus run() {
				AllocationDialog allocDialog = new AllocationDialog(shell, cdp);
				allocDialog.setTitle(Messages.AllocateHandler_ALLOCATE_INSTANCES);
				allocDialog.open();
				if (allocDialog.getReturnCode() == IDialogConstants.OK_ID) {
					return Status.OK_STATUS;
				} else {
					return Status.CANCEL_STATUS;
				}
			}
		});

		return null;
	}
}

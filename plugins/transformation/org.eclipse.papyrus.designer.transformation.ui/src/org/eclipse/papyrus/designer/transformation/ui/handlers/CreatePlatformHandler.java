/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.papyrus.designer.deployment.tools.DepCreation;
import org.eclipse.papyrus.designer.deployment.tools.DeployConstants;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationRTException;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Package;

public class CreatePlatformHandler extends CmdHandler {

	// only member, since used within new runnables
	private Package platform;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();
		if (!(selectedEObject instanceof Class)) {
			return null;
		}
		final Class selectedComposite = (Class) selectedEObject;

		CommandSupport.exec(selectedComposite, "Create platform model", new Runnable() { //$NON-NLS-1$

			@Override
			public void run() {
				// execute with transaction support
				platform = ElementUtils.getRoot(selectedComposite, DeployConstants.depPlanFolderHw);
			}
		});

		final String newPlatform = selectedComposite.getName() + DeployConstants.DepPlanPostfixHw;

		try {
			if (platform.getMember(newPlatform) != null) {
				Shell shell = Display.getCurrent().getActiveShell();
				MessageDialog.openInformation(shell, "Error", //$NON-NLS-1$
						"Platform definition \"" + newPlatform + "\" exists already"); //$NON-NLS-1$ //$NON-NLS-2$
			} else {
				CommandSupport.exec(selectedComposite, "Create platform definition", new Runnable() { //$NON-NLS-1$

					@Override
					public void run() {
						Package platformPkg = platform.createNestedPackage(newPlatform);
						try {
							DepCreation.createPlatformInstances(platformPkg, selectedComposite, null);
						} catch (TransformationException e) {
							throw new TransformationRTException(e.getMessage());
						}
					}
				});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}

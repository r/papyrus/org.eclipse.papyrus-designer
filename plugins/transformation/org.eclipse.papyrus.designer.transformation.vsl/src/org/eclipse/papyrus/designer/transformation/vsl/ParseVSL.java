/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.transformation.vsl;

import org.eclipse.papyrus.MARTE.utils.MarteUtils;
import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.DataSizeUnitKind;
import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.FrequencyUnitKind;
import org.eclipse.papyrus.MARTE_Library.MeasurementUnits.TimeUnitKind;

public class ParseVSL {

	private static final String UNIT = "unit"; //$NON-NLS-1$

	private static final String VALUE = "value"; //$NON-NLS-1$

	public static int getSizeFromVSL(String nfpDatasize) {
		if (nfpDatasize != null) {
			String unit = MarteUtils.getValueFromTuple(nfpDatasize, UNIT);
			String value = MarteUtils.getValueFromTuple(nfpDatasize, VALUE);
			return Float.valueOf(value).intValue() * getMultiplicatorFromSizeUnit(unit);
		}
		return 0;
	}

	// >>> TODO: from here ad-hoc functions to retrieve values via simple parser

	/**
	 * Get the duration in micro-seconds
	 *
	 * @param nfpDuration
	 * @return the duration in micro-seconds
	 */
	public static long getDurationFromVSL(String nfpDuration) {
		if (nfpDuration != null) {
			String unit = MarteUtils.getValueFromTuple(nfpDuration, UNIT);
			String value = MarteUtils.getValueFromTuple(nfpDuration, VALUE);
			if (value == null || unit == null) {
				Activator.log.info(String.format("non VSL value: %s", nfpDuration)); //$NON-NLS-1$
				return 0;
			}
			return Float.valueOf(value).intValue() * getMultiplicatorFromTimeUnit(unit);
		}
		return 0;
	}

	/**
	 * Get the duration in milliseconds
	 * 
	 * @param nfpDuration
	 * @return the duration in milliseconds
	 */
	public static long getMsDurationFromVSL(String nfpDuration) {
		return getDurationFromVSL(nfpDuration) / 1000;
	}


	/**
	 * Get the frequency in HZ
	 *
	 * @param nfpFrequency
	 * @return the frequency in MHz
	 */
	public static long getFrequencyFromVSL(String nfpFrequency) {
		if (nfpFrequency != null) {
			String unit = MarteUtils.getValueFromTuple(nfpFrequency, UNIT);
			String value = MarteUtils.getValueFromTuple(nfpFrequency, VALUE);
			return (int) (Float.valueOf(value) * getMultiplicatorFromFrequencyUnit(unit));
		}
		return 0;
	}

	public static long getPeriodFromArrivalPattern(String arrivalPattern) {
		if (arrivalPattern != null) {
			String period = MarteUtils.getValueFromTuple(arrivalPattern, "period"); //$NON-NLS-1$
			return getDurationFromVSL(period);
		}
		return 0;
	}

	public static int getMultiplicatorFromSizeUnit(String unit) {
		DataSizeUnitKind dsuk = DataSizeUnitKind.get(unit);
		if (dsuk == DataSizeUnitKind.BYTE) {
			return 1;
		} else if (dsuk == DataSizeUnitKind.KB) {
			return 1024;
		} else if (dsuk == DataSizeUnitKind.MB) {
			return 1024 * 1024;
		} else if (dsuk == DataSizeUnitKind.GB) {
			return 1024 * 1024 * 1024;
		} else {
			// do not support bits here.
			throw new RuntimeException(String.format(Messages.ParseVSL_ErrorInExp_SIZE, unit));
		}
	}

	/**
	 * return the time multiplicator based on microseconds unit
	 *
	 * @param unit
	 *            the used unit
	 * @return the time multiplicator based on microseconds unit
	 */
	public static long getMultiplicatorFromTimeUnit(String unit) {
		TimeUnitKind tuk = TimeUnitKind.get(unit);
		if (tuk == TimeUnitKind.US) {
			return 1;
		}
		if (tuk == TimeUnitKind.MS) {
			return 1000;
		} else if (tuk == TimeUnitKind.S) {
			return 1000 * 1000;
		} else if (tuk == TimeUnitKind.MIN) {
			return 60 * 1000 * 1000;
		} else {
			throw new RuntimeException(String.format(Messages.ParseVSL_ErrorInExp_TIME, unit));
		}
	}

	/**
	 * return the multiplicator of the frequency unit
	 * 
	 * @param unit
	 *            the used unit
	 * @return the unit multiplicator (HZ = 1, KHZ = 1000, ...)
	 */
	public static float getMultiplicatorFromFrequencyUnit(String unit) {
		FrequencyUnitKind fuk = FrequencyUnitKind.get(unit);
		if (fuk == FrequencyUnitKind.HZ) {
			return 1;
		} else if (fuk == FrequencyUnitKind.KHZ) {
			return 1000;
		} else if (fuk == FrequencyUnitKind.MHZ) {
			return 1000 * 1000;
		} else if (fuk == FrequencyUnitKind.GHZ) {
			return 1000 * 1000 * 1000;
		} else if (fuk == FrequencyUnitKind.RPM) {
			return 1 / 60;
		} else {
			// do not support bits here.
			throw new RuntimeException(String.format(Messages.ParseVSL_ErrotInExp_FREQ, unit));
		}
	}
}

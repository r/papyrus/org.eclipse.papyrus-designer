/*****************************************************************************
 * Copyright (c) 2016, 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *  Ported from C++ code - Bug 568883
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.designer.transformation.languages.java.library.statemachine

import java.util.List
import org.eclipse.emf.common.util.UniqueEList
import org.eclipse.uml2.uml.Pseudostate
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.Transition

class TransitionGraph {
	public List<State> S = new UniqueEList
	public List<State> L = new UniqueEList
	public List<Pseudostate> P = new UniqueEList<Pseudostate>
	public List<Transition> T = new UniqueEList
}
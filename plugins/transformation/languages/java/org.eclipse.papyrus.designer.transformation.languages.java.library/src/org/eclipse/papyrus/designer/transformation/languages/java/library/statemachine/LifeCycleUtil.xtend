/*******************************************************************************
 * Copyright (c) 2019 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Ansgar Radermacher (CEA LIST) <ansgar.radermacher@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.languages.java.library.statemachine

import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Interface

/**
 * Utility class for life-cycle operations
 * TODO: move this class into a more general transformation context
 */
class LifeCycleUtil {
	public static val LIFE_CYCLE_QNAME = "sysinterfaces.ILifeCycle"

	public static val M_ACTIVATE = "activate"
	public static val M_DEACTIVATE = "deactivate"
	public static val M_CONFIG_COMPLETE = "configuration_complete"

	/**
	 * Return true, if the passed class implements the life-cycle interface
	 * @param clazz
	 * 		the class which to check
	 */
	static def supportsLifeCycle(Class clazz) {
		val lifeCycle = ElementUtils.getQualifiedElementFromRS(clazz, LIFE_CYCLE_QNAME) as Interface
		if (lifeCycle !== null && clazz.implementedInterfaces.contains(lifeCycle)) {
			return true
		}
		return false
	}

	/**
	 * create life-cycle operations, if they do not exist
	 * @param clazz
	 * 		the class for which we want to add operations, if required
	 */
	static def addUnimplemented(Class clazz) {
		for (op : #[M_ACTIVATE, M_DEACTIVATE, M_CONFIG_COMPLETE]) {
			val activateOp = clazz.getOperation(op, null, null)
			if (activateOp === null) {
				clazz.createOwnedOperation(op, null, null)
			}
		}
	}
}

/*****************************************************************************
 * Copyright (c) 2016, 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *  Ported from C++ code - Bug 568883
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.languages.java.library.statemachine

import org.eclipse.uml2.uml.ChangeEvent
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.OpaqueExpression

import static org.eclipse.papyrus.designer.transformation.library.statemachine.SMCodeGeneratorConstants.*

import static extension org.eclipse.papyrus.designer.uml.tools.utils.StateMachineUtils.*

//TODO Change event

class ChangeEventTransformation {
	protected extension CDefinitions cdefs;
	SM2ClassesTransformationCore core
	
	Class superContext
	
	new (SM2ClassesTransformationCore core) {
		this.core = core
		this.superContext = core.superContext
		
		this.cdefs = core.cdefs
	}
	
	def void createChangeEvents() {
		if (core.changeEvents.empty) {
			return
		}
		/* 
		var threads = superContext.createOwnedAttribute(THREADS_CHANGE_EVENT, ptTypes.pthread)
		StereotypeUtil.apply(threads, Array)
		UMLUtil.getStereotypeApplication(threads, Array).definition = '''[«core.changeEvents.size»]'''
		
		var threadStructs = superContext.createOwnedAttribute(THREAD_STRUCTS_FOR_CHANGEEVENT, core.concurrency.threadStructType)
		StereotypeUtil.apply(threadStructs, Array)
		UMLUtil.getStereotypeApplication(threadStructs, Array).definition = '''[«core.changeEvents.size»]''' 
		*/
		var WAIT_TIME = "10" //ms
		
		//create changeEvent function sleep during an amount of time and check values of boolean expressions
		var changeEventOp = superContext.createOwnedOperation(CHANGE_EVENT_LISTEN_FUNCTION, null, null)
		changeEventOp.createOwnedParameter("id", core.intType)
		core.createOpaqueBehavior(superContext, changeEventOp, '''
		struct timeval tv;
		struct timespec ts;
		bool oldValue;
		bool newValue;
		bool isFirstTime = true;
		bool periodic = «WAIT_TIME»; //ms
		while(true) {
			switch(id) {
				«FOR ch:core.changeEvents»
					case «ch.eventID»:
						newValue = «ch.changeEventExpression»;
						break;
				«ENDFOR»
			}
			if (isFirstTime) {
				oldValue = newValue;
				isFirstTime = false;
			} else {
				#ifdef WIN32
					Sleep(periodic);
				#elif _POSIX_C_SOURCE >= 199309L
				    struct timespec ts;
				    ts.tv_sec = periodic / 1000;
				    ts.tv_nsec = (periodic % 1000) * 1000000;
				    nanosleep(&ts, NULL);
				#else
				    usleep(periodic * 1000);
				#endif
			}
			if (oldValue != newValue) {
				//the expression is changed, push an event to the queue
				«EVENT_QUEUE».push(statemachine::PRIORITY_2, NULL, id, statemachine::CHANGE_EVENT, 0);
			}
			oldValue = newValue;
		}''')
	}
	
	def String getChangeEventExpression(ChangeEvent e) {
		if (e.changeExpression === null || !(e.changeExpression instanceof OpaqueExpression)) {
			return "false"
		} else {
			return (e.changeExpression as OpaqueExpression).bodies.head
		}
	}
}

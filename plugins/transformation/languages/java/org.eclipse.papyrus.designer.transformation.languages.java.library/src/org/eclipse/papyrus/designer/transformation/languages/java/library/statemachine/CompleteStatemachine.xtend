/** 
 * Copyright (c) 2016 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * SPDX-License-Identifier: EPL-2.0
 * Contributors:
 * Ansgar Radermacher  ansgar.radermacher@cea.fr
 */
package org.eclipse.papyrus.designer.transformation.languages.java.library.statemachine

import org.eclipse.emf.common.util.URI
import org.eclipse.papyrus.designer.transformation.base.utils.ModelManagement
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoElem
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.NamedElement
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.Type
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils
import org.eclipse.papyrus.designer.transformation.library.statemachine.SMCodeGeneratorConstants

class CompleteStatemachine implements IM2MTrafoElem {
	public static final String THREAD_FUNCTIONS = "ThreadFunctions" // $NON-NLS-1$
	public static final String SYSTEM_STATE_ENUM_T = "SystemStateEnum_t" // $NON-NLS-1$
	public static final String STRUCT_FOR_THREAD = "StructForThread_t" // $NON-NLS-1$
    public static final String STATE_T = "State_t" // $NON-NLS-1$
    public static final String EVENT_T = "Event_t" // $NON-NLS-1$
	public static final URI SM_JAVA_URI = URI.createURI("pathmap://DML_TRAFOS_JAVA/statemachineJava.uml") // $NON-NLS-1$
	
	// $NON-NLS-1$
	override void transformElement(M2MTrafo trafo, Element element) {
		if (element instanceof Class) {
			var Class tmClass = (element as Class)
			if (tmClass.getClassifierBehavior() instanceof StateMachine) {
				var StateMachine sm = (tmClass.getClassifierBehavior() as StateMachine)
				var LazyCopier copier = TransformationContext.current.copier
				var SM2ClassesTransformationCore trafoCore = new SM2ClassesTransformationCore(copier, sm, tmClass)

				val smModel = ElementUtils.loadPackage(SM_JAVA_URI, ModelManagement.resourceSet);
				var NamedElement stateEnum = smModel.getPackagedElement(SYSTEM_STATE_ENUM_T)
				var NamedElement threadFuncEnum = smModel.getPackagedElement(THREAD_FUNCTIONS)
				var Type structForThread = (smModel.getPackagedElement(STRUCT_FOR_THREAD) as Type)
				var Type state_t = (smModel.getPackagedElement(STATE_T) as Type)
				var Type event_t = (smModel.getPackagedElement(EVENT_T) as Type)
				trafoCore.setThreadStructType(structForThread)
				trafoCore.setState_t(state_t)
				trafoCore.setEvent_t(event_t)
				trafoCore.setSmPack(smModel)
				tmClass.createOwnedAttribute(SMCodeGeneratorConstants.SYSTEM_STATE_ATTR, (stateEnum as Type))
				tmClass.createDependency(threadFuncEnum)
				trafoCore.threadFuncEnum = threadFuncEnum as Type
				trafoCore.transform()
			}
		}
	}
}

/*****************************************************************************
 * Copyright (c) 2016, 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *  Ported from C++ code - Bug 568883
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.designer.transformation.languages.java.library.statemachine

import org.eclipse.uml2.uml.Class

import static org.eclipse.papyrus.designer.transformation.library.statemachine.SMCodeGeneratorConstants.*

/**
 * Java variant of time-event transformation
 */
 class TimeEventTransformation {
	protected extension CDefinitions cdefs;
	SM2ClassesTransformationCore core
	
	Class superContext

	new (SM2ClassesTransformationCore core) {
		this.core = core
		this.superContext = core.superContext
		
		this.cdefs = core.cdefs
	}
	
	def void createTimeEvents() {
		if (core.timeEvents.empty) {
			return
		}
		
		//TODO change the Time Event threads
		
		
		/*
		var timeEventFlags = superContext.createOwnedAttribute(FLAGS_TIME_EVENT, core.boolType)
		StereotypeUtil.apply(timeEventFlags, Array)
		UMLUtil.getStereotypeApplication(timeEventFlags, Array).definition = '''[«core.timeEvents.size»]''' */
		
		// create timeEvent function sleep during an amount of time
	    var timeEventOp = superContext.createOwnedOperation(TIME_EVENT_LISTEN_FUNCTION, null, null)
		timeEventOp.createOwnedParameter("id", core.intType)
		timeEventOp.createOwnedParameter("duration", core.intType)
		core.createOpaqueBehavior(superContext, timeEventOp, '''
			while (!Thread.currentThread().isInterrupted()) {
				long startTime = System.currentTimeMillis();
				synchronized (this) {
					try {
						wait(duration);
					} catch (InterruptedException e) {
						// interruption, exit loop
						break;
					}
				}
				long endTime = System.currentTimeMillis();
				long timedWaitResult = endTime - startTime;

				boolean commitEvent = false;
				if (timedWaitResult >= duration) {
					// timeout
					commitEvent = true;
				}
				timeEventFlags[id] = false;
				if (commitEvent) {
					Event_t event = new Event_t(EventPriority_t.PRIORITY_2, id, EventType_t.TIME_EVENT, id, 0);
					eventQueue.add(event);
				}
			}
		''')
	}
}

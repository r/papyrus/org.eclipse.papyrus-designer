#ifndef PKG_COMPLEXSM_CLASSES_EVENTS
#define PKG_COMPLEXSM_CLASSES_EVENTS

/************************************************************
 Pkg_events package header
 ************************************************************/

#include "ComplexSM/classes/Pkg_classes.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace ComplexSM {
namespace classes {
namespace events {

// Types defined within the package
}// of namespace events
} // of namespace classes
} // of namespace ComplexSM

/************************************************************
 End of Pkg_events package header
 ************************************************************/

#endif

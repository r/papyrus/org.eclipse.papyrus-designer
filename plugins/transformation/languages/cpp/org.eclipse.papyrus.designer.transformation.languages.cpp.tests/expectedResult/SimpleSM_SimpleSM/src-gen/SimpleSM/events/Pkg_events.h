#ifndef PKG_SIMPLESM_EVENTS
#define PKG_SIMPLESM_EVENTS

/************************************************************
 Pkg_events package header
 ************************************************************/

#include "SimpleSM/Pkg_SimpleSM.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace SimpleSM {
namespace events {

// Types defined within the package
}// of namespace events
} // of namespace SimpleSM

/************************************************************
 End of Pkg_events package header
 ************************************************************/

#endif

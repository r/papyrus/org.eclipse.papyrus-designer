/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.languages.cpp.library.statemachine

import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.Class
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils

class PThreadTypes {

	val static PTHREAD_COND_T = "pthread::pthread_cond_t"
	val static PTHREAD_MUTEX_T = "pthread::pthread_mutex_t"
	val static PTHREAD_T = "pthread::pthread_t"

	new (Class contextClass) {
		this.contextClass = contextClass;
	}
	Class contextClass
	
	def getPthreadCond() {
		ElementUtils.getQualifiedElementFromRS(contextClass, PTHREAD_COND_T) as Type
	}

	def getPthreadMutex() {
		ElementUtils.getQualifiedElementFromRS(contextClass, PTHREAD_MUTEX_T) as Type
	}

	def getPthread() {
		ElementUtils.getQualifiedElementFromRS(contextClass, PTHREAD_T) as Type
	}
}

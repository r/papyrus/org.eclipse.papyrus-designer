/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Van Cam Pham        <VanCam.PHAM@cea.fr>
 *
 *****************************************************************************/
 
package org.eclipse.papyrus.designer.transformation.languages.cpp.library.statemachine

class SMCppCodeGeneratorConstants {
	public static final String DEFAULT_FLAGS_TIME_EVENT = "defaultTimeEventFlags"

	public static final String STRUCT_FOR_THREAD = "statemachine::StructForThread_t"

	public static final String THREAD_FUNC_TIMEEVENT_TYPE = "statemachine::ThreadFunctions::TF_TIME_EVENT"
	public static final String THREAD_FUNC_CHANGEEVENT_TYPE = "statemachine::ThreadFunctions::TF_CHANGE_EVENT"
	public static final String THREAD_FUNC_DOACTIVITY_TYPE = "statemachine::ThreadFunctions::TF_DO_ACTIVITY"
	public static final String THREAD_FUNC_ENTER_REGION_TYPE = "statemachine::ThreadFunctions::TF_ENTER_REGION"
	public static final String THREAD_FUNC_EXIT_REGION_TYPE = "statemachine::ThreadFunctions::TF_EXIT_REGION"
	public static final String THREAD_FUNC_TRANSITION_TYPE = "statemachine::ThreadFunctions::TF_TRANSITION"
	public static final String THREAD_FUNC_STATE_MACHINE_TYPE = "statemachine::ThreadFunctions::TF_STATE_MACHINE_TYPE"
}

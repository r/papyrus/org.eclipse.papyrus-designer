/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.languages.cpp.library.statemachine

import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Include
import org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.Transition
import org.eclipse.uml2.uml.util.UMLUtil
import static extension org.eclipse.papyrus.designer.transformation.languages.cpp.library.IncludeUtils.*
import org.eclipse.papyrus.designer.monitoring.prefs.MonitoringPreferencesUtil

class MonitoringTransformation {
	public static final String MONITOR_SOCKET_NAME = "monitorSocket"
	public static final String MONITOR_ADDRESS_NAME = "monitorAddress"

	SM2ClassesTransformationCore core
	Class superContext
	boolean isMonitored;

	new(SM2ClassesTransformationCore core) {
		this.core = core
		this.superContext = core.superContext
		isMonitored = MonitoringPreferencesUtil.enabled;
	}

	def void appendInclude() {
		if (isMonitored) {
			val include = UMLUtil.getStereotypeApplication(superContext, Include)
			if (include !== null) {
				val header = include.header
				if (!header.contains('''#include <netinet/in.h>''')) {
					superContext.appendIncludeHeader('''#include <netinet/in.h>''')
				}
			}

			if (include !== null) {
				val body = UMLUtil.getStereotypeApplication(superContext, Include).header
				if (!body.contains('''#include <unistd.h>''')) {
					superContext.appendIncludeBody('''#include <unistd.h>''')
				}
				if (!body.contains('''#include <sys/socket.h>''')) {
					superContext.appendIncludeBody('''#include <sys/socket.h>''')
				}
				if (!body.contains('''#include <arpa/inet.h>''')) {
					superContext.appendIncludeBody('''#include <arpa/inet.h>''')
				}
			}

		}
	}

	def createMonitorAttributes() {
		if (isMonitored) {
			superContext.createOwnedAttribute(MONITOR_SOCKET_NAME, core.intType);
			superContext.createOwnedAttribute(MONITOR_ADDRESS_NAME, core.sockAddrInType);
		}
	}

	def createDestructor() {
		if (isMonitored) {
			var sourceDestructors = superContext.ownedOperations.filter [
				StereotypeUtil.isApplied(it, "StandardProfile::Destroy") && it.name == superContext.name
			]
			var targetDestructors = sourceDestructors.map[it]
			if (targetDestructors.empty) {
				// create destructor
				var destructor = superContext.createOwnedOperation(superContext.name, null, null)
				StereotypeUtil.apply(destructor, "StandardProfile::Destroy")
				core.createOpaqueBehavior(superContext, destructor, '''close(«MONITOR_SOCKET_NAME»);''')
			} else {
				targetDestructors.forEach [
					var opaque = it.methods.head as OpaqueBehavior
					if (opaque !== null) {
						var body = opaque.bodies.get(0)
						core.createOpaqueBehavior(superContext, it, '''
						«body»
						close(«MONITOR_SOCKET_NAME»);''')
					} else {
						core.createOpaqueBehavior(superContext, it, '''close(«MONITOR_SOCKET_NAME»);''')
					}
				]
			}
		}
	}

	def createConstructor() {
		if (isMonitored) {
			var sourceContructors = superContext.ownedOperations.filter [
				StereotypeUtil.isApplied(it, "StandardProfile::Create") && it.name == superContext.name
			]
			var targetContructors = sourceContructors.map[it]
			if (targetContructors.empty) {
				// create constructor
				var constructor = superContext.createOwnedOperation(superContext.name, null, null)
				StereotypeUtil.apply(constructor, "StandardProfile::Create")
				core.createOpaqueBehavior(superContext, constructor, '''
				«MONITOR_SOCKET_NAME» = socket(AF_INET, SOCK_DGRAM, 17);
				«MONITOR_ADDRESS_NAME».sin_addr.s_addr = inet_addr("«MonitoringPreferencesUtil.ipAddress»");
				«MONITOR_ADDRESS_NAME».sin_family = AF_INET;
				«MONITOR_ADDRESS_NAME».sin_port = htons(«MonitoringPreferencesUtil.port»);''')
			} else {
				targetContructors.forEach [
					var opaque = it.methods.head as OpaqueBehavior
					if (opaque !== null) {
						var body = opaque.bodies.get(0)
						core.createOpaqueBehavior(superContext, it, '''
						«MONITOR_SOCKET_NAME» = socket(AF_INET, SOCK_DGRAM, 17);
						«MONITOR_ADDRESS_NAME».sin_addr.s_addr = inet_addr("«MonitoringPreferencesUtil.ipAddress»");
						«MONITOR_ADDRESS_NAME».sin_family = AF_INET;
						«MONITOR_ADDRESS_NAME».sin_port = htons(«MonitoringPreferencesUtil.port»);
						«body»''')
					} else {
						core.createOpaqueBehavior(superContext, it, '''
						«MONITOR_SOCKET_NAME» = socket(AF_INET, SOCK_DGRAM, 17);
						«MONITOR_ADDRESS_NAME».sin_addr.s_addr = inet_addr("«MonitoringPreferencesUtil.ipAddress»");
						«MONITOR_ADDRESS_NAME».sin_family = AF_INET;
						«MONITOR_ADDRESS_NAME».sin_port = htons(«MonitoringPreferencesUtil.port»);''')
					}
				]
			}
		}
	}

	def generateTransitionCode(Transition t) {
		if (isMonitored) {
			var monitorThis = true
			val stateMachine = t.containingStateMachine
			if (stateMachine !== null) {
				val monitoredStereotype = UMLUtil.getStereotypeApplication(stateMachine, Monitored)
				if (monitoredStereotype !== null) {
					monitorThis = monitoredStereotype.generateMonitoringCode
					if (monitorThis) {
						monitorThis = !monitoredStereotype.generateExclude.contains(t)
						if (monitorThis && !monitoredStereotype.generateExclusivelyInclude.empty) {
							monitorThis = monitoredStereotype.generateExclusivelyInclude.contains(t)
						}
					}
				}
			}

			if (monitorThis) {
				// TODO get separator "|" from preferences or static final String in another plugin
				if (t.qualifiedName !== null && !t.qualifiedName.empty) {
					return '''
						if («MONITOR_SOCKET_NAME» != -1) {
							sendto(«MONITOR_SOCKET_NAME», "TRANSITION|«t.qualifiedName»", sizeof("TRANSITION|«t.qualifiedName»"), 0, reinterpret_cast<const sockaddr*>(&«MONITOR_ADDRESS_NAME»), sizeof(«MONITOR_ADDRESS_NAME»));
						}
					'''
				}
			}
		}

		return ""
	}
}

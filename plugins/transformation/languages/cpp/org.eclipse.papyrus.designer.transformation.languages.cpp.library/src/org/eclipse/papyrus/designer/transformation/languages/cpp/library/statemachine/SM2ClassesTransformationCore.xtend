/*******************************************************************************
 * Copyright (c) 2017, 2019 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Ansgar Radermacher - Initial API and implementation
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - Bug 530251
 *     Yoann Farre (CIL4Sys) - Bug 550446
 *     Yoann Farre (CIL4Sys) - Bug 576960
 *******************************************************************************/

package org.eclipse.papyrus.designer.transformation.languages.cpp.library.statemachine

import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap
import java.util.List
import java.util.Map
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.papyrus.designer.languages.common.base.StdUriConstants
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.NoCodeGen
import org.eclipse.papyrus.designer.languages.common.profile.CommonProfileResource
import org.eclipse.papyrus.designer.languages.cpp.codegen.utils.CppGenUtils
import org.eclipse.papyrus.designer.languages.cpp.library.CppUriConstants
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Array
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.EnumStyle
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Include
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Typedef
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Virtual
import org.eclipse.papyrus.designer.transformation.base.utils.OperationSync
import org.eclipse.papyrus.designer.transformation.core.transformations.LazyCopier
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext
import org.eclipse.papyrus.designer.transformation.profile.Transformation.DerivedElement
import org.eclipse.papyrus.designer.transformation.vsl.ParseVSL
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil
import org.eclipse.uml2.uml.AnyReceiveEvent
import org.eclipse.uml2.uml.Behavior
import org.eclipse.uml2.uml.CallEvent
import org.eclipse.uml2.uml.ChangeEvent
import org.eclipse.uml2.uml.Class
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.Event
import org.eclipse.uml2.uml.FinalState
import org.eclipse.uml2.uml.LiteralBoolean
import org.eclipse.uml2.uml.OpaqueBehavior
import org.eclipse.uml2.uml.OpaqueExpression
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.Profile
import org.eclipse.uml2.uml.Property
import org.eclipse.uml2.uml.Pseudostate
import org.eclipse.uml2.uml.PseudostateKind
import org.eclipse.uml2.uml.Region
import org.eclipse.uml2.uml.SignalEvent
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.StateMachine
import org.eclipse.uml2.uml.TimeEvent
import org.eclipse.uml2.uml.Transition
import org.eclipse.uml2.uml.Trigger
import org.eclipse.uml2.uml.Type
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.Vertex
import org.eclipse.uml2.uml.profile.standard.Create
import org.eclipse.uml2.uml.util.UMLUtil

import static org.eclipse.papyrus.designer.transformation.languages.cpp.library.statemachine.SMCppCodeGeneratorConstants.*
import static org.eclipse.papyrus.designer.transformation.library.statemachine.SMCodeGeneratorConstants.*

import static extension org.eclipse.papyrus.designer.transformation.languages.cpp.library.IncludeUtils.*
import static extension org.eclipse.papyrus.designer.transformation.library.statemachine.SMCommon.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.BehaviorUtils.appendBody
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StateMachineUtils.*
import static extension org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil.*
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.External

class SM2ClassesTransformationCore {
	protected extension CDefinitions cdefs;
	public int MAX_NUMBER_ORTHOGONAL_REGIONS = 1
	StateMachine sm
	public Type boolType
	public Type threadFuncEnum;
	public Class superContext
	List<Transition> transitions = new ArrayList
	List<OpaqueBehavior> actions = new ArrayList
	public Region topRegion
	public List<State> states = new ArrayList
	List<Vertex> vertexes = new ArrayList
	public LazyCopier copier
	String langID = "C++"
	Type stateStruct

	public boolean generateActionLog = false
	boolean createDoActivity = true
	boolean useThreadCpp11 = true
	Type threadCpp11
	public Type voidType
	Enumeration stateIdEnum
	public Type intType
	public Type charType
	Enumeration eventIdEnum
	public Type fptr
	public PThreadTypes ptTypes
	public Type sockAddrInType;

	public List<TimeEvent> timeEvents = new ArrayList
	public List<ChangeEvent> changeEvents = new ArrayList
	public List<CallEvent> callEvents = new ArrayList
	List<SignalEvent> signalEvents = new ArrayList
	List<AnyReceiveEvent> anyEvents = new ArrayList
	List<Pseudostate> junctions = new ArrayList
	public ConcurrencyGenerator concurrency
	public EventTransformation eventTransform
	public PseudostateGenerator pseudostateGenerator
	public MonitoringTransformation monitoringTransformation;

	public static String smLibraryUri = "platform:/resource/org.eclipse.papyrus.designer.codegen.statemachine/models/SmLibrary.uml"
	public List<Behavior> doActivityList = new ArrayList
	public Package smPack
	List<Region> regions = new ArrayList
	public List<Transition> parallelTransitions = new ArrayList
	public Map<State, List<TimeEvent>> states2TimeEvents = new HashMap
	Type threadStructType

	new(LazyCopier copier, StateMachine sm, Class tmClass) {
		this.copier = copier
		this.superContext = tmClass

		// perClassPackage = copier.target.createNestedPackage("PerClass_" + mContainerClass.name)
		// perClassPackage = copier.target
		this.sm = sm

		val ResourceSet resourceSet = targetPacket.eResource.resourceSet
		boolType = getPrimitiveType("bool", resourceSet)
		voidType = getPrimitiveType("void", resourceSet)
		intType = getPrimitiveType("int", resourceSet)
		charType = getPrimitiveType("char", resourceSet)
		sockAddrInType = getSocketType("sockaddr_in", resourceSet)
		ptTypes = new PThreadTypes(superContext)
		this.cdefs = new CDefinitions(superContext)
	}

	def setSmPack(Package smPack) {
		this.smPack = smPack
	}

	def getThreadStructType() {
		return threadStructType
	}

	def setThreadStructType(Type threadStructType) {
		this.threadStructType = threadStructType;
	}

	/*
	 * @since 0.7.7_2018
	 */
	def getSocketType(String name, ResourceSet resourceSet) {
		val Package socketLibrary = ElementUtils.loadPackage(CppUriConstants.SOCKET_LIB_URI, resourceSet)
		val Element element = socketLibrary.getPackagedElement(name)
		if (element instanceof Type) {
			return element
		}
		return null
	}

	def getRoot() {
		TransformationContext.current.copier.source
	}

	def getPrimitiveType(String name, ResourceSet resourceSet) {
		val Package ansiCLibrary = ElementUtils.loadPackage(CppUriConstants.ANSIC_LIB_URI, resourceSet)
		val Element element = ansiCLibrary.getPackagedElement(name)
		if (element instanceof Type) {
			return element
		}
		return null
	}

	def getTargetPacket() {
		copier.target;
	}

	def getExternalPackage(Package parentPack) {
		if (parentPack.getNestedPackage("external") === null) {
			var createdPack = parentPack.createNestedPackage("external")
			createdPack.apply(NoCodeGen)
		}
		return parentPack.getNestedPackage("external")
	}

	// each state class has a super context and ancestor context
	def transform() {
		val targetPack = getTargetPacket;
		// copier = new SM2ClassCopier(mContainerClass.model, targetPack, false, true, mContainerClass, superContext, sm)
		val ResourceSet resourceSet = targetPack.eResource.resourceSet

		val Package stdProfile = ElementUtils.loadPackage(StdUriConstants.UML_STD_PROFILE_URI, resourceSet)
		if (stdProfile instanceof Profile) {
			PackageUtil.applyProfile(targetPack, stdProfile as Profile, true)
		}
		val Package cgProfile = ElementUtils.loadPackage(CommonProfileResource.PROFILE_PATH_URI, resourceSet)
		if (cgProfile instanceof Profile) {
			PackageUtil.applyProfile(targetPack, cgProfile as Profile, true)
		}

		if (useThreadCpp11) {
			var externalPackage = getExternalPackage(targetPack)
			threadCpp11 = externalPackage.createOwnedType("std::thread", UMLPackage.Literals.DATA_TYPE)
			threadCpp11.apply(External)
		}
		topRegion = sm.regions.head
		concurrency = new ConcurrencyGenerator(this)
		eventTransform = new EventTransformation(this)
		pseudostateGenerator = new PseudostateGenerator(this)
		monitoringTransformation = new MonitoringTransformation(this);

		var eventMap = topRegion.allEvents
		eventMap.forEach [ e, k |
			if (e instanceof TimeEvent) {
				timeEvents.add(e)
			} else if (e instanceof CallEvent) {
				callEvents.add(e)
			} else if (e instanceof SignalEvent) {
				signalEvents.add(e)
			} else if (e instanceof ChangeEvent) {
				changeEvents.add(e)
			} else if (e instanceof AnyReceiveEvent) {
				anyEvents.add(e)
			}
		]
		// create state-id enumg
		eventIdEnum = superContext.createNestedClassifier(EVENT_ID, UMLPackage.Literals.ENUMERATION) as Enumeration
		// don't use class enumerations (rely on conversion to int)
		val eventIdEnumStyle = eventIdEnum.applyApp(EnumStyle)
		eventIdEnumStyle.classEnum = false;

		timeEvents.forEach [
			eventIdEnum.createOwnedLiteral(it.eventID)
		]
		changeEvents.forEach [
			eventIdEnum.createOwnedLiteral(it.eventID)
		]
		appendIncludeHeader(superContext, '''
			#define «TIME_EVENT_LOWER_BOUND» (0)
			#define «CHANGE_EVENT_LOWER_BOUND» («timeEvents.size»)
			#define «TE_INDEX»(id) (id - «TIME_EVENT_LOWER_BOUND»)
			#define «CHE_INDEX»(id) (id - «CHANGE_EVENT_LOWER_BOUND»)
		''')

		// SM Monitoring
		monitoringTransformation.appendInclude

		signalEvents.forEach [
			eventIdEnum.createOwnedLiteral(it.eventID)
		]
		callEvents.forEach [
			eventIdEnum.createOwnedLiteral(it.eventID)
		]
		anyEvents.forEach [
			eventIdEnum.createOwnedLiteral(it.eventID)
		]

		eventIdEnum.createOwnedLiteral(COMPLETION_EVENT.toUpperCase + "_ID")

		topRegion.allActionsTransitionsStates
		states.filter[it.orthogonal].forEach [
			if (it.regions.size > MAX_NUMBER_ORTHOGONAL_REGIONS) {
				MAX_NUMBER_ORTHOGONAL_REGIONS = it.regions.size
			}
		]

		states.forEach [
			val triggers = new ArrayList<Trigger>
			it.outgoings.forEach [
				triggers.addAll(it.triggers)
			]
			val events = triggers.map[it.event]
			val timeEvents = events.filter(TimeEvent).toList
			states2TimeEvents.put(it, timeEvents)
		]

		vertexes.filter(Pseudostate).forEach [
			if (it.kind == PseudostateKind.JUNCTION_LITERAL) {
				junctions.add(it)
			}
		]

		// for each junction, create a class variable
		junctions.forEach [
			superContext.createOwnedAttribute(it.name, intType)
		]

		// create state ID enumeration
		// val stateIdEnum = targetPack.createOwnedEnumeration("StateIDEnum") as Enumeration
		stateIdEnum = superContext.createNestedClassifier("StateIDEnum", UMLPackage.Literals.ENUMERATION) as Enumeration
		// don't use class enumerations (rely on conversion to unsigned int)
		val stateIdEnumStyle = stateIdEnum.applyApp(EnumStyle)
		stateIdEnumStyle.classEnum = false;

		// SM Monitoring
		monitoringTransformation.createMonitorAttributes

		// create state struct
		createStateStruct

		var stateArrayAttr = superContext.createOwnedAttribute(STATE_ARRAY_ATTRIBUTE, stateStruct)
		stateArrayAttr.apply(Array)
		// StereotypeUtil.apply(stateArrayAttr, Ptr)
		UMLUtil.getStereotypeApplication(stateArrayAttr, Array).definition = '''[«states.size»]'''
		// create root active state ID
		superContext.createOwnedAttribute(ACTIVE_ROOT_STATE_ID, stateIdEnum)

		createRegionMethods

		val startCode = '''startBehavior();'''
		if (LifeCycleUtil.supportsLifeCycle(superContext)) {
			// class implements life-cycle interface => add "startBehavior" call to activation function
			LifeCycleUtil.addUnimplemented(superContext)
			var activateOp = superContext.getOperation(LifeCycleUtil.M_ACTIVATE, null, null)
			activateOp.appendBody(startCode)
		} else {
			// add startBehavior to all constructor
			val constructors = superContext.ownedOperations.filter [
				it.isApplied(Create) && it.name == superContext.name
			]
			if (constructors.empty) {
				// create constructor
				var ctor = superContext.createOwnedOperation(superContext.name, null, null)
				ctor.apply(Create)
				superContext.createOpaqueBehavior(ctor, '''startBehavior();''')
			} else {
				constructors.forEach [
					it.appendBody(startCode)
				]
			}
		}

		monitoringTransformation.createConstructor
		monitoringTransformation.createDestructor

		var startBehavior = superContext.createOwnedOperation("startBehavior", null, null)
		superContext.createOpaqueBehavior(startBehavior, '''
			«SYSTEM_STATE_ATTR» = statemachine::SystemStateEnum_t::IDLE;
			«FOR s : states»
				«IF s.entry.isBehaviorExist»
					«STATE_ARRAY_ATTRIBUTE»[«s.name.toUpperCase»_ID].«ENTRY_NAME» = &«superContext.name»::«s.name + "_" + ENTRY_NAME»;
				«ENDIF»
				«IF s.exit.isBehaviorExist»
					«STATE_ARRAY_ATTRIBUTE»[«s.name.toUpperCase»_ID].«EXIT_NAME» = &«superContext.name»::«s.name + "_" + EXIT_NAME»;
				«ENDIF»
				«IF s.doActivity.isBehaviorExist»
					«STATE_ARRAY_ATTRIBUTE»[«s.name.toUpperCase»_ID].«DO_ACTIVITY_NAME» = &«superContext.name»::«s.name + "_" + DO_ACTIVITY_NAME»;
				«ENDIF»
				«DO_ACTIVITY_TABLE»[«s.name.toUpperCase»_ID] =  «STATE_ARRAY_ATTRIBUTE»[«s.name.toUpperCase»_ID].«DO_ACTIVITY_NAME»;
			«ENDFOR»
			
			// initialize all threads, the threads wait until the associated flag is set
			for(int i = 0; i < (int) «STATE_MAX»; i++) {
				if («STATE_ARRAY_ATTRIBUTE»[i].«DO_ACTIVITY_NAME» != &«superContext.name»::doActivity_dft) {
					«THREAD_STRUCTS»[i].id = i;
					«THREAD_STRUCTS»[i].ptr = this;
					«THREAD_STRUCTS»[i].func_type = «THREAD_FUNC_DOACTIVITY_TYPE»;
					«MUTEXES»[i] = PTHREAD_MUTEX_INITIALIZER;
					«CONDITIONS»[i] = PTHREAD_COND_INITIALIZER;
					«FORK_NAME»(&«THREADS»[i], NULL, &«superContext.name»::«THREAD_FUNC_WRAPPER», &«THREAD_STRUCTS»[i]);
				}
			}
			
			«IF timeEvents.size > 0»
				«FOR e:timeEvents»
					«var duration = ParseVSL.getMsDurationFromVSL((e.when.expr as OpaqueExpression).bodies.get(0))»
					«THREAD_STRUCTS_FOR_TIMEEVENT»[«TE_INDEX»(«e.eventID»)].duration = «duration»;
				«ENDFOR»
				for(int i = «TIME_EVENT_LOWER_BOUND»; i < «timeEvents.size»; i++) {
					«THREAD_STRUCTS_FOR_TIMEEVENT»[«TE_INDEX»(i)].id = i;
					«THREAD_STRUCTS_FOR_TIMEEVENT»[«TE_INDEX»(i)].ptr = this;
					«THREAD_STRUCTS_FOR_TIMEEVENT»[«TE_INDEX»(i)].func_type = «THREAD_FUNC_TIMEEVENT_TYPE»;
					«MUTEXES_TIME_EVENT»[«TE_INDEX»(i)] = PTHREAD_MUTEX_INITIALIZER;
					«CONDITIONS_TIME_EVENT»[«TE_INDEX»(i)] = PTHREAD_COND_INITIALIZER;
					«FORK_NAME»(&«THREADS_TIME_EVENT»[«TE_INDEX»(i)], NULL, &«superContext.name»::«THREAD_FUNC_WRAPPER», &«THREAD_STRUCTS_FOR_TIMEEVENT»[«TE_INDEX»(i)]);
					while («FLAGS_TIME_EVENT»[«TE_INDEX»(i)]) {
						usleep(100);
					}
				}
			«ENDIF»
			
			«IF !orthogonalRegions.empty»
				«FOR r:orthogonalRegions»
					«REGION_TABLE»[«r.regionMacroId»] = &«superContext.name»::«r.regionMethodName»;
					«REGION_TABLE_EXIT»[«r.regionMacroId»] = &«superContext.name»::«r.regionMethodExitName»;
				«ENDFOR»
			«ENDIF»
			
			«IF !parallelTransitions.empty»
				«FOR t:parallelTransitions»
					«PARALLEL_TRANSITION_TABLE»[«concurrency.parallelTransitionId(t)»] = &«superContext.name»::«concurrency.parallelTransitionMethodName(t)»;
				«ENDFOR»
			«ENDIF»
			
			«RUN_TO_COMPLETION_MUTEX» = PTHREAD_MUTEX_INITIALIZER;
			«RUN_TO_COMPLETION_COND» = PTHREAD_COND_INITIALIZER;
			
			dispatchStruct = «STRUCT_FOR_THREAD»(this, 0, 0, «THREAD_FUNC_STATE_MACHINE_TYPE», 0);
			«superContext.name»_THREAD_CREATE(dispatchThread, dispatchStruct)
			while (!dispatchFlag) {
				usleep(100);
			}
			
			«IF changeEvents.size > 0»
				// threads for changeEvent
				for(int i = «CHANGE_EVENT_LOWER_BOUND»; i < «CHANGE_EVENT_LOWER_BOUND» + «changeEvents.size»; i++) {
					«THREAD_STRUCTS_FOR_CHANGEEVENT»[«CHE_INDEX»(i)].id = i;
					«THREAD_STRUCTS_FOR_CHANGEEVENT»[«CHE_INDEX»(i)].ptr = this;
					«THREAD_STRUCTS_FOR_CHANGEEVENT»[«CHE_INDEX»(i)].func_type = «THREAD_FUNC_CHANGEEVENT_TYPE»;
					«FORK_NAME»(&«THREADS_CHANGE_EVENT»[«CHE_INDEX»(i)], NULL, &«superContext.name»::«THREAD_FUNC_WRAPPER», &«THREAD_STRUCTS_FOR_CHANGEEVENT»[«CHE_INDEX»(i)]);
				}
			«ENDIF»
			
			//initialze root active state
			//execute initial effect
			«getRegionMethodName(topRegion)»(«topRegion.initialMacroName»);
		''')

		val dispatchFlagAttribute = superContext.createOwnedAttribute("dispatchFlag", boolType)		
		val litBool = dispatchFlagAttribute.createDefaultValue("defaultValue", boolType, UMLPackage.Literals.LITERAL_BOOLEAN) as LiteralBoolean
		litBool.value = false;

		eventMap.forEach [ e, trans |
			eventTransform.createEventMethod(e, trans)
		]

		val autoTrans = new ArrayList<Transition>
		transitions.forEach [
			if (it.source instanceof State && it.triggers.map[it.event].size == 0) {
				autoTrans.add(it)
			}
		]

		eventTransform.createEventMethod(COMPLETION_EVENT, autoTrans)

		// create entry/exit/doactivity of each state
		states.forEach [
			stateIdEnum.createOwnedLiteral(it.name.toUpperCase + "_ID")
			if (it.entry.isBehaviorExist) {
				var entry = superContext.createOwnedOperation(it.name + "_" + ENTRY_NAME, null, null)
				var opaque = superContext.createOpaqueBehavior(entry, (it.entry as OpaqueBehavior).bodies.head)
				opaque.languages.add(langID)
			}
			if (it.exit.isBehaviorExist) {
				var exit = superContext.createOwnedOperation(it.name + "_" + EXIT_NAME, null, null)
				var opaque = superContext.createOpaqueBehavior(exit, (it.exit as OpaqueBehavior).bodies.head)
				opaque.languages.add(langID)
			}
			if (it.doActivity.isBehaviorExist) {
				doActivityList.add(it.doActivity)
				var doActivity = superContext.createOwnedOperation(it.name + "_" + DO_ACTIVITY_NAME, null, null)
				var callCompletionEvent = ''''''
				if (!it.composite) {
					// callCompletionEvent = '''process«COMPLETION_EVENT»();'''
				}
				var opaque = superContext.createOpaqueBehavior(doActivity,
					(it.doActivity as OpaqueBehavior).bodies.head + "\n" + callCompletionEvent)
				opaque.languages.add(langID)
			} else {
//				var doActivity = superContext.createOwnedOperation(it.name + "_" + DO_ACTIVITY_NAME, null, null)
//				var callCompletionEvent = ''''''
//				if (!it.composite) {
//					//callCompletionEvent = '''process«COMPLETION_EVENT»();'''
//				}
//				var opaque = superContext.createOpaqueBehavior(doActivity, callCompletionEvent)
//				opaque.languages.add(langID)
			}
		]
		stateIdEnum.createOwnedLiteral(STATE_MAX)
		concurrency.createThreadBasedParallelism

		superContext.appendIncludeHeader('''
			#define «superContext.name»_THREAD_CREATE(thThread, str) «FORK_NAME»(&thThread, NULL, &«superContext.name»::«THREAD_FUNC_WRAPPER», &str);
			#define «superContext.name.toUpperCase»_GET_CONTROL /*mutex synchronization to protect run-to-completion semantics*/ \
					pthread_mutex_lock(&«RUN_TO_COMPLETION_MUTEX»); \
					while («SYSTEM_STATE_ATTR» != statemachine::SystemStateEnum_t::IDLE) {\
						pthread_cond_wait(&«RUN_TO_COMPLETION_COND», &«RUN_TO_COMPLETION_MUTEX»);\
					}
			#define «superContext.name.toUpperCase»_RELEASE_CONTROL «SYSTEM_STATE_ATTR» = statemachine::SystemStateEnum_t::IDLE; pthread_cond_signal(&«RUN_TO_COMPLETION_COND»); \
						pthread_mutex_unlock(&«RUN_TO_COMPLETION_MUTEX»);
		''')

		var eventClass = smPack.getOwnedType("Event_t")
		var eventQueueClass = smPack.getOwnedType("EventPriorityQueue")
		superContext.createOwnedAttribute(EVENT_QUEUE, eventQueueClass)
		val curentEvent = superContext.createOwnedAttribute("currentEvent", eventClass)
		curentEvent.apply(Ptr);

		var eventDispatch = superContext.createOwnedOperation(EVENT_DISPATCH, null, null)
		superContext.createOpaqueBehavior(eventDispatch, '''
			bool popDeferred = false;
			while(true) {
				//run-to-completion: need to have a mutex here
				currentEvent = «EVENT_QUEUE».pop(popDeferred);
				dispatchFlag = true;
				if (currentEvent != NULL) {
					unsigned int eventID = currentEvent->eventID;
					«superContext.name.toUpperCase»_GET_CONTROL
					switch(eventID) {
						«FOR e : eventMap.keySet.filter[!(it instanceof CallEvent)]»
							case «e.eventID»: {
								«IF e instanceof SignalEvent»
									«IF e.signal !== null»
										«CppGenUtils.cgu(superContext).cppQualifiedName(e.signal)» sig_«e.eventID»;
										if (currentEvent != NULL) {
											memcpy(&sig_«e.eventID», currentEvent->data, sizeof(«CppGenUtils.cgu(superContext).cppQualifiedName(e.signal)»));
											process«e.eventName»(sig_«e.eventID»);
										}
									«ELSE»
										process«e.eventName»();
									«ENDIF»
								«ELSE»
									process«e.eventName»();
								«ENDIF»
								break;
							}
						«ENDFOR»
						case COMPLETIONEVENT_ID: {
							processCompletionEvent();
							break;
							}
					}
					if («SYSTEM_STATE_ATTR» == statemachine::SystemStateEnum_t::EVENT_DEFERRED) {
						«EVENT_QUEUE».saveDeferred(*currentEvent);
					}
					popDeferred = («SYSTEM_STATE_ATTR» != statemachine::SystemStateEnum_t::EVENT_DEFERRED);
					«SYSTEM_STATE_ATTR» = statemachine::SystemStateEnum_t::IDLE;
					«superContext.name.toUpperCase»_RELEASE_CONTROL
				}
				usleep(100);
			}
		''')

		concurrency.createConcurrencyForTransitions
		superContext.createOwnedAttribute("dispatchThread", ptTypes.pthread)
		superContext.createOwnedAttribute("dispatchStruct", concurrency.threadStructType)

		superContext.createOwnedAttribute(RUN_TO_COMPLETION_MUTEX, ptTypes.pthreadMutex)
		superContext.createOwnedAttribute(RUN_TO_COMPLETION_COND, ptTypes.pthreadCond)
		createChangeEvents
	}

	private def createChangeEvents() {
		new ChangeEventTransformation(this).createChangeEvents
	}

	def String generateChangeState(State s) {
		if (s.container == topRegion) {
			return '''«ACTIVE_ROOT_STATE_ID» = «s.name.toUpperCase»_ID;'''
		}
		var rIndex = s.container.state.regions.indexOf(s.container)
		return '''«STATE_ARRAY_ATTRIBUTE»[«s.container.state.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«rIndex»] = «s.name.toUpperCase»_ID;'''
	}

	def String generateExitingSubStates(State parent, boolean exitParent) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«IF parent.orthogonal»
			//exiting concurrent state «parent.name»
			«FOR r:parent.regions»
				«concurrency.generateForkCall(r, false, "0")»
			«ENDFOR»
			«FOR r:parent.regions»
				«concurrency.generateJoinCall(r, false)»
			«ENDFOR»
		«ELSEIF parent.composite»
			«parent.regions.head.regionMethodExitName»();
		«ELSE»
		«ENDIF»
		«IF exitParent»
			«IF parent.doActivity.isBehaviorExist»
				//signal to exit the doActivity of «parent.name»
				«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», false);
			«ENDIF»
			«parent.generateActivateTimeEvent("false")»
			«IF parent.exit.isBehaviorExist»
				//exit action of «parent.name»
				«getFptrCall(pAttr, false, EXIT_NAME)»;
			«ENDIF»
		«ENDIF»'''
	}

	def generateActivateTimeEvent(State s, String trueOrFalse) '''
		«IF states2TimeEvents.get(s) !== null»
			«FOR te : states2TimeEvents.get(s)»
				«SET_FLAG»(«TE_INDEX»(«te.eventID»), «THREAD_FUNC_TIMEEVENT_TYPE», «trueOrFalse»);
			«ENDFOR»
		«ENDIF»
	'''

	@Deprecated
	def String generateExitingSubStatesWithTransition(State parent, boolean exitParent, Transition t) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«IF parent.orthogonal»
			«FOR r:parent.regions»
				«FOR s:r.subvertices.filter(State).filter[!(it instanceof FinalState) && it.composite] SEPARATOR ' else '»
					if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] == &«STATE_ARRAY_ATTRIBUTE»[«s.name.toUpperCase»_ID]) {
						«generateExitingSubStates(s, true)»
					}
				«ENDFOR»
				if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] != NULL) {
					«FOR sub:r.subvertices.filter(State).filter[!(it instanceof FinalState)] SEPARATOR ' else '»
						if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] == &«STATE_ARRAY_ATTRIBUTE»[«sub.name.toUpperCase»_ID]) {
							«SET_FLAG»(«sub.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», false);
						}
					«ENDFOR»
					«getFptrCall(STATE_ARRAY_ATTRIBUTE+"["+parent.name.toUpperCase+"_ID]."+ACTIVE_SUB_STATES+"["+parent.regions.indexOf(r)+"]", true, EXIT_NAME)»;
				}
				«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] = NULL;
			«ENDFOR»
		«ELSEIF parent.composite»
			«FOR s:parent.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState) && it.composite] SEPARATOR ' else '»
				if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0] == &«STATE_ARRAY_ATTRIBUTE»[«s.name.toUpperCase»_ID]) {
					«generateExitingSubStates(s, true)»
				}
			«ENDFOR»
			if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0] != NULL) {
				«FOR sub:parent.regions.head.subvertices.filter(State).filter[!(it instanceof FinalState)] SEPARATOR ' else '»
					if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0] == &«STATE_ARRAY_ATTRIBUTE»[«sub.name.toUpperCase»_ID]) {
						«SET_FLAG»(«sub.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», false);
					}
				«ENDFOR»
				«getFptrCall(STATE_ARRAY_ATTRIBUTE+"["+parent.name.toUpperCase+"_ID]."+ACTIVE_SUB_STATES+"[0]", true, EXIT_NAME)»;
			}
			«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0] = NULL;
		«ELSE»
		«ENDIF»
		«IF exitParent»
			«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», false);
			«getFptrCall(pAttr, false, EXIT_NAME)»;
		«ENDIF»
		«t.getTransitionEffect()»'''
	}

	@Deprecated
	def String generateEnteringSubStates(State parent, boolean enterParent) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«IF enterParent»
			«generateChangeState(parent)»
			«getFptrCall(pAttr, false, ENTRY_NAME)»;
			//start activity of «parent.name» by calling setFlag
			«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
		«ENDIF»
		«IF parent.orthogonal»
			«FOR r:parent.regions»
				«IF r.findInitialState() !== null»
					«r.getInitialEffect()»
					«generateEnteringSubStates(r.findInitialState(), true)»
				«ENDIF»
			«ENDFOR»
		«ELSEIF parent.composite»
			«IF parent.regions.head.findInitialState() !== null»
				«parent.regions.head.getInitialEffect()»
				«generateEnteringSubStates(parent.regions.head.findInitialState(), true)»
			«ENDIF»
		«ELSE»
		«ENDIF»'''
	}

	@Deprecated
	def String generateEnteringSubStates(State parent, State child, boolean enterParent, boolean gotoSubstate) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«IF enterParent»
			«generateChangeState(parent)»
			«getFptrCall(pAttr, false, ENTRY_NAME)»;
			//start activity of «parent.name» by calling setFlag
			«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
		«ENDIF»
		«IF parent.orthogonal»
			«FOR r:parent.regions»
				«IF child.container == r»
					«IF gotoSubstate»
						«generateEnteringSubStates(child, true)»
					«ENDIF»
				«ELSEIF r.findInitialState() !== null»
					«r.getInitialEffect()»
					«generateEnteringSubStates(r.findInitialState(), true)»
				«ENDIF»
			«ENDFOR»
		«ELSEIF parent.composite»
			//«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0] = &«STATE_ARRAY_ATTRIBUTE»[«child.name.toUpperCase»_ID];
			«generateEnteringSubStates(child, true)»
		«ELSE»
		«ENDIF»'''
	}

	@Deprecated
	def String generateEnteringTransitiveSubStates(State parent, State child) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«generateChangeState(parent)»
		«getFptrCall(pAttr, false, ENTRY_NAME)»;
		//start activity of «parent.name» by calling setFlag
		«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
		«IF parent.orthogonal»
			«FOR r:parent.regions»
				«IF child.container == r»
					«generateEnteringSubStates(child, true)»
				«ELSEIF r.transitiveSubStates().contains(child)»
					«var nextParent = r.subvertices.filter(State).filter[it.transitiveSubStates().contains(child)].head»
					«generateEnteringTransitiveSubStates(nextParent, child)»
				«ELSEIF r.findInitialState() !== null»
					«r.getInitialEffect()»
					«generateEnteringSubStates(r.findInitialState(), true)»
				«ENDIF»
			«ENDFOR»
		«ELSEIF parent.composite»
			«IF child.container.state == parent»
				«generateEnteringSubStates(parent, child, false, false)»
			«ELSE»
				«var nextParent = parent.regions.head.subvertices.filter(State).filter[it.transitiveSubStates().contains(child)].head»
				«generateEnteringTransitiveSubStates(nextParent, child)»
			«ENDIF»
		«ELSE»
		«ENDIF»'''
	}

	@Deprecated
	def String generateEnteringTransitiveSubStates(State parent, State child, boolean enterParent, boolean gotoChild) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«IF enterParent»
			«generateChangeState(parent)»
			«getFptrCall(pAttr, false, ENTRY_NAME)»;
			//start activity of «parent.name» by calling setFlag
			«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
		«ENDIF»
		«IF parent.orthogonal»
			«FOR r:parent.regions»
				«IF child.container == r»
					«IF gotoChild»
						«generateEnteringSubStates(child, true)»
					«ENDIF»
				«ELSEIF r.transitiveSubStates().contains(child)»
					«var nextParent = r.subvertices.filter(State).filter[it.transitiveSubStates().contains(child)].head»
					«generateEnteringTransitiveSubStates(nextParent, child)»
				«ELSEIF r.findInitialState() !== null»
					«r.getInitialEffect()»
					«generateEnteringSubStates(r.findInitialState(), true)»
				«ENDIF»
			«ENDFOR»
		«ELSEIF parent.composite»
			«IF child.container.state == parent»
				«IF gotoChild»
					«generateEnteringSubStates(parent, child, enterParent, false)»
				«ENDIF»
			«ELSE»
				«var nextParent = parent.regions.head.subvertices.filter(State).filter[it.transitiveSubStates().contains(child)].head»
				«generateEnteringTransitiveSubStates(nextParent, child)»
			«ENDIF»
		«ELSE»
		«ENDIF»'''
	}

	@Deprecated
	def String generateEnteringSpecial(State parent, Pseudostate child) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«generateChangeState(parent)»
		«getFptrCall(pAttr, false, ENTRY_NAME)»;
		//start activity of «parent.name» by calling setFlag
		«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
		«IF parent.orthogonal»
			«FOR r:parent.regions»
				«IF child.container == r»
					«pseudostateGenerator.generatePseudo(child)»
				«ELSEIF r.transitiveSubStates().contains(child)»
					«var nextParent = r.subvertices.filter(State).filter[it.transitiveSubStates().contains(child)].head»
					«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] = «nextParent.name.toUpperCase»_ID;
					«generateEnteringSpecial(nextParent, child)»
				«ELSEIF r.findInitialState() !== null»
					«r.getInitialEffect()»
					«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] = «r.findInitialState().name.toUpperCase»_ID;
					«generateEnteringSubStates(r.findInitialState(), true)»
				«ENDIF»
			«ENDFOR»
		«ELSEIF parent.composite»
			«IF child.container.state == parent»
				«pseudostateGenerator.generatePseudo(child)»
			«ELSE»
				«var nextParent = parent.regions.head.subvertices.filter(State).filter[it.transitiveSubStates.contains(child)].head»
				«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0] = «nextParent.name.toUpperCase»_ID;
				«generateEnteringSpecial(nextParent, child)»
			«ENDIF»
		«ELSE»
		«ENDIF»'''
	}

	@Deprecated
	def String generateEnteringSpecial(State parent, Pseudostate child, Transition t) {
		var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]'''
		return '''
		«generateChangeState(parent)»
		«t.getTransitionEffect()»
		«getFptrCall(pAttr, false, ENTRY_NAME)»;
		//start activity of «parent.name» by calling setFlag
		«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
		
		«IF parent.orthogonal»
			«FOR r:parent.regions»
				«IF child.container == r»
					«pseudostateGenerator.generatePseudo(child)»
				«ELSEIF r.transitiveSubStates.contains(child)»
					«var nextParent = r.subvertices.filter(State).filter[it.transitiveSubStates.contains(child)].head»
					«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] = «nextParent.name.toUpperCase»_ID;
					«generateEnteringSpecial(nextParent, child)»
				«ELSEIF r.findInitialState() !== null»
					«r.getInitialEffect()»
					«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«parent.regions.indexOf(r)»] = «r.findInitialState().name.toUpperCase»_ID;
					«generateEnteringSubStates(r.findInitialState(), true)»
				«ENDIF»
			«ENDFOR»
		«ELSEIF parent.composite»
			«IF child.container.state == parent»
				«pseudostateGenerator.generatePseudo(child)»
			«ELSE»
				«var nextParent = parent.regions.head.subvertices.filter(State).filter[it.transitiveSubStates.contains(child)].head»
				«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0] = «nextParent.name.toUpperCase»_ID;
				«generateEnteringSpecial(nextParent, child)»
			«ENDIF»
		«ELSE»
		«ENDIF»'''
	}

	def String generateCompletionCall(State state) {
		var callCompletionEvent = ''''''
		var composite = state.container.state
		if (composite === null) {
			callCompletionEvent = ''''''
		} else {
			callCompletionEvent = '''
			if («FOR r : composite.regions SEPARATOR ' && '»(«STATE_ARRAY_ATTRIBUTE»[«composite.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«composite.regions.indexOf(r)»] == «STATE_MAX»)«ENDFOR») {
				«EVENT_QUEUE».push(statemachine::EventPriority_t::PRIORITY_1, NULL, COMPLETIONEVENT_ID, statemachine::EventType_t::COMPLETION_EVENT, «composite.name.toUpperCase»_ID);
			}'''
		}
		return callCompletionEvent
	}

	def TransitionGraph calculateTransitionGraphs(State s, Transition t) {
		var ret = new TransitionGraph
		ret.P.add(t.target as Pseudostate)
		ret.T.add(t)
		ret.S.add(s)
		var target = t.target as Pseudostate
		if (target.kind == PseudostateKind.JOIN_LITERAL) {
			var ins = target.incomings.filter[it.source.container.state == s.container.state].toList
			ret.S.addAll(ins.map[it.source].filter(State))
			ret.T.addAll(ins)
		}
		var nexts = t.target.findTrans
		ret.T.addAll(nexts)
		var H = nexts.map[t.target].filter(Pseudostate).filter [
			it.kind == PseudostateKind.DEEP_HISTORY_LITERAL || it.kind == PseudostateKind.SHALLOW_HISTORY_LITERAL
		]
		ret.P.addAll(H)
		ret.P.addAll(nexts.map[it.source].filter(Pseudostate))
		ret.L.addAll(nexts.map[it.target].filter(State))
		return ret
	}

	def generateDelegation(State s, List<Transition> transitions) {
		return '''
		«IF checkTransitiveSubStatesAcceptEvent(s, transitions)»
			«IF s.orthogonal»
			«ELSE»
				
			«ENDIF»
		«ENDIF»'''
	}

	// check whether the substate of a parent state accepts the event
	def checkSubStatesAcceptEvent(State parent, List<Transition> transitions) {
		val substates = new ArrayList<State>
		parent.regions.forEach [
			substates.addAll(it.subvertices.filter(State))
		]
		var ret = false
		for (t : transitions) {
			if (!ret && substates.contains(t.source)) {
				ret = true
			}
		}
		return ret
	}

	// check whether the transitive substates of a parent state accepts the event
	def checkTransitiveSubStatesAcceptEvent(State parent, List<Transition> transitions) {
		val substates = parent.transitiveSubStates()
		var ret = false
		for (t : transitions) {
			if (!ret && substates.contains(t.source)) {
				ret = true
			}
		}
		return ret
	}

	// create State_t struct
	def createStateStruct() {
		superContext.createOwnedOperation("entry_dft", null, null)
		superContext.createOwnedOperation("exit_dft", null, null)
		superContext.createOwnedOperation("doActivity_dft", null, null)

		// stateStruct = targetPack.createOwnedPrimitiveType(STATE_STRUCT_NAME)
		stateStruct = superContext.createNestedClassifier(STATE_STRUCT_NAME, UMLPackage.Literals.PRIMITIVE_TYPE)
		stateStruct.apply(Typedef)
		UMLUtil.getStereotypeApplication(stateStruct, Typedef).definition = '''
		struct «STATE_STRUCT_NAME» {
			//«MAX_NUMBER_ORTHOGONAL_REGIONS» is configured as the maximum number of orthogonal regions a composite states can have
			unsigned int /*«STATE_ID_ENUM»*/ «PREVIOUS_STATES»[«MAX_NUMBER_ORTHOGONAL_REGIONS»]; //for history states
			unsigned int /*«STATE_ID_ENUM»*/ «ACTIVE_SUB_STATES»[«MAX_NUMBER_ORTHOGONAL_REGIONS»];
			void («superContext.name»::*entry)();
			void («superContext.name»::*exit)();
			void («superContext.name»::*doActivity)();
			«STATE_STRUCT_NAME»() {
				entry = &«superContext.name»::entry_dft;
				exit = &«superContext.name»::exit_dft;
				doActivity = &«superContext.name»::doActivity_dft;
				for(int i = 0; i < «MAX_NUMBER_ORTHOGONAL_REGIONS»; i++) {
					«PREVIOUS_STATES»[i] = «STATE_MAX»;
					«ACTIVE_SUB_STATES»[i] = «STATE_MAX»;
				}
			}
		} '''
		stateStruct.createDependency(stateIdEnum)
	}

	def getFptrCall(String attr, boolean isPointer, String fptrName) {
		if (isPointer) {
			return '''(this->*«attr»->«fptrName»)()'''
		}
		return '''(this->*«attr».«fptrName»)()'''
	}

	private def Map<Event, List<Transition>> getAllEvents(Region region) {
		val Map<Event, List<Transition>> ret = new LinkedHashMap
		var subVertices = region.subvertices
		var transitions = region.transitions

		transitions.forEach [
			for (trigger : it.triggers) {
				if (trigger.event !== null) {
					if (!ret.containsKey(trigger.event)) {
						ret.put(trigger.event, new ArrayList)
					}
					if (!ret.get(trigger.event).contains(it)) {
						ret.get(trigger.event).add(it)
					}
				}
			}
		]

		subVertices.filter(typeof(State)).forEach [
			for (subRe : it.regions) {
				var m = subRe.allEvents
				for (e : m.entrySet) {
					if (!ret.containsKey(e.key)) {
						ret.put(e.key, e.value)
					} else {
						ret.get(e.key).addAll(e.value)
					}
				}

			}
		]

		return ret
	}

	private def void getAllActionsTransitionsStates(Region region) {
		if (!regions.contains(region)) {
			regions.add(region)
		}
		transitions.addAll(region.transitions.filter[it.source !== null && it.target !== null])

		transitions.filter[!parallelTransitions.contains(it)].forEach [
			if (it.source instanceof Pseudostate) {
				if ((it.source as Pseudostate).kind == PseudostateKind.FORK_LITERAL && it.effect !== null) {
					parallelTransitions.add(it)
				}
			}

			if (it.target instanceof Pseudostate) {
				if ((it.target as Pseudostate).kind == PseudostateKind.JOIN_LITERAL && it.effect !== null) {
					parallelTransitions.add(it)
				}
			}
		]

		for (s : region.subvertices) {
			vertexes.add(s)
			if (s instanceof State) {
				if (!(s instanceof FinalState)) {
					states.add(s)
				}

				if (s.entry !== null && s.entry instanceof OpaqueBehavior) {
					actions.add(s.entry as OpaqueBehavior)
				}

				if (s.exit !== null && s.exit instanceof OpaqueBehavior) {
					actions.add(s.exit as OpaqueBehavior)
				}

				if (createDoActivity && s.doActivity !== null && s.doActivity instanceof OpaqueBehavior) {
					actions.add(s.doActivity as OpaqueBehavior)
				}

				s.regions.forEach [
					it.getAllActionsTransitionsStates
				]
			}

		}
	}

	public List<Region> orthogonalRegions = new ArrayList

	private def createRegionMethods() {
		// create macro for regions
		// regions.remove(topRegion)
		var macros = ""
		for (var i = 0; i < regions.size; i++) {
			macros += '''
				#define «regions.get(i).regionMacroName» («i»)
			'''
			regions.get(i).createRegionMethod
			createRegionMethodExit(regions.get(i))

			if (regions.get(i).state !== null && regions.get(i).state.orthogonal) {
				orthogonalRegions.add(regions.get(i))
			}
		}

		for (var i = 0; i < orthogonalRegions.size; i++) {
			appendIncludeHeader(superContext, '''
				#define «orthogonalRegions.get(i).regionMacroId» («i»)''')
		}

		if (!superContext.isApplied(Include)) {
			superContext.apply(Include)
		}
		var header = UMLUtil.getStereotypeApplication(superContext, Include).header
		UMLUtil.getStereotypeApplication(superContext, Include).header = '''
		«header»
		«macros»'''
	}

	def getRegionMacroId(Region r) {
		return '''REGION_ID_«r.state.name.toUpperCase»_«r.name.toUpperCase»'''
	}

	def getRegionMethodName(Region r) {
		if (r == topRegion) {
			return '''«r.stateMachine.name»_«r.name»_Enter'''
		}
		return '''«r.state.name»_«r.name»_Enter'''
	}

	def getRegionMacroName(Region r) {
		if (r == topRegion) {
			return '''«r.stateMachine.name.toUpperCase»_«r.name.toUpperCase»'''
		}
		return '''«r.state.name.toUpperCase»_«r.name.toUpperCase»'''
	}

	def getVertexMacroName(Vertex v) {
		if (v.container == topRegion) {
			return '''«topRegion.stateMachine.name.toUpperCase»_«v.container.name.toUpperCase»_«v.name.toUpperCase»'''
		}
		return '''«v.container.state.name.toUpperCase»_«v.container.name.toUpperCase»_«v.name.toUpperCase»'''
	}

	def getInitialMacroName(Region r) {
		if (r == topRegion) {
			return '''«r.stateMachine.name.toUpperCase»_«r.name.toUpperCase»_DEFAULT'''
		}
		return '''«r.state.name.toUpperCase»_«r.name.toUpperCase»_DEFAULT'''
	}

	private def getSubVertexes(State state) {
		val ret = new ArrayList<Vertex>
		state.regions.forEach [
			ret.addAll(it.subvertices)
		]
		return ret
	}

	private def void createRegionMethod(Region r) {
		var endVertices = new ArrayList<Vertex>
	
		// looking for vertexes having transitions incoming from vertexes which are not direct or indirect sub-vertex of r
		for (v : r.subvertices) {
			// Bug 549799 - check container of source of incoming transition (and not container of the
			// transition itself), otherwise it depends on the transition container, whether an element
			// is added to the endVertices list
			val incomings = v.incomings.filter [
				it.source.container != v.container ||
					(it.source instanceof State && (it.source as State).subVertexes.contains(it.target))
			]
			val sources = incomings.map[it.source]
			if (sources.size > 0) {
				if (!endVertices.contains(v)) {
					endVertices.add(v)
				}
			}
		}
		var body = ""
		var macros = ""
		// create macros for vertexes which are used to differentiate ways entering the region/state
		if (r.firstPseudoState(PseudostateKind.INITIAL_LITERAL) !== null) {
			var initialP = r.firstPseudoState(PseudostateKind.INITIAL_LITERAL)
			var initialState = initialP.outgoings.head.target as State
			var pAttr = '''«STATE_ARRAY_ATTRIBUTE»[«initialState.name.toUpperCase»_ID]'''
			macros = '''
				«macros»
				#define «r.initialMacroName» (0)'''
			body = '''
				«body»
				case «r.initialMacroName»:
					«initialP.outgoings.head.getTransitionEffect()»
					«generateChangeState(initialState)»
				
					«IF initialState.entry.isBehaviorExist»
						«getFptrCall(pAttr, false, ENTRY_NAME)»;
						//starting the counters for time events
					«ENDIF»
					«initialState.generateActivateTimeEvent("true")»
				
					«IF initialState.doActivity.isBehaviorExist || initialState.hasTriggerlessTransition»
						//start activity of «initialState.name» by calling setFlag
						«SET_FLAG»(«initialState.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
					«ENDIF»
					«IF initialState.composite»
						«IF initialState.orthogonal»
							//TODO: fork region funtions
							«FOR subRegion: initialState.regions»
								//«subRegion.regionMethodName»(«subRegion.initialMacroName»);
								«concurrency.generateForkCall(subRegion, true, subRegion.initialMacroName)»
							«ENDFOR»
							//TODO: join region functions
							«FOR subRegion: initialState.regions»
								//«subRegion.regionMethodName»(«subRegion.initialMacroName»);
								«concurrency.generateJoinCall(subRegion, true)»
							«ENDFOR»
						«ELSE»
							«initialState.regions.head.regionMethodName»(«initialState.regions.head.initialMacroName»);
						«ENDIF»
					«ENDIF»
					//TODO: set systemState to EVENT_CONSUMED
					break;
			'''
		}
		for (var i = 0; i < endVertices.size; i++) {
			macros = '''
				«macros»
				#define «endVertices.get(i).vertexMacroName» («i + 1»)'''
		}
		superContext.appendIncludeHeader(macros)

		var regionMethod = superContext.createOwnedOperation(r.regionMethodName, null, null)
		regionMethod.createOwnedParameter(ENTER_MODE_PARAM, charType)
		body += '''
		«FOR v : endVertices»
			«IF v instanceof State»
				case «v.vertexMacroName»:
					«generateChangeState(v)»
					«IF v.entry.isBehaviorExist»
						«getFptrCall('''«STATE_ARRAY_ATTRIBUTE»[«v.name.toUpperCase»_ID]''', false, ENTRY_NAME)»;
					«ENDIF»
					//starting the counters for time events
					«v.generateActivateTimeEvent("true")»
					«IF v.doActivity.isBehaviorExist || v.hasTriggerlessTransition»
						//start activity of «v.name» by calling setFlag
						«SET_FLAG»(«v.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
					«ENDIF»
					«IF v.composite»
						«IF v.orthogonal»
							//TODO: fork region funtions
							«FOR subRegion: v.regions»
								//«subRegion.regionMethodName»(«subRegion.initialMacroName»);
								«concurrency.generateForkCall(subRegion, true, subRegion.initialMacroName)»
							«ENDFOR»
							//TODO: join region functions
							«FOR subRegion: v.regions»
								//«subRegion.regionMethodName»(«subRegion.initialMacroName»);
								«concurrency.generateJoinCall(subRegion, true)»
							«ENDFOR»
						«ELSE»
							«v.regions.head.regionMethodName»(«v.regions.head.initialMacroName»);
						«ENDIF»
					«ENDIF»
			«ELSE»
				case «v.vertexMacroName»: 
					«pseudostateGenerator.generatePseudo(v as Pseudostate)»
			«ENDIF»
				//TODO: set systemState to EVENT_CONSUMED
				break;
		«ENDFOR»
		'''
		val switchBody = '''
			switch(«ENTER_MODE_PARAM») {
				«body»
			}
		'''
		superContext.createOpaqueBehavior(regionMethod, switchBody)
	}

	def generateEnteringOnSubVertex(State parent, Vertex subVertex) '''
		«generateChangeState(parent)»
		«IF parent.entry.isBehaviorExist»
			«getFptrCall('''«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID]''', false, ENTRY_NAME)»;
		«ENDIF»
		//starting the counters for time events
		«parent.generateActivateTimeEvent("true")»
		«IF parent.doActivity.isBehaviorExist || parent.hasTriggerlessTransition»
			//start activity of «parent.name» by calling setFlag
			«SET_FLAG»(«parent.name.toUpperCase»_ID, «THREAD_FUNC_DOACTIVITY_TYPE», true);
		«ENDIF»
		«IF parent.composite && parent != subVertex»
			«IF parent.orthogonal»
				«var toJoinList = new ArrayList<Region>»
				«FOR r:parent.regions»
					«IF subVertex !== null»
						«IF subVertex.container == r»
							«toJoinList.add(r)»
							//«getRegionMethodName(r)»(«subVertex.vertexMacroName»);
							«concurrency.generateForkCall(r, true, subVertex.vertexMacroName)»
						«ELSEIF r.allSubVertexes.contains(subVertex)»
							«toJoinList.add(r)»
							//«getRegionMethodName(r)»(«subVertex.vertexMacroName»);
							«concurrency.generateForkCall(r, true, subVertex.vertexMacroName)»
						«ELSEIF r.findInitialState() !== null»
							«toJoinList.add(r)»
							//«getRegionMethodName(r)»(«r.initialMacroName»);
							«concurrency.generateForkCall(r, true, r.initialMacroName)»
						«ENDIF»
					«ELSEIF subVertex !== null && subVertex.container === null»
						«IF subVertex.eContainer == parent»
							«toJoinList.add(r)»
							«concurrency.generateForkCall(r, true, subVertex.vertexMacroName)»
						«ELSE»
							«toJoinList.add(r)»
							«concurrency.generateForkCall(r, true, subVertex.vertexMacroName)»
						«ENDIF»
					«ELSE»
						«IF r.findInitialState() !== null»
							«toJoinList.add(r)»
							//«getRegionMethodName(r)»(«r.initialMacroName»);
							«concurrency.generateForkCall(r, true, r.initialMacroName)»
						«ENDIF»
					«ENDIF»
				«ENDFOR»
				«FOR r:toJoinList»
					«concurrency.generateJoinCall(r, true)»
				«ENDFOR»
			«ELSEIF parent.composite»
				«IF subVertex !== null»
					«IF subVertex.container === null && subVertex.eContainer instanceof State»
						«getRegionMethodName(parent.regions.head)»(«subVertex.vertexMacroName»);
					«ELSEIF subVertex.container.state == parent»
						«getRegionMethodName(parent.regions.head)»(«subVertex.vertexMacroName»);
					«ELSE»
						«var containingRegion = parent.regions.filter[it.allSubVertexes.contains(subVertex)].head»
						«IF containingRegion !== null»
							«getRegionMethodName(containingRegion)»(«subVertex.vertexMacroName»);
						«ENDIF»
					«ENDIF»
				«ELSE»
					«getRegionMethodName(parent.regions.head)»(«parent.regions.head.initialMacroName»);
				«ENDIF»
			«ELSE»
			«ENDIF»
		«ELSEIF parent.composite && parent == subVertex»
			«IF parent.orthogonal»
				«var toJoinList = new ArrayList<Region>»
				«FOR r:parent.regions»
					«IF r.findInitialState() !== null»
						«toJoinList.add(r)»
						//«getRegionMethodName(r)»(«r.initialMacroName»);
						«concurrency.generateForkCall(r, true, r.initialMacroName)»
					«ENDIF»
				«ENDFOR»
				«FOR r:toJoinList»
					«concurrency.generateJoinCall(r, true)»
				«ENDFOR»
			«ELSE»
				«IF parent.regions.head.findInitialState() !== null»
					«getRegionMethodName(parent.regions.head)»(«parent.regions.head.initialMacroName»);
				«ENDIF»
			«ENDIF»
		«ENDIF»
	'''
	
	private def createRegionMethodExit(Region r) {
		if (r.state === null || !r.state.composite) {
			return
		}

		// TODO: save states for history
		var parent = r.state
		var body = '''
		//exiting region «r.name»
		«var regionIndex = r.state.regions.indexOf(r)»
		«FOR s : r.subvertices.filter(State).filter[!(it instanceof FinalState) && it.composite] SEPARATOR ' else '»
			if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«regionIndex»] == «s.name.toUpperCase»_ID) {
				«IF s.orthogonal»
					«FOR subRegion:s.regions»
						«concurrency.generateForkCall(subRegion, false, "0")»
					«ENDFOR»
					«FOR subRegion:s.regions»
						«concurrency.generateJoinCall(subRegion, false)»
					«ENDFOR»
				«ELSE»
					«getRegionMethodExitName(s.regions.head)»();
				«ENDIF»
			}
		«ENDFOR»
		if («STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«regionIndex»] != «STATE_MAX») {
			//signal to exit the doActivity of sub-state of «parent.name»
			«SET_FLAG»(«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0], «THREAD_FUNC_DOACTIVITY_TYPE», false);
			«FOR sub : r.subvertices.filter(State) SEPARATOR ' else '»
				«IF states2TimeEvents.get(sub) !== null»
					if («sub.name.toUpperCase»_ID == «STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«regionIndex»]) {
						«sub.generateActivateTimeEvent("false")»
					}
				«ENDIF»	
			«ENDFOR»
			//exit action of sub-state of «parent.name»
			(this->*«STATE_ARRAY_ATTRIBUTE»[«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[0]].«EXIT_NAME»)();
			«IF r.isSavehistory()»
				//save history region «r.name» of state «parent.name»
				«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«PREVIOUS_STATES»[«regionIndex»] = «STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«regionIndex»];
			«ENDIF»
			//set active sub-state of «parent.name» to «STATE_MAX» meaning NULL
			«STATE_ARRAY_ATTRIBUTE»[«parent.name.toUpperCase»_ID].«ACTIVE_SUB_STATES»[«regionIndex»] = «STATE_MAX»;
		}'''
		var exitOp = superContext.createOwnedOperation(r.regionMethodExitName, null, null)
		superContext.createOpaqueBehavior(exitOp, body)
	}

	private def getRegionMethodExitName(Region r) {
		if (r == topRegion) {
			return '''«r.stateMachine.name»_«r.name»_Exit'''
		}
		return '''«r.state.name»_«r.name»_Exit'''
	}

	def List<Vertex> allSubVertexes(Region r) {
		val ret = new ArrayList<Vertex>
		ret.addAll(r.subvertices)
		r.subvertices.filter(State).forEach [
			it.regions.forEach [
				ret.addAll(it.allSubVertexes)
			]
		]
		return ret
	}

	private def createOpaqueBehavior(Class container, Operation op) {
		var opaque = op.methods.filter(typeof(OpaqueBehavior)).head
		if (opaque === null) {
			opaque = container.createClassifierBehavior(op.name, UMLPackage.Literals.OPAQUE_BEHAVIOR) as OpaqueBehavior
			op.methods.add(opaque)
			opaque.languages.add(langID)
		}
		return opaque
	}

	def createOpaqueBehavior(Class container, Operation op, String body) {
		var opaque = container.createOpaqueBehavior(op)
		if (opaque.bodies.size > 0) {
			opaque.bodies.set(0, body)
		} else {
			opaque.languages.add(langID)
			opaque.bodies.add(body)
		}
		return opaque
	}

	private def createOpaqueExpressionDefaultValue(Property container, String name) {
		var opaque = container.defaultValue as OpaqueExpression
		if (opaque === null) {
			opaque = container.createDefaultValue(name, null, UMLPackage.Literals.OPAQUE_EXPRESSION) as OpaqueExpression
			opaque.languages.add(langID)
		}
		return opaque
	}

	def createOpaqueExpressionDefaultValue(Property container, String name, String body) {
		var opaque = container.createOpaqueExpressionDefaultValue(name)
		if (opaque.bodies.size > 0) {
			opaque.bodies.set(0, body)
		} else {
			opaque.languages.add(langID)
			opaque.bodies.add(body)
		}
		return opaque
	}

	def createDerivedOperation(Class clz, Operation source) {
		var name = source.name
		var derivedOp = clz.createOwnedOperation(name, null, null)
		OperationSync.sync(source, derivedOp)
		derivedOp.name = name
		derivedOp.apply(DerivedElement)
		UMLUtil.getStereotypeApplication(derivedOp, DerivedElement).source = source
		derivedOp.apply(Virtual)
		derivedOp

	}

	def getGuard(Transition t) {
		return (t.guard.specification as OpaqueExpression).bodies.head
	}
}

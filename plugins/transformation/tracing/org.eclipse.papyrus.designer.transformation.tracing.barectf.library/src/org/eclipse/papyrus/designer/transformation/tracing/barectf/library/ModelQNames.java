/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.transformation.tracing.barectf.library;


/**
 * Store qualified names of some classes within the model libraries
 */
public class ModelQNames {
	public static String BareCTFInit = "barectf::classes::BareCTFInit"; //$NON-NLS-1$

	public static String BareCTF = "barectf::classes::BareCTF"; //$NON-NLS-1$
}

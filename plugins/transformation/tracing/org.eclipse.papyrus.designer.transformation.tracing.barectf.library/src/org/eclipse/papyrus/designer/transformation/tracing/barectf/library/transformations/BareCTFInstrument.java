/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.library.transformations;

import java.util.List;

import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.transformation.tracing.barectf.library.InstrumentCTF;
import org.eclipse.papyrus.designer.transformation.tracing.library.EventNames;
import org.eclipse.papyrus.designer.transformation.tracing.library.utils.TraceUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;

/**
 * Instrument the C++ code with barectf trace events
 */
public class BareCTFInstrument implements IM2MTrafoCDP {

	static List<Element> elementsToTrace;
	
	@Override
	public void applyTrafo(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {
		Package root = PackageUtil.getRootPackage(deploymentPlan);
		elementsToTrace = TraceUtils.getElementsToTrace(root);
		EventNames.init();
		InstrumentCTF.instrument(elementsToTrace);
	}

	/**
	 * Return a list of elements to trace (cached data from 1st transformation).
	 * We do not recalculate the list via traceUtils, since this list might be different due to
	 * model transformation steps. For instance, the SM transformation adds additional methods
	 * that have not been instrumented by this transformation, but could appear later, if "all operations"
	 * option is set.
	 */
	public static List<Element> getElementsToTrace() {
		return elementsToTrace;
	}
}

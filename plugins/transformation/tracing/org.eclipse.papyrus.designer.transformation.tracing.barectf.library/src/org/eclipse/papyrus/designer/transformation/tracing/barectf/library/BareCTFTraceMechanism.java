/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.transformation.tracing.barectf.library;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.transformation.tracing.library.DesignerTraceMechanism;
import org.eclipse.papyrus.moka.tracepoint.service.ITraceMechanism;

/**
 * Extension point for BareCTF trace transformations
 */
public class BareCTFTraceMechanism extends DesignerTraceMechanism implements ITraceMechanism {
	
	protected static final String BARE_CTF = "BareCTF"; //$NON-NLS-1$

	@Override
	public EList<String> getTraceMechanismIDs(EObject eObj) {
		EList<String> ids = new BasicEList<String>();
		ids.add(BARE_CTF);
		return ids;
	}

	@Override
	public String getTraceMechanismDescription(EObject eObj, String id) {
		if (id.equals(BARE_CTF)) {
			return Messages.BareCTFTraceMechanism_description;
		}
		return "none"; //$NON-NLS-1$
	}
}

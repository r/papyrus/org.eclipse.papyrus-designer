/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.library

import org.eclipse.core.resources.IFolder
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext
import org.eclipse.uml2.uml.Classifier
import org.eclipse.uml2.uml.Package
import org.eclipse.uml2.uml.UMLFactory
import org.osgi.framework.FrameworkUtil
import org.eclipse.uml2.uml.UMLPackage
import org.eclipse.uml2.uml.LiteralString
import org.eclipse.papyrus.designer.infra.base.StringUtils
import org.eclipse.core.runtime.FileLocator
import org.eclipse.papyrus.designer.infra.base.StringConstants
import java.io.File

class InitCTF {

	/**
	 * Copy a file from the code folder (in the tracing plugin) into the
	 * passed target folder
	 * @param fileName the name of the file to copy
	 * @param targetFolder the target folder
	 */
	def static copyCode(String fileName, IFolder targetFolder) {
		val bundle = FrameworkUtil.getBundle(InstrumentCTF)
		val codeFolder = "code/"
		val inputURL = bundle.getResource(codeFolder + fileName);
		val createdFile = targetFolder.getFile(fileName);
		if (createdFile.exists()) {
			createdFile.setContents(inputURL.openStream(), true, true, null);
		} else {
			createdFile.create(inputURL.openStream(), true, null);
		}
		TransformationContext.current.keepFiles.add(createdFile.fullPath.toString);
	}

	/**
	 * copy platform files into srcCTF folder and create an instance for the
	 * BareCTFInit class.
	 */
	def static initCode(IFolder srcCTF, Package cdp) {
		// val root = PackageUtil.getRootPackage(cdp);
		var initCl = ElementUtils.getQualifiedElementFromRS(cdp, ModelQNames.BareCTFInit) as Classifier
		if (initCl !== null) {
			val is = UMLFactory.eINSTANCE.createInstanceSpecification()
			val ctfInitCopy = TransformationContext.current.copier.getCopy(initCl)
			val metadataPath = ctfInitCopy.getAttribute("metadataPath", null);
			val defaultVal = metadataPath.createDefaultValue(null, metadataPath.type,
				UMLPackage.eINSTANCE.literalString) as LiteralString;
			var projectFN = FileLocator.resolve(TransformationContext.current.project.getLocationURI().toURL()).
				getFile() + "/src-gen/ctf/metadata";
			if (File.separator == StringConstants.BSLASH) {
				// windows file name, replace separator char
				if (projectFN.startsWith(StringConstants.SLASH)) {
					// remove leading "/"
					projectFN = projectFN.substring(1);
				}
				// replace slash with double backslash (single backslash escapes)
				projectFN = projectFN.replace(StringConstants.SLASH,
					StringConstants.BSLASH + StringConstants.BSLASH);
			}
			defaultVal.value = StringUtils.quote(projectFN)
			is.name = "ctfInit"
			is.classifiers.add(ctfInitCopy)
			cdp.packagedElements.add(is)
		}

		val platformFile = "barectf-platform-fs"
		copyCode(platformFile + ".h", srcCTF)
		copyCode(platformFile + ".c", srcCTF)
		val initCode = "barectf-init"
		copyCode(initCode + ".h", srcCTF)
		copyCode(initCode + ".c", srcCTF)
	}
}

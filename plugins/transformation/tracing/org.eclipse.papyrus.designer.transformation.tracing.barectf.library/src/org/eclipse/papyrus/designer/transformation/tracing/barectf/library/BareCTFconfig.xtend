/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.library

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets
import java.util.List
import java.util.Map
import org.eclipse.core.resources.IFolder
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.Element
import org.eclipse.uml2.uml.Operation
import org.eclipse.uml2.uml.Port
import org.eclipse.uml2.uml.State
import org.eclipse.uml2.uml.Transition
import org.eclipse.uml2.uml.Type

import static extension org.eclipse.papyrus.designer.transformation.tracing.library.EventNames.*
import static extension org.eclipse.papyrus.designer.transformation.tracing.barectf.library.CTFTypeUtils.ctfType
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext
import org.eclipse.papyrus.designer.transformation.tracing.barectf.library.Activator
import static extension org.eclipse.papyrus.designer.transformation.tracing.library.utils.TraceUtils.*
import static extension org.eclipse.papyrus.designer.transformation.tracing.barectf.library.flatten.Flattener.flattenDecl
import org.eclipse.papyrus.designer.uml.tools.utils.OperationUtils
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAState
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAOperation
import java.util.LinkedHashMap
import org.eclipse.papyrus.designer.infra.base.StringConstants
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TATransition
import org.eclipse.uml2.uml.SignalEvent

/**
 * Create a BareCTF config.yaml file containing the information what to trace
 */
class BareCTFconfig {
	protected static IFolder srcCTF

	final protected static String METADATA = "metadata"

	public static Map<Type, Boolean> types

	/**
	 * Create the YAML header. Must be called after createYaml, as it needs the list of declared
	 * types
	 */
	def static createYamlHdr() '''
		# Needed YAML tag for the configuration object
		--- !<tag:barectf.org,2020/3/config>
		
		# Configuration's trace
		trace:
			# Type of the trace
			type:
				# Add standard field type aliases
				$include:
					- stdint.yaml
					- stdreal.yaml
					- stdmisc.yaml
					«FOR type : types.keySet»
						- «type.name».yaml
					«ENDFOR»
		
	'''

	def static createYaml(List<Element> elemsToTrace) '''
				# Native byte order is little-endian
				native-byte-order: little-endian
		
				# One clock type
				clock-types:
					# The Linux FS platform requires a clock type named `default`
					# which has a 1-GHz frequency and the `uint64_t` C type.
					default:
						frequency: 1000000000
						$c-type: uint64_t
		
				# One data stream type
				data-stream-types:
					# Stream type named `default`
					default:
						# Default data stream type
						$is-default: true
		
						# Default clock type: `default`
						$default-clock-type-name: default
		
						# Two event record types
						event-record-types:
							«FOR elemToTrace : elemsToTrace»
								«IF elemToTrace instanceof Port»
									«(elemToTrace as Port).createPortEvent»
								«ELSEIF elemToTrace instanceof State»
									«(elemToTrace as State).createStateEvent»
								«ELSEIF elemToTrace instanceof Transition»
									«(elemToTrace as Transition).createTransitionEvent»
								«ELSEIF elemToTrace instanceof Operation»
									«(elemToTrace as Operation).createOperationEvent(null)»
								«ELSEIF elemToTrace instanceof DataType»
									«(elemToTrace as DataType).createDataTypeEvent»
								«ENDIF»

			«ENDFOR»
	'''

	/**
	 * Create config.yaml entries for a datatype
	 */
	def static createDataTypeEvent(DataType dt) '''
		# UML datatype named `«dt.name»`
			«dt.name»:
				payload-field-type:
					class: structure
					members:
						«IF dt.hasHint»
							«dt.declarationHint»
						«ELSE»
							«FOR attribute : dt.attributes.flattenDecl»
								- «attribute.name»: «attribute.type.ctfType»
							«ENDFOR»
						«ENDIF»
	'''

	/**
	 * Create entries for an interface exposed by a port. Create one event for each
	 * operation
	 */
	def static createPortEvent(Port port) '''
		«FOR intf : port.provideds»
			«FOR iOp : intf.operations»
				«val cOp = OperationUtils.getSameOperation(iOp, port.class_)»
				«cOp.createOperationEvent(port)»

			«ENDFOR»
		«ENDFOR»
	'''

	/**
	 * Create entries for an operation.
	 */
	def static createOperationEvent(Operation operation, Port port) '''
		# operation «operation.name» of classifier «operation.class_.name»
		«operation.operationStartsEventName(port)»:
			payload-field-type:
				class: structure
				«val members = operationMembers(operation, port)»
				«IF members.length > 0»
					members:
						«members»
				«ENDIF»
		«IF operation.traceOpOrPortAction(port, TAOperation.MethodEnds) && operation.type === null»
			«operation.operationEndsEventName(port)»:
				payload-field-type:
					class: structure
					«IF operation.needInstanceName»
						members:
							- instanceId : string
					«ENDIF»
		«ENDIF»
	'''

	/**
	 * Calculate the list of members for an operation
	 */
	def static operationMembers(Operation operation, Port port) '''
		«IF operation.needInstanceName»
			- instanceId : string
		«ENDIF»
		«IF operation.hasHint»
			«operation.declarationHint»
		«ELSEIF (operation.traceOpOrPortAction(port, TAOperation.ParameterValues))»
			«FOR param : operation.inAndInout.flattenDecl»
				- «param.name» : «param.type.ctfType»
			«ENDFOR»
		«ENDIF»
	'''
	
	/**
	 * Create entries for a transition.
	 */
	def static createTransitionEvent(Transition t) '''
		# transition «t.name» of state-machine «t.containingStateMachine.name»
		«t.transitionEventName»:
			payload-field-type:
				class: structure
				members:
					- instanceId : string
					«IF t.traceTransitionAction(TATransition.TriggerValues)»
						«FOR trigger : t.triggers»
							«IF trigger.event instanceof SignalEvent»
								«val sigEvent = trigger.event as SignalEvent»
								«FOR attr : sigEvent.signal.attributes.flattenDecl»
									- «attr.name» : «attr.type.ctfType»
								«ENDFOR»
							«ENDIF»
						«ENDFOR»
					«ENDIF»

	'''

	/**
	 * Create entries for a state.
	 */
	def static createStateEvent(State state) '''
		«IF state.traceStateAction(TAState.StateEnter)»
			# enter state «state.name» of state-machine «state.containingStateMachine.name»
			«state.enterStateEventName»:
				payload-field-type:
					class: structure
					members:
						- instanceId : string

		«ENDIF»
		«IF state.traceStateAction(TAState.StateLeave)»
			# exit state «state.name» of state-machine «state.containingStateMachine.name»
			«state.exitStateEventName»:
				payload-field-type:
					class: structure
					members:
						- instanceId : string

		«ENDIF»
	'''

	/**
	 * Write a barectf configuration file (config.yaml)
	 */
	def static writeConfig(IFolder srcCTF, List<Element> elemsToTrace) {
		// obtain interfaces of ports
		BareCTFconfig.srcCTF = srcCTF;
		// use a linked hash map, in order to guarantee order when traversing
		// keyset
		types = new LinkedHashMap<Type, Boolean>()

		// val events = new BasicEList<Classifier>()
		val yamlEnd = createYaml(elemsToTrace)
		val yaml = new StringBuffer(createYamlHdr).append(yamlEnd)
		writeFile("config.yaml", yaml)
	}

	def static writeFile(String fileName, CharSequence content) {
		val configFile = srcCTF.getFile(fileName)
		TransformationContext.current.keepFiles.add(configFile.fullPath.toString);
		val contentStr = CTFTypeUtils.confTabs(content).trim() + StringConstants.EOL;
		val contentStream = new ByteArrayInputStream(contentStr.bytes)
		if (configFile.exists()) {
			configFile.setContents(contentStream, true, true, null)
		} else {
			configFile.create(contentStream, true, null)
		}
	}

	/**
	 * Inject the URIs of the elements to trace into the meta-data file. This is
	 * required, as barectf itself does not support such an attribute in the config.yaml
	 * file.
	 */
	def static injectURIs(List<Element> elemsToTrace) {
		val metaData = srcCTF.getFile("metadata")
		if (!metaData.exists()) {
			Activator.log.debug("Cannot read CTF metadata file")
			return
		}
		var contentStr = new String(metaData.contents.readAllBytes, StandardCharsets.UTF_8)
		for (Element elemToTrace : elemsToTrace) {
			var CharSequence eventName = null
			if (elemToTrace instanceof Port) {
				for (intf : elemToTrace.provideds) {
					for (iOp : intf.operations) {
						val cOp = OperationUtils.getSameOperation(iOp, elemToTrace.class_)
						contentStr = injectURI(contentStr, elemToTrace, cOp.operationStartsEventName(elemToTrace as Port))
					}
				}
			} else if (elemToTrace instanceof State) {
				eventName = (elemToTrace as State).enterStateEventName
				contentStr = injectURI(contentStr, elemToTrace, eventName)
				eventName = (elemToTrace as State).exitStateEventName
			} else if (elemToTrace instanceof Transition) {
				eventName = (elemToTrace as Transition).transitionEventName
			} else if (elemToTrace instanceof Operation) {
				eventName = (elemToTrace as Operation).operationStartsEventName(null)
				contentStr = injectURI(contentStr, elemToTrace, eventName)
				eventName = (elemToTrace as Operation).operationEndsEventName(null)
			} else if (elemToTrace instanceof DataType) {
				// eventName = (elemToTrace as DataType).
			}
			if (eventName !== null) {
				contentStr = injectURI(contentStr, elemToTrace, eventName)
			}
		}
		val contentStream = new ByteArrayInputStream(contentStr.bytes)
		metaData.setContents(contentStream, true, true, null)
	}

	/**
	 * Inject the URI of an element to trace into the meta-data file. The
	 * place where it needs to be inserted is identified via the assignment of the
	 * event name.
	 */
	def static injectURI(String contentStr, Element elemToTrace, CharSequence eventName) {
		val sourceElemToTrace = elemToTrace.getSourceElement()
		if (sourceElemToTrace !== null) {
			val r = sourceElemToTrace.eResource
			return contentStr.replaceFirst('''name = "«eventName.toString»";''', '''
			name = "«eventName»";
				model.emf.uri = "«r.URI»#«r.getURIFragment(sourceElemToTrace)»";''')
		}
	}
}

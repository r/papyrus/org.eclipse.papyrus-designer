/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.library.flatten;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Ptr;
import org.eclipse.papyrus.designer.transformation.tracing.library.utils.TraceUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Enumeration;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.TypedElement;

/**
 * Utility class for flattening parameter or attribute lists
 * This functionality is provided for declaration (e.g. in a CTF yaml file) as well as
 * for instrumentation.
 * In case of a declaration, nested fields are separated with an "_" char
 * In case of an access to a field, the separation is done with a ".". This
 * will work in case of non-pointer access.
 */
public class Flattener {
	/**
	 * what to flatten, a declaration or the trace instrumentation providing
	 * values
	 */
	public enum TMode {
		DECLARATION, // declaration of a trace variable
		PARAMS, // code providing parameters to the trace call
	}

	public static <T extends TypedElement> List<NameType> flattenDecl(List<T> teList) {
		List<NameType> ntList = new ArrayList<NameType>();
		for (T te : teList) {
			if (te instanceof MultiplicityElement && ((MultiplicityElement) te).getUpper() == 1) {
				ntList.addAll(flattenDecl(te));
			}
		}
		return ntList;
	}

	public static <T extends TypedElement> List<NameType> flattenParams(List<T> teList) {
		List<NameType> ntList = new ArrayList<NameType>();
		for (T te : teList) {
			if (te instanceof MultiplicityElement && ((MultiplicityElement) te).getUpper() == 1) {
				ntList.addAll(flattenParams(te));
			}
		}
		return ntList;
	}

	public static List<NameType> flattenDecl(TypedElement te) {
		return flatten(TMode.DECLARATION, StringConstants.EMPTY, te);
	}

	public static List<NameType> flattenParams(TypedElement te) {
		return flatten(TMode.PARAMS, StringConstants.EMPTY, te);
	}

	/**
	 * Flatten the passed type element. Includes specific handling of std::string
	 * 
	 * @param mode
	 *            the mode to use (declaration of parameters)
	 * @param prefix
	 *            the prefix
	 * @param te
	 *            a typed element
	 * @return the list of name type pairs
	 */
	public static List<NameType> flatten(TMode mode, String prefix, TypedElement te) {
		List<NameType> ntList = new ArrayList<NameType>();
		// only handle elements that do not apply the pointer stereotype
		if (!StereotypeUtil.isApplied(te, Ptr.class)) {
			String sepChar = (mode == TMode.DECLARATION) ? StringConstants.UNDERSCORE : StringConstants.DOT;
			Type type = te.getType();
			if (type instanceof PrimitiveType || type instanceof Enumeration) {
				String postfix = StringConstants.EMPTY;
				if (mode == TMode.PARAMS) {
					if (TraceUtils.isSTLstring(type)) {
						postfix = ".c_str()"; //$NON-NLS-1$
					}
				}
				String deref = StringConstants.EMPTY;
				if (mode == TMode.PARAMS && te instanceof Parameter &&
						((Parameter) te).getDirection() == ParameterDirectionKind.INOUT_LITERAL) {
					// dereference inout parameters, wnich are passed as pointers
					deref = StringConstants.STAR;
				}
				NameType nt = new NameType(prefix + deref + te.getName() + postfix, type);
				ntList.add(nt);
			} else if (type instanceof DataType) {
				for (Property attribute : ((DataType) type).getOwnedAttributes()) {
					if (attribute.getUpper() == 1) {
						List<NameType> subNtList = flatten(mode, prefix + te.getName() + sepChar, attribute);
						ntList.addAll(subNtList);
					}
				}
			}
		}
		return ntList;
	}
}

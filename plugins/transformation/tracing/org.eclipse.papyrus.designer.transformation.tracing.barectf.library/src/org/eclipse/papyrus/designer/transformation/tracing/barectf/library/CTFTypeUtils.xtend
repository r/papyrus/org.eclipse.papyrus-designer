/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/
 
 package org.eclipse.papyrus.designer.transformation.tracing.barectf.library

import org.eclipse.uml2.uml.Type
import static extension org.eclipse.papyrus.designer.transformation.library.xtend.CppUtils.cppType
import org.eclipse.uml2.uml.Enumeration
import org.eclipse.uml2.uml.DataType
import org.eclipse.uml2.uml.EnumerationLiteral
import org.eclipse.papyrus.designer.transformation.tracing.barectf.library.BareCTFconfig
import org.eclipse.uml2.uml.PrimitiveType
import org.eclipse.uml2.uml.util.UMLUtil
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Typedef

class CTFTypeUtils {
	/**
	 * Calculate the typename, defaulting to ctf_integer
	 */
	static def ctfType(Type type) '''
		«IF
			(type.qualifiedName == 'CORBA::String') ||
			(type.qualifiedName == 'PrimitiveTypes::String') ||
			(type.qualifiedName == 'UMLPrimitiveTypes::String')»
			string
		«ELSEIF
			(type.qualifiedName == 'CORBA::Float') ||
			(type.qualifiedName == 'CORBA::Double') ||
			(type.qualifiedName == 'AnsiCLibrary::float') ||
			(type.qualifiedName == 'AnsiCLibrary::double')»
			float
		«ELSEIF
			(type.qualifiedName == 'UMLPrimitiveTypes::Boolean') ||
			(type.qualifiedName == 'PrimitiveTypes::Boolean') ||
			(type.qualifiedName == 'CORBA::Boolean')»
			/* bool */ uint8
		«ELSEIF
			type instanceof PrimitiveType»
			«type.primitiveType»
		«ELSEIF
			type instanceof Enumeration»
			«type.enumType»
		«ELSEIF
			type instanceof DataType»
			// should not happen due to flattening
		«ELSE»
			«type.cppType»
		«ENDIF»
	'''

	/**
	 * Handle primitive types applying the typedef stereotype
	 * Map some C/C++ types to their according CTF types 
	 */
	static def String primitiveType(PrimitiveType pt) {
		val typedef = UMLUtil.getStereotypeApplication(pt, Typedef)
		var name = pt.name
		if (typedef !== null) {
			name = typedef.definition
		}
		if (name == "int" || name == "Integer" || name == "long") {
			return "int32"
		}
		else if (name == "unsigned int" || name == "unsigned long") {
			return "uint32"
		}
		else if (name == "short") {
			return "int16"
		}
		else if (name == "unsigned short") {
			return "uint16"
		}
		else if (name == "char") {
			return "uint16"
		}
		else if (name == "bool") {
			return "uint8"
		}
		else if (name == "std::string") {
			return "string"
		}
		return name
	}

	static def String enumType(Enumeration enumT) {
		if (!BareCTFconfig.types.containsKey(enumT)) {
			BareCTFconfig.types.put(enumT, true)
			BareCTFconfig.writeFile(enumT.name + ".yaml", enumT.enumTypeContents)
		}
		return enumT.name
	}

	/**
	 * Create BareCTF enumeration declaration based on mappings.
	 * Assumes that number of values fit in 8bit (0..255)
	 */
	static def String enumTypeContents(Enumeration enumT) '''
		«var i = 0»
		$field-type-aliases:
			«enumT.name»:
				class: unsigned-enum
				size: 8
				alignment: 8
				mappings:
					«FOR literal : enumT.ownedLiterals»
						«IF literal.specification !== null»«i = Integer.parseInt(literal.specification.stringValue)»«ENDIF»
						«literal.escapeLitName» : [«i++»]
					«ENDFOR»
	'''

	/**
	 * postfix with _ to avoid problematic literals like true and false
	 * Might need completition with additional keyword
	 */
	static def escapeLitName(EnumerationLiteral literal) {
		val name = literal.name
		val lcName = literal.name.toLowerCase
		if (lcName == 'true' || lcName == 'false') {
			return '''«name»_'''
		}
		return name
	}

	/**
	 * convert tabs to space
	 */
	static def confTabs(CharSequence seq) {
		return seq.toString.replaceAll("\t", "  ")
	}
}
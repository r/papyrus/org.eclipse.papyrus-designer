/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.library.transformations;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.papyrus.designer.deployment.tools.Activator;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoCDP;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.transformation.tracing.barectf.library.BareCTFCmd;
import org.eclipse.papyrus.designer.transformation.tracing.barectf.library.BareCTFconfig;
import org.eclipse.papyrus.designer.transformation.tracing.barectf.library.InitCTF;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;

/**
 * Create the CTF configuration file as well as the initialization code
 * and a copy of the platform code
 */
public class BareCTFConfigInit implements IM2MTrafoCDP {

	protected static final String SRC_GEN = "src-gen"; //$NON-NLS-1$
	
	protected static final String CTF = "ctf"; //$NON-NLS-1$

	@Override
	public void applyTrafo(M2MTrafo trafo, Package deploymentPlan) throws TransformationException {
		try {
			IFolder srcGEN = TransformationContext.current.project.getFolder(SRC_GEN);
			if (!srcGEN.exists()) {
				srcGEN.create(true, true, null);
			}
			IFolder srcCTF = srcGEN.getFolder(CTF);
			if (!srcCTF.exists()) {
				srcCTF.create(true, true, null);
			}

			List<Element> elemsToTrace = BareCTFInstrument.getElementsToTrace();

			if (elemsToTrace.size() > 0) {
				// assure that folder does not get deleted
				TransformationContext.current.keepFiles.add(srcCTF.getFullPath().toString());

				BareCTFconfig.writeConfig(srcCTF, elemsToTrace);
				InitCTF.initCode(srcCTF, deploymentPlan);

				BareCTFCmd cmd = new BareCTFCmd();
				cmd.start(srcCTF);

				BareCTFconfig.injectURIs(elemsToTrace);
			}

		} catch (CoreException | IOException e) {
			Activator.log.error(e);
		}
	}

}

/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.barectf.library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;

public class BareCTFCmd {
	protected static final String CONFIG_YAML = "config.yaml"; //$NON-NLS-1$

	protected static final String BARECTFCMD = "barectf"; //$NON-NLS-1$

	ProcessBuilder pb;

	public BareCTFCmd() {
		pb = new ProcessBuilder();
		pb.command().add(BARECTFCMD);
		pb.command().add(CONFIG_YAML);
	}

	public Process start(IFolder folder) throws IOException, CoreException {
		pb.directory(folder.getLocation().toFile());
		Process process = pb.start();
		InputStream es = process.getErrorStream();
		if (es.read() != -1) {
			// input on error stream => check whether output contains package
			BufferedReader errors = new BufferedReader(new InputStreamReader(es));
			String errorMsg;
			StringBuffer errorMsgs = new StringBuffer();
			while (errors.ready() && (errorMsg = errors.readLine()) != null) {
				if (errorMsgs != null) {
					errorMsgs.append(errorMsg);
					errorMsgs.append("\n"); //$NON-NLS-1$
				}
			}
			Activator.log.warn(errorMsgs.toString());
			errors.close();
		}
		else {
			// keep files (only), if barectf completed without errors. 
			String baseFN = folder.getFullPath().toString() + StringConstants.SLASH;
			@SuppressWarnings("nls")
			String[] genFiles = {
					"barectf.c", "barectf.h",
					"barectf-bitfield.c", "barectf-bitfield.h",
					"metadata"
			};
			for (String genFile : genFiles) {
				TransformationContext.current.keepFiles.add(baseFN + genFile);
			}
		}

		// refresh folder
		folder.refreshLocal(IResource.DEPTH_ONE, null);
		return process;
	}

	public List<String> command() {
		return pb.command();
	}
}

#ifndef PKG_TRACING_ICONFIGURATORS
#define PKG_TRACING_ICONFIGURATORS

/************************************************************
 Pkg_iconfigurators package header
 ************************************************************/

#include "tracing/Pkg_tracing.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace tracing {
namespace iconfigurators {

// Types defined within the package
}// of namespace iconfigurators
} // of namespace tracing

/************************************************************
 End of Pkg_iconfigurators package header
 ************************************************************/

#endif

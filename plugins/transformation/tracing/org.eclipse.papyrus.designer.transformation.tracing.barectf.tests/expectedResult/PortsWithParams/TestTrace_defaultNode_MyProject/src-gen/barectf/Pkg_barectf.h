#ifndef PKG_BARECTF
#define PKG_BARECTF

/************************************************************
 Pkg_barectf package header
 ************************************************************/

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace barectf {

// Types defined within the package
}// of namespace barectf

/************************************************************
 End of Pkg_barectf package header
 ************************************************************/

#endif

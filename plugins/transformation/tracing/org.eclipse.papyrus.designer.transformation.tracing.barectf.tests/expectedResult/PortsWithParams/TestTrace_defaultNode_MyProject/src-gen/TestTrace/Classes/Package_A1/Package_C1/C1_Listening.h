// --------------------------------------------------------
// Code generated by Papyrus C++
// --------------------------------------------------------

#ifndef TESTTRACE_CLASSES_PACKAGE_A1_PACKAGE_C1_C1_LISTENING_H
#define TESTTRACE_CLASSES_PACKAGE_A1_PACKAGE_C1_C1_LISTENING_H

/************************************************************
 C1_Listening class header
 ************************************************************/

#include "TestTrace/Classes/Package_A1/Package_C1/Pkg_Package_C1.h"

#include "TestTrace/Interfaces/I_MyInterface_1.h"
#include "TestTrace/Types/Pkg_Types.h"

// derived using directives
using namespace TestTrace::Interfaces;
using namespace TestTrace::Types;

namespace TestTrace {
namespace Classes {
namespace Package_A1 {
namespace Package_C1 {

/************************************************************/
/**
 * 
 */
class C1_Listening: public I_MyInterface_1 {
public:

	/**
	 * 
	 * @param state 
	 */
	void MyOperation_1(BooleanType /*in*/state);

};
/************************************************************/
/* External declarations (package visibility)               */
/************************************************************/

/* Inline functions                                         */

} // of namespace Package_C1
} // of namespace Package_A1
} // of namespace Classes
} // of namespace TestTrace

/************************************************************
 End of C1_Listening class header
 ************************************************************/

#endif

#ifndef PKG_TESTTRACE
#define PKG_TESTTRACE

/************************************************************
 Pkg_TestTrace package header
 ************************************************************/

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

// Include from Include stereotype (header)
#include <iostream>
#include <vector>

// End of Include stereotype (header)
namespace TestTrace {

// Types defined within the package
}// of namespace TestTrace

/************************************************************
 End of Pkg_TestTrace package header
 ************************************************************/

#endif

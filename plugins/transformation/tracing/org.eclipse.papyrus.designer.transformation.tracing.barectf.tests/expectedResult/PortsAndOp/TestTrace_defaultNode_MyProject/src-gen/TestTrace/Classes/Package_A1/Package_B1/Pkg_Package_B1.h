#ifndef PKG_TESTTRACE_CLASSES_PACKAGE_A1_PACKAGE_B1
#define PKG_TESTTRACE_CLASSES_PACKAGE_A1_PACKAGE_B1

/************************************************************
 Pkg_Package_B1 package header
 ************************************************************/

#include "TestTrace/Classes/Package_A1/Pkg_Package_A1.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace TestTrace {
namespace Classes {
namespace Package_A1 {
namespace Package_B1 {

// Types defined within the package
}// of namespace Package_B1
} // of namespace Package_A1
} // of namespace Classes
} // of namespace TestTrace

/************************************************************
 End of Pkg_Package_B1 package header
 ************************************************************/

#endif

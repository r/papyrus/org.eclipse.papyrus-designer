#ifndef PKG_TESTTRACE_CLASSES
#define PKG_TESTTRACE_CLASSES

/************************************************************
 Pkg_Classes package header
 ************************************************************/

#include "TestTrace/Pkg_TestTrace.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace TestTrace {
namespace Classes {

// Types defined within the package
}// of namespace Classes
} // of namespace TestTrace

/************************************************************
 End of Pkg_Classes package header
 ************************************************************/

#endif

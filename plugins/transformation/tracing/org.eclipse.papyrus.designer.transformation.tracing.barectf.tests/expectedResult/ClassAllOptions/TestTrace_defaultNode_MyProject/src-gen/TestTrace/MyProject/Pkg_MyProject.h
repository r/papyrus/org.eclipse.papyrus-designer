#ifndef PKG_TESTTRACE_MYPROJECT
#define PKG_TESTTRACE_MYPROJECT

/************************************************************
 Pkg_MyProject package header
 ************************************************************/

#include "TestTrace/Pkg_TestTrace.h"

#ifndef _IN_
#define _IN_
#endif
#ifndef _OUT_
#define _OUT_
#endif
#ifndef _INOUT_
#define _INOUT_
#endif

/* Package dependency header include                        */

namespace TestTrace {
namespace MyProject {

// Types defined within the package
}// of namespace MyProject
} // of namespace TestTrace

/************************************************************
 End of Pkg_MyProject package header
 ************************************************************/

#endif

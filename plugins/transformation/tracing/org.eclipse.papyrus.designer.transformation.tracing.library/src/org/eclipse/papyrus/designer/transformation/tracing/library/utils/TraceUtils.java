/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.transformation.tracing.library.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.languages.common.profile.Codegen.TraceHint;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Typedef;
import org.eclipse.papyrus.designer.transformation.core.transformations.TransformationContext;
import org.eclipse.papyrus.moka.tracepoint.service.MarkerUtils;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAOperation;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAState;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TATransition;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TraceFeature;
import org.eclipse.papyrus.moka.tracepoint.service.TracepointConstants;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.profile.standard.Create;
import org.eclipse.uml2.uml.util.UMLUtil;

public class TraceUtils {

	/**
	 * Get all markers associated with the resource of the passed eObject
	 * 
	 * @param eObject
	 * @param markerType
	 * @return an array of markers
	 */
	public static IMarker[] getMarkersForEObject(EObject eObject, String markerType) {
		Resource resource = eObject.eResource();
		IFile file = WorkspaceSynchronizer.getFile(resource);
		if (file != null) {
			try {
				return file.findMarkers(markerType, true, IResource.DEPTH_INFINITE);
			} catch (CoreException e) {
			}
		}
		return new IMarker[0];
	}

	/**
	 * @param eObject
	 *            an element to trace. The passed eObject can be a copy of the original model.
	 *            return true, if the object has a trace marker, i.e. should be traced
	 */
	public static boolean hasTrace(EObject eObject) {
		if (getMarkerForTraceElement(eObject) != null) {
			// return true, if there is a trace marker on the object itself
			return true;
		}

		// now check, whether a related object has a trace marker
		if (eObject instanceof Operation) {
			return ImplicitTrace.traceOperation((Operation) eObject);
		} else if (eObject instanceof Port) {
			return ImplicitTrace.tracePort((Port) eObject);
		} else if (eObject instanceof State) {
			return ImplicitTrace.traceState((State) eObject);
		} else if (eObject instanceof Transition) {
			return ImplicitTrace.traceTransition((Transition) eObject);
		}
		return false;
	}

	/**
	 * @param operation
	 *            an operation
	 * @param port
	 *            a port. If non null, markers on the part take precedence over markers on the operation
	 * @param opAction
	 *            an action to check
	 * @return true, if passed action should be executed
	 */
	public static boolean traceOpOrPortAction(Operation operation, Port port, TAOperation opAction) {
		IMarker marker = (port != null) ? getMarkerForTraceElement(port) : null;
		if (marker == null) {
			marker = getMarkerForTraceElement(operation);
		}
		if (marker == null && operation.getClass_() != null) {
			// try on class
			marker = getMarkerForTraceElement(operation.getClass_());
		}
		return marker != null && isActionActive(marker, TraceFeature.Operation, opAction.ordinal());
	}

	/**
	 * @param state
	 *            a state
	 * @param stateAction
	 *            an action to check
	 * @return true, if passed action should be executed
	 */
	public static boolean traceStateAction(State state, TAState stateAction) {
		IMarker marker = getMarkerForTraceElement(state);
		if (marker == null) {
			marker = getMarkerForTraceElement(state.containingStateMachine().getContext());
		}
		return marker != null && isActionActive(marker, TraceFeature.State, stateAction.ordinal());
	}

	/**
	 * @param transition
	 *            a transition
	 * @param transitionAction
	 *            an action to check
	 * @return true, if passed action should be executed
	 */
	public static boolean traceTransitionAction(Transition transition, TATransition transitionAction) {
		IMarker marker = getMarkerForTraceElement(transition);
		if (marker == null) {
			marker = getMarkerForTraceElement(transition.containingStateMachine().getContext());
		}
		return marker != null && isActionActive(marker, TraceFeature.Transition, transitionAction.ordinal());
	}

	/**
	 * Check if a binary encode feature is active
	 * 
	 * @param marker
	 *            a marker
	 * @param feature
	 *            a trace feature
	 * @param ordinal
	 *            the ordinal of the enum literal we're interested in
	 * @return true, if feature is active
	 */
	public static boolean isActionActive(IMarker marker, TraceFeature feature, int ordinal) {
		if (marker != null) {
			String traceAction = marker.getAttribute(TracepointConstants.traceAction, StringConstants.EMPTY);
			int binaryEncoding = 0;
			if (!traceAction.equals(StringConstants.EMPTY)) {
				String option = TraceActions.getOptions(traceAction, feature);
				if (option == null) {
					// handle case that state and transitions are not prefixed
					option = traceAction;
				}
				try {
					binaryEncoding = Integer.parseInt(option);
				}
				catch (NumberFormatException ne) {
					// ignore (can happen, if trace-point action does not follow expected format)
				}
			} else {
				// after import, traceAction is recognized as integer - if not containing characters
				binaryEncoding = marker.getAttribute(TracepointConstants.traceAction, 0);
			}
			return (binaryEncoding & (1 << ordinal)) != 0;
		}
		return false;
	}

	/**
	 * Return a source EObject based on the assumption that the transformation preserves XML id
	 * This is useful in the trace context, as markers are applied to the source EObject
	 * 
	 * @param eObject
	 *            an element to trace. The passed eObject can be a copy of the original model.
	 * @return the element to trace in the source model if found, the same object otherwise
	 */
	public static EObject getSourceElement(EObject eObject) {
		Resource currentRes = eObject.eResource();
		Resource sourceRes = TransformationContext.initialSourceRoot.eResource();
		String fragment = currentRes.getURIFragment(eObject);
		EObject sourceEObj = sourceRes.getEObject(fragment);
		if (sourceEObj != null) {
			return sourceEObj;
		}
		return eObject;
	}

	public static IMarker getMarkerForTraceElement(EObject eObject) {
		IMarker markers[] = getMarkersForEObject(TransformationContext.initialSourceRoot, TracepointConstants.tracepointMarker);
		for (IMarker marker : markers) {
			if (MarkerUtils.isActive(marker)) {
				// explicitly pass resourceSet of eObject we want to compare. Otherwise, the marker utils would
				// load resources into its own resource set (leading to non-comparable eObjects)
				EObject eObjOfMarker = MarkerUtils.getEObjectOfMarker(TransformationContext.initialSourceRoot.eResource().getResourceSet(), marker);

				if (eObjOfMarker == eObject) {
					return marker;
				} else {
					EObject eObjCopy = eObjOfMarker;
					for (TransformationContext tc : TransformationContext.chainContexts()) {
						eObjCopy = tc.copier.get(eObjCopy);
						if (eObjCopy == eObject) {
							return marker;
						}
					}
				}
			}
		}
		return null;
	}

	/**
	 * Obtain a list of elements to trace
	 * 
	 * @param root
	 *            the root package
	 * @return a list of elements to trace
	 */
	public static List<Element> getElementsToTrace(Package root) {
		List<Element> list = new ArrayList<Element>();
		for (TreeIterator<EObject> iter = root.eAllContents(); iter.hasNext();) {
			EObject eObj = iter.next();
			if (TraceUtils.hasTrace(eObj) && !list.contains(eObj)) {
				if (eObj instanceof Port) {
					Port port = (Port) eObj;
					if (port.getProvideds().size() > 0) {
						// only add ports, if they provide an interface
						list.add(port);
					}
				} else if (eObj instanceof Operation ||
						eObj instanceof State ||
						eObj instanceof Transition ||
						eObj instanceof Class) {
					list.add((Element) eObj);
				}
			}
		}
		return list;
	}

	/**
	 * Return true, if passed type is a STL string (via C++ typedef)
	 * 
	 * @param type
	 */
	public static boolean isSTLstring(Type type) {
		Typedef typedef = UMLUtil.getStereotypeApplication(type, Typedef.class);
		if (typedef != null) {
			String def = typedef.getDefinition();
			if (def.equals("std::string")) { //$NON-NLS-1$
				return true;
			}
		}
		return false;
	}

	/**
	 * @param element
	 *            a UML element
	 * @return true, if the element has a specific trace hint
	 */
	public static boolean hasHint(Element element) {
		return StereotypeUtil.isApplied(element, TraceHint.class);
	}

	/**
	 * @param element
	 *            a UML element
	 * @return the "declaration" trace hint, if there is any
	 */
	public static String declarationHint(Element element) {
		TraceHint hint = UMLUtil.getStereotypeApplication(element, TraceHint.class);
		if (hint != null) {
			return hint.getDeclaration();
		}
		return null;
	}

	/**
	 * @param element
	 *            a UML element
	 * @return the "parameters" trace hint, if there is any (and non-empty), null otherwise
	 */
	public static String paramsHint(Element element) {
		TraceHint hint = UMLUtil.getStereotypeApplication(element, TraceHint.class);
		if (hint != null) {
			String params = hint.getParams();
			if (params != null && params.length() > 0) {
				return params;
			}
		}
		return null;
	}

	/**
	 * @param element
	 *            a UML element
	 * @return the "parameters" trace hint, if there is any (and non-empty), null otherwise
	 */
	public static String prepareHint(Element element) {
		TraceHint hint = UMLUtil.getStereotypeApplication(element, TraceHint.class);
		if (hint != null) {
			String prepare = hint.getPrepare();
			if (prepare != null && prepare.length() > 0) {
				return prepare;
			}
		}
		return null;
	}

	/**
	 * Only non static operations have an instanceName parameter. Constructors are excluded as well, as the instance
	 * name is typically not yet instantiated.
	 * 
	 * @param op
	 *            an operation
	 * @return true, if needs instance name
	 */
	public static boolean needInstanceName(Operation op) {
		if (op.isStatic()) {
			return false;
		}
		return !StereotypeUtil.isApplied(op, Create.class);
	}

	/**
	 * Filter parameter list
	 * 
	 * @param op
	 *            an operation
	 * @return the list of in and inout parameters
	 */
	public static List<Parameter> getInAndInout(Operation op) {
		List<Parameter> list = new ArrayList<Parameter>();
		for (Parameter param : op.getOwnedParameters()) {
			if (param.getDirection() == ParameterDirectionKind.IN_LITERAL ||
					param.getDirection() == ParameterDirectionKind.INOUT_LITERAL) {
				list.add(param);
			}
		}
		return list;
	}
}

/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.transformation.tracing.library;

import org.eclipse.papyrus.designer.deployment.tools.DepPlanUtils;
import org.eclipse.papyrus.designer.infra.base.StringConstants;
import org.eclipse.papyrus.designer.transformation.extensions.IInstanceConfigurator;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Property;

public class InstanceNameConfigurator implements IInstanceConfigurator {

	public static final String PROP_INSTANCE_NAME = "instanceName"; //$NON-NLS-1$

	/**
	 * Configure the passed trace instance
	 *
	 * @see IInstanceConfigurator
	 */
	@Override
	public void configureInstance(InstanceSpecification instance, Property componentPart, InstanceSpecification parentInstance) {
		String instanceName = instance.getName();
	
		DepPlanUtils.configureProperty(instance, PROP_INSTANCE_NAME, StringConstants.QUOTE + instanceName + StringConstants.QUOTE);
	}
}

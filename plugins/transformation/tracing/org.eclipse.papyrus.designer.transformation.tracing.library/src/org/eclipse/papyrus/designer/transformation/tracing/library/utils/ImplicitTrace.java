/*****************************************************************************
 * Copyright (c) 2021 CEA LIST.
 * 
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 * 
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.library.utils;

import org.eclipse.core.resources.IMarker;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAClass;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TAState;
import org.eclipse.papyrus.moka.tracepoint.service.TraceActions.TraceFeature;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.BehavioredClassifier;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.profile.standard.Create;
import org.eclipse.uml2.uml.profile.standard.Destroy;

/**
 * Handle implicit trace-points, for instance the option that a tracepoint on a class implies
 * to also trace all the operations (if the appropriate option is set)
 */
public class ImplicitTrace {

	/**
	 * check whether the owning class of an operation has trace instructions - either to trace
	 * all operations, constructors or destructors
	 * 
	 * @param op
	 *            an operation
	 * @return if the operation should be traced
	 */
	public static boolean traceOperation(Operation op) {
		Class clazz = op.getClass_();
		IMarker marker = TraceUtils.getMarkerForTraceElement(clazz);
		if (marker != null) {
			// class should be traced, check whether operations are included.
			if (TraceUtils.isActionActive(marker, TraceFeature.Class, TAClass.AllOperations.ordinal())) {
				return true;
			}
			if (StereotypeUtil.isApplied(op, Create.class) && TraceUtils.isActionActive(marker, TraceFeature.Class, TAClass.Creation.ordinal())) {
				return true;
			}
			if (StereotypeUtil.isApplied(op, Destroy.class) && TraceUtils.isActionActive(marker, TraceFeature.Class, TAClass.Destruction.ordinal())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * check whether the owning class of a port has trace instructions or the port is connected
	 * to a connector with trace information
	 * 
	 * @param port
	 *            a UML port
	 * @return if the port should be traced
	 */
	public static boolean tracePort(Port port) {
		if (port.getProvideds().size() > 0) {
			Class clazz = port.getClass_();
			IMarker marker = TraceUtils.getMarkerForTraceElement(clazz);
			if (marker != null) {
				// class should be traced, check whether ports are included.
				if (TraceUtils.isActionActive(marker, TraceFeature.Class, TAClass.AllPorts.ordinal())) {
					return true;
				}
			}
			// find connectors with trace option via inverse references (connector end
			// points to port via "role" attribute)
			for (Setting setting : UML2Util.getInverseReferences(port)) {
				EObject invRef = setting.getEObject();
				if (invRef instanceof ConnectorEnd) {
					ConnectorEnd connEnd = (ConnectorEnd) invRef;
					marker = TraceUtils.getMarkerForTraceElement(connEnd.getOwner());
					if (marker != null) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * check whether the owning class of a state has trace instructions
	 * 
	 * @param state
	 *            a state-machine state
	 * @return if the state should be traced
	 */
	public static boolean traceState(State state) {

		BehavioredClassifier clazz = state.containingStateMachine().getContext();
		IMarker marker = TraceUtils.getMarkerForTraceElement(clazz);
		if (marker != null) {
			// class should be traced, check whether states are included.
			if (TraceUtils.isActionActive(marker, TraceFeature.Class, TAClass.AllStates.ordinal())) {
				return true;
			}
		}
		return false;

	}

	/**
	 * check whether the owning class of an operation has trace instructions - either to trace
	 * all operations, constructors or destructors
	 * 
	 * @param transition
	 *            a transition
	 * @return if the operation should be traced
	 */
	public static boolean traceTransition(Transition transition) {
		// in case of a transition, check whether there are indications on the owning class,
		// or the source and target state
		BehavioredClassifier clazz = transition.containingStateMachine().getContext();
		IMarker marker = TraceUtils.getMarkerForTraceElement(clazz);
		if (TraceUtils.isActionActive(marker, TraceFeature.Class, TAClass.AllTransitions.ordinal())) {
			return true;
		}
		IMarker markers[] = {
				TraceUtils.getMarkerForTraceElement(transition.getSource()),
				TraceUtils.getMarkerForTraceElement(transition.getTarget())
		};
		for (IMarker stMarker : markers) {
			if (stMarker != null) {
				// class should be traced, check whether states are included.
				if (TraceUtils.isActionActive(stMarker, TraceFeature.State, TAState.Transition.ordinal())) {
					return true;
				}
			}
		}
		return false;
	}

}

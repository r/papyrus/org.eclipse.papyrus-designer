/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.tracing.library.transformations;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.languages.cpp.profile.C_Cpp.Include;
import org.eclipse.papyrus.designer.transformation.base.utils.TransformationException;
import org.eclipse.papyrus.designer.transformation.core.m2minterfaces.IM2MTrafoElem;
import org.eclipse.papyrus.designer.transformation.core.templates.TextTemplateBinding;
import org.eclipse.papyrus.designer.transformation.library.transformations.MergeClass;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.transformation.tracing.library.utils.TraceUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.BehaviorUtils;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Old and imcomplete implementation of an LTTng based CTF tracing. Currently
 * unused and only kept in case of parts of it might be need for a new LTTng implementation
 * (in addition to bareCTF based tracing).
 */
public class LTTngTrafo extends MergeClass implements IM2MTrafoElem {

	public void instrumentOperation(M2MTrafo trafo, Operation op) throws TransformationException {
		Class templateClass = trafo.getBase_Class();
		for (Operation templateOp : templateClass.getOwnedOperations()) {
			// create a TemplateInstantiation
			String templateBody = TextTemplateBinding.bind(BehaviorUtils.body(templateOp), op, null);
			String body = BehaviorUtils.body(op);
			BehaviorUtils.set(op, templateBody + body);
		}
	}
	
	@Override
	public void transformElement(M2MTrafo trafo, Element element) throws TransformationException {
		if (element instanceof Class) {
			boolean first = true;
			Class clazz = (Class) element;
			// work on copy
			EList<Operation> opList = new BasicEList<Operation>(clazz.getOwnedOperations());
			for (Operation operation : opList) {
				if (TraceUtils.hasTrace(operation)) {
					if (first) {
						// merge trace
						super.transformElement(trafo, element);
						// handle C++ Include stereotype
						Include cppIncludeOnTemplate = UMLUtil.getStereotypeApplication(trafo.getBase_Class(), Include.class);
						if (cppIncludeOnTemplate != null) {
							Include cppOnElement = UMLUtil.getStereotypeApplication(clazz, Include.class);
							if (cppOnElement == null) {
								cppOnElement = StereotypeUtil.applyApp(clazz, Include.class);
							}
							// merge attributes of C++ include stereotype
							if (cppIncludeOnTemplate.getHeader() != null) {
								cppOnElement.setHeader(cppOnElement.getHeader() +
										TextTemplateBinding.bind(cppIncludeOnTemplate.getHeader(), clazz, null));
							}
							if (cppIncludeOnTemplate.getPreBody() != null) {
								cppOnElement.setPreBody(cppOnElement.getPreBody() +
										TextTemplateBinding.bind(cppIncludeOnTemplate.getPreBody(), clazz, null));
							}
							if (cppIncludeOnTemplate.getBody() != null) {
								cppOnElement.setBody(cppOnElement.getBody() +
										TextTemplateBinding.bind(cppIncludeOnTemplate.getBody(), clazz, null));
							}
						}
						first = false;
					}
					// inject trace code into beginning of method
					instrumentOperation(trafo, operation);
				}			
			}
			// Tracepoint on class, need to instrument constructor & destructor as well as all operations		
		}
		if (TraceUtils.hasTrace(element)) {
			if (element instanceof Operation) {
				// Tracepoint on operation, need to instrument operation
				Operation op = (Operation) element;
				System.err.println(op);
			}
		}
	}
}

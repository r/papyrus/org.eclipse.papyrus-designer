/*****************************************************************************
 * Copyright (c) 2013 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.base.utils;

import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.designer.transformation.base.Activator;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLFactory;


/**
 * Manages a model (and the associated resource). Allows saving this model. It uses its own static resource set.
 */
public class ModelManagement {

	public static final String TEMP_UML = "temp.uml"; //$NON-NLS-1$

	public static final String UML = ".uml"; //$NON-NLS-1$

	/**
	 * A resource set for UML models that is managed by this class
	 */
	private static ResourceSet resourceSet = null;

	/**
	 * A model managed by this instance
	 */
	private Package model;

	/**
	 * The resource associated with this model
	 */
	private Resource resource;

	/**
	 * Handle updates of resources within this resource set that are done outside (we are not
	 * interested in resource-updates caused by saves within this class). A typical use case is that
	 * an imported model library is modified by a Papyrus editor. We want to assure that it gets
	 * removed from this resource set, assuring that an up-to-date copy will be loaded again when
	 * needed.
	 */
	static class ResourceDeltaVisitor implements IResourceDeltaVisitor {

		static ResourceDeltaVisitor instance = new ResourceDeltaVisitor();

		@Override
		public boolean visit(final IResourceDelta delta) {
			if (delta.getResource().getType() == IResource.FILE) {
				if (delta.getKind() == IResourceDelta.REMOVED || delta.getKind() == IResourceDelta.CHANGED) {
					URI resourceURI = URI.createPlatformResourceURI(delta.getFullPath().toString(), true);
					final ResourceSet modelSet = ModelManagement.getResourceSet();
					Resource resource = modelSet.getResource(resourceURI, false);
					if (resource == null) {
						URI pluginURI = URI.createPlatformPluginURI(delta.getFullPath().toString(), true);
						resource = modelSet.getResource(pluginURI, false);
					}
					if (resource != null) {
						resource.unload();
						modelSet.getResources().remove(resource);
					}
				}
				return false;
			}
			return true;
		}
	}

	static class ResourceChangeListener implements IResourceChangeListener {

		/**
		 * indicated whether saving is active. We don't want to track updates
		 * caused by a "save" in this class.
		 */
		private static boolean isSaving = false;

		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			IResourceDelta delta = event.getDelta();
			try {
				if (!isSaving) {
					delta.accept(ResourceDeltaVisitor.instance);
				}
			} catch (CoreException exception) {
				Activator.log.error(exception);
			}
		}
	};

	/**
	 * Create a new model and associate it with a temporary
	 * resource
	 */
	public ModelManagement(Package model) {
		getResourceSet();
		this.model = model;
		resource = resourceSet.createResource(URI.createURI(TEMP_UML));
		resource.getContents().add(model);
	}

	/**
	 * Create a new model and associate it with a temporary
	 * resource
	 */
	public ModelManagement() {
		this(UMLFactory.eINSTANCE.createModel());
	}

	public void dispose() {
		if (model != null) {
			model.destroy();
			model = null;
		}
		resourceSet.getResources().remove(resource);
	}

	/**
	 * provide access to the model
	 * 
	 * @return the model managed by this instance of model manager
	 */
	public Package getModel() {
		return model;
	}

	/**
	 * Set the URI of the managed resource via a string
	 * 
	 * @param path
	 */
	public void setURI(String path) {
		URI uri = URI.createURI(path);
		setURI(uri);
	}

	/**
	 * Set the URI of the managed resource
	 * 
	 * @param uri
	 */
	public void setURI(URI uri) {
		resource.setURI(uri);
	}

	/**
	 * Set the URI model within a given project, folder and postfix
	 * 
	 * @param project
	 * @param modelFolder
	 * @param modelPostfix
	 */
	public void setURI(IProject project, String modelFolder, String modelPostfix) {
		String path = this.getPath(project, modelFolder, this.getModel().getName() + modelPostfix);
		setURI(path);
	}

	/**
	 * Save a model
	 */
	public void save() {

		try {
			ResourceChangeListener.isSaving = true;
			resource.save(null);
			ResourceChangeListener.isSaving = false;
		} catch (IOException e) {
			Activator.log.error(e);
		}
	}

	/**
	 * Return a path based on project, folder and file name
	 *
	 * @param project
	 *            an existing project
	 * @param subFolder
	 *            a subfolder within the project (will be created, if it does not exist)
	 *            if null, the project will be saved in the root of the project
	 * @param filename
	 *            the name of the file or null (in his case, the name of the
	 *            model with the postfix .uml is used)
	 *
	 * @return The access path to a file
	 */
	public String getPath(IProject project, String subFolder, String filename) {
		IFile file;
		if (filename == null) {
			filename = model.getName() + UML;
		}
		if (subFolder != null) {
			IFolder ifolder = project.getFolder(subFolder);
			if (!ifolder.exists()) {
				try {
					ifolder.create(false, true, null);
				} catch (CoreException e) {
					Activator.log.error(e);
				}
			}
			file = ifolder.getFile(filename);
		} else {
			file = project.getFile(filename);
		}
		return file.getFullPath().toString();
	}

	/**
	 * Create a new empty model from an existing model that applies the same
	 * profiles and has the same imports
	 *
	 * @param name
	 *            the name of the new model
	 * @return the model-management instance for the new model (use getModel() to obtain the actual model)
	 * @throws TransformationException
	 */
	public static ModelManagement createNewModel(String name) throws TransformationException {
		ModelManagement mm = new ModelManagement();
		Package newModel = mm.getModel();
		newModel.setName(name);

		return mm;
	}

	/**
	 * Create a new empty model from an existing model that applies the same
	 * profiles and has the same imports. Sets the URI according to the existing
	 * model with a temp-model extension
	 * 
	 * @param existingModel
	 *            an existing model that must have an eResource
	 * @return a new ModelManagement instance
	 * @throws TransformationException
	 */
	public static ModelManagement createNewModel(Package existingModel) throws TransformationException {
		ModelManagement mm = createNewModel(existingModel.getName());
		mm.setURI(getTempURI(existingModel));
		return mm;
	}

	/**
	 * return the used resource set (a singleton)
	 */
	public static ResourceSet getResourceSet() {
		if (resourceSet == null) {
			resourceSet = new ResourceSetImpl();
			ResourcesPlugin.getWorkspace().addResourceChangeListener(new ResourceChangeListener(), IResourceChangeEvent.POST_CHANGE);
		}
		return resourceSet;
	}

	/**
	 * Return the URI string of a newly created transformation model.
	 * Replaces the .uml postfix with .temp.uml
	 * 
	 * @param eObj
	 *            An eObject
	 * @return the temporary URI of the intermediate model
	 */
	public static String getTempURI(EObject eObj) {
		String uriStr = eObj.eResource().getURI().toString();
		if (uriStr.endsWith(UML)) {
			uriStr = uriStr.substring(0, uriStr.length() - 3);
		}
		return uriStr + TEMP_UML;
	}
}

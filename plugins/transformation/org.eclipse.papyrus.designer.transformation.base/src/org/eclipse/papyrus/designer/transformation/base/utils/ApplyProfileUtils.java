/*****************************************************************************
 * Copyright (c) 2020 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.base.utils;

import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.designer.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;

/**
 * Small utility class to apply a profile
 */
public class ApplyProfileUtils {
	
	/**
	 * Apply a profile
	 * 
	 * @param element
	 *            an arbitrary element of a model (the profile will be applied to the root package of the model)
	 * @param profileURI
	 *            a profile URI
	 * @throws TransformationException
	 *             if the profile cannot be loaded (or is actually not a profile)
	 */
	public static void applyProfile(Element element, URI profileURI) throws TransformationException {
		Package profile = PackageUtil.loadPackage(profileURI,
				element.eResource().getResourceSet());
		if (profile instanceof Profile) {
			PackageUtil.getRootPackage(element).applyProfile((Profile) profile);
		} else {
			throw new TransformationException(String.format(
					"Cannot find profile with URI %s", profileURI));
		}
	}
}

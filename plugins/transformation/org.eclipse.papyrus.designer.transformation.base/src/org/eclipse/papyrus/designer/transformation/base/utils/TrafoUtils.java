/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.base.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.papyrus.designer.deployment.profile.Deployment.DeploymentPlan;
import org.eclipse.papyrus.designer.uml.tools.utils.ElementUtils;
import org.eclipse.papyrus.designer.transformation.base.Messages;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.ExecuteTrafoChain;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafo;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.M2MTrafoChain;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.util.UMLUtil;

/**
 * Transformation utility functions
 */
public class TrafoUtils {

	public static final String TRAFOS_M2MTRANSFORMATIONS_SIMPLE = "trafos::m2mtransformations::Simple"; //$NON-NLS-1$

	public static final String TRAFOS_M2MTRANSFORMATIONS_STANDARD = "trafos::m2mtransformations::Standard"; //$NON-NLS-1$

	public static M2MTrafoChain getTransformationChain(Package cdp) {
		M2MTrafoChain chain = null;
		boolean isDeploymentPlan = false;

		if (cdp != null) {
			ExecuteTrafoChain execTrafoChain = UMLUtil.getStereotypeApplication(cdp, ExecuteTrafoChain.class);
			isDeploymentPlan = execTrafoChain instanceof DeploymentPlan;
			if (execTrafoChain != null) {
				chain = execTrafoChain.getChain();
			}
		}
		if (chain == null) {

			// first load standard M2M library into resource set
			ElementUtils.loadPackage(StdModelLibs.DESIGNER_TRAFOLIB_URI, cdp);
			NamedElement defaultChainNE = ElementUtils.getQualifiedElementFromRS(cdp,
					isDeploymentPlan ? TRAFOS_M2MTRANSFORMATIONS_STANDARD : TRAFOS_M2MTRANSFORMATIONS_SIMPLE);
			if (defaultChainNE != null) {
				// chain null and default chain could be found.
				chain = UMLUtil.getStereotypeApplication(defaultChainNE, M2MTrafoChain.class);
			}
			if (chain == null) {
				throw new RuntimeException(Messages.TrafoUtils_CANT_FIND_DEFAULT_CHAIN);
			}
		}
		return chain;
	}

	/**
	 * Obtain additional transformations from a deployment plan
	 * 
	 * @param cdp
	 *            a deployment plan or package applying the ExecuteTrafoChain
	 * @return a list of additional transformations
	 */
	public static List<M2MTrafo> getAdditionalTransformations(Package cdp) {
		if (cdp != null) {
			ExecuteTrafoChain execTrafoChain = UMLUtil.getStereotypeApplication(cdp, ExecuteTrafoChain.class);
			if (execTrafoChain != null) {
				List<M2MTrafo> additionalTransformations = new ArrayList<M2MTrafo>();
				additionalTransformations.addAll(execTrafoChain.getAdditionalTrafos());
				for (M2MTrafoChain additionalChain : execTrafoChain.getAdditionalChains()) {
					for (Property attribute : additionalChain.getBase_Class().getAllAttributes()) {
						M2MTrafo trafo = UMLUtil.getStereotypeApplication(attribute.getType(), M2MTrafo.class);
						additionalTransformations.add(trafo);
					}
				}
				return additionalTransformations;
			}
		}
		return Collections.emptyList();
	}
}

/*****************************************************************************
 * Copyright (c) 2012 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/
package org.eclipse.papyrus.designer.transformation.base.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.papyrus.designer.transformation.base.Activator;

/**
 * Class used to initialize default preference values.
 */
public class TransfoBasePreferenceInitializer extends AbstractPreferenceInitializer {

	/*
	 * (non-Javadoc)
	 *
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IEclipsePreferences prefs = DefaultScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		prefs.putBoolean(TransfoBasePreferenceConstants.P_SHOW_PROV_REQ_AS_PORT_ICONS_KEY, TransfoBasePreferenceConstants.P_SHOW_PROV_REQ_AS_PORT_ICONS_DVAL);
		prefs.putBoolean(TransfoBasePreferenceConstants.P_ALL_ATTRIBUTES_ARE_CONFIG_ATTRIBUTES_KEY, TransfoBasePreferenceConstants.P_ALL_ATTRIBUTES_ARE_CONFIG_ATTRIBUTES_DVAL);
		prefs.putBoolean(TransfoBasePreferenceConstants.P_TREAT_NONE_AS_COMPOSITE_KEY, TransfoBasePreferenceConstants.P_TREAT_NONE_AS_COMPOSITE_DVAL);
		prefs.put(TransfoBasePreferenceConstants.P_CODE_GEN_PREFIX_KEY, TransfoBasePreferenceConstants.P_CODE_GEN_PREFIX_DVAL);
	}
}

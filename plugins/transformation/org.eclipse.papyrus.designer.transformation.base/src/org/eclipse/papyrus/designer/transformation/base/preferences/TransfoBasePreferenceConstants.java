/*****************************************************************************
 * Copyright (c) 2016 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  CEA LIST - Initial API and implementation
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.base.preferences;

/**
 * Constant definitions for plug-in preferences
 */
public class TransfoBasePreferenceConstants {

	/**
	 * For attributes, the default "aggregation kind" value is "none". When this option is set
	 * it will be treated as composite
	 */
	public static final String P_TREAT_NONE_AS_COMPOSITE_KEY = "treatNoneAsComposite"; //$NON-NLS-1$
	public static final boolean P_TREAT_NONE_AS_COMPOSITE_DVAL = false;

	public static final String P_ALL_ATTRIBUTES_ARE_CONFIG_ATTRIBUTES_KEY = "allAttributesAreConfigAttributes"; //$NON-NLS-1$
	public static final boolean P_ALL_ATTRIBUTES_ARE_CONFIG_ATTRIBUTES_DVAL = false;

	public static final String P_SHOW_PROV_REQ_AS_PORT_ICONS_KEY = "showProvReqAsPortIcons"; //$NON-NLS-1$
	public static final boolean P_SHOW_PROV_REQ_AS_PORT_ICONS_DVAL = false;

	public static final String P_CODE_GEN_PREFIX_KEY = "codeGenPrefix"; //$NON-NLS-1$
	public static final String P_CODE_GEN_PREFIX_DVAL = "// generated with SW Designer toolchain"; //$NON-NLS-1$
}

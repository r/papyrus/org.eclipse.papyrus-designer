package org.eclipse.papyrus.designer.transformation.base.preferences;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.papyrus.designer.transformation.base.Activator;

public class TransfoBasePreferenceUtils {
	/**
	 * Return true, if aggregation type "none" should be treated in the same way
	 * as aggregation kind "composite". This information is used to create instances
	 * in compositions
	 * 
	 * @return true, if aggregation type "none" should be as "composite"
	 */
	public static boolean treatNoneAsComposite() {
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		return prefs.getBoolean(TransfoBasePreferenceConstants.P_TREAT_NONE_AS_COMPOSITE_KEY, TransfoBasePreferenceConstants.P_TREAT_NONE_AS_COMPOSITE_DVAL);
	}

	/**
	 * @return true, if all attributes of a class are considered as configuration attributes.
	 */
	public static boolean allAttributesAreConfigAttributs() {
		IEclipsePreferences prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		return prefs.getBoolean(TransfoBasePreferenceConstants.P_ALL_ATTRIBUTES_ARE_CONFIG_ATTRIBUTES_KEY, TransfoBasePreferenceConstants.P_ALL_ATTRIBUTES_ARE_CONFIG_ATTRIBUTES_DVAL);
	}

}

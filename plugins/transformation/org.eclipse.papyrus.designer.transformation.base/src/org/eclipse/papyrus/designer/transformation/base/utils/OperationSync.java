/*****************************************************************************
 * Copyright (c) 2013, 2022 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.base.utils;

import org.eclipse.papyrus.designer.uml.tools.utils.StUtils;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Type;

public class OperationSync {

	/**
	 * synchronize source and target operation
	 * (remove all parameters, copy afterwards)
	 *
	 * @param sourceOp
	 *            the source operation
	 * @param targetOp
	 *            the target operation
	 */
	public static void sync(Operation sourceOp, Operation targetOp) {
		if (targetOp != null) {
			CopyUtils.copyFeatureModifiers(sourceOp, targetOp);
			// ordered and unique are derived from ret-parameter
			targetOp.setIsQuery(sourceOp.isQuery());
			targetOp.setIsAbstract(sourceOp.isAbstract());
			targetOp.setName(sourceOp.getName());
			targetOp.getOwnedParameters().clear();
			for (Parameter parameter : sourceOp.getOwnedParameters()) {
				Type type = parameter.getType();
				Parameter newParameter = targetOp.createOwnedParameter(parameter.getLabel(), type);
				newParameter.setDirection(parameter.getDirection());
				CopyUtils.copyMultElemModifiers(parameter, newParameter);
				StUtils.copyStereotypes(parameter, newParameter);
			}
		}
		StUtils.copyStereotypes(sourceOp, targetOp);
	}

}

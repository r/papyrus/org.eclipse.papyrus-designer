/*****************************************************************************
 * Copyright (c) 2013, 2017 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.transformation.base.utils;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.designer.transformation.base.preferences.TransfoBasePreferenceUtils;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Property;

/**
 * Utility function that returns the parts of a composite class.
 */
public class PartsUtil {

	/**
	 * Return the parts of a composite class. In contrast to the standard UML2 function, it will also
	 * return attributes with aggregation kind NONE (i.e. all != SHARED), depending on a user preference.
	 *
	 * @param implementation
	 *            A composite class
	 * @return the contained parts
	 */
	public static EList<Property> getParts(Class implementation) {
		if (TransfoBasePreferenceUtils.treatNoneAsComposite()) {
			EList<Property> parts = new BasicEList<>();
			for (Property part : implementation.getAttributes()) {
				if (part.getAggregation() != AggregationKind.SHARED_LITERAL) {
					parts.add(part);
				}
			}
			return parts;
		} else {
			return implementation.getParts();
		}
	}
}

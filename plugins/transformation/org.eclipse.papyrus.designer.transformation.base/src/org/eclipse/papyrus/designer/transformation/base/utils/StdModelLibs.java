/*******************************************************************************
 * Copyright (c) 2013 CEA LIST.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     CEA LIST - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.designer.transformation.base.utils;

import org.eclipse.emf.common.util.URI;

/**
 * This class adds models via a given URI to a resourceSet. It also defines common URIs
 */
public class StdModelLibs {

	public static final URI DESIGNER_TRAFOLIB_URI = URI.createURI("pathmap://DML_TRAFO/trafos.uml"); //$NON-NLS-1$

	public static final URI DESIGNER_MARTE_CALLS_URI = URI.createURI("pathmap://DML_MARTE/marte.uml"); //$NON-NLS-1$

	public static final URI FCM_PROFILE_URI = URI.createURI("pathmap://FCM_PROFILES/FCM.profile.uml"); //$NON-NLS-1$

	public static final URI MARTE_PROFILE_URI = URI.createURI("pathmap://Papyrus_PROFILES/MARTE.profile.uml");//$NON-NLS-1$
	
	public static final URI DEP_PROFILE_URI = URI.createURI("pathmap://DEP_PROFILE/Deployment.profile.uml"); //$NON-NLS-1$

	public static final URI TRAFO_PROFILE_URI = URI.createURI("pathmap://TRAFO_PROFILE/Transformation.profile.uml"); //$NON-NLS-1$
}

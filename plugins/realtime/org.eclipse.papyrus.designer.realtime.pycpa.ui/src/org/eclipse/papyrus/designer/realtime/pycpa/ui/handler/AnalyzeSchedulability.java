package org.eclipse.papyrus.designer.realtime.pycpa.ui.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.papyrus.designer.realtime.pycpa.m2m.executors.RTMLToPyCPATransformExecutor;
import org.eclipse.papyrus.designer.realtime.pycpa.m2t.executors.PyCPAModelToPythonCodeTransformExecutor;
import org.eclipse.papyrus.designer.realtime.pycpa.ui.Activator;
import org.eclipse.papyrus.designer.realtime.pycpa.ui.json.JsonBackpropagation;
import org.eclipse.papyrus.designer.realtime.pycpa.ui.python.PythonExecutor;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.uml2.uml.Activity;

/**
 * Handler class for generating PyCPA scripts from MARTE model
 */
public class AnalyzeSchedulability extends CmdHandler {

	/**
	 * Execute the generation MoCC code from the selected UML Class.
	 * 
	 * {@inheritDoc}
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		updateSelectedEObject();

		if (selectedEObject instanceof Activity) {
			Activity activity = (Activity) selectedEObject;
			
			// Eclipse resource creation
			URI uri = activity.eResource().getURI();
			if (uri.segmentCount() < 2) {
				return null;
			}
			
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			IWorkspaceRoot root = workspace.getRoot();
			IProject project = root.getProject(uri.segment(1));
			IFile umlModelFile = project.getFile(activity.eResource().getURI().lastSegment().toString());
			IContainer container = umlModelFile.getParent();
			
			// M2M
			RTMLToPyCPATransformExecutor.run(activity);

			// M2T
			//String xxx = activity.eResource().getURI().trimSegments(1).appendSegment(activity.getName()).appendFileExtension("pycpa").toFileString();
			//IPath pycpaModelPath = new Path(activity.eResource().getURI().trimSegments(1).appendSegment(activity.getName()).appendFileExtension("pycpa").toFileString());
			//IResource pycpaModel = root.findMember(pycpaModelPath);
			//IFile pycpaModelFile = (IFile) pycpaModel;
			
			IFile pycpaModelFile = container.getFile(new Path(activity.getName() + ".pycpa"));
			if (pycpaModelFile.exists()) {
				URI pycpaModelUri = URI.createPlatformResourceURI(pycpaModelFile.getFullPath().toString(), true);
				PyCPAModelToPythonCodeTransformExecutor generator = new PyCPAModelToPythonCodeTransformExecutor(
						pycpaModelUri,
						container,
						new ArrayList<String>());
				try {
					generator.doGenerate(null);
				} catch (IOException e) {
					Activator.log.error(e);
				}

				// Refresh
				try {
					project.refreshLocal(IResource.DEPTH_INFINITE, null);
				} catch (CoreException e) {
					Activator.log.error(e);
				}

				// Launch Python analysis script
				IFile pyAnalysisFile = container.getFile(new Path(activity.getName() + "_analysis" + ".py"));
				PythonExecutor.executePython(pyAnalysisFile, event);

				// Read JSON and update MARTE
				IFile jsonResultsFile = container.getFile(new Path(activity.getName() + "_result" + ".json"));
				JsonBackpropagation.backpropagateResult(jsonResultsFile, activity);

				// Save MARTE model
				try {
					activity.eResource().save(getDefaultSaveOptions());
				} catch (IOException e) {
					Activator.log.error(e);
				}
			}
			
		}
		return null;
	}

	public static Map<Object, Object> getDefaultSaveOptions() {
		Map<Object, Object> saveOptions = new HashMap<Object, Object>();

		// default save options.
		saveOptions.put(XMLResource.OPTION_DECLARE_XML, Boolean.TRUE);
		saveOptions.put(XMLResource.OPTION_PROCESS_DANGLING_HREF, XMLResource.OPTION_PROCESS_DANGLING_HREF_DISCARD);
		saveOptions.put(XMLResource.OPTION_SCHEMA_LOCATION, Boolean.TRUE);
		saveOptions.put(XMIResource.OPTION_USE_XMI_TYPE, Boolean.TRUE);
		saveOptions.put(XMLResource.OPTION_SAVE_TYPE_INFORMATION, Boolean.TRUE);
		saveOptions.put(XMLResource.OPTION_SKIP_ESCAPE_URI, Boolean.FALSE);
		saveOptions.put(XMLResource.OPTION_ENCODING, "UTF-8");
		saveOptions.put(XMLResource.OPTION_USE_FILE_BUFFER, true);
		saveOptions.put(XMLResource.OPTION_FLUSH_THRESHOLD, 4 * 1024 * 1024); // 4 MB Buffer

		// see bug 397987: [Core][Save] The referenced plugin models are saved using
		// relative path
		saveOptions.put(XMLResource.OPTION_URI_HANDLER,
				new org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl.PlatformSchemeAware());

		return saveOptions;
	}
}

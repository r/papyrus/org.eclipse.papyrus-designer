package org.eclipse.papyrus.designer.realtime.pycpa.ui.json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.json.provisonnal.com.eclipsesource.json.JsonObject;
import org.eclipse.json.provisonnal.com.eclipsesource.json.JsonValue;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.SAM.SaStep;
import org.eclipse.papyrus.designer.realtime.pycpa.ui.Activator;
import org.eclipse.papyrus.uml.service.types.element.UMLElementTypes;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.util.UMLUtil;

public class JsonBackpropagation {

	public static void backpropagateResult(IFile jsonResultsFile, Activity marteActivity) {
		try {
			FileReader jsonReader = new FileReader(jsonResultsFile.getRawLocation().makeAbsolute().toFile());
			JsonObject jsonResult = JsonObject.readFrom(jsonReader);
			for (JsonValue jsonTask : jsonResult.get("task").asArray()) {
				String jsonTaskName = jsonTask.asObject().getString("name", "");
				int jsonWcrt = jsonTask.asObject().getInt("wcrt", 0);
				int jsonBcrt = jsonTask.asObject().getInt("bcrt", 0);

				if (!jsonTaskName.isEmpty()) {
					for (Element activityOwnedElement : marteActivity.allOwnedElements()) {
						if (activityOwnedElement instanceof ActivityPartition && ((ActivityPartition) activityOwnedElement).getName().equals(jsonTaskName)) {
							SaStep saStep = UMLUtil.getStereotypeApplication(activityOwnedElement, SaStep.class);
							if (saStep != null) {
								ConfigureRequest configureRequest = new ConfigureRequest(activityOwnedElement, UMLElementTypes.ACTIVITY_PARTITION);
								ICommand configureCommand = new ConfigureElementCommand(configureRequest) {
									@Override
									protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
										ActivityPartition activityPartition = (ActivityPartition) configureRequest.getElementToConfigure();
										SaStep saStep = UMLUtil.getStereotypeApplication(activityOwnedElement, SaStep.class);
										saStep.getRespT().clear();
										saStep.getRespT().add("wcrt=" + jsonWcrt);
										saStep.getRespT().add("bcrt=" + jsonBcrt);
										return CommandResult.newOKCommandResult(activityPartition);
									}
								};
								try {
									configureCommand.execute(null, null);
								} catch (ExecutionException e) {
									Activator.log.error(e);
								}
							}
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			Activator.log.error(e);
		} catch (IOException e) {
			Activator.log.error(e);
		} catch (UnsupportedOperationException e) {
			Activator.log.error(e);
		}
	}

}

/*****************************************************************************
 * Copyright (c) 2019 COMMISSARIAT A L’ENERGIE ATOMIQUE  ENERGIES ALTERNATIVES
 * - CEA LIST
 * You cannot redistribute and/or modify it in any way, shape or form
 * and under any circumstance, without prior written authorization from CEA LIST
 * This program and the accompanying materials are the property of CEA LIST,
 * their use is subject to specific license agreement with CEA LIST.
 * All rights reserved.
 *
 * Contributors:
 *  CEA LIST - initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.designer.realtime.pycpa.ui.python;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

public class PythonExecutor {
	//private IReplEngine engine;

	public static void executePython(IFile file, ExecutionEvent event) {
		if (file != null && file.exists()) {
			String fileOsPath = file.getRawLocation().makeAbsolute().toOSString();
			try {
				Process p = Runtime.getRuntime().exec("python \"" + fileOsPath + "\" --propagation=jitter_offset");

				String line;
				BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
				while ((line = in.readLine()) != null) {
					System.out.println(line);
				}
				in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
				while ((line = in.readLine()) != null) {
					System.err.println(line);
					if (line.startsWith("ERROR") || line.startsWith("WARNING")) {
						IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
						Shell shell = window.getShell();
						if (shell != null) {
							MessageBox dialog = null;
							if (line.startsWith("ERROR: Deadline violated for task")) {
								dialog = new MessageBox(shell, SWT.ERROR | SWT.OK);
								dialog.setText("Missed deadline");
								dialog.setMessage(line);
							} else if (line.startsWith("WARNING: load too high:")) {
								dialog = new MessageBox(shell, SWT.ICON_WARNING | SWT.OK);
								dialog.setText("Load too high");
								dialog.setMessage(line);
							}
							if (dialog != null) {
								dialog.open();
							}
						}
					}
					
				}
				in.close();
			} catch (IOException e) {				
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}	
		}
	}
}

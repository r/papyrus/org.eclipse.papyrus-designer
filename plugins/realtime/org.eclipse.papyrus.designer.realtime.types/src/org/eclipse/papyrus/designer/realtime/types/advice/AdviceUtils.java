/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.realtime.types.advice;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * @author SL245932
 *
 */
public interface AdviceUtils {
	public static Package getPlatformResourcesPackage(Element element) {
		if (element != null) {
			Model model = element.getModel();
			if (model != null) {
				Package platformResourcesPackage = (Package) model.getOwnedMember("PlatformResources", true, UMLPackage.eINSTANCE.getPackage());
				if (platformResourcesPackage == null) {
					platformResourcesPackage = (Package) model.createPackagedElement("PlatformResources", UMLPackage.eINSTANCE.getPackage());
				}
				return platformResourcesPackage;
			}
		}
		return null;
	}
}

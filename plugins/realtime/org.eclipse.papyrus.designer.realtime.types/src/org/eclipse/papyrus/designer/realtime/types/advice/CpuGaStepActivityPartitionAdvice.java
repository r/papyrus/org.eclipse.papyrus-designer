/*****************************************************************************
 * Copyright (c) 2018 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Shuai Li (CEA LIST) <shuai.li@cea.fr> - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.realtime.types.advice;

import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.ConfigureElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.edithelper.AbstractEditHelperAdvice;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyDependentsRequest;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaExecHost;
import org.eclipse.papyrus.MARTE.MARTE_AnalysisModel.GQAM.GaStep;
import org.eclipse.papyrus.MARTE.MARTE_DesignModel.HRM.HwLogical.HwComputing.HwComputingResource;
import org.eclipse.papyrus.MARTE.MARTE_Foundations.GRM.Scheduler;
import org.eclipse.papyrus.MARTE_Library.GRM_BasicTypes.SchedPolicyKind;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLUtil;

public class CpuGaStepActivityPartitionAdvice extends AbstractEditHelperAdvice {
	@Override
	protected ICommand getAfterConfigureCommand(ConfigureRequest request) {
		ICommand configureCommand = new ConfigureElementCommand(request) {
			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
				ActivityPartition element = (ActivityPartition) request.getElementToConfigure();
				element.setName(element.getName().replaceAll("GaStep", "CPU"));
				element.setName(element.getName().replaceAll("ActivityPartition", "CPU"));
				GaStep gaStep = UMLUtil.getStereotypeApplication(element, GaStep.class);
				if (gaStep != null) {
					Package platformResourcesPackage = AdviceUtils.getPlatformResourcesPackage(element);
					if (platformResourcesPackage != null) {
						org.eclipse.uml2.uml.Class cpuResource = platformResourcesPackage.createOwnedClass(element.getName() + "_" + element.hashCode(), false);
						if (cpuResource != null) {
							StereotypeUtil.apply(cpuResource, HwComputingResource.class);
							StereotypeUtil.apply(cpuResource, GaExecHost.class);
							HwComputingResource hwComputingResource = UMLUtil.getStereotypeApplication(cpuResource, HwComputingResource.class);
							GaExecHost gaExecHost = UMLUtil.getStereotypeApplication(cpuResource, GaExecHost.class);
							if (gaExecHost != null && hwComputingResource != null) {
								org.eclipse.uml2.uml.Class schedulerResource = platformResourcesPackage.createOwnedClass(element.getName() + "Scheduler" + "_" + element.hashCode(), false);
								if (schedulerResource != null) {
									StereotypeUtil.apply(schedulerResource, Scheduler.class);
									Scheduler scheduler = UMLUtil.getStereotypeApplication(schedulerResource, Scheduler.class);
									if (scheduler != null) {
										hwComputingResource.setMainScheduler(scheduler);
										gaExecHost.setMainScheduler(scheduler);
										scheduler.setHost(hwComputingResource);
										scheduler.setIsPreemptible(true);
										scheduler.setSchedPolicy(SchedPolicyKind.FIXED_PRIORITY);
										gaExecHost.setIsPreemptible(true);
										gaExecHost.setSchedPolicy(SchedPolicyKind.FIXED_PRIORITY);
									}
								}
								gaStep.setHost(gaExecHost);
							}
						}
					}
				}
				return CommandResult.newOKCommandResult(element);
			}
		};
		return CompositeCommand.compose(configureCommand, super.getAfterConfigureCommand(request));
	}

	@Override
	protected ICommand getBeforeDestroyDependentsCommand(DestroyDependentsRequest request) {
		ActivityPartition element = (ActivityPartition) request.getElementToDestroy();
		Package platformResourcesPackage = AdviceUtils.getPlatformResourcesPackage(element);
		GaStep gaStep = UMLUtil.getStereotypeApplication(element, GaStep.class);
		if (gaStep != null) {
			GaExecHost gaExecHost = gaStep.getHost();
			if (gaExecHost != null) {
				Classifier baseGaExecHostClassifier = gaExecHost.getBase_Classifier();
				Scheduler scheduler = gaExecHost.getMainScheduler();
				if (scheduler == null && baseGaExecHostClassifier != null) {
					HwComputingResource hwComputingResource = UMLUtil.getStereotypeApplication(baseGaExecHostClassifier, HwComputingResource.class);
					if (hwComputingResource != null) {
						scheduler = hwComputingResource.getMainScheduler();
					}
				}
				if (scheduler != null) {
					Classifier baseSchedulerClassifier = scheduler.getBase_Classifier();
					if (platformResourcesPackage != null) {
						ICommand configureCommand = new AbstractTransactionalCommand(request.getEditingDomain(), "Destroy host and its main scheduler", Collections.EMPTY_LIST) {
							@Override
							protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
								for (Element ownedElement : platformResourcesPackage.getOwnedElements()) {
									if (baseGaExecHostClassifier != null && ownedElement == baseGaExecHostClassifier) {
										baseGaExecHostClassifier.destroy();
									}

									if (baseSchedulerClassifier != null && ownedElement == baseSchedulerClassifier) {
										baseSchedulerClassifier.destroy();
									}

									if (baseGaExecHostClassifier == null && baseSchedulerClassifier == null) {
										break;
									}
								}
								return CommandResult.newOKCommandResult(element);
							}
						};
						return CompositeCommand.compose(configureCommand, super.getBeforeDestroyDependentsCommand(request));
					}
				} else {
					if (platformResourcesPackage != null) {
						ICommand configureCommand = new AbstractTransactionalCommand(request.getEditingDomain(), "Destroy host and its main scheduler", Collections.EMPTY_LIST) {
							@Override
							protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
								for (Element ownedElement : platformResourcesPackage.getOwnedElements()) {
									if (baseGaExecHostClassifier != null && ownedElement == baseGaExecHostClassifier) {
										baseGaExecHostClassifier.destroy();
										break;
									}
								}
								return CommandResult.newOKCommandResult(element);
							}
						};
						return CompositeCommand.compose(configureCommand, super.getBeforeDestroyDependentsCommand(request));
					}
				}
				
			}
		}
		return super.getBeforeDestroyDependentsCommand(request);
	}
}

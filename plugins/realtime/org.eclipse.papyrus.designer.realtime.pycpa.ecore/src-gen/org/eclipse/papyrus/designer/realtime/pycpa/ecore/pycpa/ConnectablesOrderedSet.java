/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Connectables Ordered Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectablesOrderedSet#getElements <em>Elements</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getConnectablesOrderedSet()
 * @model abstract="true"
 * @generated
 */
public interface ConnectablesOrderedSet extends PyCPAElement {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getConnectablesOrderedSet_Elements()
	 * @model lower="2"
	 * @generated
	 */
	EList<ConnectableElement> getElements();

} // ConnectablesOrderedSet

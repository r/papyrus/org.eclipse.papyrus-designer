/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>PJd Event Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getPeriod <em>Period</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getJitter <em>Jitter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getMin_distance <em>Min distance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getPJdEventModel()
 * @model
 * @generated
 */
public interface PJdEventModel extends EventModel {
	/**
	 * Returns the value of the '<em><b>Period</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' attribute.
	 * @see #setPeriod(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getPJdEventModel_Period()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getPeriod();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getPeriod <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' attribute.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(int value);

	/**
	 * Returns the value of the '<em><b>Jitter</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Jitter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Jitter</em>' attribute.
	 * @see #setJitter(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getPJdEventModel_Jitter()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getJitter();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getJitter <em>Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Jitter</em>' attribute.
	 * @see #getJitter()
	 * @generated
	 */
	void setJitter(int value);

	/**
	 * Returns the value of the '<em><b>Min distance</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min distance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min distance</em>' attribute.
	 * @see #setMin_distance(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getPJdEventModel_Min_distance()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getMin_distance();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getMin_distance <em>Min distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min distance</em>' attribute.
	 * @see #getMin_distance()
	 * @generated
	 */
	void setMin_distance(int value);

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(int)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getPJdEventModel_Offset()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getOffset();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(int value);

} // PJdEventModel

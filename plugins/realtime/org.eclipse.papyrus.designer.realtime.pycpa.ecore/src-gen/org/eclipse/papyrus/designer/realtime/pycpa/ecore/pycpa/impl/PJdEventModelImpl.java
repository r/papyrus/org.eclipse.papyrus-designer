/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>PJd Event Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl#getPeriod <em>Period</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl#getJitter <em>Jitter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl#getMin_distance <em>Min distance</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PJdEventModelImpl extends EventModelImpl implements PJdEventModel {
	/**
	 * The default value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final int PERIOD_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected int period = PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getJitter() <em>Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJitter()
	 * @generated
	 * @ordered
	 */
	protected static final int JITTER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getJitter() <em>Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJitter()
	 * @generated
	 * @ordered
	 */
	protected int jitter = JITTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin_distance() <em>Min distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_distance()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_DISTANCE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMin_distance() <em>Min distance</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_distance()
	 * @generated
	 * @ordered
	 */
	protected int min_distance = MIN_DISTANCE_EDEFAULT;

	/**
	 * The default value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected static final int OFFSET_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected int offset = OFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PJdEventModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PyCPAPackage.Literals.PJD_EVENT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPeriod() {
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPeriod(int newPeriod) {
		int oldPeriod = period;
		period = newPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.PJD_EVENT_MODEL__PERIOD, oldPeriod, period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getJitter() {
		return jitter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJitter(int newJitter) {
		int oldJitter = jitter;
		jitter = newJitter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.PJD_EVENT_MODEL__JITTER, oldJitter, jitter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMin_distance() {
		return min_distance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin_distance(int newMin_distance) {
		int oldMin_distance = min_distance;
		min_distance = newMin_distance;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.PJD_EVENT_MODEL__MIN_DISTANCE, oldMin_distance, min_distance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getOffset() {
		return offset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffset(int newOffset) {
		int oldOffset = offset;
		offset = newOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.PJD_EVENT_MODEL__OFFSET, oldOffset, offset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PyCPAPackage.PJD_EVENT_MODEL__PERIOD:
				return getPeriod();
			case PyCPAPackage.PJD_EVENT_MODEL__JITTER:
				return getJitter();
			case PyCPAPackage.PJD_EVENT_MODEL__MIN_DISTANCE:
				return getMin_distance();
			case PyCPAPackage.PJD_EVENT_MODEL__OFFSET:
				return getOffset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PyCPAPackage.PJD_EVENT_MODEL__PERIOD:
				setPeriod((Integer)newValue);
				return;
			case PyCPAPackage.PJD_EVENT_MODEL__JITTER:
				setJitter((Integer)newValue);
				return;
			case PyCPAPackage.PJD_EVENT_MODEL__MIN_DISTANCE:
				setMin_distance((Integer)newValue);
				return;
			case PyCPAPackage.PJD_EVENT_MODEL__OFFSET:
				setOffset((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PyCPAPackage.PJD_EVENT_MODEL__PERIOD:
				setPeriod(PERIOD_EDEFAULT);
				return;
			case PyCPAPackage.PJD_EVENT_MODEL__JITTER:
				setJitter(JITTER_EDEFAULT);
				return;
			case PyCPAPackage.PJD_EVENT_MODEL__MIN_DISTANCE:
				setMin_distance(MIN_DISTANCE_EDEFAULT);
				return;
			case PyCPAPackage.PJD_EVENT_MODEL__OFFSET:
				setOffset(OFFSET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PyCPAPackage.PJD_EVENT_MODEL__PERIOD:
				return period != PERIOD_EDEFAULT;
			case PyCPAPackage.PJD_EVENT_MODEL__JITTER:
				return jitter != JITTER_EDEFAULT;
			case PyCPAPackage.PJD_EVENT_MODEL__MIN_DISTANCE:
				return min_distance != MIN_DISTANCE_EDEFAULT;
			case PyCPAPackage.PJD_EVENT_MODEL__OFFSET:
				return offset != OFFSET_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (period: ");
		result.append(period);
		result.append(", jitter: ");
		result.append(jitter);
		result.append(", min_distance: ");
		result.append(min_distance);
		result.append(", offset: ");
		result.append(offset);
		result.append(')');
		return result.toString();
	}

} //PJdEventModelImpl

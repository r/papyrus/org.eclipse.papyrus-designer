/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getScheduler()
 * @model
 * @generated
 */
public interface Scheduler extends PyCPAElement {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind
	 * @see #setKind(SchedulerKind)
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getScheduler_Kind()
	 * @model required="true"
	 * @generated
	 */
	SchedulerKind getKind();

	/**
	 * Sets the value of the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(SchedulerKind value);

} // Scheduler

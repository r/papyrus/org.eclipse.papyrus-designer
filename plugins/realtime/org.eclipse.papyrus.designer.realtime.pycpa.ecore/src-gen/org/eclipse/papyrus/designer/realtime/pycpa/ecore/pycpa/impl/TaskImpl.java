/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl#getWcet <em>Wcet</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl#getBcet <em>Bcet</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl#getDeadline <em>Deadline</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl#getScheduling_parameter <em>Scheduling parameter</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl#getIn_eventmodel <em>In eventmodel</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl#getPrecedence_deps <em>Precedence deps</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl#getData_deps <em>Data deps</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TaskImpl extends ConnectableElementImpl implements Task {
	/**
	 * The default value of the '{@link #getWcet() <em>Wcet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWcet()
	 * @generated
	 * @ordered
	 */
	protected static final int WCET_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWcet() <em>Wcet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWcet()
	 * @generated
	 * @ordered
	 */
	protected int wcet = WCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getBcet() <em>Bcet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBcet()
	 * @generated
	 * @ordered
	 */
	protected static final int BCET_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBcet() <em>Bcet</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBcet()
	 * @generated
	 * @ordered
	 */
	protected int bcet = BCET_EDEFAULT;

	/**
	 * The default value of the '{@link #getDeadline() <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected static final int DEADLINE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getDeadline() <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected int deadline = DEADLINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getScheduling_parameter() <em>Scheduling parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduling_parameter()
	 * @generated
	 * @ordered
	 */
	protected static final int SCHEDULING_PARAMETER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getScheduling_parameter() <em>Scheduling parameter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduling_parameter()
	 * @generated
	 * @ordered
	 */
	protected int scheduling_parameter = SCHEDULING_PARAMETER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIn_eventmodel() <em>In eventmodel</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIn_eventmodel()
	 * @generated
	 * @ordered
	 */
	protected EventModel in_eventmodel;

	/**
	 * The cached value of the '{@link #getPrecedence_deps() <em>Precedence deps</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrecedence_deps()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectableElement> precedence_deps;

	/**
	 * The cached value of the '{@link #getData_deps() <em>Data deps</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getData_deps()
	 * @generated
	 * @ordered
	 */
	protected EList<ConnectableElement> data_deps;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PyCPAPackage.Literals.TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWcet() {
		return wcet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWcet(int newWcet) {
		int oldWcet = wcet;
		wcet = newWcet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.TASK__WCET, oldWcet, wcet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBcet() {
		return bcet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBcet(int newBcet) {
		int oldBcet = bcet;
		bcet = newBcet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.TASK__BCET, oldBcet, bcet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getDeadline() {
		return deadline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeadline(int newDeadline) {
		int oldDeadline = deadline;
		deadline = newDeadline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.TASK__DEADLINE, oldDeadline, deadline));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getScheduling_parameter() {
		return scheduling_parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScheduling_parameter(int newScheduling_parameter) {
		int oldScheduling_parameter = scheduling_parameter;
		scheduling_parameter = newScheduling_parameter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.TASK__SCHEDULING_PARAMETER, oldScheduling_parameter, scheduling_parameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventModel getIn_eventmodel() {
		return in_eventmodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIn_eventmodel(EventModel newIn_eventmodel, NotificationChain msgs) {
		EventModel oldIn_eventmodel = in_eventmodel;
		in_eventmodel = newIn_eventmodel;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, PyCPAPackage.TASK__IN_EVENTMODEL, oldIn_eventmodel, newIn_eventmodel);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIn_eventmodel(EventModel newIn_eventmodel) {
		if (newIn_eventmodel != in_eventmodel) {
			NotificationChain msgs = null;
			if (in_eventmodel != null)
				msgs = ((InternalEObject)in_eventmodel).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - PyCPAPackage.TASK__IN_EVENTMODEL, null, msgs);
			if (newIn_eventmodel != null)
				msgs = ((InternalEObject)newIn_eventmodel).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - PyCPAPackage.TASK__IN_EVENTMODEL, null, msgs);
			msgs = basicSetIn_eventmodel(newIn_eventmodel, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.TASK__IN_EVENTMODEL, newIn_eventmodel, newIn_eventmodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectableElement> getPrecedence_deps() {
		if (precedence_deps == null) {
			precedence_deps = new EObjectResolvingEList<ConnectableElement>(ConnectableElement.class, this, PyCPAPackage.TASK__PRECEDENCE_DEPS);
		}
		return precedence_deps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ConnectableElement> getData_deps() {
		if (data_deps == null) {
			data_deps = new EObjectResolvingEList<ConnectableElement>(ConnectableElement.class, this, PyCPAPackage.TASK__DATA_DEPS);
		}
		return data_deps;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PyCPAPackage.TASK__IN_EVENTMODEL:
				return basicSetIn_eventmodel(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PyCPAPackage.TASK__WCET:
				return getWcet();
			case PyCPAPackage.TASK__BCET:
				return getBcet();
			case PyCPAPackage.TASK__DEADLINE:
				return getDeadline();
			case PyCPAPackage.TASK__SCHEDULING_PARAMETER:
				return getScheduling_parameter();
			case PyCPAPackage.TASK__IN_EVENTMODEL:
				return getIn_eventmodel();
			case PyCPAPackage.TASK__PRECEDENCE_DEPS:
				return getPrecedence_deps();
			case PyCPAPackage.TASK__DATA_DEPS:
				return getData_deps();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PyCPAPackage.TASK__WCET:
				setWcet((Integer)newValue);
				return;
			case PyCPAPackage.TASK__BCET:
				setBcet((Integer)newValue);
				return;
			case PyCPAPackage.TASK__DEADLINE:
				setDeadline((Integer)newValue);
				return;
			case PyCPAPackage.TASK__SCHEDULING_PARAMETER:
				setScheduling_parameter((Integer)newValue);
				return;
			case PyCPAPackage.TASK__IN_EVENTMODEL:
				setIn_eventmodel((EventModel)newValue);
				return;
			case PyCPAPackage.TASK__PRECEDENCE_DEPS:
				getPrecedence_deps().clear();
				getPrecedence_deps().addAll((Collection<? extends ConnectableElement>)newValue);
				return;
			case PyCPAPackage.TASK__DATA_DEPS:
				getData_deps().clear();
				getData_deps().addAll((Collection<? extends ConnectableElement>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PyCPAPackage.TASK__WCET:
				setWcet(WCET_EDEFAULT);
				return;
			case PyCPAPackage.TASK__BCET:
				setBcet(BCET_EDEFAULT);
				return;
			case PyCPAPackage.TASK__DEADLINE:
				setDeadline(DEADLINE_EDEFAULT);
				return;
			case PyCPAPackage.TASK__SCHEDULING_PARAMETER:
				setScheduling_parameter(SCHEDULING_PARAMETER_EDEFAULT);
				return;
			case PyCPAPackage.TASK__IN_EVENTMODEL:
				setIn_eventmodel((EventModel)null);
				return;
			case PyCPAPackage.TASK__PRECEDENCE_DEPS:
				getPrecedence_deps().clear();
				return;
			case PyCPAPackage.TASK__DATA_DEPS:
				getData_deps().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PyCPAPackage.TASK__WCET:
				return wcet != WCET_EDEFAULT;
			case PyCPAPackage.TASK__BCET:
				return bcet != BCET_EDEFAULT;
			case PyCPAPackage.TASK__DEADLINE:
				return deadline != DEADLINE_EDEFAULT;
			case PyCPAPackage.TASK__SCHEDULING_PARAMETER:
				return scheduling_parameter != SCHEDULING_PARAMETER_EDEFAULT;
			case PyCPAPackage.TASK__IN_EVENTMODEL:
				return in_eventmodel != null;
			case PyCPAPackage.TASK__PRECEDENCE_DEPS:
				return precedence_deps != null && !precedence_deps.isEmpty();
			case PyCPAPackage.TASK__DATA_DEPS:
				return data_deps != null && !data_deps.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (wcet: ");
		result.append(wcet);
		result.append(", bcet: ");
		result.append(bcet);
		result.append(", deadline: ");
		result.append(deadline);
		result.append(", scheduling_parameter: ");
		result.append(scheduling_parameter);
		result.append(')');
		return result.toString();
	}

} //TaskImpl

/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EffectChain;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Effect Chain</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EffectChainImpl extends ConnectablesOrderedSetImpl implements EffectChain {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EffectChainImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PyCPAPackage.Literals.EFFECT_CHAIN;
	}

} //EffectChainImpl

/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Path</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getPath()
 * @model
 * @generated
 */
public interface Path extends ConnectablesOrderedSet {

} // Path

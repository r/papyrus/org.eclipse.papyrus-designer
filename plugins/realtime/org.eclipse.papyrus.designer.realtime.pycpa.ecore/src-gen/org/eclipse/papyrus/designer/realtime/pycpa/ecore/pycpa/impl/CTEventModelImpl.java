/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CT Event Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.CTEventModelImpl#getNum_events <em>Num events</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.CTEventModelImpl#getTime <em>Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CTEventModelImpl extends EventModelImpl implements CTEventModel {
	/**
	 * The default value of the '{@link #getNum_events() <em>Num events</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_events()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_EVENTS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNum_events() <em>Num events</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNum_events()
	 * @generated
	 * @ordered
	 */
	protected int num_events = NUM_EVENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final int TIME_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected int time = TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CTEventModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PyCPAPackage.Literals.CT_EVENT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNum_events() {
		return num_events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNum_events(int newNum_events) {
		int oldNum_events = num_events;
		num_events = newNum_events;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.CT_EVENT_MODEL__NUM_EVENTS, oldNum_events, num_events));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getTime() {
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(int newTime) {
		int oldTime = time;
		time = newTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PyCPAPackage.CT_EVENT_MODEL__TIME, oldTime, time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PyCPAPackage.CT_EVENT_MODEL__NUM_EVENTS:
				return getNum_events();
			case PyCPAPackage.CT_EVENT_MODEL__TIME:
				return getTime();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PyCPAPackage.CT_EVENT_MODEL__NUM_EVENTS:
				setNum_events((Integer)newValue);
				return;
			case PyCPAPackage.CT_EVENT_MODEL__TIME:
				setTime((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PyCPAPackage.CT_EVENT_MODEL__NUM_EVENTS:
				setNum_events(NUM_EVENTS_EDEFAULT);
				return;
			case PyCPAPackage.CT_EVENT_MODEL__TIME:
				setTime(TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PyCPAPackage.CT_EVENT_MODEL__NUM_EVENTS:
				return num_events != NUM_EVENTS_EDEFAULT;
			case PyCPAPackage.CT_EVENT_MODEL__TIME:
				return time != TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (num_events: ");
		result.append(num_events);
		result.append(", time: ");
		result.append(time);
		result.append(')');
		return result.toString();
	}

} //CTEventModelImpl

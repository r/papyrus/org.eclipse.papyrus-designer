/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAFactory
 * @model kind="package"
 * @generated
 */
public interface PyCPAPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "pycpa";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://designer.papyrus.eclipse.org/realtime/pycpa/0.1";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "pycpa";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PyCPAPackage eINSTANCE = org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAElementImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getPyCPAElement()
	 * @generated
	 */
	int PY_CPA_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PY_CPA_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PY_CPA_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PY_CPA_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl <em>System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getSystem()
	 * @generated
	 */
	int SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__NAME = PY_CPA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Resources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__RESOURCES = PY_CPA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Paths</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__PATHS = PY_CPA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Effectchains</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__EFFECTCHAINS = PY_CPA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Junctions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM__JUNCTIONS = PY_CPA_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE_COUNT = PY_CPA_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_OPERATION_COUNT = PY_CPA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ResourceImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME = PY_CPA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__SCHEDULER = PY_CPA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__TASKS = PY_CPA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = PY_CPA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_OPERATION_COUNT = PY_CPA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectableElementImpl <em>Connectable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectableElementImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getConnectableElement()
	 * @generated
	 */
	int CONNECTABLE_ELEMENT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTABLE_ELEMENT__NAME = PY_CPA_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Connectable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTABLE_ELEMENT_FEATURE_COUNT = PY_CPA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Connectable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTABLE_ELEMENT_OPERATION_COUNT = PY_CPA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__NAME = CONNECTABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Wcet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__WCET = CONNECTABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bcet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__BCET = CONNECTABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__DEADLINE = CONNECTABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Scheduling parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__SCHEDULING_PARAMETER = CONNECTABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>In eventmodel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__IN_EVENTMODEL = CONNECTABLE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Precedence deps</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__PRECEDENCE_DEPS = CONNECTABLE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Data deps</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__DATA_DEPS = CONNECTABLE_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = CONNECTABLE_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = CONNECTABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SchedulerImpl <em>Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SchedulerImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getScheduler()
	 * @generated
	 */
	int SCHEDULER = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULER__NAME = PY_CPA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULER__KIND = PY_CPA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULER_FEATURE_COUNT = PY_CPA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULER_OPERATION_COUNT = PY_CPA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EventModelImpl <em>Event Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EventModelImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getEventModel()
	 * @generated
	 */
	int EVENT_MODEL = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_MODEL__NAME = PY_CPA_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Event Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_MODEL_FEATURE_COUNT = PY_CPA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Event Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_MODEL_OPERATION_COUNT = PY_CPA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl <em>PJd Event Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getPJdEventModel()
	 * @generated
	 */
	int PJD_EVENT_MODEL = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PJD_EVENT_MODEL__NAME = EVENT_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PJD_EVENT_MODEL__PERIOD = EVENT_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Jitter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PJD_EVENT_MODEL__JITTER = EVENT_MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Min distance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PJD_EVENT_MODEL__MIN_DISTANCE = EVENT_MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PJD_EVENT_MODEL__OFFSET = EVENT_MODEL_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>PJd Event Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PJD_EVENT_MODEL_FEATURE_COUNT = EVENT_MODEL_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>PJd Event Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PJD_EVENT_MODEL_OPERATION_COUNT = EVENT_MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.CTEventModelImpl <em>CT Event Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.CTEventModelImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getCTEventModel()
	 * @generated
	 */
	int CT_EVENT_MODEL = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CT_EVENT_MODEL__NAME = EVENT_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Num events</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CT_EVENT_MODEL__NUM_EVENTS = EVENT_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CT_EVENT_MODEL__TIME = EVENT_MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>CT Event Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CT_EVENT_MODEL_FEATURE_COUNT = EVENT_MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>CT Event Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CT_EVENT_MODEL_OPERATION_COUNT = EVENT_MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectablesOrderedSetImpl <em>Connectables Ordered Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectablesOrderedSetImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getConnectablesOrderedSet()
	 * @generated
	 */
	int CONNECTABLES_ORDERED_SET = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTABLES_ORDERED_SET__NAME = PY_CPA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTABLES_ORDERED_SET__ELEMENTS = PY_CPA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Connectables Ordered Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTABLES_ORDERED_SET_FEATURE_COUNT = PY_CPA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Connectables Ordered Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONNECTABLES_ORDERED_SET_OPERATION_COUNT = PY_CPA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EffectChainImpl <em>Effect Chain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EffectChainImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getEffectChain()
	 * @generated
	 */
	int EFFECT_CHAIN = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT_CHAIN__NAME = CONNECTABLES_ORDERED_SET__NAME;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT_CHAIN__ELEMENTS = CONNECTABLES_ORDERED_SET__ELEMENTS;

	/**
	 * The number of structural features of the '<em>Effect Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT_CHAIN_FEATURE_COUNT = CONNECTABLES_ORDERED_SET_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Effect Chain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EFFECT_CHAIN_OPERATION_COUNT = CONNECTABLES_ORDERED_SET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PathImpl <em>Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PathImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getPath()
	 * @generated
	 */
	int PATH = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH__NAME = CONNECTABLES_ORDERED_SET__NAME;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH__ELEMENTS = CONNECTABLES_ORDERED_SET__ELEMENTS;

	/**
	 * The number of structural features of the '<em>Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_FEATURE_COUNT = CONNECTABLES_ORDERED_SET_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PATH_OPERATION_COUNT = CONNECTABLES_ORDERED_SET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.JunctionImpl <em>Junction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.JunctionImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getJunction()
	 * @generated
	 */
	int JUNCTION = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__NAME = CONNECTABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__STRATEGY = CONNECTABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Prev tasks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__PREV_TASKS = CONNECTABLE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Next task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION__NEXT_TASK = CONNECTABLE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Junction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_FEATURE_COUNT = CONNECTABLE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Junction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JUNCTION_OPERATION_COUNT = CONNECTABLE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ForkImpl <em>Fork</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ForkImpl
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getFork()
	 * @generated
	 */
	int FORK = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__NAME = TASK__NAME;

	/**
	 * The feature id for the '<em><b>Wcet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__WCET = TASK__WCET;

	/**
	 * The feature id for the '<em><b>Bcet</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__BCET = TASK__BCET;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__DEADLINE = TASK__DEADLINE;

	/**
	 * The feature id for the '<em><b>Scheduling parameter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__SCHEDULING_PARAMETER = TASK__SCHEDULING_PARAMETER;

	/**
	 * The feature id for the '<em><b>In eventmodel</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__IN_EVENTMODEL = TASK__IN_EVENTMODEL;

	/**
	 * The feature id for the '<em><b>Precedence deps</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__PRECEDENCE_DEPS = TASK__PRECEDENCE_DEPS;

	/**
	 * The feature id for the '<em><b>Data deps</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__DATA_DEPS = TASK__DATA_DEPS;

	/**
	 * The feature id for the '<em><b>Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK__STRATEGY = TASK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Fork</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK_FEATURE_COUNT = TASK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Fork</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORK_OPERATION_COUNT = TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind <em>Scheduler Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getSchedulerKind()
	 * @generated
	 */
	int SCHEDULER_KIND = 14;


	/**
	 * The meta object id for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind <em>Junction Strategy Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getJunctionStrategyKind()
	 * @generated
	 */
	int JUNCTION_STRATEGY_KIND = 15;


	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System
	 * @generated
	 */
	EClass getSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getResources <em>Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resources</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getResources()
	 * @see #getSystem()
	 * @generated
	 */
	EReference getSystem_Resources();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getPaths <em>Paths</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Paths</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getPaths()
	 * @see #getSystem()
	 * @generated
	 */
	EReference getSystem_Paths();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getEffectchains <em>Effectchains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Effectchains</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getEffectchains()
	 * @see #getSystem()
	 * @generated
	 */
	EReference getSystem_Effectchains();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getJunctions <em>Junctions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Junctions</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getJunctions()
	 * @see #getSystem()
	 * @generated
	 */
	EReference getSystem_Junctions();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource#getScheduler <em>Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scheduler</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource#getScheduler()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Scheduler();

	/**
	 * Returns the meta object for the containment reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource#getTasks()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Tasks();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getWcet <em>Wcet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wcet</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getWcet()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Wcet();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getBcet <em>Bcet</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bcet</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getBcet()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Bcet();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getDeadline()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getScheduling_parameter <em>Scheduling parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scheduling parameter</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getScheduling_parameter()
	 * @see #getTask()
	 * @generated
	 */
	EAttribute getTask_Scheduling_parameter();

	/**
	 * Returns the meta object for the containment reference '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getIn_eventmodel <em>In eventmodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>In eventmodel</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getIn_eventmodel()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_In_eventmodel();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getPrecedence_deps <em>Precedence deps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Precedence deps</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getPrecedence_deps()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Precedence_deps();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getData_deps <em>Data deps</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Data deps</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task#getData_deps()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_Data_deps();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler <em>Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scheduler</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler
	 * @generated
	 */
	EClass getScheduler();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler#getKind()
	 * @see #getScheduler()
	 * @generated
	 */
	EAttribute getScheduler_Kind();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EventModel <em>Event Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Model</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EventModel
	 * @generated
	 */
	EClass getEventModel();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel <em>PJd Event Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>PJd Event Model</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel
	 * @generated
	 */
	EClass getPJdEventModel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getPeriod()
	 * @see #getPJdEventModel()
	 * @generated
	 */
	EAttribute getPJdEventModel_Period();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getJitter <em>Jitter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Jitter</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getJitter()
	 * @see #getPJdEventModel()
	 * @generated
	 */
	EAttribute getPJdEventModel_Jitter();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getMin_distance <em>Min distance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min distance</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getMin_distance()
	 * @see #getPJdEventModel()
	 * @generated
	 */
	EAttribute getPJdEventModel_Min_distance();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel#getOffset()
	 * @see #getPJdEventModel()
	 * @generated
	 */
	EAttribute getPJdEventModel_Offset();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel <em>CT Event Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CT Event Model</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel
	 * @generated
	 */
	EClass getCTEventModel();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel#getNum_events <em>Num events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num events</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel#getNum_events()
	 * @see #getCTEventModel()
	 * @generated
	 */
	EAttribute getCTEventModel_Num_events();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel#getTime()
	 * @see #getCTEventModel()
	 * @generated
	 */
	EAttribute getCTEventModel_Time();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAElement
	 * @generated
	 */
	EClass getPyCPAElement();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAElement#getName()
	 * @see #getPyCPAElement()
	 * @generated
	 */
	EAttribute getPyCPAElement_Name();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EffectChain <em>Effect Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Effect Chain</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EffectChain
	 * @generated
	 */
	EClass getEffectChain();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Path <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Path</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Path
	 * @generated
	 */
	EClass getPath();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectablesOrderedSet <em>Connectables Ordered Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connectables Ordered Set</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectablesOrderedSet
	 * @generated
	 */
	EClass getConnectablesOrderedSet();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectablesOrderedSet#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Elements</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectablesOrderedSet#getElements()
	 * @see #getConnectablesOrderedSet()
	 * @generated
	 */
	EReference getConnectablesOrderedSet_Elements();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction <em>Junction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Junction</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction
	 * @generated
	 */
	EClass getJunction();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getStrategy <em>Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Strategy</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getStrategy()
	 * @see #getJunction()
	 * @generated
	 */
	EAttribute getJunction_Strategy();

	/**
	 * Returns the meta object for the reference list '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getPrev_tasks <em>Prev tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Prev tasks</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getPrev_tasks()
	 * @see #getJunction()
	 * @generated
	 */
	EReference getJunction_Prev_tasks();

	/**
	 * Returns the meta object for the reference '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getNext_task <em>Next task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next task</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction#getNext_task()
	 * @see #getJunction()
	 * @generated
	 */
	EReference getJunction_Next_task();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Fork <em>Fork</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fork</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Fork
	 * @generated
	 */
	EClass getFork();

	/**
	 * Returns the meta object for the attribute '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Fork#getStrategy <em>Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Strategy</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Fork#getStrategy()
	 * @see #getFork()
	 * @generated
	 */
	EAttribute getFork_Strategy();

	/**
	 * Returns the meta object for class '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement <em>Connectable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Connectable Element</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement
	 * @generated
	 */
	EClass getConnectableElement();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind <em>Scheduler Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Scheduler Kind</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind
	 * @generated
	 */
	EEnum getSchedulerKind();

	/**
	 * Returns the meta object for enum '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind <em>Junction Strategy Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Junction Strategy Kind</em>'.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind
	 * @generated
	 */
	EEnum getJunctionStrategyKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PyCPAFactory getPyCPAFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl <em>System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getSystem()
		 * @generated
		 */
		EClass SYSTEM = eINSTANCE.getSystem();

		/**
		 * The meta object literal for the '<em><b>Resources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM__RESOURCES = eINSTANCE.getSystem_Resources();

		/**
		 * The meta object literal for the '<em><b>Paths</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM__PATHS = eINSTANCE.getSystem_Paths();

		/**
		 * The meta object literal for the '<em><b>Effectchains</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM__EFFECTCHAINS = eINSTANCE.getSystem_Effectchains();

		/**
		 * The meta object literal for the '<em><b>Junctions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYSTEM__JUNCTIONS = eINSTANCE.getSystem_Junctions();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ResourceImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
		 * The meta object literal for the '<em><b>Scheduler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__SCHEDULER = eINSTANCE.getResource_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__TASKS = eINSTANCE.getResource_Tasks();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.TaskImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Wcet</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__WCET = eINSTANCE.getTask_Wcet();

		/**
		 * The meta object literal for the '<em><b>Bcet</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__BCET = eINSTANCE.getTask_Bcet();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__DEADLINE = eINSTANCE.getTask_Deadline();

		/**
		 * The meta object literal for the '<em><b>Scheduling parameter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TASK__SCHEDULING_PARAMETER = eINSTANCE.getTask_Scheduling_parameter();

		/**
		 * The meta object literal for the '<em><b>In eventmodel</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__IN_EVENTMODEL = eINSTANCE.getTask_In_eventmodel();

		/**
		 * The meta object literal for the '<em><b>Precedence deps</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__PRECEDENCE_DEPS = eINSTANCE.getTask_Precedence_deps();

		/**
		 * The meta object literal for the '<em><b>Data deps</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__DATA_DEPS = eINSTANCE.getTask_Data_deps();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SchedulerImpl <em>Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SchedulerImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getScheduler()
		 * @generated
		 */
		EClass SCHEDULER = eINSTANCE.getScheduler();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULER__KIND = eINSTANCE.getScheduler_Kind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EventModelImpl <em>Event Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EventModelImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getEventModel()
		 * @generated
		 */
		EClass EVENT_MODEL = eINSTANCE.getEventModel();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl <em>PJd Event Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PJdEventModelImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getPJdEventModel()
		 * @generated
		 */
		EClass PJD_EVENT_MODEL = eINSTANCE.getPJdEventModel();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PJD_EVENT_MODEL__PERIOD = eINSTANCE.getPJdEventModel_Period();

		/**
		 * The meta object literal for the '<em><b>Jitter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PJD_EVENT_MODEL__JITTER = eINSTANCE.getPJdEventModel_Jitter();

		/**
		 * The meta object literal for the '<em><b>Min distance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PJD_EVENT_MODEL__MIN_DISTANCE = eINSTANCE.getPJdEventModel_Min_distance();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PJD_EVENT_MODEL__OFFSET = eINSTANCE.getPJdEventModel_Offset();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.CTEventModelImpl <em>CT Event Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.CTEventModelImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getCTEventModel()
		 * @generated
		 */
		EClass CT_EVENT_MODEL = eINSTANCE.getCTEventModel();

		/**
		 * The meta object literal for the '<em><b>Num events</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CT_EVENT_MODEL__NUM_EVENTS = eINSTANCE.getCTEventModel_Num_events();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CT_EVENT_MODEL__TIME = eINSTANCE.getCTEventModel_Time();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAElementImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getPyCPAElement()
		 * @generated
		 */
		EClass PY_CPA_ELEMENT = eINSTANCE.getPyCPAElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PY_CPA_ELEMENT__NAME = eINSTANCE.getPyCPAElement_Name();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EffectChainImpl <em>Effect Chain</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.EffectChainImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getEffectChain()
		 * @generated
		 */
		EClass EFFECT_CHAIN = eINSTANCE.getEffectChain();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PathImpl <em>Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PathImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getPath()
		 * @generated
		 */
		EClass PATH = eINSTANCE.getPath();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectablesOrderedSetImpl <em>Connectables Ordered Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectablesOrderedSetImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getConnectablesOrderedSet()
		 * @generated
		 */
		EClass CONNECTABLES_ORDERED_SET = eINSTANCE.getConnectablesOrderedSet();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONNECTABLES_ORDERED_SET__ELEMENTS = eINSTANCE.getConnectablesOrderedSet_Elements();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.JunctionImpl <em>Junction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.JunctionImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getJunction()
		 * @generated
		 */
		EClass JUNCTION = eINSTANCE.getJunction();

		/**
		 * The meta object literal for the '<em><b>Strategy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JUNCTION__STRATEGY = eINSTANCE.getJunction_Strategy();

		/**
		 * The meta object literal for the '<em><b>Prev tasks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JUNCTION__PREV_TASKS = eINSTANCE.getJunction_Prev_tasks();

		/**
		 * The meta object literal for the '<em><b>Next task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JUNCTION__NEXT_TASK = eINSTANCE.getJunction_Next_task();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ForkImpl <em>Fork</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ForkImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getFork()
		 * @generated
		 */
		EClass FORK = eINSTANCE.getFork();

		/**
		 * The meta object literal for the '<em><b>Strategy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FORK__STRATEGY = eINSTANCE.getFork_Strategy();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectableElementImpl <em>Connectable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.ConnectableElementImpl
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getConnectableElement()
		 * @generated
		 */
		EClass CONNECTABLE_ELEMENT = eINSTANCE.getConnectableElement();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind <em>Scheduler Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getSchedulerKind()
		 * @generated
		 */
		EEnum SCHEDULER_KIND = eINSTANCE.getSchedulerKind();

		/**
		 * The meta object literal for the '{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind <em>Junction Strategy Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind
		 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.PyCPAPackageImpl#getJunctionStrategyKind()
		 * @generated
		 */
		EEnum JUNCTION_STRATEGY_KIND = eINSTANCE.getJunctionStrategyKind();

	}

} //PyCPAPackage

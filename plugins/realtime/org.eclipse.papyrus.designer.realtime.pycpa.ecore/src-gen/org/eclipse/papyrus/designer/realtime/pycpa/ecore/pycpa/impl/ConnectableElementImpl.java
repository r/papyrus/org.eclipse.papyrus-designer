/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Connectable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ConnectableElementImpl extends PyCPAElementImpl implements ConnectableElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConnectableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PyCPAPackage.Literals.CONNECTABLE_ELEMENT;
	}

} //ConnectableElementImpl

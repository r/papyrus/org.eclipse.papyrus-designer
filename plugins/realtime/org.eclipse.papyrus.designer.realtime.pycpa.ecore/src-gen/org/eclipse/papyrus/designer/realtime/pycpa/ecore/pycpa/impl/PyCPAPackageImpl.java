/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.CTEventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectableElement;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.ConnectablesOrderedSet;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EffectChain;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Fork;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.JunctionStrategyKind;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PJdEventModel;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Path;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAElement;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAFactory;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.SchedulerKind;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PyCPAPackageImpl extends EPackageImpl implements PyCPAPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass systemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass schedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pJdEventModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ctEventModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pyCPAElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass effectChainEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectablesOrderedSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass junctionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass connectableElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum schedulerKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum junctionStrategyKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PyCPAPackageImpl() {
		super(eNS_URI, PyCPAFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link PyCPAPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PyCPAPackage init() {
		if (isInited) return (PyCPAPackage)EPackage.Registry.INSTANCE.getEPackage(PyCPAPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredPyCPAPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		PyCPAPackageImpl thePyCPAPackage = registeredPyCPAPackage instanceof PyCPAPackageImpl ? (PyCPAPackageImpl)registeredPyCPAPackage : new PyCPAPackageImpl();

		isInited = true;

		// Create package meta-data objects
		thePyCPAPackage.createPackageContents();

		// Initialize created meta-data
		thePyCPAPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		thePyCPAPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PyCPAPackage.eNS_URI, thePyCPAPackage);
		return thePyCPAPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystem() {
		return systemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystem_Resources() {
		return (EReference)systemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystem_Paths() {
		return (EReference)systemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystem_Effectchains() {
		return (EReference)systemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSystem_Junctions() {
		return (EReference)systemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResource() {
		return resourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResource_Scheduler() {
		return (EReference)resourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResource_Tasks() {
		return (EReference)resourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTask() {
		return taskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_Wcet() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_Bcet() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_Deadline() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTask_Scheduling_parameter() {
		return (EAttribute)taskEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTask_In_eventmodel() {
		return (EReference)taskEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTask_Precedence_deps() {
		return (EReference)taskEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTask_Data_deps() {
		return (EReference)taskEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduler() {
		return schedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduler_Kind() {
		return (EAttribute)schedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventModel() {
		return eventModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPJdEventModel() {
		return pJdEventModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPJdEventModel_Period() {
		return (EAttribute)pJdEventModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPJdEventModel_Jitter() {
		return (EAttribute)pJdEventModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPJdEventModel_Min_distance() {
		return (EAttribute)pJdEventModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPJdEventModel_Offset() {
		return (EAttribute)pJdEventModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCTEventModel() {
		return ctEventModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCTEventModel_Num_events() {
		return (EAttribute)ctEventModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCTEventModel_Time() {
		return (EAttribute)ctEventModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPyCPAElement() {
		return pyCPAElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPyCPAElement_Name() {
		return (EAttribute)pyCPAElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEffectChain() {
		return effectChainEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPath() {
		return pathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectablesOrderedSet() {
		return connectablesOrderedSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getConnectablesOrderedSet_Elements() {
		return (EReference)connectablesOrderedSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJunction() {
		return junctionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJunction_Strategy() {
		return (EAttribute)junctionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJunction_Prev_tasks() {
		return (EReference)junctionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJunction_Next_task() {
		return (EReference)junctionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFork() {
		return forkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFork_Strategy() {
		return (EAttribute)forkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConnectableElement() {
		return connectableElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSchedulerKind() {
		return schedulerKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getJunctionStrategyKind() {
		return junctionStrategyKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PyCPAFactory getPyCPAFactory() {
		return (PyCPAFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		systemEClass = createEClass(SYSTEM);
		createEReference(systemEClass, SYSTEM__RESOURCES);
		createEReference(systemEClass, SYSTEM__PATHS);
		createEReference(systemEClass, SYSTEM__EFFECTCHAINS);
		createEReference(systemEClass, SYSTEM__JUNCTIONS);

		resourceEClass = createEClass(RESOURCE);
		createEReference(resourceEClass, RESOURCE__SCHEDULER);
		createEReference(resourceEClass, RESOURCE__TASKS);

		taskEClass = createEClass(TASK);
		createEAttribute(taskEClass, TASK__WCET);
		createEAttribute(taskEClass, TASK__BCET);
		createEAttribute(taskEClass, TASK__DEADLINE);
		createEAttribute(taskEClass, TASK__SCHEDULING_PARAMETER);
		createEReference(taskEClass, TASK__IN_EVENTMODEL);
		createEReference(taskEClass, TASK__PRECEDENCE_DEPS);
		createEReference(taskEClass, TASK__DATA_DEPS);

		schedulerEClass = createEClass(SCHEDULER);
		createEAttribute(schedulerEClass, SCHEDULER__KIND);

		eventModelEClass = createEClass(EVENT_MODEL);

		pJdEventModelEClass = createEClass(PJD_EVENT_MODEL);
		createEAttribute(pJdEventModelEClass, PJD_EVENT_MODEL__PERIOD);
		createEAttribute(pJdEventModelEClass, PJD_EVENT_MODEL__JITTER);
		createEAttribute(pJdEventModelEClass, PJD_EVENT_MODEL__MIN_DISTANCE);
		createEAttribute(pJdEventModelEClass, PJD_EVENT_MODEL__OFFSET);

		ctEventModelEClass = createEClass(CT_EVENT_MODEL);
		createEAttribute(ctEventModelEClass, CT_EVENT_MODEL__NUM_EVENTS);
		createEAttribute(ctEventModelEClass, CT_EVENT_MODEL__TIME);

		pyCPAElementEClass = createEClass(PY_CPA_ELEMENT);
		createEAttribute(pyCPAElementEClass, PY_CPA_ELEMENT__NAME);

		effectChainEClass = createEClass(EFFECT_CHAIN);

		pathEClass = createEClass(PATH);

		connectablesOrderedSetEClass = createEClass(CONNECTABLES_ORDERED_SET);
		createEReference(connectablesOrderedSetEClass, CONNECTABLES_ORDERED_SET__ELEMENTS);

		junctionEClass = createEClass(JUNCTION);
		createEAttribute(junctionEClass, JUNCTION__STRATEGY);
		createEReference(junctionEClass, JUNCTION__PREV_TASKS);
		createEReference(junctionEClass, JUNCTION__NEXT_TASK);

		forkEClass = createEClass(FORK);
		createEAttribute(forkEClass, FORK__STRATEGY);

		connectableElementEClass = createEClass(CONNECTABLE_ELEMENT);

		// Create enums
		schedulerKindEEnum = createEEnum(SCHEDULER_KIND);
		junctionStrategyKindEEnum = createEEnum(JUNCTION_STRATEGY_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		systemEClass.getESuperTypes().add(this.getPyCPAElement());
		resourceEClass.getESuperTypes().add(this.getPyCPAElement());
		taskEClass.getESuperTypes().add(this.getConnectableElement());
		schedulerEClass.getESuperTypes().add(this.getPyCPAElement());
		eventModelEClass.getESuperTypes().add(this.getPyCPAElement());
		pJdEventModelEClass.getESuperTypes().add(this.getEventModel());
		ctEventModelEClass.getESuperTypes().add(this.getEventModel());
		effectChainEClass.getESuperTypes().add(this.getConnectablesOrderedSet());
		pathEClass.getESuperTypes().add(this.getConnectablesOrderedSet());
		connectablesOrderedSetEClass.getESuperTypes().add(this.getPyCPAElement());
		junctionEClass.getESuperTypes().add(this.getConnectableElement());
		forkEClass.getESuperTypes().add(this.getTask());
		connectableElementEClass.getESuperTypes().add(this.getPyCPAElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(systemEClass, org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System.class, "System", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSystem_Resources(), this.getResource(), null, "resources", null, 0, -1, org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystem_Paths(), this.getPath(), null, "paths", null, 0, -1, org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystem_Effectchains(), this.getEffectChain(), null, "effectchains", null, 0, -1, org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSystem_Junctions(), this.getJunction(), null, "junctions", null, 0, -1, org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceEClass, Resource.class, "Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResource_Scheduler(), this.getScheduler(), null, "scheduler", null, 1, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResource_Tasks(), this.getTask(), null, "tasks", null, 0, -1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTask_Wcet(), ecorePackage.getEInt(), "wcet", "0", 1, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_Bcet(), ecorePackage.getEInt(), "bcet", "0", 1, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_Deadline(), ecorePackage.getEInt(), "deadline", "0", 1, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTask_Scheduling_parameter(), ecorePackage.getEInt(), "scheduling_parameter", "0", 1, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_In_eventmodel(), this.getEventModel(), null, "in_eventmodel", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_Precedence_deps(), this.getConnectableElement(), null, "precedence_deps", null, 0, -1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTask_Data_deps(), this.getConnectableElement(), null, "data_deps", null, 0, -1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(schedulerEClass, Scheduler.class, "Scheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScheduler_Kind(), this.getSchedulerKind(), "kind", null, 1, 1, Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventModelEClass, EventModel.class, "EventModel", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pJdEventModelEClass, PJdEventModel.class, "PJdEventModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPJdEventModel_Period(), ecorePackage.getEInt(), "period", "0", 1, 1, PJdEventModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPJdEventModel_Jitter(), ecorePackage.getEInt(), "jitter", "0", 1, 1, PJdEventModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPJdEventModel_Min_distance(), ecorePackage.getEInt(), "min_distance", "0", 1, 1, PJdEventModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPJdEventModel_Offset(), ecorePackage.getEInt(), "offset", "0", 1, 1, PJdEventModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ctEventModelEClass, CTEventModel.class, "CTEventModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCTEventModel_Num_events(), ecorePackage.getEInt(), "num_events", "0", 1, 1, CTEventModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCTEventModel_Time(), ecorePackage.getEInt(), "time", "0", 1, 1, CTEventModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pyCPAElementEClass, PyCPAElement.class, "PyCPAElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPyCPAElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, PyCPAElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(effectChainEClass, EffectChain.class, "EffectChain", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pathEClass, Path.class, "Path", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(connectablesOrderedSetEClass, ConnectablesOrderedSet.class, "ConnectablesOrderedSet", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getConnectablesOrderedSet_Elements(), this.getConnectableElement(), null, "elements", null, 2, -1, ConnectablesOrderedSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(junctionEClass, Junction.class, "Junction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getJunction_Strategy(), this.getJunctionStrategyKind(), "strategy", null, 1, 1, Junction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJunction_Prev_tasks(), this.getTask(), null, "prev_tasks", null, 1, -1, Junction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJunction_Next_task(), this.getTask(), null, "next_task", null, 1, 1, Junction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(forkEClass, Fork.class, "Fork", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFork_Strategy(), ecorePackage.getEString(), "strategy", "model.StandardForkStrategy()", 1, 1, Fork.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(connectableElementEClass, ConnectableElement.class, "ConnectableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(schedulerKindEEnum, SchedulerKind.class, "SchedulerKind");
		addEEnumLiteral(schedulerKindEEnum, SchedulerKind.ROUND_ROBIN);
		addEEnumLiteral(schedulerKindEEnum, SchedulerKind.SP_NP);
		addEEnumLiteral(schedulerKindEEnum, SchedulerKind.SP_P);
		addEEnumLiteral(schedulerKindEEnum, SchedulerKind.SP_PACTIVOFFSETS);

		initEEnum(junctionStrategyKindEEnum, JunctionStrategyKind.class, "JunctionStrategyKind");
		addEEnumLiteral(junctionStrategyKindEEnum, JunctionStrategyKind.AND);
		addEEnumLiteral(junctionStrategyKindEEnum, JunctionStrategyKind.OR);

		// Create resource
		createResource(eNS_URI);
	}

} //PyCPAPackageImpl

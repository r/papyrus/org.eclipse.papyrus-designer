/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EffectChain;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Path;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl#getResources <em>Resources</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl#getPaths <em>Paths</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl#getEffectchains <em>Effectchains</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.impl.SystemImpl#getJunctions <em>Junctions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemImpl extends PyCPAElementImpl implements org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System {
	/**
	 * The cached value of the '{@link #getResources() <em>Resources</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResources()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> resources;

	/**
	 * The cached value of the '{@link #getPaths() <em>Paths</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPaths()
	 * @generated
	 * @ordered
	 */
	protected EList<Path> paths;

	/**
	 * The cached value of the '{@link #getEffectchains() <em>Effectchains</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEffectchains()
	 * @generated
	 * @ordered
	 */
	protected EList<EffectChain> effectchains;

	/**
	 * The cached value of the '{@link #getJunctions() <em>Junctions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<Junction> junctions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PyCPAPackage.Literals.SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getResources() {
		if (resources == null) {
			resources = new EObjectContainmentEList<Resource>(Resource.class, this, PyCPAPackage.SYSTEM__RESOURCES);
		}
		return resources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Path> getPaths() {
		if (paths == null) {
			paths = new EObjectContainmentEList<Path>(Path.class, this, PyCPAPackage.SYSTEM__PATHS);
		}
		return paths;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EffectChain> getEffectchains() {
		if (effectchains == null) {
			effectchains = new EObjectContainmentEList<EffectChain>(EffectChain.class, this, PyCPAPackage.SYSTEM__EFFECTCHAINS);
		}
		return effectchains;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Junction> getJunctions() {
		if (junctions == null) {
			junctions = new EObjectContainmentEList<Junction>(Junction.class, this, PyCPAPackage.SYSTEM__JUNCTIONS);
		}
		return junctions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PyCPAPackage.SYSTEM__RESOURCES:
				return ((InternalEList<?>)getResources()).basicRemove(otherEnd, msgs);
			case PyCPAPackage.SYSTEM__PATHS:
				return ((InternalEList<?>)getPaths()).basicRemove(otherEnd, msgs);
			case PyCPAPackage.SYSTEM__EFFECTCHAINS:
				return ((InternalEList<?>)getEffectchains()).basicRemove(otherEnd, msgs);
			case PyCPAPackage.SYSTEM__JUNCTIONS:
				return ((InternalEList<?>)getJunctions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PyCPAPackage.SYSTEM__RESOURCES:
				return getResources();
			case PyCPAPackage.SYSTEM__PATHS:
				return getPaths();
			case PyCPAPackage.SYSTEM__EFFECTCHAINS:
				return getEffectchains();
			case PyCPAPackage.SYSTEM__JUNCTIONS:
				return getJunctions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PyCPAPackage.SYSTEM__RESOURCES:
				getResources().clear();
				getResources().addAll((Collection<? extends Resource>)newValue);
				return;
			case PyCPAPackage.SYSTEM__PATHS:
				getPaths().clear();
				getPaths().addAll((Collection<? extends Path>)newValue);
				return;
			case PyCPAPackage.SYSTEM__EFFECTCHAINS:
				getEffectchains().clear();
				getEffectchains().addAll((Collection<? extends EffectChain>)newValue);
				return;
			case PyCPAPackage.SYSTEM__JUNCTIONS:
				getJunctions().clear();
				getJunctions().addAll((Collection<? extends Junction>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PyCPAPackage.SYSTEM__RESOURCES:
				getResources().clear();
				return;
			case PyCPAPackage.SYSTEM__PATHS:
				getPaths().clear();
				return;
			case PyCPAPackage.SYSTEM__EFFECTCHAINS:
				getEffectchains().clear();
				return;
			case PyCPAPackage.SYSTEM__JUNCTIONS:
				getJunctions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PyCPAPackage.SYSTEM__RESOURCES:
				return resources != null && !resources.isEmpty();
			case PyCPAPackage.SYSTEM__PATHS:
				return paths != null && !paths.isEmpty();
			case PyCPAPackage.SYSTEM__EFFECTCHAINS:
				return effectchains != null && !effectchains.isEmpty();
			case PyCPAPackage.SYSTEM__JUNCTIONS:
				return junctions != null && !junctions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SystemImpl

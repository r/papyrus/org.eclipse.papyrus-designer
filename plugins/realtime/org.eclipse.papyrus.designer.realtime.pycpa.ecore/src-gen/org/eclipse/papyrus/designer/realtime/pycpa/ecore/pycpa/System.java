/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getResources <em>Resources</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getPaths <em>Paths</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getEffectchains <em>Effectchains</em>}</li>
 *   <li>{@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.System#getJunctions <em>Junctions</em>}</li>
 * </ul>
 *
 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getSystem()
 * @model
 * @generated
 */
public interface System extends PyCPAElement {
	/**
	 * Returns the value of the '<em><b>Resources</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resources</em>' containment reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getSystem_Resources()
	 * @model containment="true"
	 * @generated
	 */
	EList<Resource> getResources();

	/**
	 * Returns the value of the '<em><b>Paths</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Path}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paths</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paths</em>' containment reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getSystem_Paths()
	 * @model containment="true"
	 * @generated
	 */
	EList<Path> getPaths();

	/**
	 * Returns the value of the '<em><b>Effectchains</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EffectChain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Effectchains</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Effectchains</em>' containment reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getSystem_Effectchains()
	 * @model containment="true"
	 * @generated
	 */
	EList<EffectChain> getEffectchains();

	/**
	 * Returns the value of the '<em><b>Junctions</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Junction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Junctions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Junctions</em>' containment reference list.
	 * @see org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAPackage#getSystem_Junctions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Junction> getJunctions();

} // System

/**
 */
package RealTimeSymbols;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see RealTimeSymbols.RealTimeSymbolsFactory
 * @model kind="package"
 * @generated
 */
public interface RealTimeSymbolsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "RealTimeSymbols";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.eclipse.org/papyrus/RealTimeSymbols/1";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "RealTimeSymbols";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RealTimeSymbolsPackage eINSTANCE = RealTimeSymbols.impl.RealTimeSymbolsPackageImpl.init();

	/**
	 * The meta object id for the '{@link RealTimeSymbols.impl.SymbolComputingResourceImpl <em>Symbol Computing Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see RealTimeSymbols.impl.SymbolComputingResourceImpl
	 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolComputingResource()
	 * @generated
	 */
	int SYMBOL_COMPUTING_RESOURCE = 0;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_COMPUTING_RESOURCE__BASE_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Symbol Computing Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_COMPUTING_RESOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol Computing Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_COMPUTING_RESOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link RealTimeSymbols.impl.SymbolSchedulableResourceImpl <em>Symbol Schedulable Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see RealTimeSymbols.impl.SymbolSchedulableResourceImpl
	 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolSchedulableResource()
	 * @generated
	 */
	int SYMBOL_SCHEDULABLE_RESOURCE = 1;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_SCHEDULABLE_RESOURCE__BASE_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Symbol Schedulable Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_SCHEDULABLE_RESOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol Schedulable Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_SCHEDULABLE_RESOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link RealTimeSymbols.impl.SymbolDelayImpl <em>Symbol Delay</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see RealTimeSymbols.impl.SymbolDelayImpl
	 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolDelay()
	 * @generated
	 */
	int SYMBOL_DELAY = 2;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_DELAY__BASE_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Symbol Delay</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_DELAY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol Delay</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_DELAY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link RealTimeSymbols.impl.SymbolWorkloadEventImpl <em>Symbol Workload Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see RealTimeSymbols.impl.SymbolWorkloadEventImpl
	 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolWorkloadEvent()
	 * @generated
	 */
	int SYMBOL_WORKLOAD_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_WORKLOAD_EVENT__BASE_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Symbol Workload Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_WORKLOAD_EVENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol Workload Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_WORKLOAD_EVENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link RealTimeSymbols.impl.SymbolEndToEndFlowImpl <em>Symbol End To End Flow</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see RealTimeSymbols.impl.SymbolEndToEndFlowImpl
	 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolEndToEndFlow()
	 * @generated
	 */
	int SYMBOL_END_TO_END_FLOW = 4;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_END_TO_END_FLOW__BASE_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Symbol End To End Flow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_END_TO_END_FLOW_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol End To End Flow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_END_TO_END_FLOW_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link RealTimeSymbols.impl.SymbolMutualExclusionResourceImpl <em>Symbol Mutual Exclusion Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see RealTimeSymbols.impl.SymbolMutualExclusionResourceImpl
	 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolMutualExclusionResource()
	 * @generated
	 */
	int SYMBOL_MUTUAL_EXCLUSION_RESOURCE = 5;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_MUTUAL_EXCLUSION_RESOURCE__BASE_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Symbol Mutual Exclusion Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_MUTUAL_EXCLUSION_RESOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol Mutual Exclusion Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_MUTUAL_EXCLUSION_RESOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link RealTimeSymbols.impl.SymbolRealTimeObservationImpl <em>Symbol Real Time Observation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see RealTimeSymbols.impl.SymbolRealTimeObservationImpl
	 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolRealTimeObservation()
	 * @generated
	 */
	int SYMBOL_REAL_TIME_OBSERVATION = 6;

	/**
	 * The feature id for the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_REAL_TIME_OBSERVATION__BASE_ELEMENT = 0;

	/**
	 * The number of structural features of the '<em>Symbol Real Time Observation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_REAL_TIME_OBSERVATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Symbol Real Time Observation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYMBOL_REAL_TIME_OBSERVATION_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link RealTimeSymbols.SymbolComputingResource <em>Symbol Computing Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Computing Resource</em>'.
	 * @see RealTimeSymbols.SymbolComputingResource
	 * @generated
	 */
	EClass getSymbolComputingResource();

	/**
	 * Returns the meta object for the reference '{@link RealTimeSymbols.SymbolComputingResource#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see RealTimeSymbols.SymbolComputingResource#getBase_Element()
	 * @see #getSymbolComputingResource()
	 * @generated
	 */
	EReference getSymbolComputingResource_Base_Element();

	/**
	 * Returns the meta object for class '{@link RealTimeSymbols.SymbolSchedulableResource <em>Symbol Schedulable Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Schedulable Resource</em>'.
	 * @see RealTimeSymbols.SymbolSchedulableResource
	 * @generated
	 */
	EClass getSymbolSchedulableResource();

	/**
	 * Returns the meta object for the reference '{@link RealTimeSymbols.SymbolSchedulableResource#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see RealTimeSymbols.SymbolSchedulableResource#getBase_Element()
	 * @see #getSymbolSchedulableResource()
	 * @generated
	 */
	EReference getSymbolSchedulableResource_Base_Element();

	/**
	 * Returns the meta object for class '{@link RealTimeSymbols.SymbolDelay <em>Symbol Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Delay</em>'.
	 * @see RealTimeSymbols.SymbolDelay
	 * @generated
	 */
	EClass getSymbolDelay();

	/**
	 * Returns the meta object for the reference '{@link RealTimeSymbols.SymbolDelay#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see RealTimeSymbols.SymbolDelay#getBase_Element()
	 * @see #getSymbolDelay()
	 * @generated
	 */
	EReference getSymbolDelay_Base_Element();

	/**
	 * Returns the meta object for class '{@link RealTimeSymbols.SymbolWorkloadEvent <em>Symbol Workload Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Workload Event</em>'.
	 * @see RealTimeSymbols.SymbolWorkloadEvent
	 * @generated
	 */
	EClass getSymbolWorkloadEvent();

	/**
	 * Returns the meta object for the reference '{@link RealTimeSymbols.SymbolWorkloadEvent#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see RealTimeSymbols.SymbolWorkloadEvent#getBase_Element()
	 * @see #getSymbolWorkloadEvent()
	 * @generated
	 */
	EReference getSymbolWorkloadEvent_Base_Element();

	/**
	 * Returns the meta object for class '{@link RealTimeSymbols.SymbolEndToEndFlow <em>Symbol End To End Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol End To End Flow</em>'.
	 * @see RealTimeSymbols.SymbolEndToEndFlow
	 * @generated
	 */
	EClass getSymbolEndToEndFlow();

	/**
	 * Returns the meta object for the reference '{@link RealTimeSymbols.SymbolEndToEndFlow#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see RealTimeSymbols.SymbolEndToEndFlow#getBase_Element()
	 * @see #getSymbolEndToEndFlow()
	 * @generated
	 */
	EReference getSymbolEndToEndFlow_Base_Element();

	/**
	 * Returns the meta object for class '{@link RealTimeSymbols.SymbolMutualExclusionResource <em>Symbol Mutual Exclusion Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Mutual Exclusion Resource</em>'.
	 * @see RealTimeSymbols.SymbolMutualExclusionResource
	 * @generated
	 */
	EClass getSymbolMutualExclusionResource();

	/**
	 * Returns the meta object for the reference '{@link RealTimeSymbols.SymbolMutualExclusionResource#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see RealTimeSymbols.SymbolMutualExclusionResource#getBase_Element()
	 * @see #getSymbolMutualExclusionResource()
	 * @generated
	 */
	EReference getSymbolMutualExclusionResource_Base_Element();

	/**
	 * Returns the meta object for class '{@link RealTimeSymbols.SymbolRealTimeObservation <em>Symbol Real Time Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Symbol Real Time Observation</em>'.
	 * @see RealTimeSymbols.SymbolRealTimeObservation
	 * @generated
	 */
	EClass getSymbolRealTimeObservation();

	/**
	 * Returns the meta object for the reference '{@link RealTimeSymbols.SymbolRealTimeObservation#getBase_Element <em>Base Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Base Element</em>'.
	 * @see RealTimeSymbols.SymbolRealTimeObservation#getBase_Element()
	 * @see #getSymbolRealTimeObservation()
	 * @generated
	 */
	EReference getSymbolRealTimeObservation_Base_Element();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RealTimeSymbolsFactory getRealTimeSymbolsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link RealTimeSymbols.impl.SymbolComputingResourceImpl <em>Symbol Computing Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see RealTimeSymbols.impl.SymbolComputingResourceImpl
		 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolComputingResource()
		 * @generated
		 */
		EClass SYMBOL_COMPUTING_RESOURCE = eINSTANCE.getSymbolComputingResource();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_COMPUTING_RESOURCE__BASE_ELEMENT = eINSTANCE.getSymbolComputingResource_Base_Element();

		/**
		 * The meta object literal for the '{@link RealTimeSymbols.impl.SymbolSchedulableResourceImpl <em>Symbol Schedulable Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see RealTimeSymbols.impl.SymbolSchedulableResourceImpl
		 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolSchedulableResource()
		 * @generated
		 */
		EClass SYMBOL_SCHEDULABLE_RESOURCE = eINSTANCE.getSymbolSchedulableResource();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_SCHEDULABLE_RESOURCE__BASE_ELEMENT = eINSTANCE.getSymbolSchedulableResource_Base_Element();

		/**
		 * The meta object literal for the '{@link RealTimeSymbols.impl.SymbolDelayImpl <em>Symbol Delay</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see RealTimeSymbols.impl.SymbolDelayImpl
		 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolDelay()
		 * @generated
		 */
		EClass SYMBOL_DELAY = eINSTANCE.getSymbolDelay();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_DELAY__BASE_ELEMENT = eINSTANCE.getSymbolDelay_Base_Element();

		/**
		 * The meta object literal for the '{@link RealTimeSymbols.impl.SymbolWorkloadEventImpl <em>Symbol Workload Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see RealTimeSymbols.impl.SymbolWorkloadEventImpl
		 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolWorkloadEvent()
		 * @generated
		 */
		EClass SYMBOL_WORKLOAD_EVENT = eINSTANCE.getSymbolWorkloadEvent();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_WORKLOAD_EVENT__BASE_ELEMENT = eINSTANCE.getSymbolWorkloadEvent_Base_Element();

		/**
		 * The meta object literal for the '{@link RealTimeSymbols.impl.SymbolEndToEndFlowImpl <em>Symbol End To End Flow</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see RealTimeSymbols.impl.SymbolEndToEndFlowImpl
		 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolEndToEndFlow()
		 * @generated
		 */
		EClass SYMBOL_END_TO_END_FLOW = eINSTANCE.getSymbolEndToEndFlow();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_END_TO_END_FLOW__BASE_ELEMENT = eINSTANCE.getSymbolEndToEndFlow_Base_Element();

		/**
		 * The meta object literal for the '{@link RealTimeSymbols.impl.SymbolMutualExclusionResourceImpl <em>Symbol Mutual Exclusion Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see RealTimeSymbols.impl.SymbolMutualExclusionResourceImpl
		 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolMutualExclusionResource()
		 * @generated
		 */
		EClass SYMBOL_MUTUAL_EXCLUSION_RESOURCE = eINSTANCE.getSymbolMutualExclusionResource();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_MUTUAL_EXCLUSION_RESOURCE__BASE_ELEMENT = eINSTANCE.getSymbolMutualExclusionResource_Base_Element();

		/**
		 * The meta object literal for the '{@link RealTimeSymbols.impl.SymbolRealTimeObservationImpl <em>Symbol Real Time Observation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see RealTimeSymbols.impl.SymbolRealTimeObservationImpl
		 * @see RealTimeSymbols.impl.RealTimeSymbolsPackageImpl#getSymbolRealTimeObservation()
		 * @generated
		 */
		EClass SYMBOL_REAL_TIME_OBSERVATION = eINSTANCE.getSymbolRealTimeObservation();

		/**
		 * The meta object literal for the '<em><b>Base Element</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SYMBOL_REAL_TIME_OBSERVATION__BASE_ELEMENT = eINSTANCE.getSymbolRealTimeObservation_Base_Element();

	}

} //RealTimeSymbolsPackage

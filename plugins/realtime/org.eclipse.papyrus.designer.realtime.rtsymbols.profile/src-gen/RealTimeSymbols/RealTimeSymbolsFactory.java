/**
 */
package RealTimeSymbols;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see RealTimeSymbols.RealTimeSymbolsPackage
 * @generated
 */
public interface RealTimeSymbolsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RealTimeSymbolsFactory eINSTANCE = RealTimeSymbols.impl.RealTimeSymbolsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Symbol Computing Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Computing Resource</em>'.
	 * @generated
	 */
	SymbolComputingResource createSymbolComputingResource();

	/**
	 * Returns a new object of class '<em>Symbol Schedulable Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Schedulable Resource</em>'.
	 * @generated
	 */
	SymbolSchedulableResource createSymbolSchedulableResource();

	/**
	 * Returns a new object of class '<em>Symbol Delay</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Delay</em>'.
	 * @generated
	 */
	SymbolDelay createSymbolDelay();

	/**
	 * Returns a new object of class '<em>Symbol Workload Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Workload Event</em>'.
	 * @generated
	 */
	SymbolWorkloadEvent createSymbolWorkloadEvent();

	/**
	 * Returns a new object of class '<em>Symbol End To End Flow</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol End To End Flow</em>'.
	 * @generated
	 */
	SymbolEndToEndFlow createSymbolEndToEndFlow();

	/**
	 * Returns a new object of class '<em>Symbol Mutual Exclusion Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Mutual Exclusion Resource</em>'.
	 * @generated
	 */
	SymbolMutualExclusionResource createSymbolMutualExclusionResource();

	/**
	 * Returns a new object of class '<em>Symbol Real Time Observation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Symbol Real Time Observation</em>'.
	 * @generated
	 */
	SymbolRealTimeObservation createSymbolRealTimeObservation();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	RealTimeSymbolsPackage getRealTimeSymbolsPackage();

} //RealTimeSymbolsFactory

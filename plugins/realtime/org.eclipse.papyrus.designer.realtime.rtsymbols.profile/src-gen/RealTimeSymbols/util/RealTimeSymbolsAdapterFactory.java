/**
 */
package RealTimeSymbols.util;

import RealTimeSymbols.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see RealTimeSymbols.RealTimeSymbolsPackage
 * @generated
 */
public class RealTimeSymbolsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RealTimeSymbolsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealTimeSymbolsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = RealTimeSymbolsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RealTimeSymbolsSwitch<Adapter> modelSwitch =
		new RealTimeSymbolsSwitch<Adapter>() {
			@Override
			public Adapter caseSymbolComputingResource(SymbolComputingResource object) {
				return createSymbolComputingResourceAdapter();
			}
			@Override
			public Adapter caseSymbolSchedulableResource(SymbolSchedulableResource object) {
				return createSymbolSchedulableResourceAdapter();
			}
			@Override
			public Adapter caseSymbolDelay(SymbolDelay object) {
				return createSymbolDelayAdapter();
			}
			@Override
			public Adapter caseSymbolWorkloadEvent(SymbolWorkloadEvent object) {
				return createSymbolWorkloadEventAdapter();
			}
			@Override
			public Adapter caseSymbolEndToEndFlow(SymbolEndToEndFlow object) {
				return createSymbolEndToEndFlowAdapter();
			}
			@Override
			public Adapter caseSymbolMutualExclusionResource(SymbolMutualExclusionResource object) {
				return createSymbolMutualExclusionResourceAdapter();
			}
			@Override
			public Adapter caseSymbolRealTimeObservation(SymbolRealTimeObservation object) {
				return createSymbolRealTimeObservationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link RealTimeSymbols.SymbolComputingResource <em>Symbol Computing Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see RealTimeSymbols.SymbolComputingResource
	 * @generated
	 */
	public Adapter createSymbolComputingResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link RealTimeSymbols.SymbolSchedulableResource <em>Symbol Schedulable Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see RealTimeSymbols.SymbolSchedulableResource
	 * @generated
	 */
	public Adapter createSymbolSchedulableResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link RealTimeSymbols.SymbolDelay <em>Symbol Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see RealTimeSymbols.SymbolDelay
	 * @generated
	 */
	public Adapter createSymbolDelayAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link RealTimeSymbols.SymbolWorkloadEvent <em>Symbol Workload Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see RealTimeSymbols.SymbolWorkloadEvent
	 * @generated
	 */
	public Adapter createSymbolWorkloadEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link RealTimeSymbols.SymbolEndToEndFlow <em>Symbol End To End Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see RealTimeSymbols.SymbolEndToEndFlow
	 * @generated
	 */
	public Adapter createSymbolEndToEndFlowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link RealTimeSymbols.SymbolMutualExclusionResource <em>Symbol Mutual Exclusion Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see RealTimeSymbols.SymbolMutualExclusionResource
	 * @generated
	 */
	public Adapter createSymbolMutualExclusionResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link RealTimeSymbols.SymbolRealTimeObservation <em>Symbol Real Time Observation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see RealTimeSymbols.SymbolRealTimeObservation
	 * @generated
	 */
	public Adapter createSymbolRealTimeObservationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //RealTimeSymbolsAdapterFactory

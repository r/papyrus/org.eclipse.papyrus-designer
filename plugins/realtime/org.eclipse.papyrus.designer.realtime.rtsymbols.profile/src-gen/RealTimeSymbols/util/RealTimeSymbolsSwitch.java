/**
 */
package RealTimeSymbols.util;

import RealTimeSymbols.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see RealTimeSymbols.RealTimeSymbolsPackage
 * @generated
 */
public class RealTimeSymbolsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static RealTimeSymbolsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealTimeSymbolsSwitch() {
		if (modelPackage == null) {
			modelPackage = RealTimeSymbolsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case RealTimeSymbolsPackage.SYMBOL_COMPUTING_RESOURCE: {
				SymbolComputingResource symbolComputingResource = (SymbolComputingResource)theEObject;
				T result = caseSymbolComputingResource(symbolComputingResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RealTimeSymbolsPackage.SYMBOL_SCHEDULABLE_RESOURCE: {
				SymbolSchedulableResource symbolSchedulableResource = (SymbolSchedulableResource)theEObject;
				T result = caseSymbolSchedulableResource(symbolSchedulableResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RealTimeSymbolsPackage.SYMBOL_DELAY: {
				SymbolDelay symbolDelay = (SymbolDelay)theEObject;
				T result = caseSymbolDelay(symbolDelay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RealTimeSymbolsPackage.SYMBOL_WORKLOAD_EVENT: {
				SymbolWorkloadEvent symbolWorkloadEvent = (SymbolWorkloadEvent)theEObject;
				T result = caseSymbolWorkloadEvent(symbolWorkloadEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RealTimeSymbolsPackage.SYMBOL_END_TO_END_FLOW: {
				SymbolEndToEndFlow symbolEndToEndFlow = (SymbolEndToEndFlow)theEObject;
				T result = caseSymbolEndToEndFlow(symbolEndToEndFlow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RealTimeSymbolsPackage.SYMBOL_MUTUAL_EXCLUSION_RESOURCE: {
				SymbolMutualExclusionResource symbolMutualExclusionResource = (SymbolMutualExclusionResource)theEObject;
				T result = caseSymbolMutualExclusionResource(symbolMutualExclusionResource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case RealTimeSymbolsPackage.SYMBOL_REAL_TIME_OBSERVATION: {
				SymbolRealTimeObservation symbolRealTimeObservation = (SymbolRealTimeObservation)theEObject;
				T result = caseSymbolRealTimeObservation(symbolRealTimeObservation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Computing Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Computing Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolComputingResource(SymbolComputingResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Schedulable Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Schedulable Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolSchedulableResource(SymbolSchedulableResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Delay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Delay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolDelay(SymbolDelay object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Workload Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Workload Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolWorkloadEvent(SymbolWorkloadEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol End To End Flow</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol End To End Flow</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolEndToEndFlow(SymbolEndToEndFlow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Mutual Exclusion Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Mutual Exclusion Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolMutualExclusionResource(SymbolMutualExclusionResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Symbol Real Time Observation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Symbol Real Time Observation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSymbolRealTimeObservation(SymbolRealTimeObservation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //RealTimeSymbolsSwitch

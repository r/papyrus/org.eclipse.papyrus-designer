/**
 */
package RealTimeSymbols;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Symbol End To End Flow</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link RealTimeSymbols.SymbolEndToEndFlow#getBase_Element <em>Base Element</em>}</li>
 * </ul>
 *
 * @see RealTimeSymbols.RealTimeSymbolsPackage#getSymbolEndToEndFlow()
 * @model
 * @generated
 */
public interface SymbolEndToEndFlow extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Element</em>' reference.
	 * @see #setBase_Element(Element)
	 * @see RealTimeSymbols.RealTimeSymbolsPackage#getSymbolEndToEndFlow_Base_Element()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Element getBase_Element();

	/**
	 * Sets the value of the '{@link RealTimeSymbols.SymbolEndToEndFlow#getBase_Element <em>Base Element</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Element</em>' reference.
	 * @see #getBase_Element()
	 * @generated
	 */
	void setBase_Element(Element value);

} // SymbolEndToEndFlow

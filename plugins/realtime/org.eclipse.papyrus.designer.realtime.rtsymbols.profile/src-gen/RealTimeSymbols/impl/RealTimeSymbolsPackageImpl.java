/**
 */
package RealTimeSymbols.impl;

import RealTimeSymbols.RealTimeSymbolsFactory;
import RealTimeSymbols.RealTimeSymbolsPackage;
import RealTimeSymbols.SymbolComputingResource;
import RealTimeSymbols.SymbolDelay;
import RealTimeSymbols.SymbolEndToEndFlow;
import RealTimeSymbols.SymbolMutualExclusionResource;
import RealTimeSymbols.SymbolRealTimeObservation;
import RealTimeSymbols.SymbolSchedulableResource;
import RealTimeSymbols.SymbolWorkloadEvent;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.uml2.types.TypesPackage;

import org.eclipse.uml2.uml.UMLPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RealTimeSymbolsPackageImpl extends EPackageImpl implements RealTimeSymbolsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolComputingResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolSchedulableResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolDelayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolWorkloadEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolEndToEndFlowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolMutualExclusionResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass symbolRealTimeObservationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see RealTimeSymbols.RealTimeSymbolsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RealTimeSymbolsPackageImpl() {
		super(eNS_URI, RealTimeSymbolsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link RealTimeSymbolsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RealTimeSymbolsPackage init() {
		if (isInited) return (RealTimeSymbolsPackage)EPackage.Registry.INSTANCE.getEPackage(RealTimeSymbolsPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredRealTimeSymbolsPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		RealTimeSymbolsPackageImpl theRealTimeSymbolsPackage = registeredRealTimeSymbolsPackage instanceof RealTimeSymbolsPackageImpl ? (RealTimeSymbolsPackageImpl)registeredRealTimeSymbolsPackage : new RealTimeSymbolsPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();
		TypesPackage.eINSTANCE.eClass();
		UMLPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theRealTimeSymbolsPackage.createPackageContents();

		// Initialize created meta-data
		theRealTimeSymbolsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRealTimeSymbolsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RealTimeSymbolsPackage.eNS_URI, theRealTimeSymbolsPackage);
		return theRealTimeSymbolsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolComputingResource() {
		return symbolComputingResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolComputingResource_Base_Element() {
		return (EReference)symbolComputingResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolSchedulableResource() {
		return symbolSchedulableResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolSchedulableResource_Base_Element() {
		return (EReference)symbolSchedulableResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolDelay() {
		return symbolDelayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolDelay_Base_Element() {
		return (EReference)symbolDelayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolWorkloadEvent() {
		return symbolWorkloadEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolWorkloadEvent_Base_Element() {
		return (EReference)symbolWorkloadEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolEndToEndFlow() {
		return symbolEndToEndFlowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolEndToEndFlow_Base_Element() {
		return (EReference)symbolEndToEndFlowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolMutualExclusionResource() {
		return symbolMutualExclusionResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolMutualExclusionResource_Base_Element() {
		return (EReference)symbolMutualExclusionResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSymbolRealTimeObservation() {
		return symbolRealTimeObservationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSymbolRealTimeObservation_Base_Element() {
		return (EReference)symbolRealTimeObservationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealTimeSymbolsFactory getRealTimeSymbolsFactory() {
		return (RealTimeSymbolsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		symbolComputingResourceEClass = createEClass(SYMBOL_COMPUTING_RESOURCE);
		createEReference(symbolComputingResourceEClass, SYMBOL_COMPUTING_RESOURCE__BASE_ELEMENT);

		symbolSchedulableResourceEClass = createEClass(SYMBOL_SCHEDULABLE_RESOURCE);
		createEReference(symbolSchedulableResourceEClass, SYMBOL_SCHEDULABLE_RESOURCE__BASE_ELEMENT);

		symbolDelayEClass = createEClass(SYMBOL_DELAY);
		createEReference(symbolDelayEClass, SYMBOL_DELAY__BASE_ELEMENT);

		symbolWorkloadEventEClass = createEClass(SYMBOL_WORKLOAD_EVENT);
		createEReference(symbolWorkloadEventEClass, SYMBOL_WORKLOAD_EVENT__BASE_ELEMENT);

		symbolEndToEndFlowEClass = createEClass(SYMBOL_END_TO_END_FLOW);
		createEReference(symbolEndToEndFlowEClass, SYMBOL_END_TO_END_FLOW__BASE_ELEMENT);

		symbolMutualExclusionResourceEClass = createEClass(SYMBOL_MUTUAL_EXCLUSION_RESOURCE);
		createEReference(symbolMutualExclusionResourceEClass, SYMBOL_MUTUAL_EXCLUSION_RESOURCE__BASE_ELEMENT);

		symbolRealTimeObservationEClass = createEClass(SYMBOL_REAL_TIME_OBSERVATION);
		createEReference(symbolRealTimeObservationEClass, SYMBOL_REAL_TIME_OBSERVATION__BASE_ELEMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		UMLPackage theUMLPackage = (UMLPackage)EPackage.Registry.INSTANCE.getEPackage(UMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(symbolComputingResourceEClass, SymbolComputingResource.class, "SymbolComputingResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolComputingResource_Base_Element(), theUMLPackage.getElement(), null, "base_Element", null, 1, 1, SymbolComputingResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(symbolSchedulableResourceEClass, SymbolSchedulableResource.class, "SymbolSchedulableResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolSchedulableResource_Base_Element(), theUMLPackage.getElement(), null, "base_Element", null, 1, 1, SymbolSchedulableResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(symbolDelayEClass, SymbolDelay.class, "SymbolDelay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolDelay_Base_Element(), theUMLPackage.getElement(), null, "base_Element", null, 1, 1, SymbolDelay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(symbolWorkloadEventEClass, SymbolWorkloadEvent.class, "SymbolWorkloadEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolWorkloadEvent_Base_Element(), theUMLPackage.getElement(), null, "base_Element", null, 1, 1, SymbolWorkloadEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(symbolEndToEndFlowEClass, SymbolEndToEndFlow.class, "SymbolEndToEndFlow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolEndToEndFlow_Base_Element(), theUMLPackage.getElement(), null, "base_Element", null, 1, 1, SymbolEndToEndFlow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(symbolMutualExclusionResourceEClass, SymbolMutualExclusionResource.class, "SymbolMutualExclusionResource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolMutualExclusionResource_Base_Element(), theUMLPackage.getElement(), null, "base_Element", null, 1, 1, SymbolMutualExclusionResource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(symbolRealTimeObservationEClass, SymbolRealTimeObservation.class, "SymbolRealTimeObservation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSymbolRealTimeObservation_Base_Element(), theUMLPackage.getElement(), null, "base_Element", null, 1, 1, SymbolRealTimeObservation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //RealTimeSymbolsPackageImpl

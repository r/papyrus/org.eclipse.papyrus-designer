from pycpa import simulation
from pycpa import analysis
from pycpa import options
from operator import itemgetter
import random
import numpy as np
import plotly.offline as py
import plotly.figure_factory as ff
import plotly.graph_objs as go

def define_colors(s):
    colors = {}
    for r in s.resources:
        for t in r.tasks:
            tClr = t.name;
            _r = lambda: random.randint(0,255);
            colors[tClr]='#%02X%02X%02X' % (_r(),_r(),_r());
    return colors;

def parse_options():
    options.init_pycpa()

def analyze_system(s):
    # perform the analysis
    results = analysis.analyze_system(s);

    # simulate resources
    for r in s.resources:
        simmodel = simulation.ResourceModel(r);
        hptasks = sorted(r.tasks, key=lambda x: x.scheduling_parameter, reverse=False);
        simmodel.runModel(task=hptasks[-1], scheduler=simulation.SimSPP(name="SPP", sim=simmodel));
    return results;

def plot_gantt(r, res, colors_):
    # collect the input data
    df = []; annotations = []; colors = {};
    activs_min_dist = 1; t_count = len(r.tasks);
    for t in sorted(r.tasks, key=str):
        t_count -= 1;
        t_a = []; t_anp_g = [];
        colors[t.name]=colors_[t.name];
        for u in range(0, len(t.q_exec_windows)):
            # save activation times
            t_a.append(t.in_event_model.delta_min(u+1));
            # exec time datas
            for v in range(0, len(t.q_exec_windows[u])):
                tsrt = '2014-03-03 23:30:30.%03d' % t.q_exec_windows[u][v][0];
                tfsh = '2014-03-03 23:30:30.%03d' % t.q_exec_windows[u][v][1];
                df.append(dict( Task=t.name, Start=tsrt, Finish=tfsh, Resource=t.name ));
        # group task activations that are next to each other (multiple activations)
        t_anp   = np.array(t_a);
        t_anp_i = np.concatenate(([0],np.flatnonzero(abs(t_anp[:-1]-t_anp[1:]) > activs_min_dist)+1,[t_anp.size]));
        t_anp_g = zip(t_anp[t_anp_i[:-1]],np.diff(t_anp_i));
        for a in t_anp_g:
            annotations.append(go.layout.Annotation(
                                            text='%s' % a[1],
                                            x='2014-03-03 23:30:30.%03d' % a[0],
                                            y=t_count,
                                            ax=0,
                                            ay=-50,
                                            arrowcolor = "black",
                                            arrowhead = 5,
                                            arrowsize = 4,
                                            arrowwidth = 3,
                                            arrowside = 'start'
                                        ));

    # prepare plotting and create fig 
    df_ = sorted(df, key=itemgetter('Resource'));
    fig = ff.create_gantt(df_, colors=colors, index_col='Resource', bar_width=0.2, show_colorbar=True, group_tasks=True, title=r.name);
    fig['layout'].update(
        xaxis=dict(tickformat="%f"),
        annotations=annotations
    );

    # connect jupyter notebook and plot
    py.init_notebook_mode(connected=True);
    py.iplot(fig, filename='gantt-'+r.name);

def plot_bwcrt(r, res, colors_):
    # collect the input data
    labels = []; colors = []; wcrt = []; bcrt = [];
    for t in r.tasks:
        labels.append(t.name);
        colors.append(colors_[t.name]);
        wcrt.append(res[t].wcrt);
        bcrt.append(res[t].bcrt);

    # create traces
    trace_b = go.Pie(labels=labels, values=bcrt, hole=.55,
                   hoverinfo='label+percent', textinfo='value', 
                   textfont=dict(size=20),
                   domain=dict(x=[0,.48]),
                   marker=dict(colors=colors, 
                               line=dict(color='#000000', width=2)))
    trace_w = go.Pie(labels=labels, values=wcrt, hole=.55,
                   hoverinfo='label+percent', textinfo='value', 
                   textfont=dict(size=20),
                   domain=dict(x=[.52,1]),
                   marker=dict(colors=colors, 
                               line=dict(color='#000000', width=2)))

    # prepare plotting and convert to fig 
    layout=go.Layout(
        annotations=[
            dict(
                text="BCRT",
                x=.2,
                y=.5,
                font=dict(size=20),
                showarrow=False
            ),
            dict(
                text="WCRT",
                x=.8,
                y=.5,
                font=dict(size=20),
                showarrow=False
            )
        ]
    )
    fig = go.Figure(data=[trace_b,trace_w], layout=layout);

    # connect jupyter notebook and plot
    py.init_notebook_mode(connected=True);
    py.iplot(fig, filename='donut-'+r.name);

def plot_bwcrt(r, res, colors_):
    # collect the input data
    labels = []; colors = []; wcrt = []; bcrt = [];
    for t in r.tasks:
        labels.append(t.name);
        colors.append(colors_[t.name]);
        wcrt.append(res[t].wcrt);
        bcrt.append(res[t].bcrt);

    # create traces
    trace_b = go.Pie(labels=labels, values=bcrt, hole=.55,
                   hoverinfo='label+percent', textinfo='value', 
                   textfont=dict(size=20),
                   domain=dict(x=[0,.48]),
                   marker=dict(colors=colors, 
                               line=dict(color='#000000', width=2)))
    trace_w = go.Pie(labels=labels, values=wcrt, hole=.55,
                   hoverinfo='label+percent', textinfo='value', 
                   textfont=dict(size=20),
                   domain=dict(x=[.52,1]),
                   marker=dict(colors=colors, 
                               line=dict(color='#000000', width=2)))

    # prepare plotting and convert to fig 
    layout=go.Layout(
        annotations=[
            dict(
                text="BCRT",
                x=.2,
                y=.5,
                font=dict(size=20),
                showarrow=False
            ),
            dict(
                text="WCRT",
                x=.8,
                y=.5,
                font=dict(size=20),
                showarrow=False
            )
        ]
    )
    fig = go.Figure(data=[trace_b,trace_w], layout=layout);

    # connect jupyter notebook and plot
    py.init_notebook_mode(connected=True);
    py.iplot(fig, filename='donut-'+r.name);

def plot_load(r, colors_):
    # collect the input data
    labels = []; colors = []; load = []; bcrt = [];
    totalLoad = 0
    for t in r.tasks:
        labels.append(t.name);
        colors.append(colors_[t.name]);
        taskLoad = int(t.load() * 100)
        load.append(taskLoad);
        totalLoad += taskLoad
    if totalLoad < 100:
        labels.append("(Free load)")
        colors.append("white")
        load.append(100 - totalLoad)

    # create traces
    trace = go.Pie(labels=labels, values=load, hole=.55,
                   hoverinfo='label+percent', textinfo='value', 
                   textfont=dict(size=20),
                   domain=dict(x=[.25,.75]),
                   marker=dict(colors=colors, 
                               line=dict(color='#000000', width=2)))

    # prepare plotting and convert to fig 
    layout=go.Layout(
        annotations=[
            dict(
                text="Load",
                x=.5,
                y=.5,
                font=dict(size=20),
                showarrow=False
            )
        ]
    )
    fig = go.Figure(data=[trace], layout=layout);

    # connect jupyter notebook and plot
    py.init_notebook_mode(connected=True);
    py.iplot(fig, filename='donut-'+r.name);
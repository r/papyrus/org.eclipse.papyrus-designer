package org.eclipse.papyrus.designer.realtime.pycpa.m2t.executors;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.papyrus.designer.realtime.pycpa.m2t.batch.GenerateAnalysis;
import org.osgi.framework.Bundle;

/**
 * Main entry point of the 'GenerateAnalysis' generation module.
 */
public class PyCPAModelToPythonCodeTransformExecutor {

	/**
	 * The model URI.
	 */
	private URI modelURI;

	/**
	 * The output folder.
	 */
	private IContainer targetFolder;

	/**
	 * The other arguments.
	 */
	List<? extends Object> arguments;

	/**
	 * Constructor.
	 * 
	 * @param modelURI
	 *            is the URI of the model.
	 * @param targetFolder
	 *            is the output folder
	 * @param arguments
	 *            are the other arguments
	 * @throws IOException
	 *             Thrown when the output cannot be saved.
	 * @generated
	 */
	public PyCPAModelToPythonCodeTransformExecutor(URI modelURI, IContainer targetFolder, List<? extends Object> arguments)
	{
		this.modelURI = modelURI;
		this.targetFolder = targetFolder;
		this.arguments = arguments;
	}

	/**
	 * Launches the generation.
	 *
	 * @param monitor
	 *            This will be used to display progress information to the user.
	 * @throws IOException
	 *             Thrown when the output cannot be saved.
	 * @generated
	 */
	public void doGenerate(IProgressMonitor monitor) throws IOException {
		if (!targetFolder.getLocation().toFile().exists()) {
			targetFolder.getLocation().toFile().mkdirs();
		}

		if (monitor != null) {
			monitor.subTask("Loading...");
		}

		GenerateAnalysis gen0 = new GenerateAnalysis(modelURI, targetFolder.getLocation().toFile(), arguments);
		if (monitor != null) {
			monitor.worked(1);
		}
		String generationID = org.eclipse.acceleo.engine.utils.AcceleoLaunchingUtil.computeUIProjectID("org.eclipse.papyrus.designer.realtime.pycpa.m2t", "org.eclipse.papyrus.designer.realtime.pycpa.m2t.batch.GenerateAnalysis", modelURI.toString(), targetFolder.getFullPath().toString(), new ArrayList<String>());
		gen0.setGenerationID(generationID);
		if (monitor != null) {
			gen0.doGenerate(BasicMonitor.toMonitor(monitor));
		} else {
			gen0.doGenerate(null);
		}

		
		IFile rtmlAnalysisScript = targetFolder.getFile(new Path("pycpa_analysis.py"));
		if (!rtmlAnalysisScript.exists()) {
			Bundle bundle = Platform.getBundle("org.eclipse.papyrus.designer.realtime.pycpa.m2t");
			URL fileURL = bundle.getEntry("python/pycpa_analysis.py");
			File bundleAnalysisScript = null;
			try {
				bundleAnalysisScript = new File(FileLocator.resolve(fileURL).toURI());
				byte[] bytes = readFileToByteArray(bundleAnalysisScript);
				InputStream source = new ByteArrayInputStream(bytes);
				rtmlAnalysisScript.create(source, IResource.NONE, null);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
	}

	private static byte[] readFileToByteArray(File file){
		FileInputStream fis = null;
		byte[] bArray = new byte[(int) file.length()];
		try{
			fis = new FileInputStream(file);
			fis.read(bArray);
			fis.close();        

		}catch(IOException ioExp){
			ioExp.printStackTrace();
		}
		return bArray;
	}
}

/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.tests;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.TasksOrderedSet;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Tasks Ordered Set</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class TasksOrderedSetTest extends PyCPAElementTest {

	/**
	 * Constructs a new Tasks Ordered Set test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TasksOrderedSetTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Tasks Ordered Set test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected TasksOrderedSet getFixture() {
		return (TasksOrderedSet)fixture;
	}

} //TasksOrderedSetTest

/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.tests;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.EventModel;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Event Model</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class EventModelTest extends PyCPAElementTest {

	/**
	 * Constructs a new Event Model test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventModelTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Event Model test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EventModel getFixture() {
		return (EventModel)fixture;
	}

} //EventModelTest

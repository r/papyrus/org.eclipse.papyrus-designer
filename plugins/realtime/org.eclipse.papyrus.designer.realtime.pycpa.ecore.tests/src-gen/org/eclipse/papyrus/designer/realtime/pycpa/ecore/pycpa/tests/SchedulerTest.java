/**
 */
package org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.tests;

import junit.textui.TestRunner;

import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.PyCPAFactory;
import org.eclipse.papyrus.designer.realtime.pycpa.ecore.pycpa.Scheduler;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SchedulerTest extends PyCPAElementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(SchedulerTest.class);
	}

	/**
	 * Constructs a new Scheduler test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SchedulerTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Scheduler test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Scheduler getFixture() {
		return (Scheduler)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(PyCPAFactory.eINSTANCE.createScheduler());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //SchedulerTest

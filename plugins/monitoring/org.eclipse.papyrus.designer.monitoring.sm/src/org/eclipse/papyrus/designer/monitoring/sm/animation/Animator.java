/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.animation;

import org.eclipse.papyrus.designer.monitoring.sm.data.Message;
import org.eclipse.papyrus.designer.monitoring.sm.data.StateMachineModel;
import org.eclipse.papyrus.designer.transformation.profile.Transformation.Monitored;
import org.eclipse.papyrus.moka.animation.engine.rendering.AnimationEngine;
import org.eclipse.papyrus.moka.animation.engine.rendering.AnimationKind;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.util.UMLUtil;

public class Animator {
	private StateMachineModel stateMachineModel;
	private AnimationEngine animationEngine;

	private boolean initialized = false;
	private boolean noMarkers = true;

	public Animator(StateMachineModel stateMachineModel) {
		this.stateMachineModel = stateMachineModel;
		animationEngine = new AnimationEngine();
	}

	public synchronized void animate(String metaClass, String qualifiedName) {
		if (metaClass != null && qualifiedName != null) {
			if (metaClass.equals(Message.TRANSITION_HEADER)) {
				Transition transition = stateMachineModel.getTransitionMap().get(qualifiedName.trim());
				if (transition != null) {
					boolean monitorThis = true;
					StateMachine stateMachine = transition.containingStateMachine();
					if (stateMachine != null) {
						Monitored monitoredStereotype = UMLUtil.getStereotypeApplication(stateMachine, Monitored.class);
						if (monitoredStereotype != null) {
							monitorThis = monitoredStereotype.isMonitored();
							if (monitorThis) {
								monitorThis = !monitoredStereotype.getExclude().contains(transition);
								if (monitorThis && !monitoredStereotype.getExclusivelyInclude().isEmpty()) {
									monitorThis = monitoredStereotype.getExclusivelyInclude().contains(transition);
								}
							}
						}
					}

					if (monitorThis) {
						if (transition.getSource() != transition.getTarget()) {
							animationEngine.startRendering(transition.getSource(), null, AnimationKind.VISITED);
							if (transition.getSource() instanceof State) {
								State state = (State) transition.getSource();
								if (state.getRegions().size() > 1) {
									for (Element stateOwnedElement : state.allOwnedElements()) {
										animationEngine.stopRendering(stateOwnedElement, null, AnimationKind.ANIMATED);
										animationEngine.stopRendering(stateOwnedElement, null, AnimationKind.VISITED);
									}
								}
							}
						}
						animationEngine.startRendering(transition, null, AnimationKind.ANIMATED);
						animationEngine.startRendering(transition, null, AnimationKind.VISITED);
						animationEngine.stopRendering(transition.getTarget(), null, AnimationKind.VISITED);
						animationEngine.startRendering(transition.getTarget(), null, AnimationKind.ANIMATED);
						if (transition.getTarget() instanceof State) {
							State targetState = (State) transition.getTarget();
							if (targetState.getRegions().size() > 1) {
								for (State initialState : stateMachineModel.getInitialStates()) {
									Element owner = initialState.getOwner();
									if (owner instanceof Region) {
										Region region = (Region) owner;
										if (region.getOwner() == targetState) {
											animationEngine.stopRendering(initialState, null, AnimationKind.VISITED);
											animationEngine.startRendering(initialState, null, AnimationKind.ANIMATED);
										}
									}
								}
							}
						}
						
						
						noMarkers = false;
					}
				}
			}
		}
	}

	public synchronized void initialize() {
		if (!initialized) {
			animationEngine.deleteAllMarkers();

			if (stateMachineModel != null) {
				if (stateMachineModel.getInitialStates().size() > 0) {
					animationEngine.init(stateMachineModel.getInitialStates().get(0));

					for (State initialState : stateMachineModel.getInitialStates()) {
						Element owner = initialState.getOwner();
						if (owner instanceof Region) {
							Region region = (Region) owner;
							if (region.getOwner() instanceof StateMachine) {
								boolean monitorThis = true;
								StateMachine stateMachine = initialState.containingStateMachine();
								if (stateMachine != null) {
									Monitored monitoredStereotype = UMLUtil.getStereotypeApplication(stateMachine, Monitored.class);
									if (monitoredStereotype != null) {
										monitorThis = monitoredStereotype.isMonitored();
									}
								}

								if (monitorThis) {
									animationEngine.startRendering(initialState, null, AnimationKind.ANIMATED);
									initialized = true;
									noMarkers = false;
								}
							}
						}
					}
				}
			}
		}
	}

	public synchronized void deleteMarkers() {
		if (!noMarkers) {
			animationEngine.deleteAllMarkers();
			noMarkers = true;
		}
	}
}

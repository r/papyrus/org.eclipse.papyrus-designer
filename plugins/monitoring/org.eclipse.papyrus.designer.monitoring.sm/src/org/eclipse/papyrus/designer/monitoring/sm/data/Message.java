/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.data;

import org.eclipse.papyrus.designer.monitoring.sm.runnable.AnimatorMessageQueueReader;

public class Message {
	public static final String SEPARATOR = "|"; //$NON-NLS-1$
	public static final String TRANSITION_HEADER = "TRANSITION"; //$NON-NLS-1$
	public static final String STATE_HEADER = "STATE"; //$NON-NLS-1$
	
	private String value;
	
	public Message(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public String getHeader() {
		if (value != null) {
			if (value.contains(AnimatorMessageQueueReader.PAPYRUS_END_SM_MONITORING)) {
				return value;
			} else {
				String[] tokens = value.split("\\|"); //$NON-NLS-1$
				if (tokens.length == 2) {
					return tokens[0];
				}
			}
		}
		
		return ""; //$NON-NLS-1$
	}
	
	public String getData() {
		if (value != null) {
			if (value.contains(AnimatorMessageQueueReader.PAPYRUS_END_SM_MONITORING)) {
				return value;
			} else {
				String[] tokens = value.split("\\|"); //$NON-NLS-1$
				if (tokens.length == 2) {
					return tokens[1];
				}
			}
		}
		
		return ""; //$NON-NLS-1$
	}
}

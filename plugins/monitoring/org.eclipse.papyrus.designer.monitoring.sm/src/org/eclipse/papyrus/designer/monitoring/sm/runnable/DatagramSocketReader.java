/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.runnable;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

import org.eclipse.papyrus.designer.monitoring.prefs.MonitoringPreferencesUtil;
import org.eclipse.papyrus.designer.monitoring.sm.data.Message;
import org.eclipse.papyrus.designer.monitoring.sm.data.MessageQueue;

public class DatagramSocketReader implements Runnable {
	private boolean running;
	private DatagramSocket socket;
	private byte[] buffer;
	private MessageQueue messageQueue;
	private final int sleepTime = 10;

	public DatagramSocketReader(MessageQueue messageQueue) {
		this.messageQueue = messageQueue;

		boolean rebind = false;
		do {
			try {
				socket = new DatagramSocket(MonitoringPreferencesUtil.getPort());
				socket.setSoTimeout(100);
				buffer = new byte[1024];
			} catch (IOException e) {
				e.printStackTrace();
				/*if (e instanceof BindException) {
					rebind = true;
				} else {
					rebind = false;
				}*/
			}
		} while (rebind);

		if (socket != null) {
			running = true;
		}
	}

	@Override
	public void run() {
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		while (running && socket != null && buffer != null && messageQueue != null) {
			try {
				socket.receive(packet);
				String received = new String(packet.getData(), 0, packet.getLength());

				Message message = new Message(received);
				messageQueue.getMessages().add(message);

				if (received.equals(AnimatorMessageQueueReader.PAPYRUS_END_SM_MONITORING)) {
					running = false;
				}
			} catch (IOException e) {
				//e.printStackTrace();
				// Ignore
			} catch (Exception e) {
				// Ignore
			}

			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (socket != null) {
			socket.close();
		}
	}

	public void stop() {
		running = false;
	}

	public DatagramSocket getSocket() {
		return socket;
	}
}

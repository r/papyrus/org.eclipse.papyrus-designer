/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.sm.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;

public class StateMachineModel {
	private Map<String, Transition> transitionMap;
	private Map<String, State> stateMap;
	private List<State> initialStates;
	private Set<StateMachine> stateMachines;

	public StateMachineModel(Model model) {
		transitionMap = new HashMap<String, Transition>();
		stateMap = new HashMap<String, State>();
		initialStates = new ArrayList<State>();
		stateMachines = new HashSet<StateMachine>();

		for (Element element : model.allOwnedElements()) {
			if (element instanceof NamedElement) {
				String qualifiedName = ((NamedElement) element).getQualifiedName();

				if (element instanceof Transition) {
					if (qualifiedName != null && !qualifiedName.isEmpty()) {
						transitionMap.put(qualifiedName, (Transition) element);
					}
					stateMachines.add(((Transition) element).containingStateMachine());
				}

				if (element instanceof State) {
					if (qualifiedName != null && !qualifiedName.isEmpty()) {
						stateMap.put(qualifiedName, (State) element);
					}
					stateMachines.add(((State) element).containingStateMachine());
				}

				if (element instanceof Pseudostate) {
					Pseudostate pseudoState = (Pseudostate) element;
					if (pseudoState.getKind().equals(PseudostateKind.INITIAL_LITERAL)) {
						for (Transition transition : pseudoState.getOutgoings()) {
							Vertex target = transition.getTarget();
							if (target instanceof State) {
								initialStates.add((State) target);
								stateMachines.add(((State) target).containingStateMachine());
							}
						}
					}
				}
			}
		}
	}

	public Map<String, Transition> getTransitionMap() {
		return transitionMap;
	}

	public void setTransitionMap(HashMap<String, Transition> transitionMap) {
		this.transitionMap = transitionMap;
	}

	public Map<String, State> getStateMap() {
		return stateMap;
	}

	public void setStateMap(HashMap<String, State> stateMap) {
		this.stateMap = stateMap;
	}

	public List<State> getInitialStates() {
		return initialStates;
	}

	public void setInitialStates(List<State> initialStates) {
		this.initialStates = initialStates;
	}

	public Set<StateMachine> getStateMachines() {
		return stateMachines;
	}

	public void setStateMachines(HashSet<StateMachine> stateMachines) {
		this.stateMachines = stateMachines;
	}
}

/*******************************************************************************
 * Copyright (c) 2018, 2023 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.prefs;

import java.util.regex.Pattern;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;

/**
 * Utility class that returns the preference values
 */
public class MonitoringPreferencesUtil {
	protected static IEclipsePreferences prefs = null;

	public static void initPrefs() {
		if (prefs == null) {
			prefs = InstanceScope.INSTANCE.getNode(Activator.PLUGIN_ID);
		}
	}
	
	public static String getIpAddress() {
		initPrefs();
		String ip = prefs.get(MonitoringPreferencesConstants.P_IP_ADDRESS, MonitoringPreferencesConstants.P_DEFAULT_IP_ADDRESS);
		if (ip == null || !validate(ip)) {
			ip = MonitoringPreferencesConstants.P_DEFAULT_IP_ADDRESS;
		}
		return ip;
	}

	public static int getPort() {
		initPrefs();
		int port = prefs.getInt(MonitoringPreferencesConstants.P_PORT, MonitoringPreferencesConstants.P_DEFAULT_PORT);
		if (port <= 0) {
			port = MonitoringPreferencesConstants.P_DEFAULT_PORT;
		}
		return port;
	}
	
	public static boolean getEnabled() {
		initPrefs();
		return prefs.getBoolean(MonitoringPreferencesConstants.P_ENABLED, MonitoringPreferencesConstants.P_DEFAULT_ENABLED);
	}
	
	private static final Pattern IP_PATTERN = Pattern.compile(
	        "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$"); //$NON-NLS-1$

	private static boolean validate(final String ip) {
	    return IP_PATTERN.matcher(ip).matches();
	}
}

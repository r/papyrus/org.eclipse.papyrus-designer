/*******************************************************************************
 * Copyright (c) 2018 CEA LIST.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Shuai Li (CEA LIST) <shuai.li@cea.fr> - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.designer.monitoring.prefs;

/**
 * Monitoring preference constants (key + default value)
 */
public class MonitoringPreferencesConstants {
	
	public static final String P_IP_ADDRESS = "papyrusMonitorIpAddress"; //$NON-NLS-1$
	public static final String P_DEFAULT_IP_ADDRESS = "127.0.0.1"; //$NON-NLS-1$

	public static final String P_PORT = "papyrusMonitorPort"; //$NON-NLS-1$
	public static final int P_DEFAULT_PORT = 4445;
	
	public static final String P_ENABLED = "papyrusMonitorEnabled"; //$NON-NLS-1$
	public static final boolean P_DEFAULT_ENABLED = false;	
}

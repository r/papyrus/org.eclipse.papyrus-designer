/*****************************************************************************
 * Copyright (c) 2021 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.uml.tools.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.uml2.uml.Behavior;
import org.eclipse.uml2.uml.BehavioralFeature;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.Type;

/**
 * Some functions around UML parameters. Unify usage in conjunction with
 * Operation (BehavioralFeatures) and Behaviors which both support parameters
 */
public class ParameterUtils {
	protected static final String WRONG_ARGUMENT = "Passed element is neither a behavioral feature nor a behavior"; //$NON-NLS-1$

	public static final String RET = "ret"; //$NON-NLS-1$

	/**
	 * @param opOrBehavior
	 *            an operation (behavioral feature) or behavior - illegal argument exception if that is not the case
	 * @return Return the list of owned parameters
	 * 
	 */
	public static List<Parameter> getOwnedParameters(NamedElement opOrBehavior) {
		if (opOrBehavior instanceof BehavioralFeature) {
			return ((BehavioralFeature) opOrBehavior).getOwnedParameters();
		} else if (opOrBehavior instanceof Behavior) {
			return ((Behavior) opOrBehavior).getOwnedParameters();
		}
		throw new IllegalArgumentException(WRONG_ARGUMENT);
	}

	/**
	 * @param opOrBehavior
	 *            an operation (behavioral feature) or behavior - illegal argument exception if that is not the case
	 * @return a copy of the parameter list (useful for loops, if the original list is eventually modified)
	 */
	public static List<Parameter> getOwnedParametersCopy(NamedElement opOrBehavior) {
		List<Parameter> paramListCopy = new ArrayList<Parameter>();
		List<Parameter> ownedParameters = getOwnedParameters(opOrBehavior);
		if (ownedParameters != null) {
			paramListCopy.addAll(ownedParameters);
		}
		return paramListCopy;
	}

	/**
	 * @param opOrBehavior
	 *            an operation (behavioral feature) or behavior
	 * @return Return the created parameter
	 * 
	 */
	public static Parameter createOwnedParameter(NamedElement opOrBehavior, String paramName, Type type) {
		if (opOrBehavior instanceof BehavioralFeature) {
			return ((BehavioralFeature) opOrBehavior).createOwnedParameter(paramName, type);
		} else if (opOrBehavior instanceof Behavior) {
			return ((Behavior) opOrBehavior).createOwnedParameter(paramName, type);
		}
		return null;
	}

	/**
	 * @param opOrBehavior
	 *            an operation (behavioral feature) or behavior - illegal argument exception if that is not the case
	 * @return Return the created return parameter
	 * 
	 */
	public static Parameter createReturnResult(NamedElement opOrBehavior, Type type) {
		Parameter parameter = createOwnedParameter(opOrBehavior, RET, type);
		parameter.setDirection(ParameterDirectionKind.RETURN_LITERAL);
		return parameter;
	}

	/**
	 * Reset parameter list to passed list, e.g. use to reorder the parameters according to a new list.
	 * It clears existing parameter list first without destroying existing parameters
	 * (caller is responsible to avoid dangling references)
	 * 
	 * @param opOrBehavior
	 *            an operation (behavioral feature) or behavior - illegal argument exception if that is not the case
	 * @param paramList
	 *            the new parameter list
	 */
	public static void resetParameters(NamedElement opOrBehavior, List<Parameter> paramList) {
		// reorder parameter list
		getOwnedParameters(opOrBehavior).clear();
		getOwnedParameters(opOrBehavior).addAll(paramList);
	}

	/**
	 * @param paramList
	 *            a list of parameters
	 * @param parameterName
	 *            a parameter name (if null, implies to look return parameter)
	 * @return the first parameter from the list with matching name. If the parameter
	 *         name is null, return the first return parameter. returns null, if no
	 *         matching name is found
	 */
	public static Parameter getParameterViaName(List<Parameter> paramList, String parameterName) {
		for (Parameter existingParameter : paramList) {
			if (parameterName == null) {
				if (existingParameter.getDirection() == ParameterDirectionKind.RETURN_LITERAL) {
					return existingParameter;
				}
			} else if (existingParameter.getName().equals(parameterName)) {
				return existingParameter;
			}
		}
		return null;
	}
}

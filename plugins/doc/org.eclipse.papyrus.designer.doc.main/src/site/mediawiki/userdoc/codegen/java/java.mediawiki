The Java code generation is available via Papyrus SW designer, a Papyrus component. It can be used by installing the latest version of Papyrus. It allows to create Java source code from a class diagram.

== Installation instructions==
You can install the Java reverse mechanisms by using the Papyrus Software Designer update-site:
* http://download.eclipse.org/modeling/mdt/papyrus/components/designer/
Or you can use the nightly build update site by choosing a job below and select the update site in its description:
* https://ci.eclipse.org/papyrus/view/Designer/

Then select "Papyrus Java profile, library and code generation (Incubation)" feature. You need JDT installed (which comes with most Eclipse packages).

[[Image:InstallSWDesignerJavaFeature.png]]

== Code generation from UML elements ==

You can generate code for a specific classifier or package in your UML model. To generate code, do the following:

* Right click on classifier or package, either in a diagram or in the model explorer
* Designer > Generate Java code
* Follow the JDT dialogs that let you create a new JDT project in Eclipse, where code will be generated, if there is no JDT project associated with your model (e.g. the first time you generate code from your model)

When you generate code from a classifier, its required classifiers are also generated. Required classifiers are classifiers related for the generated classifier, e.g. typing one of its attributes, inheritance relationship, dependency relationship. When you generate code from a package (e.g. the root of your model), all of its classifiers, and their required classifiers, will be generated.

[[Image:GenerateJavaCode.jpg]]

== Generation preferences ==
You can change the Java code generation preferences using the Eclipse preferences menu, and choose Papyrus > Java code generation.
Available options are:

* Extension for Java files
* Prefix for name of created JDT project (if unspecified in the model, see below)
* Header for generated files

[[File:JavaCodegenPrefs.png]]

== Java profile and library ==

The installed Java feature, of Papyrus Software Designer, comes with a Java profile and a Java library. Furthermore, it also installs a generic code generation profile for meta-information (e.g. project name modeling).

=== Java profile ===
The Java profile adds stereotypes to model specific Java elements that otherwise cannot be modeled in UML. It also adds stereotypes for managing elements that you do not want to generate (e.g. <<NoCodeGen>>, <<External>>, <<ExternLibrary>> stereotypes). The stereotypes can be seen as tags to put on UML elements. To apply the Java profile:

* Click on your model (in the Model Explorer view)
* In the Properties view, choose the Profile tab
* Click on Apply registered profile
* In the dialog that opens, choose the Java profile

[[File:ApplyRegisteredProfile.jpg]]

[[File:JavaRegisteredProfile.jpg]]

=== Java library ===
The Java library provides Java primitives and their wrapper classes. To import, this library:

* Right click on your model (in the Model Explorer view)
* Import > Import Registered Package
* Choose the JavaLibrary package
* Then you can use the Java primitives in this package, when you type attributes, parameters, etc...

[[File:ImportPapyrusRegisteredPackage.png]]

[[File:javaLibrary.png]]

=== Common code generation profile ===
Papyrus Software Designer also offers a common code generation profile, that can be used for Java code generation. This profile lets the user model meta-information, e.g. path where code is generated.

To use this feature, first the Papyrus code generation profile must be applied:

[[File:CodeGenerationProfile.png]]

Then you can apply the <<Project>> stereotype to your model (root element). This stereotype lets you
* Generation mode: either batch or incremental
* Name of the JDT project (can be an existing one in the workspace)
* Source folder
* Prefix for all packages

[[File:SwDesignerProjectStereotype.png]]

== Incremental code generation ==

As said in the last section, the <<Project>> stereotype lets you choose incremental code generation. Currently there is an experimental state-based incremental code generator for Java in Papyrus Software Designer. Incremental code generation is seamless to the user, but this is what happens in reality:
* A temporary code is generated from the model.
* The temporary code is compared to the existing code, via JDT.UI comparison feature.
* The Java-specific DiffNodes (see Eclipse compare and JDT API) are obtained post-comparison.
* We use these DiffNodes to take decisions on how we update incrementally the existing code.
* After all updates are complete, the temporary source code is deleted.

Later we plan to add change-based incremental code generation for Java.


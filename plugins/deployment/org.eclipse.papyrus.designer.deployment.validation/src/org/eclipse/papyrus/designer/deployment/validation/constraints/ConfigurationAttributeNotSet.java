/**
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.deployment.validation.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.validation.AbstractModelConstraint;
import org.eclipse.emf.validation.IValidationContext;
import org.eclipse.papyrus.designer.deployment.profile.Deployment.ConfigurationProperty;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.StructuralFeature;

/**
 * Verify if all configuration attribute have a (user-provided) value.
 *
 * TODO: implementation assumes that at least the slot exists
 */
public class ConfigurationAttributeNotSet extends AbstractModelConstraint {

	@Override
	public IStatus validate(IValidationContext ctx)
	{
		Slot slot = (Slot) ctx.getTarget();
		StructuralFeature feature = slot.getDefiningFeature();
		if ((feature != null) && (StereotypeUtil.isApplied(feature, ConfigurationProperty.class))) {
			if (slot.getValues().size() == 0) {
				return ctx.createFailureStatus(String.format(
						"The attribute '%s' is tagged as a configuration property, but the associated slot has no value.", feature.getName())); //$NON-NLS-1$
			}
		}
		return ctx.createSuccessStatus();
	}
}

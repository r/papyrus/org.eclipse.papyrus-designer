/*****************************************************************************
 * Copyright (c) 2013, 2018 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.infra.base;

import java.util.concurrent.CancellationException;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;

/**
 * Utility function. Allow execution of commands on a transactional command
 * stack
 *
 * @author ansgar
 *
 */
public class CommandSupport {

	/**
	 * Execute the passed Runnable within a command
	 *
	 * @param eObject
	 *            an element of the model that is modified (domain will be
	 *            determined from it)
	 * @param label
	 *            A command label
	 * @param command
	 *            The command in form of a runnable
	 */
	public static void exec(EObject eObject, String label, final Runnable command) {
		exec(TransactionUtil.getEditingDomain(eObject), label, command);
	}

	public static void exec(EObject eObject, String label, final RunnableWithResult command) {
		exec(TransactionUtil.getEditingDomain(eObject), label, command);
	}

	/**
	 * Execute the passed Runnable within a command
	 *
	 * @param label
	 * @param command
	 */
	public static void exec(TransactionalEditingDomain domain, String label, final Runnable command) {
		if (domain == null) {
			command.run();
		} else {
			domain.getCommandStack().execute(new RecordingCommand(domain, label) {

				@Override
				public void doExecute() {
					command.run();
				}
			});
		}
	}

	/**
	 * Execute the passed Runnable within a command
	 *
	 * @param label
	 * @param command
	 */
	public static void exec(TransactionalEditingDomain domain, String label, final RunnableWithResult command) {
		if (domain == null) {
			command.run();
		} else {
			try {
				domain.getCommandStack().execute(new RecordingCommand(domain, label) {

					@Override
					public void doExecute() {
						IStatus status = command.run();
						if (!status.isOK()) {
							throw new CancellationException();
						}
					}
				});
			} catch (CancellationException cancelException) {
				// do nothing, exception happens if user cancels dialog }
			}
			;
		}
	}

	public static void exec(TransactionalEditingDomain domain, Command command) {
		domain.getCommandStack().execute(command);
	}
}

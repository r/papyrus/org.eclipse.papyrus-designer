/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   CEA LIST - Initial API and implementation
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.infra.base;

import org.eclipse.uml2.uml.NamedElement;

/**
 * Collection of utility functions around strings
 */
public class StringUtils {
	/**
	 * Size of tabulator character
	 */
	public static final int TABSIZE = 4;

	/**
	 * Put quotes around a string, unless string already starts with a quote.
	 *
	 * @param str
	 *            a string
	 * @return the quoted string
	 */
	public static String quote(String str) {
		if (str.startsWith(StringConstants.QUOTE)) {
			return str;
		} else {
			return StringConstants.QUOTE + str + StringConstants.QUOTE;
		}
	}

	/**
	 * Remove quotes from a string, unless it does not start with a quote
	 * 
	 * @param str
	 *            a string
	 * @return the "unquoted" string
	 */
	public static String unquote(String str) {
		if (str.startsWith(StringConstants.QUOTE) && str.length() >= 2) {
			return str.substring(1, str.length() - 1);
		}
		return str;
	}

	/**
	 * A small helper function that makes names compliant with variable
	 * names in programming languages such as C++ or Java
	 * Replaces " ", "-", "+", "/", "::" et "?" with "_"
	 *
	 * @param aName
	 *            a name
	 * @return the string with white-space replacements
	 */
	public static String varName(String aName) {
		aName = aName.replace(".", StringConstants.UNDERSCORE); //$NON-NLS-1$
		aName = aName.replace(NamedElement.SEPARATOR, StringConstants.UNDERSCORE);
		aName = aName.replace(" ", StringConstants.UNDERSCORE); //$NON-NLS-1$
		aName = aName.replace("-", StringConstants.UNDERSCORE); //$NON-NLS-1$
		aName = aName.replace("+", StringConstants.UNDERSCORE); //$NON-NLS-1$
		aName = aName.replace("?", StringConstants.UNDERSCORE); //$NON-NLS-1$
		aName = aName.replace(StringConstants.SLASH, StringConstants.UNDERSCORE);
		return aName;
	}

	/**
	 * Make the first character in a string upper case
	 * 
	 * @param str
	 *            a string
	 * @return the string with the first character upper case
	 */
	public static String upperCaseFirst(String str) {
		if (str.length() > 0) {
			return str.substring(0, 1).toUpperCase() + str.substring(1);
		} else {
			return str;
		}
	}

	/**
	 * Decrease the indentation of a text block. This function is used during synchronization, since
	 * the code within an opaque behavior is not indented, whereas the code of an operation within a
	 * file is indented with a tab.
	 * 
	 * @param contents
	 *            a character array
	 * @param start
	 *            a start position within the passed array.
	 * @param end
	 *            an end position within that array
	 * @param indentation
	 *            the number of characters to decrease the indentation
	 * @return a String containing the textblock with decreased indentation
	 */
	public static String decreaseIndent(char[] contents, int start, int end, int indentation) {
		String newBlock = ""; //$NON-NLS-1$
		int consume = indentation;
		for (int i = start; i < end; i++) {
			char c = contents[i];

			// consume either a tab or 4 spaces;
			if (consume > 0 && Character.isWhitespace(c)) {
				if (c == '\t') {
					consume -= TABSIZE;
				} else if (c == ' ') {
					consume--;
				} else {
					newBlock += c;
				}
			} else {
				newBlock += c;
				if ((c == '\n') || (c == '\r')) {
					consume = indentation;
				} else {
					consume = 0;
				}
			}
		}
		return newBlock;
	}

	/**
	 * Append a sting and use a separation char (space), if the inital string is
	 * non empty
	 * 
	 * @param str
	 *            a string
	 * @param appendStr
	 *            the string to append
	 * @return the appended string
	 */
	public static String append(String str, String appendStr) {
		String ret = str;
		if (ret.length() > 0) {
			ret += StringConstants.SPACE;
		}
		ret += appendStr;
		return ret;
	}
}

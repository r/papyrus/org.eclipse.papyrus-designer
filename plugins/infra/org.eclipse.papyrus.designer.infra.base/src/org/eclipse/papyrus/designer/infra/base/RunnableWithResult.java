/*****************************************************************************
* Copyright (c) 2013, 2022 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.infra.base;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

public interface RunnableWithResult {

	/**
	 * Execute a command
	 *
	 * @return the status, e.g. {@link Status}.OK_STATUS, {@link Status}.error(...)
	 *         or {@link Status}.Cancel_STATUS
	 */
	public IStatus run();
}

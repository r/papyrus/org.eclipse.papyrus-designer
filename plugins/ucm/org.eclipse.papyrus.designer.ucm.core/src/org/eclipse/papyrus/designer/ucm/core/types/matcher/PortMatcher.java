/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Ansgar Radermacher - initial API and implementation 
 *   
 *****************************************************************************/

package org.eclipse.papyrus.designer.ucm.core.types.matcher;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.emf.type.core.IElementMatcher;
import org.eclipse.papyrus.designer.ucm.profile.UCMProfile.ucm_components.PortTypeSpec;
import org.eclipse.papyrus.designer.uml.tools.utils.StereotypeUtil;
import org.eclipse.uml2.uml.Port;

public class PortMatcher implements IElementMatcher {

	@Override
	public boolean matches(EObject eObject) {
		if (eObject instanceof Port) {
			Port port = (Port) eObject;
			if (port.getType() != null && StereotypeUtil.isApplied(port.getType(), PortTypeSpec.class)) {
				return true;
			}
		}
		return false;
	}
}

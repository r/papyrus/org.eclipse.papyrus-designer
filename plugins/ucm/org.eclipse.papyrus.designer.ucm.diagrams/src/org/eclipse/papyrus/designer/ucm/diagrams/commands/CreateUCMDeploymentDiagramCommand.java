/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.ucm.diagrams.commands;

import org.eclipse.papyrus.uml.diagram.clazz.CreateClassDiagramCommand;

/**
 * Create UCM deployment diagram command
 */
public class CreateUCMDeploymentDiagramCommand extends CreateClassDiagramCommand {

}

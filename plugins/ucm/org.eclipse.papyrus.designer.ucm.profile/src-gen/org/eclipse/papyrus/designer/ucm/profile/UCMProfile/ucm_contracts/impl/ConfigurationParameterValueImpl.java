/**
 * Copyright (c) 2017 CEA LIST and Thales
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.ucm.profile.UCMProfile.ucm_contracts.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.papyrus.designer.ucm.profile.UCMProfile.ucm_contracts.ConfigurationParameterValue;
import org.eclipse.papyrus.designer.ucm.profile.UCMProfile.ucm_contracts.Ucm_contractsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configuration Parameter Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ConfigurationParameterValueImpl extends IConfigurationParameterValueImpl implements ConfigurationParameterValue {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConfigurationParameterValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Ucm_contractsPackage.Literals.CONFIGURATION_PARAMETER_VALUE;
	}

} //ConfigurationParameterValueImpl

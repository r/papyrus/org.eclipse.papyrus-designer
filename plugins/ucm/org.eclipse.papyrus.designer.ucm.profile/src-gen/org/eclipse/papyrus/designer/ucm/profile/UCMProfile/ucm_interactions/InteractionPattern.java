/**
 * Copyright (c) 2017 CEA LIST and Thales
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.papyrus.designer.ucm.profile.UCMProfile.ucm_interactions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interaction Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * InteractionRole and InteractionItem classes are linked by a composition relationship with an InteractionPattern
 * To extend an InteractionPattern, use the "redefinedClassifier" association from metaclass.
 * <!-- end-model-doc -->
 *
 *
 * @see org.eclipse.papyrus.designer.ucm.profile.UCMProfile.ucm_interactions.Ucm_interactionsPackage#getInteractionPattern()
 * @model
 * @generated
 */
public interface InteractionPattern extends IInteractionDefinition {
} // InteractionPattern

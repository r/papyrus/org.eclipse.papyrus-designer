/*****************************************************************************
 * Copyright (c) 2017 CEA LIST and Thales
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Ansgar Radermacher  ansgar.radermacher@cea.fr
 *
 *****************************************************************************/

package org.eclipse.papyrus.designer.ucm.ui.handlers;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.papyrus.designer.infra.base.CommandSupport;
import org.eclipse.papyrus.designer.infra.base.RunnableWithResult;
import org.eclipse.papyrus.designer.ucm.ui.dialogs.PortConfigurationDialog;
import org.eclipse.papyrus.uml.diagram.common.handlers.CmdHandler;
import org.eclipse.swt.widgets.Display;
import org.eclipse.uml2.uml.Port;

public class ConfigurePortHandler extends CmdHandler {

	// ------------------------------------------------------------------------
	// Execution
	// ------------------------------------------------------------------------

	public static final String CONFIGURE_PORT = "Configure port"; //$NON-NLS-1$

	@Override
	public boolean isEnabled() {
		updateSelectedEObject();

		if (selectedEObject instanceof Port) {
			return true;
		}

		return false;
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		if (selectedEObject instanceof Port) {
			final Port port = (Port) selectedEObject;

			CommandSupport.exec(port, CONFIGURE_PORT, new RunnableWithResult() {

				@Override
				public IStatus run() {
					PortConfigurationDialog portConfigurationDialog = new PortConfigurationDialog(
							Display.getCurrent().getActiveShell(), port);
					portConfigurationDialog.open();
					if (portConfigurationDialog.getReturnCode() == IDialogConstants.OK_ID) {
						return Status.OK_STATUS;
					} else {
						return Status.CANCEL_STATUS;
					}
				}
			});
		}
		return null;
	}
}
